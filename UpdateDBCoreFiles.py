import urllib, json, os, msgpack

from lib import respath
from lib.AssetManagement.Download import Download_Asset, get_assetlist, gameVersion_jp
from lib.api import Network, Session, DecryptOptions, KeyType


def main():
	data_files = ['MasterParam', 'QuestParam', 'QuestDropParam']
	loc_gl_files = ['LocalizedMasterParam', 'LocalizedQuestParam', 'sys', 'unit']
	loc_jp_files = ['sys', 'unit']
	####	GLOBAL   ##############################################################
	print('\n\n\n####\tGLOBAL\t####')
	assets_gl = get_assetlist('global', 'apvr')
	host_dl = assets_gl['host']
	version = assets_gl['version']
	path = os.path.join(respath, 'AssetsGL')
	
	for f in data_files:
		print(f)
		open(os.path.join(path, *['Data', f]), 'wb').write(Download_Asset(host_dl, version, assets_gl['assets'][f'Data/{f}']))
	
	for f in loc_gl_files:
		print(f)
		open(os.path.join(path, *['Loc', 'english', f]), 'wb').write(Download_Asset(host_dl, version, assets_gl['assets'][f'Loc/english/{f}']))
	
	####	JAPAN   ##############################################################
	print('\n\n\n####\tJAPAN\t####')
	assets = get_assetlist('japan', 'win32')
	host_dl = assets['host']
	version = assets['version']
	path = os.path.join(respath, 'AssetsJP')
	
	net = Network("alchemist.gu3.jp",Session(IsUseEncryption=True))
	ret = net.ReqCheckVersion2(gameVersion_jp)

	for f in data_files:
		print(f)
		digest = net.session.enviroment.MasterDigest if 'Master' in f else net.session.enviroment.QuestDigest
		data = Download_Asset(host_dl, version, assets['assets'][f'Data/{f}Serialized'], 'win32')
		data = net.EncryptionHelper.Decrypt(KeyType.APP, data, digest, DecryptOptions.IsFile)
		try:
			wdata = json.dumps(msgpack.unpackb(data,raw=False), ensure_ascii=False).encode('utf8')
		except: #QuestDropParam
			wdata = data
		open(os.path.join(path, *['Data', f]), 'wb').write(wdata)

	for f in loc_jp_files:
		print(f)
		open(os.path.join(path, *['Loc', 'japanese', f]), 'wb').write(Download_Asset(host_dl, version, assets['assets'][f'Loc/japanese/{f}'],'win32'))
	
	####	OTHER STUFF ###########################################################
	path = respath
	print('\n\n\n####\tOTHER STUFF\t####')
	# wytesong's compendium
	print('wytesong\'s compendium')
	# https://docs.google.com/spreadsheets/d/1nNmHzEfU3OSt-VlGma4diO4405fmqDLWJ8LiiOsyRmg
	SSID = '1nNmHzEfU3OSt-VlGma4diO4405fmqDLWJ8LiiOsyRmg'
	# url = "https://script.google.com/macros/s/AKfycbzMqkHxhLsiMWqtFDmMHzqgT4a1R8yhBAxHN6YRkeN1lotYmsfg/exec?id="+SSID
	url = "https://script.google.com/macros/s/AKfycbzMqkHxhLsiMWqtFDmMHzqgT4a1R8yhBAxHN6YRkeN1lotYmsfg/exec?type=array&id=" + SSID
	saveAsJSON(path, 'wytesong.json', json.loads(download(url, 'utf8')))
	
	# # Game's tierlist
	print('Tierlist')
	# # https://docs.google.com/spreadsheets/d/1DWeFk0wiPaDKAYEcmf_9LnMFYy1nBy2lPTNAX52LkPU
	SSID = '1DWeFk0wiPaDKAYEcmf_9LnMFYy1nBy2lPTNAX52LkPU'
	url = "https://script.google.com/macros/s/AKfycbzMqkHxhLsiMWqtFDmMHzqgT4a1R8yhBAxHN6YRkeN1lotYmsfg/exec?type=array&id=" + SSID
	saveAsJSON(path, 'tierlist_gl.json', json.loads(download(url, 'utf8')))


def saveAsJSON(path, name, var, subdir = False):
	if subdir:
		fpath = os.path.join(path, subdir)
	else:
		fpath = path
	os.makedirs(fpath, exist_ok = True)
	fpath = os.path.join(fpath, name)
	with open(fpath, "wb") as f:
		f.write(json.dumps(var, indent = '\t', ensure_ascii = False).encode('utf8'))


def download(url, decoder = 'utf-8-sig'):
	resource = urllib.request.urlopen(url)
	try:
		return resource.read().decode(decoder)
	except:
		print('failed decoding')


main()
