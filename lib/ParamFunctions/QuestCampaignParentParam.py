def QuestCampaignParentParam(json):
    this={}#QuestCampaignParentParamjson)
    if 'iname' in json:
        this['iname'] = json['iname']
    if 'children' in json:
        this['children'] = json['children']
    if 'begin_at' in json:
        this['begin_at']=json['begin_at']
    if 'end_at' in json:
        this['end_at']=json['end_at']
    return this
