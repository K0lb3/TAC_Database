from .TowerRewardItem import TowerRewardItem
def TowerRewardParam(json):
    this={}
    if 'iname' in json:
        this['iname'] = json['iname']
    if 'rewards' in json:
        this['mTowerRewardItems']=[
            TowerRewardItem(reward)
            for reward in json['rewards']
        ]
    return this