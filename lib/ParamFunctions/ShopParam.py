from ._variables import ENUM
def ShopParam(json):
    this={}#ShopParamjson)
    if 'iname' in json:
        this['iname'] = json['iname']
    if 'upd_type' in json:
        this['UpdateCostType'] = ENUM['ESaleType'][json['upd_type']]
    if 'upd_costs' in json:
        this['UpdateCosts'] = json['upd_costs']
    return this
