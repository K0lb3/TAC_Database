import os

mypath = os.path.dirname(os.path.realpath(__file__))
files = os.listdir(mypath)

string = ''
string += 'from ._Assignments import methods\n'

for f in files:
	if '.py' in f and '_' not in f:
		string += 'from .{name} import {name}\n'.format(name = f[:-3])
with open(mypath + '\\__init__.py', "wt") as f:
	f.write(string)
