import http.client
import io
import json
import os
import struct
import time
import urllib.request
import zipfile
import zlib
from copy import copy

import unitypack
from unitypack.asset import Asset

from lib.shared import rootpath
from lib.api.Network import Network, Session
from UnityPy import AssetsManager

### constants   ###########################
host_gl = "app.alcww.gumi.sg"
host_jp = "alchemist.gu3.jp"
versions_path = os.path.join(rootpath, *['resources', 'versions.txt'])


def LoadGameVersion():
	global gameVersion_gl, gameVersion_jp, version_tw
	with open(versions_path, 'rt') as f:
		for line in f:
			ver = line.split('\t')[-1][:-1]
			if line[:2] == 'GL':
				gameVersion_gl = ver
			elif line[:2] == 'JP':
				gameVersion_jp = ver
			elif line[:2] == 'TW':
				version_tw = ver


LoadGameVersion()


def Download_Asset(host_dl, version, asset, typ = 'aatc', decompress = True):
	data = requestAsset(host_dl, version, typ, asset['id'])
	if decompress and 'Compressed' in asset['flags']:
		data = zlib.decompress(data)
	return (data)


def get_assetlist(region = 'global', listT = 'aatc', host_dl = '', version = ''):
	if region == 'global':
		try:
			session = Session(IsUseEncryption=False)
			ver = Network(host_gl, session).ReqCheckVersion2(gameVersion_gl)
		except KeyError:
			print(f'The networkver {gameVersion_gl} is outdated.\nThe script will now download the latest apk and update the networkver.')
			UpdateGlobalVer()
			session = Session(IsUseEncryption=False)
			ver = Network(host_gl, session).ReqCheckVersion2(gameVersion_gl)
	elif region == 'japan':
		try:
			session = Session(IsUseEncryption=True)
			ver = Network(host_jp, session).ReqCheckVersion2(gameVersion_jp)
		except KeyError:
			print(f'The networkver {gameVersion_jp} is outdated.\nThe script will now download the latest apk from QooApp and update the networkver.')
			UpdateJapanVer()
			session = Session(IsUseEncryption=True)
			ver = Network(host_jp, session).ReqCheckVersion2(gameVersion_jp)
	else:
		print('Wrong region')
		exit()
	
	if not version:
		version = ver['assets']
	if not host_dl:
		host_dl = ver['host_dl']
	# lists=['aatc','aatc','text']
	
	AssetList = requestAsset(host_dl, version, listT, "ASSETLIST")
	res = parse_assetlist(AssetList)
	
	res['version'] = version
	res['host'] = host_dl
	compare_assetlist(res,region,listT)
	return res


def compare_assetlist(new, region = 'global', listT = 'aatc'):
	print('\n###### Comparing with old assets')
	path = os.path.dirname(os.path.realpath(__file__))
	lpath = os.path.join(path, 'AssetList-{}-{}.json'.format(region, listT))
	date = time.gmtime()
	date = '{year}-{month}-{day}'.format(year = date.tm_year, month = date.tm_mon, day = date.tm_mday)
	opath = os.path.join(path, *['Updates', 'AssetList-{}-{}-{}.json'.format(region, listT, date)])
	
	if os.path.isfile(lpath):
		try:
			with open(lpath, 'rb') as f:
				old = json.loads(f.read().decode('ascii'), strict = False)
		except:
			old = {'assets':{}}
		dif = copy(new)
		dif['assets'] = {}
		for path, nasset in new['assets'].items():
			if path in old['assets']:
				oasset = old['assets'][path]
				if nasset['hash'] != oasset['hash'] or nasset['size'] != oasset['size']:
					dif['assets'][path] = nasset
		
		if dif['assets']:
			with open(opath, 'wt') as f:
				json.dump(dif, f, indent = 2)
			with open(lpath, 'wt') as f:
				json.dump(new, f, indent = 2)
	else:
		dif = new
		with open(lpath, 'wt') as f:
			json.dump(new, f, indent = 2)
	return dif


def parse_assetlist(fh):
	if type(fh) == bytes:
		fh = io.BytesIO(fh)
	
	def readInt32(fh):
		return struct.unpack("i", fh.read(4))[0]
	
	def readUInt32(fh):
		return struct.unpack("I", fh.read(4))[0]
	
	def readString(fh):
		res = ""
		control = "1"
		while control == "1":
			bit_str = bin(ord(fh.read(1)))[2:].zfill(8)
			control = bit_str[0]
			res += bit_str[1:]
		
		length = int(res, 2)
		return struct.unpack("{}s".format(length), fh.read(length))[0].decode("utf8")
	
	bundleFlags = ["Compressed", "RawData", "Required", "Scene", "Tutorial", "Multiplay", "StreamingAsset", "TutorialMovie", "Persistent", "DiffAsset", None, None, "IsLanguage", "IsCombined",
	               "IsFolder"]
	
	assetlist = {
			'revision': readInt32(fh),
			'length'  : readInt32(fh),
			'assets'  : {}
	}
	for i in range(assetlist['length']):
		cAsset = {
				"id"                             : "%08x" % readUInt32(fh),
				"size"                           : readInt32(fh),
				"compSize"                       : readInt32(fh),
				"path"                           : readString(fh),
				"pathHash"                       : readInt32(fh),
				"hash"                           : readUInt32(fh),
				"flags"                          : [
						bundleFlags[index]
						for index, bit in enumerate(bin(readInt32(fh))[2:][::-1])  # flags
						if bit == "1"
				],
				"dependencies"                   : [
						readInt32(fh)
						for j in range(readInt32(fh))  # array length
				],
				"additionalDependencies"         : [
						readInt32(fh)  # array length
						for j in range(readInt32(fh))
				],
				"additionalStreamingDependencies": [
						readInt32(fh)  # array length
						for j in range(readInt32(fh))
				],
		}
		assetlist['assets'][cAsset['path']] = cAsset
	return assetlist


##API ############################################
def requestAsset(host_dl, version, typ, name):
	# request asset list
	url = '{host_dl}/assets/{version}/{typ}/{name}'.format(
			host_dl = host_dl,
			version = version,
			typ = typ,
			name = name
	)
	# print(url)
	print(url)
	asset = urllib.request.urlopen(url, timeout = 300).read()
	return asset


def req_chkver2(host, gameVersion):
	# host=host_jp
	# gameVersion=gameVersion_jp
	
	body = json.dumps({'ver': gameVersion})
	con = http.client.HTTPSConnection(host)
	con.connect()
	con.request("POST", "/chkver2", body, {
		"Content-Type": "application/json; charset=utf-8",
		'Accept': '*/*',
		"Accept-Encoding": "application/json; charset=utf-8",
		'x-taitapu-check': True
		})
	res_body = con.getresponse().read()
	con.close()
	print(res_body)
	return json.loads(res_body)['body']['environments']['alchemist']


def UpdateGlobalVer():
	from .DownloadAPK import DownloadGlobal
	fp = DownloadGlobal()
	z = zipfile.ZipFile(fp)
	am = AssetsManager()

	for f in z.namelist():
		if f[:16] == 'assets/bin/Data/':
			try:
				ret = get_networkver(z.open(f))
				if ret:
					print('Global networkver:', ret)
					update_versions('GL', ret)
					break
			except:
				pass


def UpdateJapanVer():
	from .DownloadAPK import DownloadJapan, respath
	fp = DownloadJapan()
	#fp = os.path.join(respath, 'jp.co.gu3.alchemist.apk')
	z = zipfile.ZipFile(fp)
	ret = get_networkver(z.open('assets/bin/Data/data.unity3d'))
	print('Japan networkver:', ret)
	update_versions('JP', ret)

def get_networkver(f):
	am = AssetsManager()
	am.load_file(f)
	for name,asset in am.assets.items():
		for id,obj in asset.objects.items():
			if obj.type == "TextAsset":
				data = obj.read()
				if data.name == 'networkver':
					return data.text


def update_versions(version, key):
	from .Download import versions_path
	with open(versions_path, 'rt') as f:
		versions = {
				line[:2]: line.split('\t')[-1][:-1]
				for line in f
		}
	versions[version] = key
	with open(versions_path, 'wt') as f:
		f.write('\n'.join([
				f'{ver}\t=\t{key}'
				for ver, key in versions.items()
		]))
	from .Download import LoadGameVersion
	LoadGameVersion()
