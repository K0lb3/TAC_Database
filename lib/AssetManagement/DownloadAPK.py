import os, io
from gplaycli import gplaycli
import http.client
import urllib.request

from lib import respath, rootpath


def DownloadGlobal():
	QooApp_Download_APK('sg.gumi.alchemistww', respath)
	return os.path.join(respath, 'sg.gumi.alchemistww.apk')
	# GooglePlay_Download_APK(['sg.gumi.alchemistww'], respath)
	# return os.path.join(respath,'sg.gumi.alchemistww.apk')


def DownloadJapan():
	QooApp_Download_APK('jp.co.gu3.alchemist', respath)
	return os.path.join(respath, 'jp.co.gu3.alchemist.apk')

def GooglePlay_Download_APK(apks, downloader_folder = None):
	cred_path = os.path.join(rootpath, *['lib', 'AssetManagement', 'gplaycli.conf'])
	
	token_url = "https://matlink.fr/token/email/gsfid"
	gpc = gplaycli.GPlaycli(config_file = cred_path)
	gpc.device_codename = 'angler'
	gpc.token_enable = True
	gpc.token_url = token_url
	gpc.retrieve_token(force_new = True)
	success, error = gpc.connect()
	assert error is None
	assert success == True
	gpc.progress_bar = True
	gpc.set_download_folder(os.path.abspath('.') if not downloader_folder else downloader_folder)
	gpc.download(apks if type(apks) == list else [apks])


def QooApp_Download_APK(apk, download_folder = None):
	con = http.client.HTTPSConnection('api.qoo-app.com')
	con.connect()
	con.request("GET", f'/v6/apps/{apk}/download')
	res = con.getresponse()
	con.close()
	download_url = res.headers['Location']
	data =  urllib.request.urlopen(download_url,timeout=300).read()#Download_With_Bar(download_url)  #
	if not download_folder:
		download_folder = os.path.abspath('.')
	open(os.path.join(download_folder, f'{apk}.apk'), 'wb').write(data)


def Download_With_Bar(url):
	resp = urllib.request.urlopen(url)
	length = resp.getheader('content-length')
	if length:
		length = int(length)
		blocksize = max(4096, length // 100)
	else:
		blocksize = 1000000  # just made something up
	
	# print(length, blocksize)
	
	buf = io.BytesIO()
	size = 0
	while True:
		buf1 = resp.read(blocksize)
		if not buf1:
			break
		buf.write(buf1)
		size += len(buf1)
		if length:
			print(' {:.2f}\r{}'.format(size / length, url), end = '')
	return buf.read()