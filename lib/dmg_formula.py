from .shared import ENUM


def dmg_formula(weaponParam):
	WeaponFormulaTypes = ENUM['WeaponFormulaTypes']
	
	def modifier(num):
		return str(int(num * 100)) + "%"
	
	Formulas = {
			"Atk"      : modifier(weaponParam['atk'] / 10) + " PATK",
			"Def"      : modifier(weaponParam['atk'] / 10) + " PDEF",
			"Mag"      : modifier(weaponParam['atk'] / 10) + " MATK",
			"Mnd"      : modifier(weaponParam['atk'] / 10) + " MDEF",
			"Luk"      : modifier(weaponParam['atk'] / 10) + " LUCK",
			"Dex"      : modifier(weaponParam['atk'] / 10) + " DEX",
			"Spd"      : modifier(weaponParam['atk'] / 10) + " AGI",
			"Cri"      : modifier(weaponParam['atk'] / 10) + " CRIT",
			"MHp"      : modifier(weaponParam['atk'] / 10) + " (MAX HP - current HP)",
			"AtkSpd"   : modifier(weaponParam['atk'] / 15) + " (PATK + AGI)",
			"MagSpd"   : modifier(weaponParam['atk'] / 15) + " (MATK + AGI)",
			"AtkDex"   : modifier(weaponParam['atk'] / 20) + " (PATK + DEX)",
			"MagDex"   : modifier(weaponParam['atk'] / 20) + " (MATK + DEX)",
			"AtkLuk"   : modifier(weaponParam['atk'] / 20) + " (PATK + LUCK)",
			"MagLuk"   : modifier(weaponParam['atk'] / 20) + " (MATK + LUCK)",
			"AtkMag"   : modifier(weaponParam['atk'] / 20) + " (PATK + MATK)",
			"SpAtk"    : modifier(weaponParam['atk'] / 10) + " PATK * random(150% to 250%)",
			"SpMag"    : modifier(weaponParam['atk'] / 10) + " MATK * (20% + MATK/(MATK + Target MDEF))",
			"AtkRndLuk": modifier(weaponParam['atk'] / 60) + " (3*PATK + (1 + 0~[LV/10]) * LUCK)",
			"MagRndLuk": modifier(weaponParam['atk'] / 60) + " (3*MATK + (1 + 0~[LV/10]) * LUCK)",
			"AtkSpdDex": modifier(weaponParam['atk'] / 20) + " (PATK + AGI/2 + AGI*LV/100 + DEX/4)",
			"MagSpdDex": modifier(weaponParam['atk'] / 20) + " (MATK + AGI/2 + AGI*LV/100 + DEX/4)",
			"AtkDexLuk": modifier(weaponParam['atk'] / 20) + " (PATK + DEX/2 + LUCK/2)",
			"MagDexLuk": modifier(weaponParam['atk'] / 20) + " (MATK + DEX/2 + LUCK/2)",
			"AtkEAt"   : modifier(weaponParam['atk'] / 10) + " (2*PATK - Target ATK)",
			"MagEMg"   : modifier(weaponParam['atk'] / 10) + " (2*MATK - Target MATK)",
			"LukELk"   : modifier(weaponParam['atk'] / 10) + " (2*LUCK - Target LUCK)",
			"AtkDefEDf": modifier(weaponParam['atk'] / 10) + " (PATK + PDEF - Target PDEF)",
			"MagMndEMd": modifier(weaponParam['atk'] / 10) + " (MATK + MDEF - Target MDEF)",
			"EAt"      : modifier(weaponParam['atk'] / 10) + " Target PATK",
			"EMg"      : modifier(weaponParam['atk'] / 10) + " Target MATK",
			"EDx"      : modifier(weaponParam['atk'] / 10) + " Target DEX",
			"ESp"      : modifier(weaponParam['atk'] / 10) + " Target AGI",
			"ESp2"     : modifier(weaponParam['atk'] / 100) + " (Target AGI)^2",
			"CtUpEAt"  : modifier(weaponParam['atk'] / 10) + " PATK * (Target Charge + 50) / 100",
			"CtUpEMg"  : modifier(weaponParam['atk'] / 10) + " MATK * (Target Charge + 50) / 100",
			"CtLoEAt"  : modifier(weaponParam['atk'] / 10) + " PATK * (150 - Target Charge) / 100",
			"CtLoEMg"  : modifier(weaponParam['atk'] / 10) + " MATK * (150 - Target Charge) / 100",
			# default = PATK
	}
	return Formulas[WeaponFormulaTypes[weaponParam['formula']]] if weaponParam['formula'] in WeaponFormulaTypes else '100% PATK'
