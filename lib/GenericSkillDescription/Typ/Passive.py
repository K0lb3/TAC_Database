from .Effect import ConvertCondition,ConvertBuff
def Passive(skill,master):
    eff = skill['effect_type'] if 'effect_type' in skill else ''

    ret=[]
    if eff=='Buff': #"Casts Auto Heal on self & raises All Status Res of self and adjacent allies when one or more ally units are in adjacent squares",
        # flags - SubActuate
        # target_buff_iname   
        # target_cond_iname
        if 'target_buff_iname' in skill:
            ret.append(ConvertBuff(skill['target_buff_iname'],master))
        if 'target_cond_iname' in skill:
            ret.append(ConvertCondition(skill['target_cond_iname'],master))
        ret= '\n'.join(ret)     
    elif eff=='EffReplace':   #Skill Buff
        # requires dif function
        #target_buff_iname  - buff
        #ReplaceTargetIdLists - list of skills to be replaced
        #ReplaceChangeIdLists - list of skills which will be used
        #AbilityReplaceTargetIdLists
        #AbilityChangeTargetIdLists
        if 'target_buff_iname' in skill:
            ret.append(ConvertBuff(skill['target_buff_iname'],master))
        if 'ReplaceTargetIdLists' in skill:
            ret.append('Buffs %s'%', '.join([
                master['Skill'][skl]['name'] if skl in master['Skill'] and 'name' in master['Skill'][skl] else skl
                for skl in skill['ReplaceTargetIdLists']
            ]))
        ret= '\n'.join(ret)
    elif eff=='DisableCondition':    #Nullifies Death Sentence & raises Max HP
        # flags - SubActuate
        # target_buff_iname
        # target_cond_iname
        if 'target_buff_iname' in skill:
            ret.append(ConvertBuff(skill['target_buff_iname'],master))
        if 'target_cond_iname' in skill:
            ret.append('Nullifies %s'%ConvertCondition(skill['target_cond_iname'],master))
        ret= ' & '.join(ret)
    #elif eff=='FailCondition':  #Recovers HP and Jewels when asleep
        # flags - SubActuate
        # target_cond_iname
    #    return ConvertCondition(skill['target_cond_iname'],master)
    else:
        if 'target_buff_iname' in skill:
            ret.append(ConvertBuff(skill['target_buff_iname'],master))
        if 'target_cond_iname' in skill:
            ret.append(ConvertCondition(skill['target_cond_iname'],master))
        ret= '\n'.join(ret)
    
    if ret:
        if 'T: ' in ret:
            pos=ret.index('T: ')
            ret=(ret[:pos]+ret[pos+4:]).rstrip('[]')
        if 'DuplicateCount' in skill:
            ret+='\n%sx stackable'%skill['DuplicateCount']

    return ret if ret else skill['expr'] if 'expr' in skill else ''