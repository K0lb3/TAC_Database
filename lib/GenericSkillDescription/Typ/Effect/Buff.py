FIX={}
def ConvertBuff(Buff,master,typ=0):
    #   return:
    #       duration:   x ticks/turns   ~ turn_max, chk_timing
    #       value:                      ~ value_max
    #       chance:                     ~ rate_max
    #       Buffitions strength:        ~ fix or "v_auto_mp_heal",...
    #       range   "mEffRange": "SelfNearAlly", - "mAppMct": 14, ~ range
    		# 	"mAppType": "SelfKillCount",
			# "mAppMct": 999,
    if type(Buff)==str:
        if Buff in master['Buff']:
            Buff=master['Buff'][Buff]
        else:
            return 'Buff not found'

    if 'buffs' not in Buff:
        return ''

    duration=''
    if 'turn' in Buff and Buff['chk_timing'] != 'Eternal':
        duration='Turns: %s'%Buff['turn']

    chance=''
    if 'rate' in Buff and Buff['rate'] != 100:
        chance='Chance: %s%%'%Buff['rate']

    stack=''
    if 'mAppMct' in Buff:
        stack='Stack: %sx'%Buff['mAppMct']

    stacking=''
    if 'mEffRange' in Buff:
        stacking='of self and adjacent units'
    if 'mAppType' in Buff:
        stacking=Buff['mAppType']
        
    buffs=[
        '{sign}{value}{perc} {stat}'.format(
            sign='+' if buff['value_max']>0 else '',
            value=buff['value_max'],
            perc='%' if buff['calc']=='Scale' else '',
            stat=buff['type']
        )
        for buff in Buff['buffs']
    ]

    restriction=[]
    if 'job' in Buff:
        restriction.append(Buff['job'])
    if 'buki' in Buff:
        restriction.append(Buff['buki'])
    if 'birth' in Buff:
        restriction.append(Buff['birth'])
    if 'sex' in Buff:
        restriction.append(Buff['sex'])
    if 'un_group' in Buff:
        restriction.append(Buff['un_group'])
    if 'elem' in Buff:
        restriction.append(Buff['elem'])
    if 'cond' in Buff:
        restriction.append(Buff['cond'])
    if 'custom_targets' in Buff:
        try:
            CUTA=master['CustomTarget'][Buff['custom_targets'][0]]
            if len(CUTA['element'])!=6:
                restriction.append(', '.join(CUTA['element']))
            if 'unit_groups' in CUTA:
                restriction.append(', '.join([
                    master['Unit'][unit]['name']
                    for unit_group in CUTA["unit_groups"]
                    if unit_group in master['UnitGroup']
                    for unit in master['UnitGroup'][unit_group]['units']
                ]))
        except:
            restriction.append('%s [Costum Target]'%Buff['custom_targets'][0])
    #Buffition fix
    boundary=[b for b in [duration,chance,stack] if b]
    return ''.join(string for string in [
                ('%s: '%', '.join(restriction)) if restriction else '',
                ', '.join(buffs),
                stacking,
                ' [%s]'%', '.join(boundary) if boundary else ''
            ]
            if string
        )