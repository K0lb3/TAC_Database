import json
import math
import os

from .classes import Data

# paths
rootpath = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
respath  = os.path.join(rootpath, 'resources')
exportpath = os.path.join(rootpath, 'export')

# constants
ENUM = json.load(open(os.path.join(respath, 'enum.json'), 'rb'))
DATAGL = Data(os.path.join(respath, 'AssetsGL'))
DATAJP = Data(os.path.join(respath, 'AssetsJP'))


# functions
def convertDataParam(f):
	return {
			mkey: {
					item['iname']: item
					for item in tree
			} if type(tree) == list and type(tree[0]) == dict and 'iname' in tree[0] else tree
			for mkey, tree in f.items()
	}


def parseSheet(sheet):
	return [
			{
					sheet[0][i].lower(): cell.lstrip('\n').rstrip('\n').lstrip(' ').rstrip(' ')
					for i, cell in enumerate(row)
					if cell and type(cell) == str
			}
			for row in sheet[1:]
	]


def saveAsJSON(path, name, var, indent = '\t'):
	os.makedirs(path, exist_ok = True)
	with open(os.path.join(path, name), "wb") as f:
		f.write(json.dumps(var, indent = indent, ensure_ascii = False, ).encode('utf8'))
	return 1


def TACScale(ini, max, lv, mlv):
	return math.floor(ini + ((max - ini) / (mlv - 1) * (lv - 1)))
