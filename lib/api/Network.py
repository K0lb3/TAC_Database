from enum import IntEnum
from .Session import Session
import uuid
import http.client
from .EncryptionHelper import *
import json
import msgpack


class Network(object):
	def __init__(self, host, session = Session()):
		self.host = host
		self.session = session
		self.EncryptionHelper = EncryptionHelper(self.session)
	
	def ReqGAuth(self):
		body = {
				"access_token": "",
				"param"       : {
						"device_id" : self.session.DeviceID,
						"secret_key": self.session.SecretKey,
						"idfa"      : self.session.IDFA,  # Google advertising ID
						"idfv"      : self.session.IDFV,
						"udid"      : ""
				}
		}
		res = self.Request("/gauth/accesstoken", body)
		return res
	
	def ReqCheckVersion2(self, ver: str):
		body = {'ver': ver}
		ret = self.Request('/chkver2', body)
		ret = ret['body']['environments']['alchemist']
		self.session.enviroment.UpdateEnviroment(ret)
		return ret
	
	def GetPayload(self, parameters: dict, path: str) -> bytes:
		if parameters:
			UnencryptedPayload = json.dumps(parameters).encode('utf8')
		else:
			UnencryptedPayload = json.dumps(parameters).encode('utf8')
		
		if path == "/chkver2" or not self.session.IsUseEncryption:
			return UnencryptedPayload
		else:
			return self.EncryptionHelper.Encrypt(KeyType.APP if not StartsWith(path, "/charge") else KeyType.DLC, UnencryptedPayload, path)
	
	def GetResponsePayload(self, response: http.client.HTTPResponse, path: str):
		content_type = response.headers['Content-Type']
		pathAndQuery = path
		options = DecryptOptions.ExtraKeySaltATDI
		if EncodingTypes.BCT_NO_EXTRA_KEY_SALT in content_type:
			content_type = content_type.replace(
					f'+{EncodingTypes.BCT_NO_EXTRA_KEY_SALT}', '')
			options = DecryptOptions.None_
		elif StartsWith(pathAndQuery, '/login'):
			options = DecryptOptions.ExtraKeySaltAT

		data = response.read()
		if EncodingTypes.BCT_AES_ENCRYPTED in content_type:
			data = self.EncryptionHelper.Decrypt(
					KeyType.APP if not StartsWith(path, "/charge") else KeyType.DLC,
					data,
					path,
					options
				)
		
		if EncodingTypes.BCT_JSON_SERIALIZED in content_type or 'json' in content_type:
			return json.loads(data)

		elif EncodingTypes.BCT_MESSAGE_PACK_SERIALIZED in content_type:
			return msgpack.unpackb(data)

		else:
			return data
	
	def Request(self, path, body = {}, mode = "POST", host = False):
		# print(self.access_token)
		if path[0] != '/':
			path = f'/{path}'
		if not host:
			api = self.host
		
		body = self.GetPayload(body, path)
		
		# create headers
		headers = {
				"Content-Type"         : "application/octet-stream+karerepokai+fakamunatanga" if self.session.IsUseEncryption or not path == '/chkver2' else "application/json; charset=utf-8",
				"X-GUMI-DEVICE-OS"     : "windows",
				"X-GUMI-STORE-PLATFORM": "dmmgamesstore",
				"Content-Length"       : len(body),
				'Accept': '*/*',
				"Accept-Encoding": "application/json; charset=utf-8",
		}
		
		if path not in ["/gauth/accesstoken", '/gauth/register', '/chkver2']:
			if not getattr(self.session, 'AccessToken', False):
				self.session.AccessToken = self.ReqGAuth()['access_token']
			headers["Authorization"] = f"gauth {self.session.AccessToken}"
		try:
			con = http.client.HTTPSConnection(api)
			con.connect()
			con.request(mode, path, body, headers)
			response = con.getresponse()
			ret = self.GetResponsePayload(response, path)
			con.close()
		except http.client.RemoteDisconnected:
			return self.Request(path, body, mode)
		return ret


def StartsWith(string, start):
	return string[:len(start)] == start


class CompressMode(IntEnum):
	Lz4 = 0
	Gzip = 1
	None_ = 2


class ESerializeCompressMethod(IntEnum):
	JSON = 0
	TYPED_MESSAGE_PACK = 1
	TYPED_MESSAGE_PACK_GZIP = 2
	TYPED_MESSAGE_PACK_LZ4 = 3


class EncodingTypes():
	BASE = "application/octet-stream"
	BCT_JSON_SERIALIZED = "jhotuhiahanoatuhinga"
	BCT_MESSAGE_PACK_SERIALIZED = "karerepokai"
	BCT_AES_ENCRYPTED = "fakamunatanga"
	BCT_JSON_AES = f"{BASE}+{BCT_JSON_SERIALIZED}+{BCT_AES_ENCRYPTED}"
	BCT_MESSAGEPACK = f"{BASE}+{BCT_MESSAGE_PACK_SERIALIZED}"
	BCT_MESSAGEPACK_AES = f"{BASE}+{BCT_MESSAGE_PACK_SERIALIZED}+{BCT_AES_ENCRYPTED}"
	BCT_NO_EXTRA_KEY_SALT = "noeks"
	
	def GetCompressModeFromSerializeCompressMethod(self, method: ESerializeCompressMethod) -> CompressMode:
		if method == ESerializeCompressMethod.TYPED_MESSAGE_PACK_LZ4:
			CompressMode.Lz4
		elif method == ESerializeCompressMethod.TYPED_MESSAGE_PACK_GZIP:
			CompressMode.Gzip
		else:
			CompressMode.None_
	
	def IsJsonSerializeCompressSelected(self, method: ESerializeCompressMethod) -> bool:
		return method == ESerializeCompressMethod.JSON


class EConnectMode(IntEnum):
	Online = 0
	Offline = 1


class EErrCode(IntEnum):
	TimeOut = -2,
	Failed = -1,
	Success = 0,
	Unknown = 1,
	Version = 2,
	AssetVersion = 3,
	NoVersionDbg = 4,
	NoSID = 100,
	Maintenance = 200,
	ChatMaintenance = 201,
	MultiMaintenance = 202,
	VsMaintenance = 203,
	BattleRecordMaintenance = 204,
	MultiVersionMaintenance = 205,
	MultiTowerMaintenance = 206,
	RankingQuestMaintenance = 207,
	IllegalParam = 300,
	API = 400,
	ServerNotify = 500,
	NoFile = 1000,
	NoVersion = 1100,
	SessionFailure = 1200,
	CreateStopped = 1300,
	IllegalName = 1400,
	NoMail = 1500,
	MailReadable = 1501,
	ReqFriendRequestMax = 1600,
	ReqFriendIsFull = 1601,
	ReqNoFriend = 1602,
	ReqFriendRegistered = 1603,
	ReqFriendRequesting = 1604,
	RmNoFriend = 1700,
	RmFriendIsMe = 1701,
	FindNoFriend = 6000,
	FindIsMine = 6001,
	ApprNoFriend = 5900,
	ApprNoRequest = 5901,
	ApprRequestMax = 5902,
	ApprFriendIsFull = 5903,
	NoUnitParty = 1800,
	IllegalParty = 1801,
	ExpMaterialShort = 1900,
	RareMaterialShort = 2000,
	RarePlayerLvShort = 2001,
	PlusMaterialShot = 2100,
	PlusPlayerLvShort = 2101,
	AbilityMaterialShort = 2200,
	AbilityNotFound = 2201,
	NoJobSetJob = 2300,
	CantSelectJob = 2301,
	NoUnitSetJob = 2302,
	NoAbilitySetAbility = 2400,
	NoJobSetAbility = 2401,
	UnsetAbility = 2402,
	NoJobSetEquip = 2500,
	NoEquipItem = 2501,
	Equipped = 2502,
	NoJobEnforceEquip = 2600,
	NoEquipEnforce = 2601,
	ForceMax = 2602,
	MaterialShort = 2603,
	EnforcePlayerLvShort = 2604,
	NoJobLvUpEquip = 2700,
	EquipNotComp = 2701,
	PlusShort = 2702,
	UnitAddExist = 5700,
	UnitAddCostShort = 5701,
	UnitAddCantUnlock = 5702,
	ArtifactBoxLimit = 9000,
	ArtifactPieceShort = 9001,
	ArtifactMatShort = 9002,
	ArtifactFavorite = 9003,
	SkinNoSkin = 9010,
	SkinNoJob = 9011,
	NoItemSell = 2800,
	ConvertAnotherItem = 2801,
	GoldOverSell = 2802,
	StaminaCoinShort = 2900,
	AddStaminaLimit = 2901,
	AbilityCoinShort = 3000,
	AbilityVipLvShort = 3001,
	AbilityPlayerLvShort = 3002,
	GouseiNoTarget = 3200,
	GouseiMaterialShort = 3201,
	GouseiCostShort = 3202,
	UnSelectable = 3300,
	OutOfDateQuest = 3301,
	QuestNotEnd = 3302,
	ChallengeLimit = 3303,
	RecordLimitUpload = 3309,
	QuestResume = 3400,
	QuestEnd = 3500,
	ContinueCostShort = 3600,
	CantContinue = 3601,
	ColoCantSelect = 3800,
	ColoIsBusy = 3801,
	ColoCostShort = 3802,
	ColoIntervalShort = 3803,
	ColoBattleNotEnd = 3804,
	ColoPlayerLvShort = 3805,
	ColoVipShort = 3806,
	ColoRankLower = 3807,
	ColoNoBattle = 3900,
	ColoRankModify = 3901,
	ColoMyRankModify = 3902,
	RaidTicketShort = 5600,
	ColoResetCostShort = 5500,
	NoGacha = 4000,
	GachaCostShort = 4001,
	GachaItemMax = 4002,
	GachaNotFree = 4003,
	GachaPaidLimitOver = 4004,
	GachaPlyLvOver = 4005,
	GachaPlyNewOver = 4006,
	GachaLimitSoldOut = 4007,
	GachaLimitCntOver = 4008,
	GachaOutofPeriod = 4010,
	CanNotApplyDiscount = 4011,
	AlreadyApplyDiscount = 4012,
	TrophyRewarded = 4100,
	TrophyOutOfDate = 4101,
	TrophyRollBack = 4102,
	BingoOutofDateReceive = 4112,
	ShopRefreshCostShort = 4200,
	ShopRefreshLvSort = 4201,
	ShopSoldOut = 4300,
	ShopBuyCostShort = 4301,
	ShopBuyLvShort = 4302,
	ShopBuyNotFound = 4303,
	ShopBuyItemNotFound = 4304,
	ShopRefreshItemList = 4305,
	ShopBuyOutofItemPeriod = 4306,
	GoldBuyCostShort = 4400,
	GoldBuyLimit = 4401,
	ShopBuyOutofPeriod = 4403,
	ProductIllegalDate = 4500,
	ProductPurchaseMax = 4600,
	ProductCantPurchase = 4601,
	HikkoshiNoToken = 4700,
	NoBtlInfo = 3700,
	MultiPlayerLvShort = 3701,
	MultiBtlNotEnd = 3702,
	MultiVersionMismatch = 3704,
	RoomFailedMakeRoom = 4800,
	RoomIllegalComment = 4801,
	RoomNoRoom = 4900,
	NoWatching = 4901,
	RoomPlayerLvShort = 5800,
	NoDevice = 5000,
	Authorize = 5001,
	GauthNoSid = 5002,
	ReturnForceTitle = 5003,
	MigrateIllCode = 5100,
	MigrateSameDev = 5101,
	MigrateLockCode = 5102,
	CountLimitForPlayer = 8005,
	ChargeError = 8100,
	ChargeAge000 = 8101,
	ChargeVipRemains = 8102,
	FirstChargeInvalid = 8103,
	FirstChargeNoLog = 8104,
	FirstChargeReceipt = 8105,
	FirstChargePast = 8106,
	ChargePremiumRemains = 8107,
	ChargePremiumInvalid = 8108,
	LimitedShopOutOfPeriod = 4403,
	LimitedShopOutOfBuyLimit = 4405,
	EventShopOutOfPeriod = 4403,
	EventShopOutOfBuyLimit = 4405,
	NoChannelAction = 8500,
	NoUserAction = 8501,
	SendChatInterval = 8502,
	CanNotAddBlackList = 8503,
	NotLocation = 401,
	NotGpsQuest = 3308,
	NotGpsMail = 8600,
	ReceivedGpsMail = 8601,
	AcheiveMigrateIllcode = 8800,
	AcheiveMigrateNoCoop = 8801,
	AcheiveMigrateLock = 8802,
	AcheiveMigrateAuthorize = 8803,
	TowerLocked = 8201,
	ConditionsErr = 8202,
	NotRecovery_permit = 8203,
	NotExist_tower = 8211,
	NotExist_reward = 8212,
	NotExist_floor = 8213,
	NoMatch_party = 8221,
	NoMatch_mid = 8222,
	IncorrectCoin = 8231,
	IncorrectBtlparam = 8232,
	AlreadyClear = 8241,
	AlreadyBtlend = 8242,
	FaildRegistration = 8243,
	FaildReset = 8244,
	VS_NotSelfBattle = 10_000,
	VS_NotPlayer = 10_001,
	VS_NotQuestInfo = 10_002,
	VS_NotLINERoomInfo = 10_003,
	VS_FailRoomID = 10_004,
	VS_BattleEnd = 10_005,
	VS_NotQuestData = 10_006,
	VS_NotPhotonAppID = 10_007,
	VS_Version = 10_008,
	VS_IllComment = 10_009,
	VS_LvShort = 10_010,
	VS_BattleNotEnd = 10_011,
	VS_NoRoom = 10_012,
	VS_ComBattleEnd = 10_013,
	VS_FaildSeasonGift = 10_014,
	VS_TowerNotPlay = 10_015,
	VS_NotContinuousEnemy = 10_016,
	VS_RowerNotMatching = 10_017,
	VS_EnableTimeOutOfPriod = 10_018,
	DmmSessionExpired = 11_000,
	QR_OutOfPeriod = 8008,
	QR_InvalidQRSerial = 8009,
	QR_CanNotReward = 8010,
	QR_LockSerialCampaign = 8011,
	QR_AlreadyRewardSkin = 8012,
	QuestBookmark_RequestMax = 10_100,
	QuestBookmark_AlreadyLimited = 10_101,
	MT_NotClearFloor = 12_001,
	MT_AlreadyFinish = 12_002,
	MT_NoRoom = 12_003,
	MT_CanNotSkipFloor = 12_004,
	RankingQuest_NotNewScore = 13_001,
	RankingQuest_AlreadyEntry = 13_002,
	RankingQuest_OutOfPeriod = 13_003,
	Gallery_MigrationInProgress = 14_001,
	QuestArchive_ArchiveNotFound = 16_001,
	QuestArchive_ArchiveNotOpened = 16_002,
	QuestArchive_ArchiveAlreadyOpened = 16_003,
	InspSkill_IncorrectParam = 17_001,
	InspSkill_NotExistArtifact = 17_002,
	InspSkill_NotExistInspirationSkill = 17_003,
	InspSkill_ArtifactFavorite = 17_004,
	InspSkill_IncorrectMixArtifact = 17_005,
	InspSkill_CannotLevelUpLevelMax = 17_006,
	InspSkill_CannotResetNotFound = 17_007,
	InspSkill_CostShort = 17_008,
	InspSkill_CannotAddSlotArtifact = 17_009,
	InspSkill_CannotAddSlotNumMax = 17_010,
	Gift_ConceptCardBoxLimit = 9020,
	RepelledBlockList = 4902,
	CoinBuyUse_OutOfPeriod = 20_001,
	Guild_JoinedAlready = 20_010,
	Guild_NotJoined_First = 20_020,
	Guild_NotJoined = 20_021,
	Guild_AutoJoinTargetMissing = 20_022,
	Guild_NotFound = 20_040,
	Guild_NotFoundEntryRequest = 20_050,
	Guild_AlredyMember = 20_070,
	Guild_MemberMax = 20_080,
	Guild_SubMasterMax = 20_081,
	Guild_RaidDissolutionFaild = 20_091,
	Guild_RaidLeaveFaild = 20_102,
	Guild_LeaveFailed = 20_110,
	Guild_RaidKickFaild = 20_135,
	Guild_InputNgWord = 20_140,
	Guild_InvestLimitOneDay = 20_202,
	Guild_PayerCoinShort = 20_220,
	Guild_ShortPlayerLv = 20_230,
	Guild_ApplySendLevelShort = 20_231,
	Guild_EntryCoolTime = 20_240,
	Guild_InvestCoolTime = 20_241,
	Raid_OutOfPeriod = 21_010,
	Raid_OutOfOenTime = 21_011,
	Raid_NotRaidbossCurrent = 21_020,
	Raid_IncorrectBtlparam = 21_030,
	Raid_AlreadyClear = 21_040,
	Raid_AlreadyBtlend = 21_050,
	Raid_AlreadyRaidbossSelected = 21_060,
	Raid_AlreadyBeat = 21_070,
	Raid_NotFound = 21_080,
	Raid_NotRewardReady = 21_090,
	Raid_NotRescueBattle = 21_100,
	Raid_OverRescue = 21_101,
	Raid_AlredyRescueCancel = 21_102,
	Raid_NotRewardAreaRound = 21_110,
	Raid_CanNotNextAreaBossNotBeat = 21_120,
	Raid_CanNotNextAreaNotGetReward = 21_130,
	Raid_CanNotNextAreaCurrentAreaIsLast = 21_140,
	Raid_CanNotRoundBossNotBeat = 21_150,
	Raid_CanNotRoundNotGetReward = 21_160,
	Raid_CanNotRoundCurrentAreaIsNotLast = 21_170,
	Raid_NotComplete = 21_180,
	Raid_CanNotSubBp = 21_190,
	Raid_CanNotSelectAreaClear = 21_200,
	Raid_CanNotRescuePlLvShort = 21_300,
	Raid_CanNotRescueTimeOver = 21_400,
	Raid_RankRewardOutOfPeriod = 21_500,
	Raid_RankRewardAlreadyReceived = 21_600,
	Genesis_OutOfPeriod = 19_001,
	BoxGacha_NotExistBox = 18_001,
	BoxGacha_CostShort = 18_002,
	BoxGacha_RemainShort = 18_003,
	BoxGacha_NotResetBox = 18_004


class RequestResults(IntEnum):
	Success = 0
	Failure = 1
	Retry = 2
	Back = 3
	Timeout = 4
	Maintenance = 5
	VersionMismatch = 6
	InvalidSession = 7
	IllegalParam = 8
