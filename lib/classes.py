import json
import os
import re

reLoc = re.compile(r'(SRPG_.+?Param_)?(.+?)\t(.+?)(\t(.+?))?\r?\n')


class Data():
	def __init__(self, path):
		self.path = path
	
	def load(self, filepath:str):
		return open(os.path.join(self.path, *filepath.split('/')), 'rb').read()

	def __getitem__(self, filepath: str):
		if filepath not in self.__dict__:
			byts = open(os.path.join(self.path, *filepath.split('/')), 'rb').read()
			if 'loc' in filepath.lower():
				ret = self.parse_loc(byts)
			else:
				try:
					ret = json.loads(byts)
				except:
					ret = byts
			self.__dict__[filepath] = ret
		return self.__dict__[filepath]
	
	def parse_loc(self, data: bytes):
		for e in ['utf-8-sig', 'utf-16-bom', 'utf8', 'utf16', 'SHIFT_JIS']:
			try:
				data = data.decode(e)
				break
			except:
				pass
		return {
				match[2]: match[3] if not match[4] else (match[3], match[4])
				for match in reLoc.finditer(data + '\n')  # appended newline as regex last line fix
		}
