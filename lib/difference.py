import jellyfish


def similarity(ori, inp):
	return (jellyfish.jaro_winkler(inp, ori))


def match(original, new):
	for entry in new:
		if entry in original:
			if type(new[entry]) == dict:
				original[entry] = match(original[entry], new[entry])
			elif type(new[entry]) == list:
				for i in new[entry]:
					if i not in original[entry]:
						original[entry].append(i)
			elif 1:
				pass
		else:
			original[entry] = new[entry]
	return original


def DifParam(gl, jp):
	dif = {}
	if gl == jp:
		return dif
	
	if type(jp) == dict:
		for key, item in jp.items():
			if key in gl:
				if gl[key] != jp[key]:
					dif[key] = DifParam(gl[key], jp[key])
			else:
				dif[key] = item
	
	elif type(jp) == list:
		for ji in jp:
			dif = []
			found = False
			for gi in gl:
				if gi == ji:
					found = True
					break
			if not found:
				dif.append(ji)
	else:
		dif = jp
	
	return dif
