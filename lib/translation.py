from .compendium import compendium
from .shared import DATAGL, DATAJP


def LoadTranslation():
    TRANSLATION = compendium()

    locFiles = ['LocalizedQuestParam', 'LocalizedMasterParam', 'unit',
                'external_artifact', 'external_conceptcard', 'external_item', 'external_skill']
    for f in locFiles:
        for lang, data in [('japanese', DATAJP), ('english', DATAGL)]:
            try:
                data = data[f'Loc/{lang}/{f}']
                for key, item in data.items():
                    if (key not in TRANSLATION or lang == 'english') and item not in ['', ' ']:
                        TRANSLATION[key] = item
            except FileNotFoundError:
                print(lang, f)
                pass

    ret = {}
    for mkey, val in TRANSLATION.items():
        key, skey = mkey.rsplit('_', 1)
        if key not in ret:
            ret[key] = {}
        ret[key][skey.lower()] = val
    return ret


def LoadSYS():
    sys = DATAJP['Loc/japanese/sys']
    sys.update(DATAGL['Loc/english/sys'])
    return sys


TRANSLATION = LoadTranslation()
SYS = LoadSYS()
print('Loaded: Translations')
