import re,os, json

reEnumHeader = re.compile(r'public enum (.+?) .+')
reEnumMember = re.compile(r'\tpublic const (.+?) (.+?) = (.+?)]?;.+')
def CollectEnums(fp):
	enums = {}
	with open(fp, "rt", encoding='utf8') as f:
		#find contract start and decide if enum or config
		for line in f:
			if not '// Namespace:' in line:
				continue
			line = f.readline()
			match = reEnumHeader.match(line)
			try:
				if match:
					name=match[1]
					enums[name]={}
					f.readline()	# {\n skip
					f.readline()	#	fields
					f.readline()	#	int
					for line in f:
						if line == '}\n':
							break
						else:
							match = reEnumMember.match(line)
							if match == None:
								break
							enums[name][match[3]] = match[2]
			except:
				pass

	return {key.split('.')[-1]:val for key,val in enums.items()}

if __name__ == '__main__':
	e=CollectEnums('dump.cs')
	open('Enums.json','wb').write(json.dumps(e,indent='\t').encode('utf8'))