﻿// Decompiled with JetBrains decompiler
// Type: MapPickup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

public class MapPickup : MonoBehaviour
{
  public MapPickup.PickupEvent OnPickup;
  public Transform Shadow;
  public Vector3 DropEffectOffset;

  public MapPickup()
  {
    base.\u002Ector();
  }

  public delegate void PickupEvent();
}
