﻿// Decompiled with JetBrains decompiler
// Type: SRPG.MultiPlayDebug
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class MultiPlayDebug : MonoBehaviour
  {
    public GameObject debuginfo;
    public Button m_DebugBtn;
    private static MultiPlayDebug mInstance;

    public MultiPlayDebug()
    {
      base.\u002Ector();
    }

    public static MultiPlayDebug Instance
    {
      get
      {
        return MultiPlayDebug.mInstance;
      }
    }

    private void Awake()
    {
    }

    private void Start()
    {
      ((Component) this.m_DebugBtn).get_gameObject().SetActive(false);
    }

    private void OnDestroy()
    {
    }

    private void OnClick()
    {
    }
  }
}
