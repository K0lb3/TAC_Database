﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RuneSortToggleItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SRPG
{
  public class RuneSortToggleItem : MonoBehaviour
  {
    [SerializeField]
    private Toggle m_Toggle;
    [SerializeField]
    private Text m_SortName;

    public RuneSortToggleItem()
    {
      base.\u002Ector();
    }

    public bool isOn
    {
      get
      {
        return this.m_Toggle.get_isOn();
      }
    }

    public void SetToggleSilient(bool value)
    {
      if (Object.op_Equality((Object) this.m_Toggle, (Object) null))
        return;
      GameUtility.SetToggle(this.m_Toggle, value);
    }

    public void SetToggleValueChangeListner(UnityAction<bool> onToggleValueChanged)
    {
      if (Object.op_Equality((Object) this.m_Toggle, (Object) null))
        return;
      ((UnityEvent<bool>) this.m_Toggle.onValueChanged).RemoveListener(onToggleValueChanged);
      ((UnityEvent<bool>) this.m_Toggle.onValueChanged).AddListener(onToggleValueChanged);
    }

    public void SetName(string name)
    {
      if (Object.op_Equality((Object) this.m_SortName, (Object) null))
        return;
      this.m_SortName.set_text(name);
    }
  }
}
