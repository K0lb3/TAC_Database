﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqGuildRaidRankingPortBoss
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Network.Encoding;
using MessagePack;
using System;

namespace SRPG
{
  public class ReqGuildRaidRankingPortBoss : WebAPI
  {
    public ReqGuildRaidRankingPortBoss(
      int gid,
      int page,
      int boss_id,
      SRPG.Network.ResponseCallback response,
      EncodingTypes.ESerializeCompressMethod serializeCompressMethod)
    {
      this.name = "guildraid/ranking/port/boss";
      this.body = WebAPI.GetRequestString<ReqGuildRaidRankingPortBoss.RequestParam>(new ReqGuildRaidRankingPortBoss.RequestParam()
      {
        gid = gid,
        page = page,
        boss_id = boss_id
      });
      this.callback = response;
      this.serializeCompressMethod = serializeCompressMethod;
    }

    [Serializable]
    public class RequestParam
    {
      public int gid;
      public int page;
      public int boss_id;
    }

    [MessagePackObject(true)]
    [Serializable]
    public class Response
    {
      public JSON_GuildRaidRankingMember[] ranking_port_boss;
      public int totalPage;
    }
  }
}
