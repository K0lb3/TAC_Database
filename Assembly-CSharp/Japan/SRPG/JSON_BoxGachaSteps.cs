﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_BoxGachaSteps
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_BoxGachaSteps
  {
    public int step;
    public int total_num;
    public int total_num_feature;
    public int total_num_normal;
    public int remain_num_feature;
    public int remain_num_normal;
    public JSON_BoxGachaItems[] box_items;
  }
}
