﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GvGPartyWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  [FlowNode.Pin(2001, "キャッシュを削除", FlowNode.PinTypes.Input, 2001)]
  public class GvGPartyWindow : PartyWindow2
  {
    [SerializeField]
    private string HPBarObjectName = "gauge_hp";
    [SerializeField]
    private bool SetPartyUnitsClear = true;
    private const int GVG_DELETE_CACHE = 2001;
    [SerializeField]
    private Text NodeName;
    private List<UnitData> Units;

    private void Awake()
    {
      this.mIsLockCurrentParty = true;
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) GvGManager.Instance, (UnityEngine.Object) null) && UnityEngine.Object.op_Inequality((UnityEngine.Object) this.NodeName, (UnityEngine.Object) null))
      {
        GvGNodeData gvGnodeData = GvGManager.Instance.NodeDataList.Find((Predicate<GvGNodeData>) (n => n.NodeId == GvGManager.Instance.SelectNodeId));
        if (gvGnodeData != null)
          this.NodeName.set_text(gvGnodeData.NodeParam.Name);
      }
      this.ReloadPartyUnits();
    }

    public override void Activated(int pinID)
    {
      if (pinID == 2001)
        this.ReloadPartyUnits();
      else
        base.Activated(pinID);
    }

    private void ReloadPartyUnits()
    {
      this.Units = new List<UnitData>();
      if (!UnityEngine.Object.op_Inequality((UnityEngine.Object) GvGBattleTop.Instance, (UnityEngine.Object) null) || GvGBattleTop.Instance.SelfParty == null)
        return;
      GvGParty selfParty = GvGBattleTop.Instance.SelfParty;
      if (selfParty == null)
        DebugUtility.LogError("< color = red > GvGのパーティ情報がない</ color > ");
      else
        selfParty.Units.ForEach((Action<GvGPartyUnit>) (u => this.Units.Add((UnitData) u)));
    }

    protected override void SetItemSlot(int slotIndex, ItemData item)
    {
    }

    protected override void BeforeSetPartyUnit()
    {
      if (this.CurrentParty == null || this.CurrentParty.Units == null)
        return;
      if (this.SetPartyUnitsClear)
        this.Units.Clear();
      for (int index = 0; index < this.CurrentParty.Units.Length && index < this.UnitSlots.Length; ++index)
      {
        if (this.CurrentParty.Units[index] != null)
        {
          if (this.Units.Count < index + 1)
            this.Units.Add(this.CurrentParty.Units[index]);
          else
            this.Units[index] = this.CurrentParty.Units[index];
        }
        if (!UnityEngine.Object.op_Equality((UnityEngine.Object) this.UnitSlots[index], (UnityEngine.Object) null))
        {
          SerializeValueBehaviour component = (SerializeValueBehaviour) ((Component) this.UnitSlots[index]).GetComponent<SerializeValueBehaviour>();
          if (!UnityEngine.Object.op_Equality((UnityEngine.Object) component, (UnityEngine.Object) null))
          {
            GameObject gameObject = component.list.GetGameObject(this.HPBarObjectName);
            if (!UnityEngine.Object.op_Equality((UnityEngine.Object) gameObject, (UnityEngine.Object) null))
              gameObject.SetActive(this.CurrentParty.Units[index] != null);
          }
        }
      }
    }

    protected override void OnForwardOrBackButtonClick(SRPG_Button button)
    {
      if (!((Selectable) button).IsInteractable())
        return;
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) button, (UnityEngine.Object) this.BackButton))
        FlowNode_GameObject.ActivateOutputLinks((Component) this, 3);
      else if (!this.CheckUsedUnitCount(false))
      {
        UIUtility.SystemMessage(LocalizedText.Get("sys.GVG_RULE_USED_UNITCOUNT_OVER"), (UIUtility.DialogResultEvent) null, (GameObject) null, false, -1);
      }
      else
      {
        long[] numArray = new long[3];
        for (int index = 0; index < this.CurrentParty.Units.Length; ++index)
        {
          if (this.CurrentParty.Units[index] != null)
            numArray[index] = this.CurrentParty.Units[index].UniqueID;
        }
        if (GvGManager.Instance.GvGPeriod != null && UnityEngine.Object.op_Inequality((UnityEngine.Object) GvGDefenseSettings.Instance, (UnityEngine.Object) null))
        {
          long[] all = Array.FindAll<long>(numArray, (Predicate<long>) (unit_uid => unit_uid != 0L));
          if (all == null || all.Length < GvGManager.Instance.GvGPeriod.DefenseUnitMin)
          {
            UIUtility.SystemMessage(string.Format(LocalizedText.Get("sys.GVG_DEFENSE_UNITCOUNT_NOT_ENOUGH"), (object) GvGManager.Instance.GvGPeriod.DefenseUnitMin), (UIUtility.DialogResultEvent) null, (GameObject) null, false, -1);
            return;
          }
        }
        if (!Array.Exists<long>(numArray, (Predicate<long>) (unit_uid => unit_uid != 0L)))
        {
          FlowNode_GameObject.ActivateOutputLinks((Component) this, 3);
        }
        else
        {
          if (UnityEngine.Object.op_Inequality((UnityEngine.Object) GvGBattleTop.Instance, (UnityEngine.Object) null))
            GvGBattleTop.Instance.SetEditParty(numArray);
          else if (UnityEngine.Object.op_Inequality((UnityEngine.Object) GvGDefenseSettings.Instance, (UnityEngine.Object) null))
            GvGDefenseSettings.Instance.SetEditParty(numArray);
          this.Units.Clear();
          FlowNode_GameObject.ActivateOutputLinks((Component) this, 1);
        }
      }
    }

    protected override void OverrideLoadTeam()
    {
      if (this.mCurrentPartyType == PartyWindow2.EditPartyTypes.Auto)
        throw new InvalidPartyTypeException();
      this.mGuestUnit.Clear();
      this.mMaxTeamCount = this.mCurrentPartyType.GetMaxTeamCount();
      PlayerPartyTypes playerPartyType = this.mCurrentPartyType.ToPlayerPartyType();
      this.mTeams.Clear();
      PartyData partyOfType = MonoSingleton<GameManager>.Instance.Player.FindPartyOfType(playerPartyType);
      if (this.Units.Count > this.mSlotData.Count)
        DebugUtility.LogError("< color = red > ForcedDeckがスロットより多い</ color > ");
      UnitData[] src = new UnitData[this.mSlotData.Count];
      for (int index = 0; index < this.Units.Count; ++index)
        src[index] = MonoSingleton<GameManager>.Instance.Player.GetUnitData(this.Units[index].UniqueID);
      PartyEditData partyEditData = new PartyEditData(string.Empty, partyOfType);
      partyEditData.SetUnitsForce(src);
      this.mTeams.Add(partyEditData);
      this.SetCurrentParty(0);
      this.Refresh(true);
      this.BeforeSetPartyUnit();
    }

    protected override void PostForwardPressed()
    {
      if (!this.CheckUsedUnitCount(false))
        UIUtility.SystemMessage(LocalizedText.Get("sys.GVG_RULE_USED_UNITCOUNT_OVER"), (UIUtility.DialogResultEvent) null, (GameObject) null, false, -1);
      else if (UnityEngine.Object.op_Inequality((UnityEngine.Object) GvGDefenseSettings.Instance, (UnityEngine.Object) null))
        UIUtility.ConfirmBox("sys.GVG_DEFENSE_CONFIRM", (UIUtility.DialogResultEvent) (go => base.PostForwardPressed()), (UIUtility.DialogResultEvent) null, (GameObject) null, false, -1, (string) null, (string) null);
      else
        base.PostForwardPressed();
    }

    protected override void OnUnitSlotClick(GenericSlot slot, bool interactable)
    {
      if (DataSource.FindDataOfClass<UnitData>(((Component) slot).get_gameObject(), (UnitData) null) == null && !this.CheckUsedUnitCount(true))
        UIUtility.SystemMessage(LocalizedText.Get("sys.GVG_RULE_USED_UNITCOUNT_OVER"), (UIUtility.DialogResultEvent) null, (GameObject) null, false, -1);
      else
        base.OnUnitSlotClick(slot, interactable);
    }

    public override void Reopen(bool farceRefresh = false)
    {
      base.Reopen(farceRefresh);
      GameParameter.UpdateAll(((Component) this).get_gameObject());
    }

    private bool CheckUsedUnitCount(bool is_check_slot)
    {
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) GvGManager.Instance, (UnityEngine.Object) null) || GvGManager.Instance.CurrentRule == null)
        return false;
      int currentRuleUnitCount = GvGManager.Instance.CurrentRuleUnitCount;
      if (currentRuleUnitCount <= 0)
        return true;
      List<long> longList = new List<long>((IEnumerable<long>) GvGManager.Instance.UsedUnitList);
      for (int index = 0; index < this.CurrentParty.Units.Length; ++index)
      {
        if (this.CurrentParty.Units[index] != null && !longList.Contains(this.CurrentParty.Units[index].UniqueID))
          longList.Add(this.CurrentParty.Units[index].UniqueID);
      }
      return !is_check_slot ? currentRuleUnitCount >= longList.Count : currentRuleUnitCount > longList.Count;
    }
  }
}
