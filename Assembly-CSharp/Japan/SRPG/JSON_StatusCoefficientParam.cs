﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_StatusCoefficientParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_StatusCoefficientParam
  {
    public float hp;
    public float atk;
    public float def;
    public float matk;
    public float mdef;
    public float dex;
    public float spd;
    public float cri;
    public float luck;
    public float cmb;
    public float move;
    public float jmp;
  }
}
