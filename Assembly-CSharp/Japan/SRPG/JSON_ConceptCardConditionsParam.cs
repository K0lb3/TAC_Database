﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_ConceptCardConditionsParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_ConceptCardConditionsParam
  {
    public string iname;
    public int el_fire;
    public int el_watr;
    public int el_wind;
    public int el_thdr;
    public int el_lit;
    public int el_drk;
    public string un_group;
    public int units_cnds_type;
    public string job_group;
    public int jobs_cnds_type;
    public int sex;
    public int[] birth_id;
  }
}
