﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_AdvanceSelectedQuestIsSkipOpen
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;

namespace SRPG
{
  [FlowNode.NodeType("Advance/スキチケ開放の可能性があるか？", 32741)]
  [FlowNode.Pin(1, "In", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(11, "スキチケ開放の可能性あり", FlowNode.PinTypes.Output, 11)]
  [FlowNode.Pin(12, "スキチケ開放の可能性なし", FlowNode.PinTypes.Output, 12)]
  public class FlowNode_AdvanceSelectedQuestIsSkipOpen : FlowNode
  {
    private const int PIN_IN = 1;
    private const int PIN_OUT_TRUE = 11;
    private const int PIN_OUT_FALSE = 12;

    public override void OnActivate(int pinID)
    {
      if (pinID != 1)
        return;
      this.CheckSkipOpen();
    }

    private void CheckSkipOpen()
    {
      QuestParam quest = MonoSingleton<GameManager>.Instance.FindQuest(GlobalVars.SelectedQuestID);
      if (quest != null && quest.IsAdvanceBoss && (quest.HasMission() && !quest.IsMissionCompleteALL()))
        this.ActivateOutputLinks(11);
      else
        this.ActivateOutputLinks(12);
    }
  }
}
