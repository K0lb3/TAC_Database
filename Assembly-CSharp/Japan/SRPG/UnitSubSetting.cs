﻿// Decompiled with JetBrains decompiler
// Type: SRPG.UnitSubSetting
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class UnitSubSetting
  {
    public eMapUnitCtCalcType startCtCalc;
    public OInt startCtVal;

    public UnitSubSetting()
    {
    }

    public UnitSubSetting(JSON_MapPartySubCT json)
    {
      this.startCtCalc = (eMapUnitCtCalcType) json.ct_calc;
      this.startCtVal = (OInt) json.ct_val;
    }
  }
}
