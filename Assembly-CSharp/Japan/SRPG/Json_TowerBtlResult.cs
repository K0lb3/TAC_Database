﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_TowerBtlResult
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class Json_TowerBtlResult : Json_PlayerDataAll
  {
    public JSON_ReqTowerResuponse.Json_TowerPlayerUnit[] pdeck;
    public Json_Artifact[] artis;
    public int arrived_num;
    public int clear;
    public Json_TowerBtlEndRank ranking;
  }
}
