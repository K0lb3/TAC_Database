﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_RaidAreaParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_RaidAreaParam : JSON_RaidMasterParam
  {
    public int id;
    public int order;
    public int period_id;
    public int boss_count;
    public int area_boss_id;
    public int clear_reward_id;
  }
}
