﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_QuestClearUnlockUnitDataParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_QuestClearUnlockUnitDataParam
  {
    public string iname;
    public string uid;
    public int add;
    public int type;
    public string new_id;
    public string old_id;
    public string parent_id;
    public int ulv;
    public string aid;
    public int alv;
    public string[] qids;
    public int qcnd;
  }
}
