﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GameObjectCallBack
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.Events;

namespace SRPG
{
  public class GameObjectCallBack : MonoBehaviour
  {
    private UnityEvent mOnDestroy;
    private UnityEvent mOnEnable;
    private UnityEvent mOnDisable;
    private UnityEvent mOnStart;

    public GameObjectCallBack()
    {
      base.\u002Ector();
    }

    public UnityEvent onDestroy
    {
      get
      {
        return this.mOnDestroy;
      }
    }

    public UnityEvent onEnable
    {
      get
      {
        return this.mOnEnable;
      }
    }

    public UnityEvent onDisable
    {
      get
      {
        return this.mOnDisable;
      }
    }

    public UnityEvent onStart
    {
      get
      {
        return this.mOnStart;
      }
    }

    private void Awake()
    {
    }

    private void Start()
    {
      if (this.mOnStart == null)
        return;
      this.mOnStart.Invoke();
    }

    private void OnEnable()
    {
      if (this.mOnEnable == null)
        return;
      this.mOnEnable.Invoke();
    }

    private void OnDisable()
    {
      if (this.mOnDisable == null)
        return;
      this.mOnDisable.Invoke();
    }

    private void OnDestroy()
    {
      this.mOnDestroy.Invoke();
      ((UnityEventBase) this.mOnDestroy).RemoveAllListeners();
      ((UnityEventBase) this.mOnEnable).RemoveAllListeners();
      ((UnityEventBase) this.mOnDisable).RemoveAllListeners();
      ((UnityEventBase) this.mOnStart).RemoveAllListeners();
    }

    private void OnApplicationQuit()
    {
      ((UnityEventBase) this.mOnDestroy).RemoveAllListeners();
      ((UnityEventBase) this.mOnEnable).RemoveAllListeners();
      ((UnityEventBase) this.mOnDisable).RemoveAllListeners();
      ((UnityEventBase) this.mOnStart).RemoveAllListeners();
    }
  }
}
