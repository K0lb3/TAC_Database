﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ConceptCardAwakeCheckWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SRPG
{
  public class ConceptCardAwakeCheckWindow : MonoBehaviour
  {
    [SerializeField]
    private RectTransform ListParent;
    [SerializeField]
    private GameObject ListItemTemplate;
    [SerializeField]
    private GameObject StarStatusParent;
    [SerializeField]
    private GameObject AfterStarStatus;
    private ConceptCardData mCurrentConceptCard;
    private int mUpCount;
    private int mCurrentAwakeCount;

    public ConceptCardAwakeCheckWindow()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      this.CreateItemIcon();
      this.SetupText();
    }

    private void CreateItemIcon()
    {
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) this.ListParent, (UnityEngine.Object) null) || UnityEngine.Object.op_Equality((UnityEngine.Object) this.ListItemTemplate, (UnityEngine.Object) null))
        return;
      this.ListItemTemplate.SetActive(false);
      ConceptCardManager instance = ConceptCardManager.Instance;
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) instance, (UnityEngine.Object) null))
        return;
      this.mCurrentConceptCard = instance.SelectedConceptCardData;
      if (this.mCurrentConceptCard == null)
        return;
      Dictionary<string, int> awakeMaterialList = instance.SelectedAwakeMaterialList;
      if (awakeMaterialList == null || awakeMaterialList.Count <= 0)
        return;
      this.mCurrentAwakeCount = (int) this.mCurrentConceptCard.AwakeCount;
      this.mUpCount = 0;
      foreach (KeyValuePair<string, int> keyValuePair in awakeMaterialList)
      {
        ConceptLimitUpItemParam limitUpItemParam = this.mCurrentConceptCard.Param.GetConcepLimitUpItemParam(keyValuePair.Key);
        if (limitUpItemParam != null && limitUpItemParam.num > 0)
        {
          GameObject gameObject = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) this.ListItemTemplate, (Transform) this.ListParent);
          gameObject.SetActive(true);
          ItemParam itemParam = MonoSingleton<GameManager>.Instance.GetItemParam(keyValuePair.Key);
          DataSource.Bind<ItemParam>(gameObject, itemParam, false);
          DataSource.Bind<int>(gameObject, keyValuePair.Value, false);
          this.mUpCount += keyValuePair.Value / limitUpItemParam.num;
        }
      }
    }

    private void SetupText()
    {
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) this.StarStatusParent, (UnityEngine.Object) null) || this.mCurrentConceptCard == null)
        return;
      DataSource.Bind<ConceptCardData>(this.StarStatusParent, this.mCurrentConceptCard, false);
      this.RefreshAwakeIcons(this.mUpCount);
    }

    private void RefreshAwakeIcons(int add_awake_count)
    {
      int num = 0;
      IEnumerator enumerator1 = this.AfterStarStatus.get_transform().GetEnumerator();
      try
      {
        while (enumerator1.MoveNext())
        {
          Transform current = (Transform) enumerator1.Current;
          IEnumerator enumerator2 = current.GetEnumerator();
          try
          {
            while (enumerator2.MoveNext())
              ((Component) enumerator2.Current).get_gameObject().SetActive(false);
          }
          finally
          {
            if (enumerator2 is IDisposable disposable)
              disposable.Dispose();
          }
          string str = "off";
          if (num < this.mCurrentAwakeCount)
            str = "on";
          else if (num < this.mCurrentAwakeCount + add_awake_count)
            str = "up";
          Transform transform = current.Find(str);
          if (UnityEngine.Object.op_Inequality((UnityEngine.Object) transform, (UnityEngine.Object) null))
          {
            ((Component) transform).get_gameObject().SetActive(true);
            ++num;
          }
        }
      }
      finally
      {
        if (enumerator1 is IDisposable disposable)
          disposable.Dispose();
      }
    }
  }
}
