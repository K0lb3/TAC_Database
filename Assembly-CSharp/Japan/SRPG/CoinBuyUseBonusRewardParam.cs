﻿// Decompiled with JetBrains decompiler
// Type: SRPG.CoinBuyUseBonusRewardParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class CoinBuyUseBonusRewardParam
  {
    private string iname;
    private CoinBuyUseBonusItemParam[] rewards;

    public string Iname
    {
      get
      {
        return this.iname;
      }
    }

    public CoinBuyUseBonusItemParam[] Rewards
    {
      get
      {
        return this.rewards;
      }
    }

    public void Deserialize(JSON_CoinBuyUseBonusRewardParam json)
    {
      this.iname = json.iname;
      if (json.rewards == null)
        return;
      this.rewards = new CoinBuyUseBonusItemParam[json.rewards.Length];
      for (int index = 0; index < json.rewards.Length; ++index)
      {
        CoinBuyUseBonusItemParam useBonusItemParam = new CoinBuyUseBonusItemParam();
        useBonusItemParam.Deserialize(json.rewards[index]);
        this.rewards[index] = useBonusItemParam;
      }
    }

    public static void Deserialize(
      ref CoinBuyUseBonusRewardParam[] param,
      JSON_CoinBuyUseBonusRewardParam[] json)
    {
      if (json == null)
        return;
      param = new CoinBuyUseBonusRewardParam[json.Length];
      for (int index = 0; index < json.Length; ++index)
      {
        CoinBuyUseBonusRewardParam bonusRewardParam = new CoinBuyUseBonusRewardParam();
        bonusRewardParam.Deserialize(json[index]);
        param[index] = bonusRewardParam;
      }
    }
  }
}
