﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_ShopLineupItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  [Serializable]
  public class Json_ShopLineupItem
  {
    public string gtid;
    public Json_ShopLineupItemDetail[] items;
  }
}
