﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqCoinBuyUseBonusProgress
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class ReqCoinBuyUseBonusProgress : WebAPI
  {
    public ReqCoinBuyUseBonusProgress(Network.ResponseCallback response)
    {
      this.name = "coin_bonuse";
      this.body = WebAPI.GetRequestString(string.Empty);
      this.callback = response;
    }

    [Serializable]
    public class Response
    {
      public JSON_PlayerCoinBuyUseBonusState[] bonus_stats;
    }
  }
}
