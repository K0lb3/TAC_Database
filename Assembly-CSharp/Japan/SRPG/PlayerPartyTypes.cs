﻿// Decompiled with JetBrains decompiler
// Type: SRPG.PlayerPartyTypes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public enum PlayerPartyTypes
  {
    Normal,
    Event,
    Multiplay,
    Arena,
    ArenaDef,
    Character,
    Tower,
    Versus,
    MultiTower,
    Ordeal,
    RankMatch,
    Raid,
    GuildRaid,
    StoryExtra,
    Support,
    GvG,
    Max,
  }
}
