﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqCheckVersion2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class ReqCheckVersion2 : WebAPI
  {
    public ReqCheckVersion2(string ver, Network.ResponseCallback response)
    {
      this.name = "chkver2";
      this.body = "{";
      ReqCheckVersion2 reqCheckVersion2 = this;
      reqCheckVersion2.body = reqCheckVersion2.body + "\"ver\":\"" + ver + "\"";
      this.body += "}";
      this.callback = response;
    }
  }
}
