﻿// Decompiled with JetBrains decompiler
// Type: SRPG.WWWResult
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine.Networking;

namespace SRPG
{
  public struct WWWResult
  {
    private UnityWebRequest mResult;
    private string mResultValue;
    public byte[] rawResult;

    public WWWResult(UnityWebRequest www)
    {
      this.mResult = www;
      this.mResultValue = (string) null;
      this.rawResult = (byte[]) null;
    }

    public WWWResult(string result)
    {
      this.mResult = (UnityWebRequest) null;
      this.mResultValue = result;
      this.rawResult = (byte[]) null;
    }

    public WWWResult(byte[] rawResult)
    {
      this.mResult = (UnityWebRequest) null;
      this.mResultValue = (string) null;
      this.rawResult = rawResult;
    }

    public string text
    {
      get
      {
        return this.mResult != null ? this.mResult.get_downloadHandler().get_text() : this.mResultValue;
      }
    }
  }
}
