﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RaidRescueList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  [FlowNode.Pin(1, "Initialize", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(101, "Open Popup", FlowNode.PinTypes.Output, 101)]
  public class RaidRescueList : MonoBehaviour, IFlowInterface
  {
    public const int PIN_INPUT_INIT = 1;
    public const int PIN_OUTPUT_OPEN_POPUP = 101;
    [SerializeField]
    private Transform mItemParent;
    [SerializeField]
    private RaidRescueListItem mItem;
    [SerializeField]
    private GameObject mNoRequest;
    [SerializeField]
    private Button mUpdateButton;
    private List<RaidRescueListItem> mItemList;

    public RaidRescueList()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
      this.mNoRequest.SetActive(false);
    }

    public void Activated(int pinID)
    {
      if (pinID != 1)
        return;
      this.Init();
    }

    private void Init()
    {
      if (Object.op_Equality((Object) this.mItemParent, (Object) null) || Object.op_Equality((Object) this.mItem, (Object) null))
        return;
      for (int index = 0; index < this.mItemList.Count; ++index)
        Object.Destroy((Object) ((Component) this.mItemList[index]).get_gameObject());
      this.mItemList.Clear();
      if (RaidManager.Instance.RaidRescueMemberList == null || RaidManager.Instance.RaidRescueMemberList.Count <= 0)
      {
        this.mNoRequest.SetActive(true);
      }
      else
      {
        for (int index = 0; index < RaidManager.Instance.RaidRescueMemberList.Count; ++index)
        {
          RaidRescueListItem raidRescueListItem = (RaidRescueListItem) Object.Instantiate<RaidRescueListItem>((M0) this.mItem, this.mItemParent);
          raidRescueListItem.Setup(index, RaidManager.Instance.RaidRescueMemberList[index]);
          ((Component) raidRescueListItem).get_gameObject().SetActive(true);
          this.mItemList.Add(raidRescueListItem);
        }
        this.mNoRequest.SetActive(false);
      }
      if (Object.op_Equality((Object) this.mUpdateButton, (Object) null))
        return;
      ((Selectable) this.mUpdateButton).set_interactable(false);
    }

    public void ListClick(RaidRescueListItem item)
    {
      RaidManager.Instance.SetRescueIndex(item.Index);
      FlowNode_GameObject.ActivateOutputLinks((Component) this, 101);
    }

    private void Update()
    {
      if (!Object.op_Inequality((Object) this.mUpdateButton, (Object) null) || ((Selectable) this.mUpdateButton).get_interactable() || !RaidManager.Instance.RescueListIsRefreshable)
        return;
      ((Selectable) this.mUpdateButton).set_interactable(true);
    }
  }
}
