﻿// Decompiled with JetBrains decompiler
// Type: SRPG.SceneRoot
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  [ExecuteInEditMode]
  [DisallowMultipleComponent]
  public class SceneRoot : MonoBehaviour
  {
    public SceneRoot()
    {
      base.\u002Ector();
    }

    protected virtual void Awake()
    {
      GameUtility.SetCurrentScene();
      SceneAwakeObserver.Invoke(((Component) this).get_gameObject());
    }
  }
}
