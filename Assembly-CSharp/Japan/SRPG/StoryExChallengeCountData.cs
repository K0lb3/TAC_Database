﻿// Decompiled with JetBrains decompiler
// Type: SRPG.StoryExChallengeCountData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System;

namespace SRPG
{
  public class StoryExChallengeCountData
  {
    private int mNum;
    private int mReset;

    public int Num
    {
      get
      {
        return this.mNum;
      }
    }

    public int Reset
    {
      get
      {
        return this.mReset;
      }
    }

    public int RestChallengeCount
    {
      get
      {
        return Math.Max(0, MonoSingleton<GameManager>.Instance.MasterParam.FixParam.StoryExChallengeMax - this.mNum);
      }
    }

    public int RestResetCount
    {
      get
      {
        return Math.Max(0, MonoSingleton<GameManager>.Instance.MasterParam.FixParam.StoryExResetMax - this.mReset);
      }
    }

    public void Deserialize(JSON_StoryExChallengeCount json)
    {
      this.mNum = json.num;
      this.mReset = json.reset;
    }

    public bool IsRestChallengeCount_Zero()
    {
      return MonoSingleton<GameManager>.Instance.MasterParam.FixParam.StoryExChallengeMax > 0 && this.mNum >= MonoSingleton<GameManager>.Instance.MasterParam.FixParam.StoryExChallengeMax;
    }

    public bool IsRestResetCount_Zero()
    {
      return this.mReset >= MonoSingleton<GameManager>.Instance.MasterParam.FixParam.StoryExResetMax;
    }
  }
}
