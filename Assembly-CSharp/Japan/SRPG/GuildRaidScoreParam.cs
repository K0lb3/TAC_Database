﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRaidScoreParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using System.Collections.Generic;

namespace SRPG
{
  public class GuildRaidScoreParam : GuildRaidMasterParam<JSON_GuildRaidScoreParam>
  {
    public int Id { get; private set; }

    public List<GuildRaidScoreDataParam> Score { get; private set; }

    public override bool Deserialize(JSON_GuildRaidScoreParam json)
    {
      if (json == null)
        return false;
      this.Id = json.id;
      if (json.score == null)
        return false;
      this.Score = new List<GuildRaidScoreDataParam>();
      for (int index = 0; index < json.score.Length; ++index)
      {
        GuildRaidScoreDataParam raidScoreDataParam = new GuildRaidScoreDataParam();
        if (raidScoreDataParam.Deserialize(json.score[index]))
          this.Score.Add(raidScoreDataParam);
      }
      return true;
    }

    public int GetScore(int round, int score)
    {
      int num1 = score;
      if (this.Score != null && this.Score.Count > 0)
      {
        GuildRaidScoreDataParam raidScoreDataParam = this.Score.Find((Predicate<GuildRaidScoreDataParam>) (data => data.Round == round));
        if (raidScoreDataParam != null)
        {
          long num2 = (long) (score * raidScoreDataParam.Scale) / 100L;
          if (num2 >= (long) int.MaxValue)
          {
            DebugUtility.LogWarning("GuildRaidScore Score Over int.MaxValue");
            num2 = (long) int.MaxValue;
          }
          num1 = (int) num2;
        }
      }
      return num1;
    }
  }
}
