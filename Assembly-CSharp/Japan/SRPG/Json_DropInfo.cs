﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_DropInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class Json_DropInfo
  {
    public string iname = string.Empty;
    public string iname_origin = string.Empty;
    public string type = string.Empty;
    public int rare = -1;
    public string get_unit = string.Empty;
    public int num;
    public int is_new;
    public int is_gift;
    public int is_feature_item;
    public int ch_piece_coin_num;
    public int is_pickup;
  }
}
