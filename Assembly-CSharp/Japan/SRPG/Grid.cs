﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Grid
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class Grid
  {
    public int cost = 1;
    public byte step = 127;
    public const int DEFAULT_COST = 1;
    public int x;
    public int y;
    public int height;
    public string tile;
    public GeoParam geo;
    public int enter;

    public bool IsDisableStopped()
    {
      return this.geo != null && (bool) this.geo.DisableStopped;
    }

    public bool IsEnter(eMovType mov_type)
    {
      return (this.enter & 1 << (int) (mov_type & (eMovType) 31)) != 0;
    }
  }
}
