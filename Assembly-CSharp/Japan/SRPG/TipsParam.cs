﻿// Decompiled with JetBrains decompiler
// Type: SRPG.TipsParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class TipsParam
  {
    public string iname;
    public ETipsType type;
    public int order;
    public string title;
    public string text;
    public string[] images;
    public bool hide;
    public string cond_text;

    public void Deserialize(JSON_TipsParam json)
    {
      this.iname = json.iname;
      this.type = (ETipsType) Enum.ToObject(typeof (ETipsType), json.type);
      this.order = json.order;
      this.title = json.title;
      this.text = json.text;
      this.images = json.images;
      this.hide = json.hide != 0;
      this.cond_text = json.cond_text;
    }
  }
}
