﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_LoadMasterParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System;
using System.Threading;
using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("System/Master/LoadMasterParam", 16777215)]
  [FlowNode.Pin(0, "Start", FlowNode.PinTypes.Input, 0)]
  [FlowNode.Pin(100, "Finished", FlowNode.PinTypes.Output, 100)]
  public class FlowNode_LoadMasterParam : FlowNode
  {
    private Mutex mMutex;
    private GameManager.LoadMasterDataResult mResult;

    public override void OnActivate(int pinID)
    {
      if (pinID != 0 || ((Behaviour) this).get_enabled())
        return;
      if (AssetManager.UseDLC && GameUtility.Config_UseAssetBundles.Value && !GameUtility.Config_UseServerData.Value)
      {
        ((Behaviour) this).set_enabled(true);
        CriticalSection.Enter(CriticalSections.Default);
        FlowNode_LoadMasterParam.ThreadStartParam threadStartParam = new FlowNode_LoadMasterParam.ThreadStartParam();
        threadStartParam.self = this;
        threadStartParam.GameManager = MonoSingleton<GameManager>.Instance;
        threadStartParam.IsUseSerialized = GameUtility.Config_UseSerializedParams.Value;
        threadStartParam.IsUseEncryption = GameUtility.Config_UseEncryption.Value;
        Debug.Log((object) "Starting Thread");
        this.mMutex = new Mutex();
        new Thread(new ParameterizedThreadStart(FlowNode_LoadMasterParam.LoadMasterDataThread)).Start((object) threadStartParam);
      }
      else
      {
        ((Behaviour) this).set_enabled(false);
        this.ActivateOutputLinks(100);
      }
    }

    protected override void OnDestroy()
    {
      if (this.mMutex != null)
      {
        this.mMutex.WaitOne();
        this.mMutex.ReleaseMutex();
        this.mMutex.Close();
        this.mMutex = (Mutex) null;
      }
      base.OnDestroy();
    }

    private void Update()
    {
      if (this.mMutex == null)
        return;
      this.mMutex.WaitOne();
      bool flag = this.mResult.Result != GameManager.ELoadMasterDataResult.NOT_YET_MATE;
      this.mMutex.ReleaseMutex();
      if (!flag)
        return;
      this.mMutex.Close();
      this.mMutex = (Mutex) null;
      GameManager.HandleAnyLoadMasterDataErrors(this.mResult, true);
      ((Behaviour) this).set_enabled(false);
      CriticalSection.Leave(CriticalSections.Default);
      this.ActivateOutputLinks(100);
    }

    private static void LoadMasterDataThread(object param)
    {
      Debug.Log((object) nameof (LoadMasterDataThread));
      FlowNode_LoadMasterParam.ThreadStartParam threadStartParam = (FlowNode_LoadMasterParam.ThreadStartParam) param;
      Debug.Log((object) "LoadMasterDataThread START");
      GameManager.LoadMasterDataResult masterDataResult = new GameManager.LoadMasterDataResult();
      try
      {
        masterDataResult = threadStartParam.IsUseSerialized || threadStartParam.IsUseEncryption ? threadStartParam.GameManager.ReloadMasterData(new GameManager.MasterDataInBinary().Load(threadStartParam.IsUseSerialized, threadStartParam.IsUseEncryption)) : threadStartParam.GameManager.ReloadMasterData((string) null, (string) null, (GameManager.MasterDataInBinary) null);
      }
      catch (Exception ex)
      {
        Debug.LogException(ex);
      }
      Debug.Log((object) "LoadMasterDataThread END");
      if (threadStartParam.self.mMutex == null)
        return;
      threadStartParam.self.mMutex.WaitOne();
      threadStartParam.self.mResult = masterDataResult;
      threadStartParam.self.mMutex.ReleaseMutex();
    }

    private class ThreadStartParam
    {
      public FlowNode_LoadMasterParam self;
      public GameManager GameManager;
      public bool IsUseSerialized;
      public bool IsUseEncryption;
    }
  }
}
