﻿// Decompiled with JetBrains decompiler
// Type: SRPG.BindRuneData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;

namespace SRPG
{
  public class BindRuneData
  {
    public long iid;
    public bool is_selected;
    public bool is_disable;
    public bool is_check;
    public bool is_owner_disable;
    public bool is_use_copy;
    private RuneData CopyRune;

    public BindRuneData()
    {
    }

    public BindRuneData(long _iid)
    {
      this.iid = _iid;
    }

    public RuneData Rune
    {
      get
      {
        return this.is_use_copy ? this.CopyRune : MonoSingleton<GameManager>.Instance.Player.FindRuneByUniqueID(this.iid);
      }
    }

    public RuneParam RuneParam
    {
      get
      {
        return this.Rune?.RuneParam;
      }
    }

    public RuneMaterial RuneMaterialList
    {
      get
      {
        return this.Rune?.RuneMaterialList;
      }
    }

    public RuneCost EnhanceCost
    {
      get
      {
        return this.Rune?.EnhanceCost;
      }
    }

    public int DisassemblyZeny
    {
      get
      {
        RuneData rune = this.Rune;
        return rune == null ? 0 : rune.DisassemblyZeny;
      }
    }

    public RuneCost EvoCost
    {
      get
      {
        return this.Rune?.EvoCost;
      }
    }

    public RuneCost[] ResetParamBaseCost
    {
      get
      {
        return this.Rune?.ResetParamBaseCost;
      }
    }

    public RuneCost[] ResetStatusEvoCost
    {
      get
      {
        return this.Rune?.ResetStatusEvoCost;
      }
    }

    public RuneCost[] ParamEnhEvoCost
    {
      get
      {
        return this.Rune?.ParamEnhEvoCost;
      }
    }

    public UnitData GetOwner()
    {
      return this.Rune?.GetOwner();
    }

    public RuneMaterial RuneMaterial
    {
      get
      {
        return this.Rune?.RuneMaterial;
      }
    }

    public UnitData UnitData
    {
      get
      {
        return this.Rune?.UnitData;
      }
    }

    public int EnhanceNum
    {
      get
      {
        RuneData rune = this.Rune;
        return rune == null ? 0 : rune.EnhanceNum;
      }
    }

    public bool IsEvoNext
    {
      get
      {
        RuneData rune = this.Rune;
        return rune != null && rune.IsEvoNext;
      }
    }

    public bool IsCanEvo
    {
      get
      {
        RuneData rune = this.Rune;
        return rune != null && rune.IsCanEvo;
      }
    }

    public int EvoNum
    {
      get
      {
        RuneData rune = this.Rune;
        return rune == null ? 0 : rune.EvoNum;
      }
    }

    public ItemParam Item
    {
      get
      {
        return this.Rune?.Item;
      }
    }

    public int Rarity
    {
      get
      {
        RuneData rune = this.Rune;
        return rune == null ? 0 : rune.Rarity;
      }
    }

    public bool IsFavorite
    {
      get
      {
        RuneData rune = this.Rune;
        return rune != null && rune.IsFavorite;
      }
    }

    public BindRuneData CreateCopyRune()
    {
      BindRuneData bindRuneData = new BindRuneData();
      bindRuneData.iid = this.iid;
      bindRuneData.is_selected = this.is_selected;
      bindRuneData.is_disable = this.is_disable;
      bindRuneData.is_check = this.is_check;
      bindRuneData.is_owner_disable = this.is_owner_disable;
      RuneData rune = this.Rune;
      if (rune != null)
      {
        bindRuneData.CopyRune = new RuneData();
        bindRuneData.CopyRune.Deserialize(rune.Serialize());
        bindRuneData.is_use_copy = true;
      }
      return bindRuneData;
    }

    public void ResetOptionParam()
    {
      this.is_selected = false;
      this.is_disable = false;
      this.is_check = false;
    }
  }
}
