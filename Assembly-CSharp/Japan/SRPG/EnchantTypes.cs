﻿// Decompiled with JetBrains decompiler
// Type: SRPG.EnchantTypes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public enum EnchantTypes
  {
    Poison,
    Paralysed,
    Stun,
    Sleep,
    Charm,
    Stone,
    Blind,
    DisableSkill,
    DisableMove,
    DisableAttack,
    Zombie,
    DeathSentence,
    Berserk,
    Knockback,
    ResistBuff,
    ResistDebuff,
    Stop,
    Fast,
    Slow,
    AutoHeal,
    Donsoku,
    Rage,
    GoodSleep,
    AutoJewel,
    DisableHeal,
    SingleAttack,
    AreaAttack,
    DecCT,
    IncCT,
    ESA_Fire,
    ESA_Water,
    ESA_Wind,
    ESA_Thunder,
    ESA_Shine,
    ESA_Dark,
    MaxDamageHp,
    MaxDamageMp,
    SideAttack,
    BackAttack,
    ObstReaction,
    ForcedTargeting,
  }
}
