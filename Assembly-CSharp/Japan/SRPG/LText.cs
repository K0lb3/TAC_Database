﻿// Decompiled with JetBrains decompiler
// Type: SRPG.LText
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class LText : Text
  {
    [HideInInspector]
    [SerializeField]
    private bool mReplaceNoBreakSpace;
    private string mCurrentText;

    public LText()
    {
      base.\u002Ector();
    }

    public bool ReplaceNoBreakSpace
    {
      get
      {
        return this.mReplaceNoBreakSpace;
      }
      set
      {
        if (value == this.mReplaceNoBreakSpace)
          return;
        this.mReplaceNoBreakSpace = value;
        this.ReplaceLocalizedText();
      }
    }

    private void LateUpdate()
    {
      if (!Application.get_isPlaying())
        return;
      if (string.IsNullOrEmpty(this.mCurrentText))
      {
        if (string.IsNullOrEmpty(this.get_text()))
          return;
      }
      else if (!string.IsNullOrEmpty(this.get_text()) && this.mCurrentText.Equals(this.get_text()))
        return;
      this.ReplaceLocalizedText();
    }

    private void ReplaceLocalizedText()
    {
      if (!Application.get_isPlaying())
        return;
      string str = LocalizedText.Get(this.get_text());
      if (this.ReplaceNoBreakSpace)
        str = str.Replace(' ', ' ');
      this.set_text(str);
      this.mCurrentText = this.get_text();
    }
  }
}
