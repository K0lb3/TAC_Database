﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RaidStagePoint
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;
using UnityEngine;

namespace SRPG
{
  public class RaidStagePoint : MonoBehaviour
  {
    [SerializeField]
    private List<Transform> mStageList;
    [SerializeField]
    private Transform mBossStage;

    public RaidStagePoint()
    {
      base.\u002Ector();
    }

    public List<Transform> StageList
    {
      get
      {
        return this.mStageList;
      }
    }

    public Transform BossStage
    {
      get
      {
        return this.mBossStage;
      }
    }
  }
}
