﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GvGRuleParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using System.Collections.Generic;

namespace SRPG
{
  public class GvGRuleParam : GvGMasterParam<JSON_GvGRuleParam>
  {
    private string mIname;
    private string mName;
    private int mUnitCount;
    private int mDefUnitCount;
    private List<EBirth> mCndsBirth;

    public string Iname
    {
      get
      {
        return this.mIname;
      }
    }

    public string Name
    {
      get
      {
        return this.mName;
      }
    }

    public int UnitCount
    {
      get
      {
        return this.mUnitCount;
      }
    }

    public int DefUnitCount
    {
      get
      {
        return this.mDefUnitCount;
      }
    }

    public List<EBirth> CndsBirth
    {
      get
      {
        return this.mCndsBirth;
      }
    }

    public override bool Deserialize(JSON_GvGRuleParam json)
    {
      if (json == null)
        return false;
      this.mIname = json.iname;
      this.mName = json.name;
      this.mUnitCount = json.unit_cnt;
      this.mDefUnitCount = json.def_unit_cnt;
      this.mCndsBirth = new List<EBirth>();
      if (json.cnds_birth != null)
      {
        for (int index = 0; index < json.cnds_birth.Length; ++index)
          this.mCndsBirth.Add((EBirth) json.cnds_birth[index]);
      }
      return true;
    }

    public bool IsExistConditions()
    {
      return this.mCndsBirth != null && this.mCndsBirth.Count > 0;
    }

    public List<UnitData> GetDisableUnits(List<UnitData> units)
    {
      List<UnitData> unitDataList = new List<UnitData>((IEnumerable<UnitData>) units);
      for (int index = unitDataList.Count - 1; index >= 0; --index)
      {
        if (this.IsEnableUnit(unitDataList[index]))
          unitDataList.Remove(unitDataList[index]);
      }
      return unitDataList;
    }

    public List<UnitData> GetEnableUnits(List<UnitData> units)
    {
      List<UnitData> unitDataList = new List<UnitData>((IEnumerable<UnitData>) units);
      for (int index = unitDataList.Count - 1; index >= 0; --index)
      {
        if (!this.IsEnableUnit(unitDataList[index]))
          unitDataList.Remove(unitDataList[index]);
      }
      return unitDataList;
    }

    private bool IsEnableUnit(UnitData unit)
    {
      return this.mCndsBirth.Count <= 0 || this.mCndsBirth.FindIndex((Predicate<EBirth>) (birth => birth == (EBirth) unit.UnitParam.birthID)) > -1;
    }
  }
}
