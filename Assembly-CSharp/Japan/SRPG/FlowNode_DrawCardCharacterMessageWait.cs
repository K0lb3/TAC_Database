﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_DrawCardCharacterMessageWait
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("DrawCard/Character/MessageWait", 32741)]
  [FlowNode.Pin(1, "Start", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(10, "Output", FlowNode.PinTypes.Output, 10)]
  public class FlowNode_DrawCardCharacterMessageWait : FlowNode
  {
    private const int PIN_IN_START = 1;
    private const int PIN_OUT_END = 10;

    public override void OnActivate(int pinID)
    {
      if (pinID != 1)
        return;
      ((Behaviour) this).set_enabled(true);
    }

    private void Update()
    {
      if (DrawCardCharacterMessage.IsMessaging)
        return;
      this.ActivateOutputLinks(10);
      ((Behaviour) this).set_enabled(false);
    }
  }
}
