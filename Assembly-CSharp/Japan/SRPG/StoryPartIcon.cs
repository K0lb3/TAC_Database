﻿// Decompiled with JetBrains decompiler
// Type: SRPG.StoryPartIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class StoryPartIcon : MonoBehaviour
  {
    private const string ANIMATION_RELEASE_NAME = "open";
    private StoryPartIcon.EndUnlockAnimStoryPart OnEndUnlockAnimation;
    [SerializeField]
    private GameObject MainIcon;
    [SerializeField]
    private Animator UnlockAnimator;
    [SerializeField]
    private Image LockCover;
    private bool mLockFlag;
    private bool mIsPlayingUnlockAnim;

    public StoryPartIcon()
    {
      base.\u002Ector();
    }

    public bool Setup(bool unlock_flag, eStoryPart story_part, bool is_unlock_anim)
    {
      bool flag = !unlock_flag;
      if (is_unlock_anim)
        flag = true;
      if (Object.op_Equality((Object) this.MainIcon, (Object) null))
        return false;
      this.mLockFlag = flag;
      if (!flag)
      {
        this.MainIcon.SetActive(true);
        GameUtility.SetGameObjectActive((Component) this.LockCover, false);
      }
      else
      {
        this.MainIcon.SetActive(false);
        GameUtility.SetGameObjectActive((Component) this.LockCover, true);
      }
      return true;
    }

    public bool PlayUnlockAnim(StoryPartIcon.EndUnlockAnimStoryPart callback)
    {
      if (!this.mLockFlag || Object.op_Equality((Object) this.UnlockAnimator, (Object) null))
        return false;
      this.mIsPlayingUnlockAnim = true;
      this.OnEndUnlockAnimation = callback;
      ((Component) this.UnlockAnimator).get_gameObject().SetActive(true);
      this.UnlockAnimator.Play("open");
      return true;
    }

    private void Update()
    {
      if (!this.mIsPlayingUnlockAnim || this.IsPlayingReleaseAnim())
        return;
      this.mIsPlayingUnlockAnim = false;
      this.OnEndUnlockAnimation();
      this.ReleaseIcon();
    }

    private bool IsPlayingReleaseAnim()
    {
      if (!this.mLockFlag)
        return false;
      bool flag = false;
      if (Object.op_Inequality((Object) this.UnlockAnimator, (Object) null))
      {
        AnimatorStateInfo animatorStateInfo = this.UnlockAnimator.GetCurrentAnimatorStateInfo(0);
        if ((double) ((AnimatorStateInfo) ref animatorStateInfo).get_normalizedTime() < 1.0)
          flag = true;
      }
      return flag;
    }

    private void ReleaseIcon()
    {
      this.MainIcon.SetActive(true);
      GameUtility.SetGameObjectActive((Component) this.LockCover, false);
      GameUtility.SetGameObjectActive((Component) this.UnlockAnimator, false);
      this.mLockFlag = false;
    }

    public delegate void EndUnlockAnimStoryPart();
  }
}
