﻿// Decompiled with JetBrains decompiler
// Type: SRPG.BuyCoinShopParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class BuyCoinShopParam
  {
    private string mShopId;
    private BuyCoinManager.BuyCoinShopType mShopType;
    private string mDisplayName;
    private long mBeginAt;
    private long mEndAt;
    private bool mAlwaysOpen;

    public string ShopId
    {
      get
      {
        return this.mShopId;
      }
    }

    public BuyCoinManager.BuyCoinShopType ShopType
    {
      get
      {
        return this.mShopType;
      }
    }

    public string DisplayName
    {
      get
      {
        return this.mDisplayName;
      }
    }

    public long BeginAt
    {
      get
      {
        return this.mBeginAt;
      }
    }

    public long EndAt
    {
      get
      {
        return this.mEndAt;
      }
    }

    public bool AlwaysOpen
    {
      get
      {
        return this.mAlwaysOpen;
      }
    }

    public bool Deserialize(JSON_BuyCoinShopParam json)
    {
      if (json == null)
        return false;
      DateTime result;
      if (json.begin_at != null)
      {
        DateTime.TryParse(json.begin_at, out result);
        this.mBeginAt = TimeManager.GetUnixSec(result);
      }
      else
        this.mBeginAt = 0L;
      if (json.end_at != null)
      {
        DateTime.TryParse(json.end_at, out result);
        this.mEndAt = TimeManager.GetUnixSec(result);
      }
      else
        this.mEndAt = 0L;
      this.mAlwaysOpen = false;
      if (this.mBeginAt == 0L && this.mEndAt == 0L)
        this.mAlwaysOpen = true;
      this.mShopId = json.shop_id;
      this.mShopType = (BuyCoinManager.BuyCoinShopType) json.shop_type;
      this.mDisplayName = json.display_name;
      return true;
    }
  }
}
