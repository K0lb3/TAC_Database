﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_RaidBossParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_RaidBossParam : JSON_RaidMasterParam
  {
    public int id;
    public int stamp_index;
    public int period_id;
    public int weight;
    public string name;
    public int hp;
    public string unit_iname;
    public string quest_iname;
    public string time_limit;
    public int battle_reward_id;
    public int beat_reward_id;
    public int damage_ratio_reward_id;
    public int damage_amount_reward_id;
    public string buff_id;
    public string mob_buff_id;
    public int is_boss;
  }
}
