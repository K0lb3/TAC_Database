﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqJukeBoxPlaylistDel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Network.Encoding;
using MessagePack;
using System;

namespace SRPG
{
  public class ReqJukeBoxPlaylistDel : WebAPI
  {
    public ReqJukeBoxPlaylistDel(
      string[] bgm_list,
      SRPG.Network.ResponseCallback response,
      EncodingTypes.ESerializeCompressMethod type)
    {
      this.name = "gallery/jukebox/delete";
      this.body = WebAPI.GetRequestString<ReqJukeBoxPlaylistDel.RequestParam>(new ReqJukeBoxPlaylistDel.RequestParam()
      {
        bgms = bgm_list
      });
      this.callback = response;
      this.serializeCompressMethod = type;
    }

    [Serializable]
    public class RequestParam
    {
      public string[] bgms;
    }

    [MessagePackObject(true)]
    [Serializable]
    public class Response
    {
      public JukeBoxWindow.ResPlayList[] playlists;
    }
  }
}
