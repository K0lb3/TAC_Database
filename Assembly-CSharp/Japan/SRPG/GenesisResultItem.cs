﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GenesisResultItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class GenesisResultItem : MonoBehaviour
  {
    [SerializeField]
    private Text TextRewardName;
    [SerializeField]
    private Text TextRewardNum;
    [SerializeField]
    private GameObject TextRewardConn;
    [Space(5f)]
    [SerializeField]
    private GenesisRewardIcon RewardIconTemplate;
    [SerializeField]
    private Transform TrRewardIconParent;
    private int mIndex;
    private GiftData mGiftData;

    public GenesisResultItem()
    {
      base.\u002Ector();
    }

    public int Index
    {
      get
      {
        return this.mIndex;
      }
    }

    public GiftData GiftData
    {
      get
      {
        return this.mGiftData;
      }
    }

    public void SetItem(int index, GiftData gift)
    {
      if (index < 0 || gift == null)
        return;
      this.mIndex = index;
      this.mGiftData = gift;
      string name;
      int amount;
      gift.GetRewardNameAndAmount(out name, out amount);
      if (Object.op_Implicit((Object) this.TextRewardName))
        this.TextRewardName.set_text(name);
      if (Object.op_Implicit((Object) this.TextRewardNum))
      {
        if (gift.CheckGiftTypeIncluded(GiftTypes.Gold))
          this.TextRewardNum.set_text(string.Format("{0:#,0}", (object) amount));
        else
          this.TextRewardNum.set_text(amount.ToString());
      }
      if (Object.op_Implicit((Object) this.TextRewardConn))
        this.TextRewardConn.SetActive(true);
      if (!Object.op_Implicit((Object) this.RewardIconTemplate))
        return;
      ((GenesisRewardIcon) Object.Instantiate<GenesisRewardIcon>((M0) this.RewardIconTemplate, this.TrRewardIconParent)).Initialize(gift);
    }
  }
}
