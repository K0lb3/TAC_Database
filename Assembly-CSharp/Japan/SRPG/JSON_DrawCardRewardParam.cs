﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_DrawCardRewardParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_DrawCardRewardParam
  {
    public string iname;
    public JSON_DrawCardRewardParam.Data[] rewards;

    [MessagePackObject(true)]
    [Serializable]
    public class Data
    {
      public int weight;
      public int item_type;
      public string item_iname;
      public int item_num;
    }
  }
}
