﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_FilterArtifactParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_FilterArtifactParam
  {
    public string iname;
    public string tab_name;
    public string name;
    public int filter_type;
    public JSON_FilterArtifactParam.Condition[] cnds;

    [MessagePackObject(true)]
    [Serializable]
    public class Condition
    {
      public string cnds_name;
      public string name;
      public int rarity;
      public int equip_type;
      public string[] arms_type;
    }
  }
}
