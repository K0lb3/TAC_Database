﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FilterRuneConditionParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class FilterRuneConditionParam
  {
    public FilterRuneParam parent;
    public string cnds_iname;
    public string name;
    public byte rarity;
    public byte slot_index;
    public byte set_eff;
    public byte evo_status;

    public FilterRuneConditionParam(FilterRuneParam _parent)
    {
      this.parent = _parent;
    }

    public void Deserialize(JSON_FilterRuneConditionParam json)
    {
      this.cnds_iname = json.cnds_iname;
      this.name = json.name;
      this.rarity = (byte) json.rarity;
      this.slot_index = (byte) json.slot_index;
      this.set_eff = (byte) json.set_eff;
      this.evo_status = (byte) json.evo_status;
    }

    public string PrefsKey
    {
      get
      {
        return FilterUtility.FilterPrefs.MakeKey(this.parent.iname, this.cnds_iname);
      }
    }
  }
}
