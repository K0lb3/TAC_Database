﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_RuneDisassemblyOpen
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System.Collections.Generic;
using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("Rune/RuneDisassemblyOpen", 32741)]
  [FlowNode.Pin(10, "ルーン分解ウィンドウを開く", FlowNode.PinTypes.Input, 10)]
  [FlowNode.Pin(100, "output", FlowNode.PinTypes.Output, 100)]
  public class FlowNode_RuneDisassemblyOpen : FlowNode
  {
    private const int INPUT_RUNE_DISASSEMBLY_WINDOW_OPEN = 10;
    private const int PIN_OUTPUT = 100;

    public override void OnActivate(int pinID)
    {
      if (pinID == 10)
      {
        RuneDisassemblyWindow popupWindow = RuneManager.CreatePopupWindow<RuneDisassemblyWindow>("UI/Rune/RuneDisassemblyWindow");
        if (Object.op_Equality((Object) popupWindow, (Object) null))
          return;
        List<BindRuneData> rune_list = new List<BindRuneData>();
        foreach (KeyValuePair<long, RuneData> rune in MonoSingleton<GameManager>.Instance.Player.Runes)
          rune_list.Add(new BindRuneData((long) rune.Value.UniqueID)
          {
            is_owner_disable = true
          });
        popupWindow.Setup((RuneManager) null, rune_list);
      }
      this.ActivateOutputLinks(100);
    }
  }
}
