﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_ShowTips
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("Tips/ShowTips", 32741)]
  public class FlowNode_ShowTips : FlowNode_GUI
  {
    private const int PIN_ID_IN = 1;
    [SerializeField]
    private string Tips;

    protected override void OnCreatePinActive()
    {
      GlobalVars.SelectTips = this.Tips;
      base.OnCreatePinActive();
    }
  }
}
