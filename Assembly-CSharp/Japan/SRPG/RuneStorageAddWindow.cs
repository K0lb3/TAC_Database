﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RuneStorageAddWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class RuneStorageAddWindow : MonoBehaviour
  {
    [SerializeField]
    private Text m_BeforeStorageSize;
    [SerializeField]
    private Text m_AfterStorageSize;
    [SerializeField]
    private Text m_Message;

    public RuneStorageAddWindow()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      int currentRuneStorageSize = MonoSingleton<GameManager>.Instance.Player.CurrentRuneStorageSize;
      int num = currentRuneStorageSize + MonoSingleton<GameManager>.Instance.MasterParam.FixParam.RuneStorageExpansion;
      int storageExpansion = MonoSingleton<GameManager>.Instance.MasterParam.FixParam.RuneStorageExpansion;
      int runeStorageMax = MonoSingleton<GameManager>.Instance.MasterParam.FixParam.RuneStorageMax;
      int runeStorageCoinCost = MonoSingleton<GameManager>.Instance.MasterParam.FixParam.RuneStorageCoinCost;
      if (Object.op_Inequality((Object) this.m_Message, (Object) null))
        this.m_Message.set_text(LocalizedText.Get("sys.RUNE_STORAGE_ADD_CONFIRM_TEXT", (object) runeStorageCoinCost, (object) storageExpansion, (object) runeStorageMax));
      if (Object.op_Inequality((Object) this.m_BeforeStorageSize, (Object) null))
        this.m_BeforeStorageSize.set_text(currentRuneStorageSize.ToString());
      if (!Object.op_Inequality((Object) this.m_AfterStorageSize, (Object) null))
        return;
      this.m_AfterStorageSize.set_text(num.ToString());
    }
  }
}
