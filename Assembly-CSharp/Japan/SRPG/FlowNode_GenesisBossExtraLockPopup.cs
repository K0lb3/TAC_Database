﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_GenesisBossExtraLockPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("Genesis/ボスロックポップ", 32741)]
  [FlowNode.Pin(1, "In", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(11, "Out", FlowNode.PinTypes.Output, 11)]
  public class FlowNode_GenesisBossExtraLockPopup : FlowNode
  {
    private const int PIN_IN = 1;
    private const int PIN_OUT = 11;
    [SerializeField]
    private QuestDifficulties QuestDifficulty;
    [SerializeField]
    private bool IsUnlockCondDisp;
    private bool mIsRunning;

    protected override void Awake()
    {
      base.Awake();
      this.mIsRunning = false;
    }

    public override void OnActivate(int pinID)
    {
      if (pinID != 1 || this.mIsRunning)
        return;
      this.StartCoroutine(this.PopupMessageOpen());
    }

    [DebuggerHidden]
    private IEnumerator PopupMessageOpen()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new FlowNode_GenesisBossExtraLockPopup.\u003CPopupMessageOpen\u003Ec__Iterator0()
      {
        \u0024this = this
      };
    }
  }
}
