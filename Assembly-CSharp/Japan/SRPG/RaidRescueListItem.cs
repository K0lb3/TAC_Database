﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RaidRescueListItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class RaidRescueListItem : MonoBehaviour
  {
    private int mIndex;

    public RaidRescueListItem()
    {
      base.\u002Ector();
    }

    public int Index
    {
      get
      {
        return this.mIndex;
      }
    }

    public void Setup(int index, RaidRescueMember member)
    {
      this.mIndex = index;
      DataSource.Bind<RaidRescueMember>(((Component) this).get_gameObject(), member, false);
    }
  }
}
