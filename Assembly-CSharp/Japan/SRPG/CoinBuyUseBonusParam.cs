﻿// Decompiled with JetBrains decompiler
// Type: SRPG.CoinBuyUseBonusParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class CoinBuyUseBonusParam
  {
    private string iname;
    private eCoinBuyUseBonusType type;
    private eCoinBuyUseBonusTrigger trigger;
    private string reward_set;
    private DateTime begin_at;
    private DateTime end_at;
    private CoinBuyUseBonusRewardSetParam mRewardSet;

    public string Iname
    {
      get
      {
        return this.iname;
      }
    }

    public eCoinBuyUseBonusType Type
    {
      get
      {
        return this.type;
      }
    }

    public eCoinBuyUseBonusTrigger Trigger
    {
      get
      {
        return this.trigger;
      }
    }

    public DateTime BeginAt
    {
      get
      {
        return this.begin_at;
      }
    }

    public DateTime EndAt
    {
      get
      {
        return this.end_at;
      }
    }

    public CoinBuyUseBonusRewardSetParam RewardSet
    {
      get
      {
        return this.mRewardSet;
      }
    }

    public bool IsEnable
    {
      get
      {
        return this.begin_at <= TimeManager.ServerTime && TimeManager.ServerTime <= this.end_at;
      }
    }

    public void Deserialize(
      JSON_CoinBuyUseBonusParam json,
      CoinBuyUseBonusRewardSetParam[] reward_set_params)
    {
      this.iname = json.iname;
      this.type = (eCoinBuyUseBonusType) json.type;
      this.trigger = (eCoinBuyUseBonusTrigger) json.trigger;
      this.reward_set = json.reward_set;
      this.begin_at = DateTime.Parse(json.begin_at);
      this.end_at = DateTime.Parse(json.end_at);
      this.mRewardSet = Array.Find<CoinBuyUseBonusRewardSetParam>(reward_set_params, (Predicate<CoinBuyUseBonusRewardSetParam>) (param => param.Iname == this.reward_set));
    }

    public static void Deserialize(
      ref CoinBuyUseBonusParam[] param,
      JSON_CoinBuyUseBonusParam[] json,
      CoinBuyUseBonusRewardSetParam[] reward_set_params)
    {
      if (json == null)
        return;
      param = new CoinBuyUseBonusParam[json.Length];
      for (int index = 0; index < json.Length; ++index)
      {
        CoinBuyUseBonusParam buyUseBonusParam = new CoinBuyUseBonusParam();
        buyUseBonusParam.Deserialize(json[index], reward_set_params);
        param[index] = buyUseBonusParam;
      }
    }
  }
}
