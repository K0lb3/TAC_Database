﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_ClearMasterParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;

namespace SRPG
{
  [FlowNode.NodeType("Master/ClearMasterParam", 32741)]
  [FlowNode.Pin(101, "Start", FlowNode.PinTypes.Input, 101)]
  [FlowNode.Pin(1001, "End", FlowNode.PinTypes.Output, 1001)]
  public class FlowNode_ClearMasterParam : FlowNode
  {
    private const int PIN_INPUT_START = 101;
    private const int PIN_OUTPUT_END = 1001;

    public override void OnActivate(int pinID)
    {
      if (pinID != 101)
        return;
      this.Clear();
      this.ActivateOutputLinks(1001);
    }

    private void Clear()
    {
      MonoSingleton<GameManager>.Instance.MasterParam = new MasterParam();
      CharacterDB.UnloadAll();
      SkillSequence.UnloadAll();
    }
  }
}
