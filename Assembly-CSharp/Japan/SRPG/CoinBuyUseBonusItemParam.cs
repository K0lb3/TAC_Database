﻿// Decompiled with JetBrains decompiler
// Type: SRPG.CoinBuyUseBonusItemParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class CoinBuyUseBonusItemParam
  {
    private eCoinBuyUseBonusRewardType type;
    private string iname;
    private int num;

    public eCoinBuyUseBonusRewardType Type
    {
      get
      {
        return this.type;
      }
    }

    public string Item
    {
      get
      {
        return this.iname;
      }
    }

    public int Num
    {
      get
      {
        return this.num;
      }
    }

    public void Deserialize(JSON_CoinBuyUseBonusItemParam json)
    {
      this.type = (eCoinBuyUseBonusRewardType) json.type;
      this.iname = json.iname;
      this.num = json.num;
    }
  }
}
