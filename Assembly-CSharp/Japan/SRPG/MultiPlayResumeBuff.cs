﻿// Decompiled with JetBrains decompiler
// Type: SRPG.MultiPlayResumeBuff
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;
using System.Collections.Generic;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class MultiPlayResumeBuff
  {
    public List<int> atl = new List<int>();
    public List<MultiPlayResumeBuff.ResistStatus> rsl = new List<MultiPlayResumeBuff.ResistStatus>();
    public string iname;
    public int turn;
    public int unitindex;
    public int checkunit;
    public int timing;
    public bool passive;
    public int condition;
    public int type;
    public int vtp;
    public int calc;
    public int curse;
    public int skilltarget;
    public string bc_id;
    public uint lid;
    public int ubc;

    [MessagePackObject(true)]
    [Serializable]
    public class ResistStatus
    {
      public int rst;
      public int rsv;
    }
  }
}
