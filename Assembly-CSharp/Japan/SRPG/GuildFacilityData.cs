﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildFacilityData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System;
using System.Collections.Generic;

namespace SRPG
{
  public class GuildFacilityData
  {
    private long mGid;
    private string mIname;
    private long mExp;
    private int mLevel;
    private int mInvestPoint;
    private GuildFacilityParam mParam;

    public long Gid
    {
      get
      {
        return this.mGid;
      }
    }

    public string Iname
    {
      get
      {
        return this.mIname;
      }
    }

    public long Exp
    {
      get
      {
        return this.mExp;
      }
    }

    public int Level
    {
      get
      {
        return this.mLevel;
      }
    }

    public int InvestPoint
    {
      get
      {
        return this.mInvestPoint;
      }
    }

    public GuildFacilityParam Param
    {
      get
      {
        return this.mParam;
      }
    }

    public bool Deserialize(JSON_GuildFacilityData json)
    {
      this.mGid = json.gid;
      this.mIname = json.facility_iname;
      this.mExp = json.exp;
      this.mLevel = json.level;
      this.mInvestPoint = json.invest_point;
      this.mParam = MonoSingleton<GameManager>.Instance.MasterParam.GetGuildFacility(this.mIname);
      return true;
    }

    public GuildFacilityEffectParam GetEffect()
    {
      return this.mParam.GetEffect(this.mLevel);
    }

    public static long GetNeedExp(int from_lv, int to_lv, GuildFacilityParam.eFacilityType type)
    {
      GuildFacilityLvParam[] facilityLevelTable = MonoSingleton<GameManager>.Instance.MasterParam.GetGuildFacilityLevelTable();
      long num = 0;
      switch (type)
      {
        case GuildFacilityParam.eFacilityType.BASE_CAMP:
          for (int index = from_lv; index < facilityLevelTable.Length && index < to_lv; ++index)
            num += (long) facilityLevelTable[index].base_camp;
          break;
        case GuildFacilityParam.eFacilityType.GUILD_SHOP:
          for (int index = from_lv; index < facilityLevelTable.Length && index < to_lv; ++index)
            num += facilityLevelTable[index].guild_shop;
          break;
      }
      return num;
    }

    public static int GetLevelMax(GuildFacilityParam.eFacilityType type)
    {
      GuildFacilityLvParam[] facilityLevelTable = MonoSingleton<GameManager>.Instance.MasterParam.GetGuildFacilityLevelTable();
      int num = 0;
      switch (type)
      {
        case GuildFacilityParam.eFacilityType.BASE_CAMP:
          for (int index = 0; index < facilityLevelTable.Length && (index == 0 || facilityLevelTable[index].base_camp > 0); ++index)
            ++num;
          break;
        case GuildFacilityParam.eFacilityType.GUILD_SHOP:
          for (int index = 0; index < facilityLevelTable.Length && (index == 0 || facilityLevelTable[index].guild_shop > 0L); ++index)
            ++num;
          break;
      }
      return num;
    }

    public static void SimlateEnhance(
      GuildFacilityData facility,
      long add_exp,
      out int new_level,
      out long rest_next_exp)
    {
      long num1 = facility.Exp + add_exp;
      int levelMax = GuildFacilityData.GetLevelMax(facility.Param.Type);
      GuildFacilityLvParam[] facilityLevelTable = MonoSingleton<GameManager>.Instance.MasterParam.GetGuildFacilityLevelTable();
      List<long> longList = new List<long>();
      switch (facility.Param.Type)
      {
        case GuildFacilityParam.eFacilityType.BASE_CAMP:
          for (int index = 0; index < facilityLevelTable.Length && index < levelMax; ++index)
            longList.Add((long) facilityLevelTable[index].base_camp);
          break;
        case GuildFacilityParam.eFacilityType.GUILD_SHOP:
          for (int index = 0; index < facilityLevelTable.Length && index < levelMax; ++index)
            longList.Add(facilityLevelTable[index].guild_shop);
          break;
      }
      new_level = 0;
      rest_next_exp = 0L;
      if (longList.Count <= 0)
        return;
      int num2 = 0;
      for (int index = 0; index < longList.Count; ++index)
      {
        num1 -= longList[index];
        if (num1 >= 0L)
          ++num2;
        else
          break;
      }
      new_level = num2;
      rest_next_exp = num1 <= 0L ? Math.Abs(num1) : 0L;
    }
  }
}
