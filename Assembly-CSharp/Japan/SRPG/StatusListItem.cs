﻿// Decompiled with JetBrains decompiler
// Type: SRPG.StatusListItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class StatusListItem : MonoBehaviour
  {
    public Text Label;
    public Text Value;
    public Text Bonus;

    public StatusListItem()
    {
      base.\u002Ector();
    }
  }
}
