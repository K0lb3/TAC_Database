﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GenericSlotFlags
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  [DisallowMultipleComponent]
  public class GenericSlotFlags : MonoBehaviour
  {
    [BitMask]
    public GenericSlotFlags.VisibleFlags Flags;

    public GenericSlotFlags()
    {
      base.\u002Ector();
    }

    [System.Flags]
    public enum VisibleFlags
    {
      Empty = 1,
      NonEmpty = 2,
      Locked = 4,
    }
  }
}
