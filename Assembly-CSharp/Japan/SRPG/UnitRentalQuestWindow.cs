﻿// Decompiled with JetBrains decompiler
// Type: SRPG.UnitRentalQuestWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System.Collections.Generic;
using UnityEngine;

namespace SRPG
{
  [FlowNode.Pin(10, "リスト切り替え", FlowNode.PinTypes.Input, 10)]
  [FlowNode.Pin(100, "クエストが選択された", FlowNode.PinTypes.Output, 100)]
  public class UnitRentalQuestWindow : MonoBehaviour, IFlowInterface
  {
    public Transform QuestList;
    public GameObject QuestItemTemplate;
    public GameObject QuestDisableItemTemplate;
    public GameObject QuestDetailTemplate;
    public string DisableFlagName;
    public GameObject CharacterImage;
    private List<QuestParam> mQuestList;
    private List<GameObject> mQuestListItems;
    private GameObject mQuestDetail;
    private bool mListRefreshing;
    private bool mIsRestore;
    private UnitData CurrentUnit;
    private UnitRentalParam RentalParam;

    public UnitRentalQuestWindow()
    {
      base.\u002Ector();
    }

    public bool IsRestore
    {
      get
      {
        return this.mIsRestore;
      }
      set
      {
        this.mIsRestore = value;
      }
    }

    private void Start()
    {
      this.CurrentUnit = MonoSingleton<GameManager>.Instance.Player.GetRentalUnit();
      if (this.CurrentUnit == null)
        return;
      this.RentalParam = UnitRentalParam.GetParam(this.CurrentUnit.RentalIname);
      if (this.RentalParam == null)
        return;
      this.RefreshQuestList();
      GameParameter.UpdateAll(((Component) this).get_gameObject());
    }

    private void CreateQuestList()
    {
      this.mQuestList.Clear();
      List<RentalQuestInfo> unitQuestInfo = this.RentalParam.UnitQuestInfo;
      GameManager instance = MonoSingleton<GameManager>.Instance;
      foreach (RentalQuestInfo data in unitQuestInfo)
      {
        QuestParam quest = instance.FindQuest(data.QuestId);
        this.mQuestList.Add(quest);
        GameObject gameObject = (int) data.Point > this.CurrentUnit.RentalFavoritePoint ? (GameObject) Object.Instantiate<GameObject>((M0) this.QuestDisableItemTemplate) : (GameObject) Object.Instantiate<GameObject>((M0) this.QuestItemTemplate);
        if (!Object.op_Equality((Object) gameObject, (Object) null))
        {
          gameObject.SetActive(true);
          gameObject.get_transform().SetParent(this.QuestList, false);
          DataSource.Bind<QuestParam>(gameObject, quest, false);
          DataSource.Bind<UnitData>(gameObject, this.CurrentUnit, false);
          DataSource.Bind<RentalQuestInfo>(gameObject, data, false);
          DataSource.Bind<UnitRentalParam>(gameObject, this.RentalParam, false);
          DataSource.Bind<UnitParam>(gameObject, this.CurrentUnit.UnitParam, false);
          ListItemEvents component = (ListItemEvents) gameObject.GetComponent<ListItemEvents>();
          component.OnSelect = new ListItemEvents.ListItemEvent(this.OnQuestSelect);
          component.OnOpenDetail = new ListItemEvents.ListItemEvent(this.OnOpenItemDetail);
          component.OnCloseDetail = new ListItemEvents.ListItemEvent(this.OnCloseItemDetail);
          this.mQuestListItems.Add(gameObject);
        }
      }
    }

    private void RefreshQuestList()
    {
      if (this.mListRefreshing || Object.op_Equality((Object) this.QuestList, (Object) null))
        return;
      this.mListRefreshing = true;
      if (this.mQuestListItems.Count <= 0)
        this.CreateQuestList();
      GameManager instance = MonoSingleton<GameManager>.Instance;
      DataSource.Bind<UnitData>(((Component) this).get_gameObject(), this.CurrentUnit, false);
      this.mListRefreshing = false;
    }

    private void OnQuestSelect(GameObject button)
    {
      QuestParam dataOfClass1 = DataSource.FindDataOfClass<QuestParam>(button.get_gameObject(), (QuestParam) null);
      RentalQuestInfo dataOfClass2 = DataSource.FindDataOfClass<RentalQuestInfo>(button.get_gameObject(), (RentalQuestInfo) null);
      if (dataOfClass1 == null || this.RentalParam == null)
        return;
      if ((int) dataOfClass2.Point > this.CurrentUnit.RentalFavoritePoint)
      {
        UIUtility.NegativeSystemMessage((string) null, string.Format(LocalizedText.Get("sys.UR_QUEST_LOCKED_MSG"), (object) this.CurrentUnit.UnitParam.name, (object) (int) ((double) ((int) dataOfClass2.Point * 100) / (double) (int) this.RentalParam.PtMax)), (UIUtility.DialogResultEvent) null, (GameObject) null, false, -1);
      }
      else
      {
        GlobalVars.SelectedQuestID = dataOfClass1.iname;
        FlowNode_GameObject.ActivateOutputLinks((Component) this, 100);
      }
    }

    private void OnCloseItemDetail(GameObject go)
    {
      if (!Object.op_Inequality((Object) this.mQuestDetail, (Object) null))
        return;
      Object.DestroyImmediate((Object) this.mQuestDetail.get_gameObject());
      this.mQuestDetail = (GameObject) null;
    }

    private void OnOpenItemDetail(GameObject go)
    {
      GameManager instance = MonoSingleton<GameManager>.Instance;
      QuestParam dataOfClass = DataSource.FindDataOfClass<QuestParam>(go, (QuestParam) null);
      if (!Object.op_Equality((Object) this.mQuestDetail, (Object) null) || dataOfClass == null)
        return;
      this.mQuestDetail = (GameObject) Object.Instantiate<GameObject>((M0) this.QuestDetailTemplate);
      DataSource.Bind<QuestParam>(this.mQuestDetail, dataOfClass, false);
      DataSource.Bind<UnitData>(this.mQuestDetail, this.CurrentUnit, false);
      this.mQuestDetail.SetActive(true);
    }

    public void Activated(int pinID)
    {
      if (pinID != 10)
        return;
      this.RefreshQuestList();
    }
  }
}
