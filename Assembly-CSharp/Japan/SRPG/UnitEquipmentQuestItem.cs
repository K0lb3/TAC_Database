﻿// Decompiled with JetBrains decompiler
// Type: SRPG.UnitEquipmentQuestItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class UnitEquipmentQuestItem : MonoBehaviour
  {
    public GameObject QuestButton;
    public GameObject QuestLockButton;
    public GameObject QuestDetail;
    public GameObject QuestDetailMask;

    public UnitEquipmentQuestItem()
    {
      base.\u002Ector();
    }
  }
}
