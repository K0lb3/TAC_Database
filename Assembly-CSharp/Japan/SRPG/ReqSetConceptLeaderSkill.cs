﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqSetConceptLeaderSkill
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Text;

namespace SRPG
{
  public class ReqSetConceptLeaderSkill : WebAPI
  {
    public ReqSetConceptLeaderSkill(long unit_iid, bool set, Network.ResponseCallback response)
    {
      this.name = "unit/concept/leaderskill/set";
      StringBuilder stringBuilder = WebAPI.GetStringBuilder();
      stringBuilder.Append("\"unit_iid\":");
      stringBuilder.Append(unit_iid);
      stringBuilder.Append(",");
      stringBuilder.Append("\"is_set\":");
      stringBuilder.Append(!set ? 0 : 1);
      this.body = WebAPI.GetRequestString(stringBuilder.ToString());
      this.callback = response;
    }
  }
}
