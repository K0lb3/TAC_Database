﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_QuestArchiveOpenResponse
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class JSON_QuestArchiveOpenResponse
  {
    public Json_PlayerData player;
    public Json_Item[] items;
    public JSON_OpenedQuestArchive archive;
    public int free_end;
  }
}
