﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_ChatStampParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class JSON_ChatStampParam
  {
    public int pk;
    public JSON_ChatStampParam.Fields fields;

    public class Fields
    {
      public int id;
      public string img_id;
      public string iname;
      public int is_private;
    }
  }
}
