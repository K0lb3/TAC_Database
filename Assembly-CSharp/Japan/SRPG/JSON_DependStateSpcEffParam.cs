﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_DependStateSpcEffParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_DependStateSpcEffParam
  {
    public string iname;
    public int is_and;
    public int[] buff_ids;
    public int[] buff_types;
    public int[] cond_ids;
    public int inv_tkrate;
    public int is_inv_t_buff;
    public int is_inv_t_cond;
    public int is_inv_s_buff;
    public int is_inv_s_cond;
  }
}
