﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ConditionsResult_Unit
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public abstract class ConditionsResult_Unit : ConditionsResult
  {
    private UnitData mUnitData;
    private UnitParam mUnitParam;

    public ConditionsResult_Unit(UnitData unitData, UnitParam unitParam)
    {
      this.mUnitData = unitData;
      this.mUnitParam = unitParam;
    }

    public UnitData unitData
    {
      get
      {
        return this.mUnitData;
      }
    }

    public bool hasUnitData
    {
      get
      {
        return this.mUnitData != null;
      }
    }

    public string unitName
    {
      get
      {
        if (this.mUnitData != null)
          return this.mUnitData.UnitParam.name;
        return this.mUnitParam != null ? this.mUnitParam.name : string.Empty;
      }
    }
  }
}
