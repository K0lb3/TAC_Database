﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GalleryHighlightGiftWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System.Collections.Generic;
using UnityEngine;

namespace SRPG
{
  [FlowNode.Pin(0, "Screen Tap", FlowNode.PinTypes.Input, 0)]
  [FlowNode.Pin(10, "Wait Until Show Buttons", FlowNode.PinTypes.Output, 10)]
  public class GalleryHighlightGiftWindow : MonoBehaviour, IFlowInterface
  {
    private const int PIN_IN_SCREEN_TAP = 0;
    private const int PIN_OUT_WAIT_UNTIL_SHOW_BUTTONS = 10;
    [SerializeField]
    private GameObject ExitButton;
    [SerializeField]
    private GameObject ReplayButton;
    [SerializeField]
    private CanvasGroup TopCanvas;
    [SerializeField]
    private GameObject ItemHolder;
    [SerializeField]
    private GameObject ItemTemplate;
    [SerializeField]
    private GameObject GoldTemplate;
    [SerializeField]
    private GameObject CoinTemplate;
    [SerializeField]
    private GameObject AwardTemplate;
    [SerializeField]
    private GameObject ConceptCardTemplate;
    [SerializeField]
    private GameObject UnitTemplate;
    [SerializeField]
    private GameObject ArtifactTemplate;
    private List<GameObject> mItems;

    public GalleryHighlightGiftWindow()
    {
      base.\u002Ector();
    }

    public void Activated(int pinID)
    {
    }

    private void Awake()
    {
      if (Object.op_Inequality((Object) this.ExitButton, (Object) null))
        this.ExitButton.SetActive(false);
      if (Object.op_Inequality((Object) this.ReplayButton, (Object) null))
        this.ReplayButton.SetActive(false);
      if (Object.op_Inequality((Object) this.ItemTemplate, (Object) null))
        this.ItemTemplate.SetActive(false);
      if (Object.op_Inequality((Object) this.GoldTemplate, (Object) null))
        this.GoldTemplate.SetActive(false);
      if (Object.op_Inequality((Object) this.CoinTemplate, (Object) null))
        this.CoinTemplate.SetActive(false);
      if (Object.op_Inequality((Object) this.AwardTemplate, (Object) null))
        this.AwardTemplate.SetActive(false);
      if (Object.op_Inequality((Object) this.ConceptCardTemplate, (Object) null))
        this.ConceptCardTemplate.SetActive(false);
      if (Object.op_Inequality((Object) this.UnitTemplate, (Object) null))
        this.UnitTemplate.SetActive(false);
      if (!Object.op_Inequality((Object) this.ArtifactTemplate, (Object) null))
        return;
      this.ArtifactTemplate.SetActive(false);
    }

    public void Refresh(HighlightParam param, bool isReward)
    {
      if (this.mItems != null)
      {
        using (List<GameObject>.Enumerator enumerator = this.mItems.GetEnumerator())
        {
          while (enumerator.MoveNext())
            Object.Destroy((Object) enumerator.Current);
        }
        this.mItems.Clear();
      }
      if (Object.op_Inequality((Object) this.ExitButton, (Object) null))
        this.ExitButton.SetActive(false);
      if (Object.op_Inequality((Object) this.ReplayButton, (Object) null))
        this.ReplayButton.SetActive(false);
      HighlightGift gift1 = param.gift;
      if (gift1 != null && gift1.gifts != null && gift1.gifts.Length > 0)
      {
        foreach (HighlightGiftData gift2 in gift1.gifts)
        {
          GameObject gameObject = (GameObject) null;
          switch (gift2.type)
          {
            case HighlightGiftType.Item:
              gameObject = this.CreateItem(gift2.item, gift2.num);
              break;
            case HighlightGiftType.Gold:
              gameObject = this.CreateGoldItem(gift2.num);
              break;
            case HighlightGiftType.Coin:
              gameObject = this.CreateCoinItem(gift2.num);
              break;
            case HighlightGiftType.Award:
              gameObject = this.CreateAwardItem(gift2.item, gift2.num);
              break;
            case HighlightGiftType.Unit:
              gameObject = this.CreateUnit(gift2.item, gift2.num);
              break;
            case HighlightGiftType.ConceptCard:
              gameObject = this.CreateConceptCardItem(gift2.item, gift2.num);
              break;
            case HighlightGiftType.Artifact:
              gameObject = this.CreateArtifact(gift2.item, gift2.num);
              break;
          }
          if (!Object.op_Equality((Object) gameObject, (Object) null) && !Object.op_Equality((Object) this.ItemHolder, (Object) null))
          {
            HighlightGiftIcon component = (HighlightGiftIcon) gameObject.GetComponent<HighlightGiftIcon>();
            if (Object.op_Inequality((Object) component, (Object) null))
            {
              component.Initialize();
              if (!isReward)
                component.SetStamp(false);
            }
            this.mItems.Add(gameObject);
            gameObject.get_transform().SetParent(this.ItemHolder.get_transform(), false);
            gameObject.SetActive(true);
          }
        }
        GameParameter.UpdateAll(((Component) this).get_gameObject());
      }
      FlowNode_GameObject.ActivateOutputLinks((Component) this, 10);
    }

    private GameObject CreateItem(string iname, int num)
    {
      GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.ItemTemplate);
      ItemData data = new ItemData();
      data.Setup(0L, iname, num);
      DataSource.Bind<ItemData>(gameObject, data, false);
      return gameObject;
    }

    private GameObject CreateCoinItem(int num)
    {
      GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.CoinTemplate);
      DataSource.Bind<int>(gameObject, num, false);
      return gameObject;
    }

    private GameObject CreateGoldItem(int num)
    {
      GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.GoldTemplate);
      DataSource.Bind<int>(gameObject, num, false);
      return gameObject;
    }

    private GameObject CreateAwardItem(string iname, int num)
    {
      AwardParam awardParam = MonoSingleton<GameManager>.Instance.GetAwardParam(iname);
      if (awardParam == null)
        return (GameObject) null;
      GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.AwardTemplate);
      ItemData data = new ItemData();
      data.Setup(0L, awardParam.ToItemParam(), num);
      DataSource.Bind<ItemData>(gameObject, data, false);
      return gameObject;
    }

    private GameObject CreateConceptCardItem(string iname, int num)
    {
      GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.ConceptCardTemplate);
      QuestResult.DropItemData data = new QuestResult.DropItemData();
      data.SetupDropItemData(EBattleRewardType.ConceptCard, 0L, iname, num);
      DataSource.Bind<QuestResult.DropItemData>(gameObject, data, false);
      return gameObject;
    }

    private GameObject CreateUnit(string iname, int num)
    {
      GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.UnitTemplate);
      ItemData data = new ItemData();
      data.Setup(0L, iname, num);
      DataSource.Bind<ItemData>(gameObject, data, false);
      return gameObject;
    }

    private GameObject CreateArtifact(string iname, int num)
    {
      ArtifactParam artifactParam = MonoSingleton<GameManager>.Instance.MasterParam.GetArtifactParam(iname);
      if (artifactParam == null)
        return (GameObject) null;
      GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.ArtifactTemplate);
      DataSource.Bind<ArtifactParam>(gameObject, artifactParam, false);
      DataSource.Bind<int>(gameObject, num, false);
      return gameObject;
    }
  }
}
