﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RuneParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using MessagePack;
using System;
using UnityEngine;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class RuneParam
  {
    public string iname;
    public string item_iname;
    public RuneSlotIndex slot_index;
    public int seteff_type;

    [IgnoreMember]
    public int SetEffTypeIconIndex
    {
      get
      {
        return Mathf.Max(0, this.seteff_type - 1);
      }
    }

    public bool Deserialize(JSON_RuneParam json)
    {
      this.iname = json.iname;
      this.item_iname = json.item_iname;
      this.slot_index = (RuneSlotIndex) (byte) (json.slot - 1);
      this.seteff_type = json.seteff_type;
      return true;
    }

    public ItemParam ItemParam
    {
      get
      {
        return MonoSingleton<GameManager>.Instance.GetItemParam(this.item_iname);
      }
    }

    public RuneSetEff RuneSetEff
    {
      get
      {
        return MonoSingleton<GameManager>.Instance.MasterParam.GetRuneSetEff(this.seteff_type);
      }
    }
  }
}
