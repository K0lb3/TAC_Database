﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AspectRatioScaleFitter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  [ExecuteInEditMode]
  public class AspectRatioScaleFitter : MonoBehaviour
  {
    [SerializeField]
    private Vector2 Padding;
    [SerializeField]
    private float ScaleForSafeArea;
    private Rect lastSafeArea;
    private Vector3 initScale;
    private float lastSetScale;
    private int lastScreenWidth;
    private int lastScreenHeight;

    public AspectRatioScaleFitter()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      RectTransform component = (RectTransform) ((Component) this).GetComponent<RectTransform>();
      if (!Object.op_Inequality((Object) component, (Object) null))
        return;
      this.initScale = ((Component) component).get_transform().get_localScale();
    }

    private void OnRectTransformDimensionsChange()
    {
      this.Refresh(SetCanvasBounds.GetSafeArea(true));
    }

    private void Refresh(Rect safeArea)
    {
      this.lastScreenWidth = Screen.get_width();
      this.lastScreenHeight = Screen.get_height();
      if (this.ApplySafeAreaScale(safeArea))
        return;
      this.Rescale();
    }

    private void Rescale()
    {
      RectTransform component = (RectTransform) ((Component) this).GetComponent<RectTransform>();
      Transform parent = ((Component) this).get_transform().get_parent();
      if (Object.op_Equality((Object) parent, (Object) null))
        return;
      Rect rect1 = (parent as RectTransform).get_rect();
      float width = ((Rect) ref rect1).get_width();
      Rect rect2 = component.get_rect();
      float num1 = (float) ((double) ((Rect) ref rect2).get_width() + this.Padding.x + this.Padding.y);
      float num2 = width / num1;
      ((Transform) component).set_localScale(new Vector3(num2, num2, 1f));
    }

    private bool ApplySafeAreaScale(Rect area)
    {
      RectTransform component = (RectTransform) ((Component) this).GetComponent<RectTransform>();
      bool flag = false;
      if (Object.op_Inequality((Object) component, (Object) null))
      {
        if ((double) (((Rect) ref area).get_width() / (float) Screen.get_width()) < 1.0)
        {
          flag = true;
          ((Transform) component).set_localScale(new Vector3(this.ScaleForSafeArea, this.ScaleForSafeArea, this.ScaleForSafeArea));
        }
        else
          ((Transform) component).set_localScale(this.initScale);
      }
      this.lastSafeArea = area;
      this.lastSetScale = this.ScaleForSafeArea;
      return flag;
    }
  }
}
