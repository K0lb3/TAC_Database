﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_ConceptCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_ConceptCard
  {
    public long iid;
    public string iname;
    public int exp;
    public int trust;
    public int fav;
    public int trust_bonus;
    public int plus;
    public int leaderskill;

    [IgnoreMember]
    public bool IsEmptyDummyData
    {
      get
      {
        return this.iid == 0L;
      }
    }

    [IgnoreMember]
    public bool EnableLeaderSkill
    {
      get
      {
        return this.leaderskill == 1;
      }
    }
  }
}
