﻿// Decompiled with JetBrains decompiler
// Type: SRPG.SkillPowerUpResultContentStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class SkillPowerUpResultContentStatus : MonoBehaviour
  {
    [SerializeField]
    private Text paramNameText;
    [SerializeField]
    private Text prevParamText;
    [SerializeField]
    private Text resultParamText;
    [SerializeField]
    private Text resultAddedParamText;

    public SkillPowerUpResultContentStatus()
    {
      base.\u002Ector();
    }

    public void SetData(SkillPowerUpResultContent.Param param, ParamTypes type, bool isScale)
    {
      string str = !isScale ? string.Empty : "%";
      int num1 = 0;
      int num2 = 0;
      int num3 = 0;
      int num4 = 0;
      switch (type)
      {
        case ParamTypes.Tokkou:
          this.paramNameText.set_text(string.Empty);
          this.prevParamText.set_text(string.Empty);
          this.resultParamText.set_text(string.Empty);
          this.resultAddedParamText.set_text(string.Empty);
          int count1 = param.currentParam.tokkou.Count;
          if (isScale)
            count1 = param.currentParamMul.tokkou.Count;
          for (int index = 0; index < count1; ++index)
          {
            if (isScale)
            {
              if (param.currentParamMul.tokkou.Count != 0)
                num1 = (int) param.currentParamMul.tokkou[index].value;
              if (param.prevParamMul.tokkou.Count != 0)
                num2 = (int) param.prevParamMul.tokkou[index].value;
              if (param.currentParamBonusMul.tokkou.Count != 0)
                num3 = (int) param.currentParamBonusMul.tokkou[index].value;
              if (param.prevParamBonusMul.tokkou.Count != 0)
                num4 = (int) param.prevParamBonusMul.tokkou[index].value;
            }
            else
            {
              if (param.currentParam.tokkou.Count != 0)
                num1 = (int) param.currentParam.tokkou[index].value;
              if (param.prevParam.tokkou.Count != 0)
                num2 = (int) param.prevParam.tokkou[index].value;
              if (param.currentParamBonus.tokkou.Count != 0)
                num3 = (int) param.currentParamBonus.tokkou[index].value;
              if (param.prevParamBonus.tokkou.Count != 0)
                num4 = (int) param.prevParamBonus.tokkou[index].value;
            }
            int num5 = num2 + num4;
            int num6 = num1 + num3;
            if (index != 0)
            {
              Text paramNameText = this.paramNameText;
              paramNameText.set_text(paramNameText.get_text() + "\n");
              Text prevParamText = this.prevParamText;
              prevParamText.set_text(prevParamText.get_text() + "\n");
              Text resultParamText = this.resultParamText;
              resultParamText.set_text(resultParamText.get_text() + "\n");
              Text resultAddedParamText = this.resultAddedParamText;
              resultAddedParamText.set_text(resultAddedParamText.get_text() + "\n");
            }
            Text paramNameText1 = this.paramNameText;
            paramNameText1.set_text(paramNameText1.get_text() + string.Format(LocalizedText.Get("sys." + (object) type), (object) param.currentParam.tokkou[index].tag));
            Text prevParamText1 = this.prevParamText;
            prevParamText1.set_text(prevParamText1.get_text() + num5.ToString() + str);
            Text resultParamText1 = this.resultParamText;
            resultParamText1.set_text(resultParamText1.get_text() + num6.ToString() + str);
            int num7 = num3;
            if (num7 >= 0)
            {
              Text resultAddedParamText = this.resultAddedParamText;
              resultAddedParamText.set_text(resultAddedParamText.get_text() + "(+" + (object) num7 + str + ")");
            }
            else
            {
              Text resultAddedParamText = this.resultAddedParamText;
              resultAddedParamText.set_text(resultAddedParamText.get_text() + "(" + (object) num7 + str + ")");
            }
          }
          break;
        case ParamTypes.Tokubou:
          this.paramNameText.set_text(string.Empty);
          this.prevParamText.set_text(string.Empty);
          this.resultParamText.set_text(string.Empty);
          this.resultAddedParamText.set_text(string.Empty);
          int count2 = param.currentParam.tokubou.Count;
          if (isScale)
            count2 = param.currentParamMul.tokubou.Count;
          for (int index = 0; index < count2; ++index)
          {
            if (isScale)
            {
              if (param.currentParamMul.tokubou.Count != 0)
                num1 = (int) param.currentParamMul.tokubou[index].value;
              if (param.prevParamMul.tokubou.Count != 0)
                num2 = (int) param.prevParamMul.tokubou[index].value;
              if (param.currentParamBonusMul.tokubou.Count != 0)
                num3 = (int) param.currentParamBonusMul.tokubou[index].value;
              if (param.prevParamBonusMul.tokubou.Count != 0)
                num4 = (int) param.prevParamBonusMul.tokubou[index].value;
            }
            else
            {
              if (param.currentParam.tokubou.Count != 0)
                num1 = (int) param.currentParam.tokubou[index].value;
              if (param.prevParam.tokubou.Count != 0)
                num2 = (int) param.prevParam.tokubou[index].value;
              if (param.currentParamBonus.tokubou.Count != 0)
                num3 = (int) param.currentParamBonus.tokubou[index].value;
              if (param.prevParamBonus.tokubou.Count != 0)
                num4 = (int) param.prevParamBonus.tokubou[index].value;
            }
            int num5 = num2 + num4;
            int num6 = num1 + num3;
            if (index != 0)
            {
              Text paramNameText = this.paramNameText;
              paramNameText.set_text(paramNameText.get_text() + "\n");
              Text prevParamText = this.prevParamText;
              prevParamText.set_text(prevParamText.get_text() + "\n");
              Text resultParamText = this.resultParamText;
              resultParamText.set_text(resultParamText.get_text() + "\n");
              Text resultAddedParamText = this.resultAddedParamText;
              resultAddedParamText.set_text(resultAddedParamText.get_text() + "\n");
            }
            Text paramNameText1 = this.paramNameText;
            paramNameText1.set_text(paramNameText1.get_text() + string.Format(LocalizedText.Get("sys." + (object) type), (object) param.currentParam.tokubou[index].tag));
            Text prevParamText1 = this.prevParamText;
            prevParamText1.set_text(prevParamText1.get_text() + num5.ToString() + str);
            Text resultParamText1 = this.resultParamText;
            resultParamText1.set_text(resultParamText1.get_text() + num6.ToString() + str);
            int num7 = num3;
            if (num7 >= 0)
            {
              Text resultAddedParamText = this.resultAddedParamText;
              resultAddedParamText.set_text(resultAddedParamText.get_text() + "(+" + (object) num7 + str + ")");
            }
            else
            {
              Text resultAddedParamText = this.resultAddedParamText;
              resultAddedParamText.set_text(resultAddedParamText.get_text() + "(" + (object) num7 + str + ")");
            }
          }
          break;
        default:
          int num8 = !isScale ? param.currentParam[type] : param.currentParamMul[type];
          int num9 = !isScale ? param.prevParam[type] : param.prevParamMul[type];
          int num10 = !isScale ? param.currentParamBonus[type] : param.currentParamBonusMul[type];
          int num11 = !isScale ? param.prevParamBonus[type] : param.prevParamBonusMul[type];
          int num12 = num9 + num11;
          int num13 = num8 + num10;
          this.paramNameText.set_text(LocalizedText.Get("sys." + (object) type));
          this.prevParamText.set_text(num12.ToString() + str);
          this.resultParamText.set_text(num13.ToString() + str);
          int num14 = num10;
          if (num14 >= 0)
          {
            this.resultAddedParamText.set_text("(+" + (object) num14 + str + ")");
            break;
          }
          this.resultAddedParamText.set_text("(" + (object) num14 + str + ")");
          break;
      }
    }
  }
}
