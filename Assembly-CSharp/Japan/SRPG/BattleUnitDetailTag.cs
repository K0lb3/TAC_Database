﻿// Decompiled with JetBrains decompiler
// Type: SRPG.BattleUnitDetailTag
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class BattleUnitDetailTag : MonoBehaviour
  {
    public Text TextValue;

    public BattleUnitDetailTag()
    {
      base.\u002Ector();
    }

    public void SetTag(string tag)
    {
      if (tag == null)
        tag = string.Empty;
      if (!Object.op_Implicit((Object) this.TextValue))
        return;
      this.TextValue.set_text(tag);
    }
  }
}
