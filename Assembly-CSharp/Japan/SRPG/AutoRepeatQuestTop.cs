﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AutoRepeatQuestTop
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System.Collections.Generic;
using UnityEngine;

namespace SRPG
{
  [FlowNode.Pin(20, "初期化", FlowNode.PinTypes.Input, 20)]
  [FlowNode.Pin(110, "初期化完了", FlowNode.PinTypes.Output, 110)]
  public class AutoRepeatQuestTop : MonoBehaviour, IFlowInterface
  {
    private const int PIN_INPUT_INIT = 20;
    private const int PIN_OUTPUT_INIT = 110;
    [SerializeField]
    private string TREASURE_BOX_INAME;
    [SerializeField]
    private string BG_MODEL_GAMEOBJECT_ID;
    [SerializeField]
    private float BG_ROTATE_SPEED;
    [SerializeField]
    private bool mIsDispShadow;
    [SerializeField]
    private bool mIsFlatUnitHeight;
    [SerializeField]
    private GameObject mBackgroundPrefab;
    [SerializeField]
    private Transform mRunningCameraPos;
    [SerializeField]
    private Transform mFinishedCameraPos;
    [SerializeField]
    private Transform mRunningUnitPos;
    [SerializeField]
    private Transform mFinishedUnitPos;
    [SerializeField]
    private Camera mCamera3D;
    [SerializeField]
    private Camera mBGCmaera;
    [SerializeField]
    private AutoRepeatQuestUnit mUnitControllerTemplate;
    [SerializeField]
    private Transform mBackGroundPos;
    [SerializeField]
    private GameObject mRotateRoot;
    [SerializeField]
    private AutoRepeatQuestUnit.UnitAnimationParam[] mAnimSettings;
    [SerializeField]
    private bool mIsPutTreasureBox;
    [SerializeField]
    private Transform[] mTreasureBoxPos;
    private List<AutoRepeatQuestUnit> mUnitControllerList;
    private List<UnitController> mTreasureBoxList;
    private bool mIsAutoRepeatQuestFinished;
    private static AutoRepeatQuestTop mInstance;

    public AutoRepeatQuestTop()
    {
      base.\u002Ector();
    }

    public static AutoRepeatQuestTop Instance
    {
      get
      {
        return AutoRepeatQuestTop.mInstance;
      }
    }

    public void Activated(int pinID)
    {
      if (pinID != 20)
        return;
      this.Init();
      FlowNode_GameObject.ActivateOutputLinks((Component) this, 110);
    }

    public void PrepareAssets()
    {
      List<UnitData> units = this.GetUnits();
      for (int index = 0; index < units.Count; ++index)
        DownloadUtility.PrepareAutoRepeatQuestUnitAssets(units[index]);
      if (!this.mIsPutTreasureBox || string.IsNullOrEmpty(this.TREASURE_BOX_INAME))
        return;
      UnitParam unitParam = MonoSingleton<GameManager>.Instance.GetUnitParam(this.TREASURE_BOX_INAME);
      if (unitParam == null)
        return;
      CharacterDB.Character character = CharacterDB.FindCharacter(unitParam.model);
      if (character == null)
        return;
      for (int index = 0; index < character.Jobs.Count; ++index)
        DownloadUtility.PrepareUnitModels(character.Jobs[index]);
    }

    private void Awake()
    {
      AutoRepeatQuestTop.mInstance = this;
      GameUtility.SetGameObjectActive((Component) this.mUnitControllerTemplate, false);
    }

    private void Update()
    {
      if (this.mIsAutoRepeatQuestFinished || !Object.op_Inequality((Object) this.mRotateRoot, (Object) null))
        return;
      this.mRotateRoot.get_transform().Rotate(new Vector3(1f, 0.0f, 0.0f), this.BG_ROTATE_SPEED * Time.get_deltaTime());
    }

    private void Init()
    {
      this.mIsAutoRepeatQuestFinished = MonoSingleton<GameManager>.Instance.Player.AutoRepeatQuestProgress.State == AutoRepeatQuestData.eState.AUTO_REPEAT_END;
      if (Object.op_Inequality((Object) this.mBackgroundPrefab, (Object) null))
        ((GameObject) Object.Instantiate<GameObject>((M0) this.mBackgroundPrefab)).get_transform().SetParent(this.mBackGroundPos, false);
      Transform transform = !this.mIsAutoRepeatQuestFinished ? this.mRunningCameraPos : this.mFinishedCameraPos;
      ((Component) this.mCamera3D).get_transform().SetParent(transform, false);
      ((Component) this.mBGCmaera).get_transform().set_position(((Component) this.mCamera3D).get_transform().get_position());
      ((Component) this.mBGCmaera).get_transform().set_rotation(((Component) this.mCamera3D).get_transform().get_rotation());
      this.CreateUnits(this.mIsAutoRepeatQuestFinished);
      if (this.mIsPutTreasureBox && this.mIsAutoRepeatQuestFinished)
        this.CreateTeasureBox();
      GameObject gameObject = GameObjectID.FindGameObject(this.BG_MODEL_GAMEOBJECT_ID);
      if (!Object.op_Inequality((Object) gameObject, (Object) null) || !Object.op_Inequality((Object) this.mRotateRoot, (Object) null))
        return;
      this.mRotateRoot.get_transform().set_position(gameObject.get_transform().get_position());
      (!this.mIsAutoRepeatQuestFinished ? (Component) this.mRunningUnitPos : (Component) this.mFinishedUnitPos).get_transform().SetParent(this.mRotateRoot.get_transform(), true);
      ((Component) transform).get_transform().SetParent(this.mRotateRoot.get_transform(), true);
    }

    private void CreateTeasureBox()
    {
      for (int index = 0; index < this.mTreasureBoxPos.Length; ++index)
      {
        Json_Unit json = new Json_Unit();
        json.iname = this.TREASURE_BOX_INAME;
        UnitData unitData = new UnitData();
        unitData.Deserialize(json);
        UnitController unitController = (UnitController) Object.Instantiate<AutoRepeatQuestUnit>((M0) this.mUnitControllerTemplate);
        unitController.SetupUnit(unitData, -1);
        ((Component) unitController).get_transform().SetParent(this.mTreasureBoxPos[index], false);
        GameUtility.SetGameObjectActive((Component) unitController, true);
        this.mTreasureBoxList.Add(unitController);
      }
    }

    private void CreateUnits(bool is_finished)
    {
      List<UnitData> units = this.GetUnits();
      for (int index = 0; index < units.Count && this.mAnimSettings.Length > index; ++index)
      {
        AutoRepeatQuestUnit autoRepeatQuestUnit = (AutoRepeatQuestUnit) Object.Instantiate<AutoRepeatQuestUnit>((M0) this.mUnitControllerTemplate);
        autoRepeatQuestUnit.SetUnitData(this.mAnimSettings[index], is_finished, this.mIsDispShadow);
        autoRepeatQuestUnit.SetupUnit(units[index], -1);
        GameUtility.SetGameObjectActive((Component) autoRepeatQuestUnit, true);
        this.mUnitControllerList.Add(autoRepeatQuestUnit);
      }
      if (is_finished)
        return;
      this.FlatUnitSize();
    }

    private List<UnitData> GetUnits()
    {
      List<UnitData> unitDataList = new List<UnitData>();
      QuestParam quest = MonoSingleton<GameManager>.Instance.FindQuest(MonoSingleton<GameManager>.Instance.Player.AutoRepeatQuestProgress.QuestIname);
      if (quest != null && quest.units != null)
      {
        for (int index = 0; index < quest.units.Length; ++index)
        {
          UnitData unitDataByUnitId = MonoSingleton<GameManager>.Instance.Player.FindUnitDataByUnitID(quest.units.Get(index));
          if (unitDataByUnitId != null && !unitDataList.Contains(unitDataByUnitId))
            unitDataList.Add(unitDataByUnitId);
        }
      }
      foreach (long unit in MonoSingleton<GameManager>.Instance.Player.AutoRepeatQuestProgress.Units)
      {
        UnitData unitDataByUniqueId = MonoSingleton<GameManager>.Instance.Player.FindUnitDataByUniqueID(unit);
        if (unitDataByUniqueId != null && !unitDataList.Contains(unitDataByUniqueId))
          unitDataList.Add(unitDataByUniqueId);
      }
      return unitDataList;
    }

    private void FlatUnitSize()
    {
      if (!this.mIsFlatUnitHeight)
        return;
      for (int index = 0; index < this.mUnitControllerList.Count; ++index)
      {
        AutoRepeatQuestUnit mUnitController = this.mUnitControllerList[index];
        if (Object.op_Inequality((Object) mUnitController, (Object) null) && mUnitController.UnitData.UnitParam.sd > (byte) 0)
        {
          float num = 1f / (float) mUnitController.UnitData.UnitParam.sd;
          ((Component) mUnitController).get_transform().set_localScale(new Vector3(num, num, num));
        }
      }
    }

    private void OnDestroy()
    {
      for (int index = 0; index < this.mUnitControllerList.Count; ++index)
      {
        GameUtility.DestroyGameObject((Component) this.mUnitControllerList[index]);
        this.mUnitControllerList[index] = (AutoRepeatQuestUnit) null;
      }
      this.mUnitControllerList.Clear();
      for (int index = 0; index < this.mTreasureBoxList.Count; ++index)
      {
        GameUtility.DestroyGameObject((Component) this.mTreasureBoxList[index]);
        this.mTreasureBoxList[index] = (UnitController) null;
      }
      this.mTreasureBoxList.Clear();
      AutoRepeatQuestTop.mInstance = (AutoRepeatQuestTop) null;
    }
  }
}
