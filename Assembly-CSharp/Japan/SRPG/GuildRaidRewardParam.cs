﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRaidRewardParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace SRPG
{
  public class GuildRaidRewardParam : GuildRaidMasterParam<JSON_GuildRaidRewardParam>
  {
    public string Id { get; private set; }

    public List<GuildRaidReward> Rewards { get; private set; }

    public override bool Deserialize(JSON_GuildRaidRewardParam json)
    {
      if (json == null)
        return false;
      this.Id = json.id;
      this.Rewards = new List<GuildRaidReward>();
      if (json.rewards != null)
      {
        for (int index = 0; index < json.rewards.Length; ++index)
        {
          GuildRaidReward guildRaidReward = new GuildRaidReward();
          if (guildRaidReward.Deserialize(json.rewards[index]))
            this.Rewards.Add(guildRaidReward);
        }
      }
      return true;
    }
  }
}
