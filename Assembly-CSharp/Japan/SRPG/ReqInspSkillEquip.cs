﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqInspSkillEquip
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class ReqInspSkillEquip : WebAPI
  {
    public ReqInspSkillEquip(
      long artifact_iid,
      long inspiration_skil_iid,
      Network.ResponseCallback response)
    {
      this.name = "unit/job/artifact/inspirationskill/equip";
      this.body = WebAPI.GetRequestString<ReqInspSkillEquip.RequestParam>(new ReqInspSkillEquip.RequestParam()
      {
        artifact_iid = artifact_iid,
        inspiration_skil_iid = inspiration_skil_iid
      });
      this.callback = response;
    }

    [Serializable]
    public class RequestParam
    {
      public long artifact_iid;
      public long inspiration_skil_iid;
    }

    [Serializable]
    public class Response
    {
      public Json_Artifact artifact;
    }
  }
}
