﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_AppealEventMaster
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class JSON_AppealEventMaster
  {
    public int pk;
    public JSON_AppealEventMaster.Fields fields;

    public class Fields
    {
      public string appeal_id;
      public string start_at;
      public string end_at;
    }
  }
}
