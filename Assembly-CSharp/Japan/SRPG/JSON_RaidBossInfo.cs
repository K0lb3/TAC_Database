﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_RaidBossInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_RaidBossInfo
  {
    public int no;
    public int boss_id;
    public int round;
    public int current_hp;
    public long start_time;
    public int is_reward;
    public int is_timeover;
    public int is_rescue_damage_zero;
    public int is_beat_resucue;
  }
}
