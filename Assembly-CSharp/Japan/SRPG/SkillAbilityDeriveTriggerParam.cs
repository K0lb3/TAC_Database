﻿// Decompiled with JetBrains decompiler
// Type: SRPG.SkillAbilityDeriveTriggerParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System.Collections.Generic;

namespace SRPG
{
  [MessagePackObject(false)]
  public class SkillAbilityDeriveTriggerParam
  {
    [Key(0)]
    public ESkillAbilityDeriveConds m_TriggerType;
    [Key(1)]
    public string m_TriggerIname;

    [SerializationConstructor]
    public SkillAbilityDeriveTriggerParam(ESkillAbilityDeriveConds triggerType, string triggerIname)
    {
      this.m_TriggerIname = triggerIname;
      this.m_TriggerType = triggerType;
    }

    public static List<SkillAbilityDeriveTriggerParam[]> CreateCombination(
      SkillAbilityDeriveTriggerParam[] triggerParams)
    {
      List<SkillAbilityDeriveTriggerParam[]> deriveTriggerParamArrayList = new List<SkillAbilityDeriveTriggerParam[]>();
      Stack<SkillAbilityDeriveTriggerParam> deriveTriggerParamStack = new Stack<SkillAbilityDeriveTriggerParam>();
      for (int index1 = 0; index1 < triggerParams.Length; ++index1)
      {
        for (int index2 = index1; index2 < triggerParams.Length; ++index2)
        {
          deriveTriggerParamStack.Push(triggerParams[index2]);
          deriveTriggerParamArrayList.Add(deriveTriggerParamStack.ToArray());
        }
        deriveTriggerParamStack.Clear();
      }
      return deriveTriggerParamArrayList;
    }
  }
}
