﻿// Decompiled with JetBrains decompiler
// Type: SRPG.TobiraLearnSkill
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class TobiraLearnSkill : MonoBehaviour
  {
    [SerializeField]
    private Text m_LearnSkillName;
    [SerializeField]
    private Text m_LearnSkillEffect;

    public TobiraLearnSkill()
    {
      base.\u002Ector();
    }

    public void Setup(AbilityData newAbility)
    {
      this.m_LearnSkillName.set_text("アビリティ：" + newAbility.AbilityName);
      this.m_LearnSkillEffect.set_text(newAbility.Param.expr);
    }

    public void Setup(SkillData skill)
    {
      this.m_LearnSkillName.set_text("リーダースキル：" + skill.Name);
      this.m_LearnSkillEffect.set_text(skill.SkillParam.expr);
    }
  }
}
