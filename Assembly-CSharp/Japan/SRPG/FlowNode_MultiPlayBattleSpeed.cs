﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_MultiPlayBattleSpeed
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("Multi/MultiPlayBattleSpeed", 32741)]
  [FlowNode.Pin(100, "Update", FlowNode.PinTypes.Input, 100)]
  [FlowNode.Pin(101, "Update Success", FlowNode.PinTypes.Output, 101)]
  [FlowNode.Pin(102, "Update Failure", FlowNode.PinTypes.Output, 102)]
  [FlowNode.Pin(200, "Test", FlowNode.PinTypes.Input, 200)]
  [FlowNode.Pin(201, "TRUE", FlowNode.PinTypes.Output, 201)]
  [FlowNode.Pin(202, "FALSE", FlowNode.PinTypes.Output, 202)]
  public class FlowNode_MultiPlayBattleSpeed : FlowNode
  {
    private const int PIN_IN_UPDATE = 100;
    private const int PIN_OUT_SUCCESS = 101;
    private const int PIN_OUT_FAILURE = 102;
    private const int PIN_IN_IS_HISPEED = 200;
    private const int PIN_OUT_IS_HISPEED_TRUE = 201;
    private const int PIN_OUT_IS_HISPEED_FALSE = 202;
    [SerializeField]
    private bool HiSpeed;

    public override void OnActivate(int pinID)
    {
      MyPhoton instance = PunMonoSingleton<MyPhoton>.Instance;
      MyPhoton.MyRoom currentRoom = instance.GetCurrentRoom();
      if (currentRoom == null)
      {
        DebugUtility.Log("CurrentRoom is null");
        this.ActivateOutputLinks(102);
      }
      else
      {
        JSON_MyPhotonRoomParam myPhotonRoomParam = JSON_MyPhotonRoomParam.Parse(currentRoom.json);
        if (myPhotonRoomParam == null)
        {
          DebugUtility.Log("no roomParam");
          this.ActivateOutputLinks(102);
        }
        else
        {
          switch (pinID)
          {
            case 100:
              if (instance.IsOldestPlayer())
              {
                myPhotonRoomParam.btlSpd = !this.HiSpeed ? 1 : 2;
                instance.SetRoomParam(myPhotonRoomParam.Serialize());
                DebugUtility.Log("Hi Speed : " + (object) myPhotonRoomParam.btlSpd);
              }
              PlayerPrefsUtility.SetInt(PlayerPrefsUtility.MULTI_HI_SPEED, !this.HiSpeed ? 0 : 1, false);
              GlobalVars.SelectedMultiPlayHiSpeed = this.HiSpeed;
              this.ActivateOutputLinks(101);
              break;
            case 200:
              if (myPhotonRoomParam.btlSpd == 2 == this.HiSpeed)
              {
                DebugUtility.Log("Hi Speed Test : HiSp( " + (!this.HiSpeed ? "Off" : "On") + " ) True");
                this.ActivateOutputLinks(201);
                break;
              }
              DebugUtility.Log("Hi Speed Test : HiSp( " + (!this.HiSpeed ? "Off" : "On") + " ) False");
              this.ActivateOutputLinks(202);
              break;
          }
        }
      }
    }
  }
}
