﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_IsActiveGameObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("Common/IsActiveGameObject", 32741)]
  [FlowNode.Pin(1, "Check", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(101, "Active", FlowNode.PinTypes.Output, 101)]
  [FlowNode.Pin(102, "NotActive", FlowNode.PinTypes.Output, 102)]
  public class FlowNode_IsActiveGameObject : FlowNode
  {
    [FlowNode.ShowInInfo]
    [FlowNode.DropTarget(typeof (GameObject), true)]
    public GameObject Target;
    [SerializeField]
    private bool CheckActiveInHierarchy;
    private const int PIN_IN_CHECK = 1;
    private const int PIN_OUT_ACTIVE = 101;
    private const int PIN_OUT_NOT_ACTIVE = 102;

    public override void OnActivate(int pinID)
    {
      if (pinID != 1)
        return;
      if (Object.op_Implicit((Object) this.Target))
      {
        if (this.CheckActiveInHierarchy)
        {
          if (this.Target.get_activeInHierarchy())
          {
            this.ActivateOutputLinks(101);
            return;
          }
        }
        else if (this.Target.get_activeSelf())
        {
          this.ActivateOutputLinks(101);
          return;
        }
      }
      this.ActivateOutputLinks(102);
    }
  }
}
