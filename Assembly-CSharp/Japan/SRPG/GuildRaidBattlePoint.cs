﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRaidBattlePoint
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class GuildRaidBattlePoint
  {
    public int PT { get; private set; }

    public int Max { get; private set; }

    public int AP { get; private set; }

    public int DefBP { get; private set; }

    public bool Deserialize(JSON_GuildRaidBattlePoint json)
    {
      this.PT = json.pt;
      this.Max = json.max;
      this.AP = json.ap;
      this.DefBP = json.defbp;
      return true;
    }
  }
}
