﻿// Decompiled with JetBrains decompiler
// Type: SRPG.eSkillFlags
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public enum eSkillFlags
  {
    EnableRankUp,
    EnableChangeRange,
    PierceAttack,
    SelfTargetSelect,
    ExecuteCutin,
    ExecuteInBattle,
    EnableHeightRangeBonus,
    EnableHeightParamAdjust,
    EnableUnitLockTarget,
    CastBreak,
    JewelAttack,
    ForceHit,
    Suicide,
    SubActuate,
    FixedDamage,
    ForceUnitLock,
    AllDamageReaction,
    ShieldReset,
    IgnoreElement,
    PrevApply,
    JudgeHpOver,
    MhmDamage,
    AcSelf,
    AcReset,
    HitTargetNumDiv,
    NoChargeCalcCT,
    JumpBreak,
    ObstReaction,
    NoUsedJewelBuff,
    AiNoAutoTiming,
    Critical,
    JewelAbsorb,
    MpUseAfter,
    ProtectIgnore,
    MAX,
  }
}
