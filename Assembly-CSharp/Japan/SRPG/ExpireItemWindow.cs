﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ExpireItemWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class ExpireItemWindow : MonoBehaviour
  {
    [SerializeField]
    private GameObject mTemplate;
    [SerializeField]
    private GameObject mTitle_Warning;
    [SerializeField]
    private GameObject mTitle_Expired;
    [SerializeField]
    private Text mWarningText_1;
    [SerializeField]
    private Text mWarningText_2;

    public ExpireItemWindow()
    {
      base.\u002Ector();
    }

    public void Setup_ExpireWarning(List<ItemData> item_list, int rest_day)
    {
      this.mTitle_Warning.SetActive(true);
      this.mTitle_Expired.SetActive(false);
      ((Component) this.mWarningText_1).get_gameObject().SetActive(false);
      ((Component) this.mWarningText_2).get_gameObject().SetActive(false);
      Text text = rest_day > 0 ? this.mWarningText_2 : this.mWarningText_1;
      text.set_text(LocalizedText.Get(text.get_text(), (object) rest_day));
      ((Component) text).get_gameObject().SetActive(true);
      this.mTemplate.SetActive(false);
      if (item_list == null)
        return;
      for (int index = 0; index < item_list.Count; ++index)
      {
        if (item_list[index].Param != null)
        {
          GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.mTemplate);
          gameObject.get_transform().SetParent(this.mTemplate.get_transform().get_parent(), false);
          gameObject.SetActive(true);
          DataSource.Bind<ItemParam>(gameObject, item_list[index].Param, false);
          DataSource.Bind<int>(gameObject, item_list[index].Num, false);
        }
      }
    }

    public void Setup_ExpiredNotify(ExpireItemParamList expire_item)
    {
      this.mTitle_Warning.SetActive(false);
      this.mTitle_Expired.SetActive(true);
      this.mTemplate.SetActive(false);
      if (expire_item == null)
        return;
      for (int index = 0; index < expire_item.items.Length; ++index)
      {
        ItemParam itemParam = MonoSingleton<GameManager>.Instance.MasterParam.GetItemParam(expire_item.items[index].iname);
        if (itemParam != null)
        {
          GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.mTemplate);
          gameObject.get_transform().SetParent(this.mTemplate.get_transform().get_parent(), false);
          gameObject.SetActive(true);
          DataSource.Bind<ItemParam>(gameObject, itemParam, false);
          DataSource.Bind<int>(gameObject, expire_item.items[index].num, false);
        }
      }
    }
  }
}
