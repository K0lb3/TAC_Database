﻿// Decompiled with JetBrains decompiler
// Type: SRPG.HighlightResource
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class HighlightResource
  {
    public HighlightType type;
    public string path;
    public string message;

    public void Deserialize(JSON_HighlightResource json)
    {
      if (json == null)
        return;
      this.type = (HighlightType) json.type;
      this.path = json.path;
      this.message = json.message;
    }
  }
}
