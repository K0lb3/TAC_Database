﻿// Decompiled with JetBrains decompiler
// Type: SRPG.QuestMultiTypes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public enum QuestMultiTypes : short
  {
    NOT_MULTI = 0,
    RAID = 1,
    VERSUS = 2,
    EVENT_TOP = 100, // 0x0064
    RAID_EVENT = 101, // 0x0065
    VERSUS_EVENT = 102, // 0x0066
    TOWER_EVENT = 103, // 0x0067
  }
}
