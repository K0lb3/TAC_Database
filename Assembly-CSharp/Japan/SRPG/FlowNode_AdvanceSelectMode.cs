﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_AdvanceSelectMode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("Advance/SelectMode", 32741)]
  [FlowNode.Pin(1, "Normal", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(2, "Elite", FlowNode.PinTypes.Input, 2)]
  [FlowNode.Pin(3, "Extra", FlowNode.PinTypes.Input, 3)]
  [FlowNode.Pin(101, "Success", FlowNode.PinTypes.Output, 101)]
  public class FlowNode_AdvanceSelectMode : FlowNode
  {
    [SerializeField]
    private FlowNode_AdvanceSelectMode.ModeTarget mModeTarget;

    public override void OnActivate(int pinID)
    {
      AdvanceEventManager instance = AdvanceEventManager.Instance;
      QuestDifficulties difficult = QuestDifficulties.Normal;
      switch (pinID)
      {
        case 1:
          difficult = QuestDifficulties.Normal;
          break;
        case 2:
          difficult = QuestDifficulties.Elite;
          break;
        case 3:
          difficult = QuestDifficulties.Extra;
          break;
      }
      if (this.mModeTarget == FlowNode_AdvanceSelectMode.ModeTarget.Stage)
        instance.SetStageDifficulty(difficult);
      else
        instance.SetBossDifficulty(difficult);
      this.ActivateOutputLinks(101);
    }

    public enum ModeTarget
    {
      Stage,
      Boss,
    }
  }
}
