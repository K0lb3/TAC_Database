﻿// Decompiled with JetBrains decompiler
// Type: SRPG.SortBadge
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class SortBadge : MonoBehaviour
  {
    [FourCC]
    public int ID;
    public Image Icon;
    public Text Value;
    public Text Name;

    public SortBadge()
    {
      base.\u002Ector();
    }

    public void SetValue(string value)
    {
      if (!Object.op_Inequality((Object) this.Value, (Object) null))
        return;
      this.Value.set_text(value);
    }

    public void SetValue(int value)
    {
      if (!Object.op_Inequality((Object) this.Value, (Object) null))
        return;
      this.Value.set_text(value.ToString());
    }

    public void SetName(string name)
    {
      if (!Object.op_Inequality((Object) this.Name, (Object) null))
        return;
      this.Name.set_text(name);
    }
  }
}
