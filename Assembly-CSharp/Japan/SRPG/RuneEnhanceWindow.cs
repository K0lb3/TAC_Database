﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RuneEnhanceWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  [FlowNode.Pin(10, "強化", FlowNode.PinTypes.Input, 10)]
  [FlowNode.Pin(11, "ゲージ使用", FlowNode.PinTypes.Input, 11)]
  [FlowNode.Pin(12, "連続強化", FlowNode.PinTypes.Input, 12)]
  [FlowNode.Pin(100, "強化通信開始", FlowNode.PinTypes.Output, 100)]
  [FlowNode.Pin(110, "強化通信完了", FlowNode.PinTypes.Input, 110)]
  [FlowNode.Pin(1000, "自身を閉じる", FlowNode.PinTypes.Output, 1000)]
  public class RuneEnhanceWindow : MonoBehaviour, IFlowInterface
  {
    private const int INPUT_RUNE_ENHANCE = 10;
    private const int INPUT_RUNE_USE_GAUGE = 11;
    private const int INPUT_RUNE_CONTINUE = 12;
    private const int OUTPUT_START_ENHANCE = 100;
    private const int INPUT_FINISHED_ENHANCE = 110;
    private const int OUTPUT_CLOSE_WINDOW = 1000;
    private const int NORMAL_ENHANCE_ADD = 1;
    [SerializeField]
    private RuneDrawInfo mRuneDrawInfo;
    [SerializeField]
    private RuneIcon mRuneIcon;
    [SerializeField]
    private RuneDrawBaseState mRuneDrawBaseState;
    [SerializeField]
    private RuneDrawBaseState mRuneDrawAfterBaseState;
    [SerializeField]
    private RuneDrawEnhanceLevel mRuneDrawEnhanceLevel;
    [SerializeField]
    private RuneDrawEvoState mRuneDrawEvoState;
    [SerializeField]
    private RuneDrawCost mRuneDrawCost;
    [SerializeField]
    private RuneDrawEnhancePercentage mRuneDrawEnhancePercentage;
    [SerializeField]
    private Button mEnhanceButton;
    private RuneManager mRuneManager;
    private BindRuneData mRuneData;
    private bool mIsUseEnforceGauge;

    public RuneEnhanceWindow()
    {
      base.\u002Ector();
    }

    public void Awake()
    {
    }

    private void OnDestroy()
    {
    }

    public void Activated(int pinID)
    {
      switch (pinID)
      {
        case 10:
          this.ConfirmEnhance();
          break;
        case 11:
          if (Object.op_Implicit((Object) this.mRuneDrawEnhancePercentage) && this.mRuneDrawEnhancePercentage.IsCanUseGauge(this.mRuneData))
          {
            this.mIsUseEnforceGauge = !this.mIsUseEnforceGauge;
            this.mRuneDrawEnhancePercentage.SetDrawParam(this.mRuneData, this.mIsUseEnforceGauge);
            break;
          }
          UIUtility.SystemMessage(LocalizedText.Get("sys.RUNE_IS_NOT_USE_ENFORCEGAUGE"), (UIUtility.DialogResultEvent) (yes_button => {}), (GameObject) null, false, -1);
          break;
        case 110:
          if (Object.op_Implicit((Object) this.mRuneManager))
          {
            this.mIsUseEnforceGauge = false;
            this.mRuneManager.RefreshRuneEnhanceFinished();
            FlowNode_ReqRuneEnhance.Clear();
          }
          FlowNode_GameObject.ActivateOutputLinks((Component) this, 1000);
          break;
      }
    }

    public void Setup(RuneManager manager, BindRuneData rune_data)
    {
      if (rune_data == null)
        return;
      this.mRuneManager = manager;
      this.mRuneData = rune_data;
      BindRuneData copyRune = this.mRuneData.CreateCopyRune();
      ++copyRune.Rune.enforce;
      if (Object.op_Implicit((Object) this.mRuneDrawInfo))
        this.mRuneDrawInfo.SetDrawParam(this.mRuneData);
      if (Object.op_Implicit((Object) this.mRuneIcon))
        this.mRuneIcon.Setup(this.mRuneData, false);
      if (Object.op_Implicit((Object) this.mRuneDrawBaseState))
        this.mRuneDrawBaseState.SetDrawParam(this.mRuneData);
      if (Object.op_Implicit((Object) this.mRuneDrawAfterBaseState))
        this.mRuneDrawAfterBaseState.SetDrawParam(copyRune);
      if (Object.op_Implicit((Object) this.mRuneDrawEvoState))
        this.mRuneDrawEvoState.SetDrawParam(this.mRuneData);
      if (Object.op_Implicit((Object) this.mRuneDrawEnhanceLevel))
        this.mRuneDrawEnhanceLevel.SetDrawParam(this.mRuneData, 1);
      if (Object.op_Implicit((Object) this.mRuneDrawCost))
        this.mRuneDrawCost.SetDrawParam(this.mRuneData.EnhanceCost, 0);
      if (Object.op_Implicit((Object) this.mRuneDrawEnhancePercentage))
        this.mRuneDrawEnhancePercentage.SetDrawParam(this.mRuneData, this.mIsUseEnforceGauge);
      this.Refresh();
    }

    public void Refresh()
    {
      if (Object.op_Implicit((Object) this.mRuneDrawInfo))
        this.mRuneDrawInfo.Refresh();
      if (Object.op_Implicit((Object) this.mRuneIcon))
        this.mRuneIcon.Refresh();
      if (Object.op_Implicit((Object) this.mRuneDrawBaseState))
        this.mRuneDrawBaseState.Refresh();
      if (Object.op_Implicit((Object) this.mRuneDrawAfterBaseState))
        this.mRuneDrawAfterBaseState.Refresh();
      if (Object.op_Implicit((Object) this.mRuneDrawEvoState))
        this.mRuneDrawEvoState.Refresh();
      if (Object.op_Implicit((Object) this.mRuneDrawEnhanceLevel))
        this.mRuneDrawEnhanceLevel.Refresh();
      if (Object.op_Implicit((Object) this.mRuneDrawCost))
        this.mRuneDrawCost.Refresh();
      if (Object.op_Implicit((Object) this.mRuneDrawEnhancePercentage))
        this.mRuneDrawEnhancePercentage.Refresh();
      this.RefreshButton();
    }

    public void RefreshButton()
    {
      if (this.mRuneData == null)
        return;
      RuneCost enhanceCost = this.mRuneData.EnhanceCost;
      if (enhanceCost == null || !Object.op_Implicit((Object) this.mEnhanceButton))
        return;
      ((Selectable) this.mEnhanceButton).set_interactable(enhanceCost.IsPlayerAmountEnough());
    }

    public void ConfirmEnhance()
    {
      if (Object.op_Equality((Object) this.mRuneManager, (Object) null))
        return;
      if (this.mIsUseEnforceGauge)
        UIUtility.ConfirmBox(LocalizedText.Get("sys.RUNE_ENHANCE_GAUGE_CONFIRM"), (UIUtility.DialogResultEvent) (yes_button =>
        {
          FlowNode_ReqRuneEnhance.SetTarget(this.mRuneData, this.mIsUseEnforceGauge);
          FlowNode_GameObject.ActivateOutputLinks((Component) this, 100);
        }), (UIUtility.DialogResultEvent) null, (GameObject) null, false, -1, (string) null, (string) null);
      else
        UIUtility.ConfirmBox(LocalizedText.Get("sys.RUNE_ENHANCE_NOGAUGE_CONFIRM"), (UIUtility.DialogResultEvent) (yes_button =>
        {
          FlowNode_ReqRuneEnhance.SetTarget(this.mRuneData, this.mIsUseEnforceGauge);
          FlowNode_GameObject.ActivateOutputLinks((Component) this, 100);
        }), (UIUtility.DialogResultEvent) null, (GameObject) null, false, -1, (string) null, (string) null);
    }
  }
}
