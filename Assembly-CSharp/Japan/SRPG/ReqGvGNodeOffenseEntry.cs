﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqGvGNodeOffenseEntry
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Network.Encoding;
using MessagePack;
using System;

namespace SRPG
{
  public class ReqGvGNodeOffenseEntry : WebAPI
  {
    public ReqGvGNodeOffenseEntry(
      int id,
      long[] party,
      int gid,
      int gvg_group_id,
      SRPG.Network.ResponseCallback response,
      EncodingTypes.ESerializeCompressMethod serializeCompressMethod)
    {
      this.name = "gvg/node/offense/entry";
      this.body = WebAPI.GetRequestString<ReqGvGNodeOffenseEntry.RequestParam>(new ReqGvGNodeOffenseEntry.RequestParam()
      {
        id = id,
        party = party,
        gid = gid,
        gvg_group_id = gvg_group_id
      });
      this.callback = response;
      this.serializeCompressMethod = serializeCompressMethod;
    }

    [Serializable]
    public class RequestParam
    {
      public int id;
      public long[] party;
      public int gid;
      public int gvg_group_id;
    }

    [MessagePackObject(true)]
    [Serializable]
    public class Response
    {
      public JSON_GvGPartyUnit[] offense;
    }
  }
}
