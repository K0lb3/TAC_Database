﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_TowerBtlEndRank
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class Json_TowerBtlEndRank
  {
    public int turn_num;
    public int died_num;
    public int retire_num;
    public int recovery_num;
    public int spd_rank;
    public int tec_rank;
    public int spd_score;
    public int tec_score;
    public int ret_score;
    public int rcv_score;
  }
}
