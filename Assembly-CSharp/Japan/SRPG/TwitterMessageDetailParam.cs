﻿// Decompiled with JetBrains decompiler
// Type: SRPG.TwitterMessageDetailParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class TwitterMessageDetailParam
  {
    private string text;
    private string[] hash_tag;
    private string cnds_key;

    public string Text
    {
      get
      {
        return this.text;
      }
    }

    public string[] HashTag
    {
      get
      {
        return this.hash_tag;
      }
    }

    public string CndsKey
    {
      get
      {
        return this.cnds_key;
      }
    }

    public void Deserialize(JSON_TwitterMessageDetailParam json)
    {
      this.text = json.text;
      this.hash_tag = json.hash_tag;
      this.cnds_key = json.cnds_key;
    }
  }
}
