﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildSVB_Key
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class GuildSVB_Key
  {
    public static readonly string GUILD_ID = "guild_id";
    public static readonly string GUILD = "guild_data";
    public static readonly string COMMNAD = "guild_command";
    public static readonly string PLAYER = "player_data";
    public static readonly string CONFIRM = "guild_confirm";
    public static readonly string MEMBER = "member_data";
    public static readonly string VIEW_GUILD = "view_guild";
    public static readonly string FACILITY = "guild_facility";
    public static readonly string ENHANCE_MATERIAL = "guild_enhance_material";
  }
}
