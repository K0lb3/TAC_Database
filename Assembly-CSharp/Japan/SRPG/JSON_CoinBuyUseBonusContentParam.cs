﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_CoinBuyUseBonusContentParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_CoinBuyUseBonusContentParam
  {
    public string name;
    public int num;
    public string reward_iname;
    public string gift_msg;
  }
}
