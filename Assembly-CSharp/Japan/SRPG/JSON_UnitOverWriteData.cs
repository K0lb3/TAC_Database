﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_UnitOverWriteData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_UnitOverWriteData
  {
    public long unit_iid;
    public long job_iid;
    public long[] abils;
    public long[] artifacts;
    public JSON_ConceptCard[] concept_cards;
  }
}
