﻿// Decompiled with JetBrains decompiler
// Type: SRPG.BattleUnitDetailCond
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class BattleUnitDetailCond : MonoBehaviour
  {
    public ImageArray ImageCond;
    public Text TextValue;
    private string[] mStrShieldDesc;

    public BattleUnitDetailCond()
    {
      base.\u002Ector();
    }

    public void SetCond(EUnitCondition cond)
    {
      int index = new List<EUnitCondition>((IEnumerable<EUnitCondition>) Enum.GetValues(typeof (EUnitCondition))).IndexOf(cond);
      if (UnityEngine.Object.op_Implicit((UnityEngine.Object) this.ImageCond) && this.ImageCond.Images != null && (index >= 0 && index < this.ImageCond.Images.Length))
        this.ImageCond.ImageIndex = index;
      if (!UnityEngine.Object.op_Implicit((UnityEngine.Object) this.TextValue) || index < 0 || index >= Unit.StrNameUnitConds.Length)
        return;
      this.TextValue.set_text(Unit.StrNameUnitConds[index]);
    }

    public void SetCondShield(ShieldTypes s_type, int val)
    {
      int maxUnitCondition = (int) Unit.MAX_UNIT_CONDITION;
      if (UnityEngine.Object.op_Implicit((UnityEngine.Object) this.ImageCond) && this.ImageCond.Images != null)
        this.ImageCond.ImageIndex = 0 > maxUnitCondition || maxUnitCondition >= this.ImageCond.Images.Length ? this.ImageCond.Images.Length - 1 : maxUnitCondition;
      if (!UnityEngine.Object.op_Implicit((UnityEngine.Object) this.TextValue))
        return;
      this.TextValue.set_text(string.Format(LocalizedText.Get("quest.BUD_COND_SHIELD_DETAIL"), (object) string.Format(LocalizedText.Get(this.mStrShieldDesc[(int) s_type]), (object) val)));
    }

    public void SetCondForcedTargeting()
    {
      int num = (int) Unit.MAX_UNIT_CONDITION + 1;
      if (UnityEngine.Object.op_Implicit((UnityEngine.Object) this.ImageCond) && this.ImageCond.Images != null && (0 <= num && num < this.ImageCond.Images.Length))
        this.ImageCond.ImageIndex = num;
      if (!UnityEngine.Object.op_Implicit((UnityEngine.Object) this.TextValue))
        return;
      this.TextValue.set_text(LocalizedText.Get("quest.BUD_COND_FORCED_TARGETING"));
    }

    public void SetCondBeForcedTargeted()
    {
      int num = (int) Unit.MAX_UNIT_CONDITION + 2;
      if (UnityEngine.Object.op_Implicit((UnityEngine.Object) this.ImageCond) && this.ImageCond.Images != null && (0 <= num && num < this.ImageCond.Images.Length))
        this.ImageCond.ImageIndex = num;
      if (!UnityEngine.Object.op_Implicit((UnityEngine.Object) this.TextValue))
        return;
      this.TextValue.set_text(LocalizedText.Get("quest.BUD_COND_BE_FORCED_TARGETED"));
    }

    public void SetCondProtect()
    {
      int num = (int) Unit.MAX_UNIT_CONDITION + 3;
      if (UnityEngine.Object.op_Implicit((UnityEngine.Object) this.ImageCond) && this.ImageCond.Images != null && (0 <= num && num < this.ImageCond.Images.Length))
        this.ImageCond.ImageIndex = num;
      if (!UnityEngine.Object.op_Implicit((UnityEngine.Object) this.TextValue))
        return;
      this.TextValue.set_text(LocalizedText.Get("quest.BUD_COND_PROTECT"));
    }

    public void SetCondGuard()
    {
      int num = (int) Unit.MAX_UNIT_CONDITION + 4;
      if (UnityEngine.Object.op_Implicit((UnityEngine.Object) this.ImageCond) && this.ImageCond.Images != null && (0 <= num && num < this.ImageCond.Images.Length))
        this.ImageCond.ImageIndex = num;
      if (!UnityEngine.Object.op_Implicit((UnityEngine.Object) this.TextValue))
        return;
      this.TextValue.set_text(LocalizedText.Get("quest.BUD_COND_GUARD"));
    }
  }
}
