﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AdvanceQuestConfirm
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  [FlowNode.Pin(1, "Initialize", FlowNode.PinTypes.Input, 1)]
  public class AdvanceQuestConfirm : MonoBehaviour, IFlowInterface
  {
    public const int PIN_IN_INIT = 1;
    [SerializeField]
    private QuestCampaignList CampaignPrefab;
    [SerializeField]
    private GameObject DetailInfoPrefab;
    [SerializeField]
    private Text Text_StaminaVal;
    [SerializeField]
    private List<GameObject> mRefMissionStarLists;
    [SerializeField]
    private List<GameObject> mRefDifficultyObject;
    private QuestParam mQuestParam;
    private QuestCampaignData[] mCampaigns;

    public AdvanceQuestConfirm()
    {
      base.\u002Ector();
    }

    private bool Init()
    {
      SerializeValueBehaviour component = (SerializeValueBehaviour) ((Component) this).GetComponent<SerializeValueBehaviour>();
      if (Object.op_Equality((Object) component, (Object) null))
        return false;
      this.mQuestParam = component.list.GetObject<QuestParam>("ADVANCE_QUEST_PARAM");
      if (this.mQuestParam == null)
        return false;
      Transform transform = ((Component) this).get_transform().Find("CanvasBoundsPanel/window/campaign_root");
      DataSource.Bind<QuestParam>(((Component) this).get_gameObject(), this.mQuestParam, false);
      this.mRefDifficultyObject[(int) this.mQuestParam.difficulty].SetActive(true);
      if (Object.op_Inequality((Object) this.CampaignPrefab, (Object) null))
      {
        this.mCampaigns = MonoSingleton<GameManager>.Instance.FindQuestCampaigns(this.mQuestParam);
        if (this.mCampaigns != null)
        {
          QuestCampaignList questCampaignList = (QuestCampaignList) Object.Instantiate<QuestCampaignList>((M0) this.CampaignPrefab, transform, false);
          if (Object.op_Inequality((Object) questCampaignList, (Object) null))
          {
            DataSource.Bind<QuestCampaignData[]>(((Component) this).get_gameObject(), this.mCampaigns == null || this.mCampaigns.Length <= 0 ? (QuestCampaignData[]) null : this.mCampaigns, false);
            questCampaignList.TextConsumeAp = this.Text_StaminaVal;
            questCampaignList.RefreshIcons();
          }
        }
      }
      this.SetMissionStar(this.mQuestParam);
      GlobalVars.SelectedQuestID = this.mQuestParam.iname;
      return true;
    }

    public void OnOpenItemDetail()
    {
      if (this.mQuestParam == null || Object.op_Equality((Object) this.DetailInfoPrefab, (Object) null))
        return;
      GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.DetailInfoPrefab);
      DataSource.Bind<QuestParam>(gameObject, this.mQuestParam, false);
      DataSource.Bind<QuestCampaignData[]>(gameObject, this.mCampaigns == null || this.mCampaigns.Length <= 0 ? (QuestCampaignData[]) null : this.mCampaigns, false);
      gameObject.SetActive(true);
    }

    public void Activated(int pinID)
    {
      if (pinID != 1 || this.Init())
        return;
      DebugUtility.LogError("おかしい");
    }

    public void SetMissionStar(QuestParam param)
    {
      if (this.mRefMissionStarLists == null || param.bonusObjective == null || (param.bonusObjective.Length <= 0 || this.mRefMissionStarLists == null))
        return;
      int num = param.bonusObjective.Length - 1;
      ImageArray[] imageArrayArray = (ImageArray[]) null;
      for (int index = 0; index < this.mRefMissionStarLists.Count; ++index)
      {
        GameObject refMissionStarList = this.mRefMissionStarLists[index];
        if (Object.op_Inequality((Object) refMissionStarList, (Object) null))
        {
          refMissionStarList.SetActive(index == num);
          if (index == num)
            imageArrayArray = (ImageArray[]) refMissionStarList.GetComponentsInChildren<ImageArray>();
        }
      }
      if (imageArrayArray == null || imageArrayArray.Length <= 0 || param.bonusObjective.Length > imageArrayArray.Length)
        return;
      for (int index = 0; index < imageArrayArray.Length; ++index)
        imageArrayArray[index].ImageIndex = !param.IsMissionClear(index) ? 0 : 1;
    }
  }
}
