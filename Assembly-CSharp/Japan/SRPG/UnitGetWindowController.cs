﻿// Decompiled with JetBrains decompiler
// Type: SRPG.UnitGetWindowController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

namespace SRPG
{
  public class UnitGetWindowController : MonoBehaviour
  {
    private UnitGetWindow mController;
    private bool mIsEnd;

    public UnitGetWindowController()
    {
      base.\u002Ector();
    }

    public bool IsEnd
    {
      get
      {
        return this.mIsEnd;
      }
    }

    public void Init(UnitGetParam rewards = null)
    {
      UnitGetParam unitGetParam = rewards == null ? GlobalVars.UnitGetReward : rewards;
      if (unitGetParam == null || unitGetParam.Params.Count <= 0)
      {
        this.mIsEnd = true;
      }
      else
      {
        bool flag = true;
        string[] unitIds = new string[unitGetParam.Params.Count];
        bool[] isConvert = new bool[unitGetParam.Params.Count];
        int[] covertPieces = new int[unitGetParam.Params.Count];
        for (int index = 0; index < unitGetParam.Params.Count; ++index)
        {
          if (unitGetParam.Params[index].ItemType != EItemType.Unit)
          {
            unitIds[index] = string.Empty;
          }
          else
          {
            if (flag)
              flag = false;
            unitIds[index] = unitGetParam.Params[index].ItemId;
            isConvert[index] = unitGetParam.Params[index].IsConvert;
            covertPieces[index] = unitGetParam.Params[index].ConvertPieceNum;
            if (!isConvert[index])
            {
              DownloadUtility.DownloadUnit(unitGetParam.Params[index].UnitParam, (JobData[]) null);
              FlowNode_ExtraUnitOpenPopup.ReserveOpenExtraQuestPopup(unitGetParam.Params[index].UnitParam.iname);
            }
          }
        }
        this.mIsEnd = flag;
        if (this.mIsEnd)
          return;
        this.StartCoroutine(this.SpawnEffectAsync(unitIds, isConvert, covertPieces));
      }
    }

    [DebuggerHidden]
    private IEnumerator SpawnEffectAsync(
      string[] unitIds,
      bool[] isConvert,
      int[] covertPieces)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new UnitGetWindowController.\u003CSpawnEffectAsync\u003Ec__Iterator0()
      {
        unitIds = unitIds,
        isConvert = isConvert,
        covertPieces = covertPieces,
        \u0024this = this
      };
    }
  }
}
