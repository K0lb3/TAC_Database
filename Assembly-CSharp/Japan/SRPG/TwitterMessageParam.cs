﻿// Decompiled with JetBrains decompiler
// Type: SRPG.TwitterMessageParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class TwitterMessageParam
  {
    private eTwitterMessageId id;
    private TwitterMessageDetailParam[] detail;

    public eTwitterMessageId Id
    {
      get
      {
        return this.id;
      }
    }

    public TwitterMessageDetailParam[] Detail
    {
      get
      {
        return this.detail;
      }
    }

    public void Deserialize(JSON_TwitterMessageParam json)
    {
      this.id = (eTwitterMessageId) json.id;
      this.detail = new TwitterMessageDetailParam[json.detail.Length];
      for (int index = 0; index < json.detail.Length; ++index)
      {
        TwitterMessageDetailParam messageDetailParam = new TwitterMessageDetailParam();
        messageDetailParam.Deserialize(json.detail[index]);
        this.detail[index] = messageDetailParam;
      }
    }

    public static void Deserialize(ref TwitterMessageParam[] param, JSON_TwitterMessageParam[] json)
    {
      if (json == null)
        return;
      param = new TwitterMessageParam[json.Length];
      for (int index = 0; index < json.Length; ++index)
      {
        TwitterMessageParam twitterMessageParam = new TwitterMessageParam();
        twitterMessageParam.Deserialize(json[index]);
        param[index] = twitterMessageParam;
      }
    }
  }
}
