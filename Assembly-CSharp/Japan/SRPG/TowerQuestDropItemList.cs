﻿// Decompiled with JetBrains decompiler
// Type: SRPG.TowerQuestDropItemList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SRPG
{
  public class TowerQuestDropItemList : QuestDropItemList
  {
    protected override void Refresh()
    {
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) this.ItemTemplate, (UnityEngine.Object) null))
        return;
      for (int index = this.mItems.Count - 1; index >= 0; --index)
        UnityEngine.Object.Destroy((UnityEngine.Object) this.mItems[index]);
      QuestParam dataOfClass = DataSource.FindDataOfClass<QuestParam>(((Component) this).get_gameObject(), (QuestParam) null);
      if (dataOfClass == null)
        return;
      List<TowerRewardItem> towerRewardItem = MonoSingleton<GameManager>.Instance.FindTowerReward(MonoSingleton<GameManager>.Instance.FindTowerFloor(dataOfClass.iname).reward_id).GetTowerRewardItem();
      for (int rewardIndex = 0; rewardIndex < towerRewardItem.Count; ++rewardIndex)
      {
        TowerRewardItem reward = towerRewardItem[rewardIndex];
        if (reward != null && !reward.IsDisableReward)
        {
          GameObject rewardObject = this.CreateRewardObject();
          if (!UnityEngine.Object.op_Equality((UnityEngine.Object) rewardObject, (UnityEngine.Object) null))
          {
            this.mItems.Add(rewardObject);
            this.BindData(rewardObject, reward, rewardIndex);
          }
        }
      }
    }

    private bool IsDisableReward(TowerRewardItem reward)
    {
      return reward == null || !reward.visible || reward.type == TowerRewardItem.RewardType.Gold;
    }

    private GameObject CreateRewardObject()
    {
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) this.ItemTemplate, (UnityEngine.Object) null))
        return (GameObject) null;
      GameObject gameObject = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) this.ItemTemplate);
      gameObject.get_transform().SetParent(((Component) this).get_transform(), false);
      gameObject.SetActive(true);
      return gameObject;
    }

    private void BindData(GameObject obj, TowerRewardItem reward, int rewardIndex)
    {
      DataSource.Bind<TowerRewardItem>(obj, reward, false);
      foreach (GameParameter componentsInChild in (GameParameter[]) obj.GetComponentsInChildren<GameParameter>())
        componentsInChild.Index = rewardIndex;
      TowerRewardUI componentInChildren1 = (TowerRewardUI) obj.GetComponentInChildren<TowerRewardUI>();
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) componentInChildren1, (UnityEngine.Object) null))
        componentInChildren1.Refresh();
      if (reward.type != TowerRewardItem.RewardType.Artifact)
        return;
      ArtifactIcon componentInChildren2 = (ArtifactIcon) obj.GetComponentInChildren<ArtifactIcon>();
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) componentInChildren2, (UnityEngine.Object) null))
        return;
      ArtifactParam artifactParam = MonoSingleton<GameManager>.Instance.MasterParam.GetArtifactParam(reward.iname);
      DataSource.Bind<ArtifactParam>(obj, artifactParam, false);
      ((Behaviour) componentInChildren2).set_enabled(true);
      componentInChildren2.UpdateValue();
      if (MonoSingleton<GameManager>.Instance.Player.Artifacts.Find((Predicate<ArtifactData>) (x => x.ArtifactParam.iname == reward.iname)) == null)
        reward.is_new = true;
      else
        reward.is_new = false;
    }
  }
}
