﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqGuildRaidRankingReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Network.Encoding;
using MessagePack;
using System;

namespace SRPG
{
  public class ReqGuildRaidRankingReward : WebAPI
  {
    public ReqGuildRaidRankingReward(
      SRPG.Network.ResponseCallback response,
      EncodingTypes.ESerializeCompressMethod serializeCompressMethod)
    {
      this.name = "guildraid/reward";
      this.body = WebAPI.GetRequestString(string.Empty);
      this.callback = response;
      this.serializeCompressMethod = serializeCompressMethod;
    }

    [Serializable]
    public class RewardResponse
    {
      public Json_RaidRankRewardInfoData my_info;
      public string reward_id;
    }

    [MessagePackObject(true)]
    [Serializable]
    public class Response
    {
      public int status;
      public int period_id;
      public JSON_GuildRaidGuildRanking my_guild_info;
    }
  }
}
