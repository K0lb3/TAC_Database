﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ArtiFilterItemFilter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SRPG
{
  public class ArtiFilterItemFilter : MonoBehaviour
  {
    [SerializeField]
    private Toggle ToggleFilter;
    private int mIndex;
    private FilterArtifactParam.Condition mCondition;

    public ArtiFilterItemFilter()
    {
      base.\u002Ector();
    }

    public int Index
    {
      get
      {
        return this.mIndex;
      }
    }

    public FilterArtifactParam.Condition Condition
    {
      get
      {
        return this.mCondition;
      }
    }

    public bool IsOn
    {
      get
      {
        return Object.op_Implicit((Object) this.ToggleFilter) && this.ToggleFilter.get_isOn();
      }
      set
      {
        if (!Object.op_Implicit((Object) this.ToggleFilter))
          return;
        this.ToggleFilter.set_isOn(value);
      }
    }

    public void SetToggleSilent(bool val)
    {
      GameUtility.SetToggle(this.ToggleFilter, val);
    }

    public void SetItem(
      int index,
      FilterArtifactParam.Condition condition,
      UnityAction<bool> action = null)
    {
      if (index < 0 || condition == null)
        return;
      this.mIndex = index;
      this.mCondition = condition;
      DataSource.Bind<FilterUtility.FilterBindData>(((Component) this).get_gameObject(), new FilterUtility.FilterBindData(condition.Rarity, condition.Name, condition.EquipType, (byte) 0), false);
      if (action == null || !Object.op_Implicit((Object) this.ToggleFilter))
        return;
      ((UnityEvent<bool>) this.ToggleFilter.onValueChanged).AddListener(action);
    }
  }
}
