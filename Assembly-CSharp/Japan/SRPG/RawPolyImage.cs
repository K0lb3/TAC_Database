﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RawPolyImage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace SRPG
{
  public class RawPolyImage : RawImage
  {
    public Quad[] Quads;
    public bool Transparent;
    public string Preview;
    private RectTransform mRectTransform;

    public RawPolyImage()
    {
      base.\u002Ector();
    }

    protected virtual void Awake()
    {
      ((UIBehaviour) this).Awake();
      this.mRectTransform = (RectTransform) ((Component) this).GetComponent<RectTransform>();
    }

    protected virtual void OnPopulateMesh(VertexHelper vh)
    {
      vh.Clear();
      if (Object.op_Equality((Object) this.get_texture(), (Object) null) && this.Transparent || ((Graphic) this).get_color().a <= 0.0)
        return;
      UIVertex uiVertex = (UIVertex) null;
      Rect rect = this.mRectTransform.get_rect();
      Rect uvRect = this.get_uvRect();
      Color32 color32 = Color32.op_Implicit(((Graphic) this).get_color());
      int num = 0;
      if (color32.r == (int) byte.MaxValue && color32.g == (int) byte.MaxValue && (color32.b == (int) byte.MaxValue && color32.a == (int) byte.MaxValue))
      {
        for (int index = this.Quads.Length - 1; index >= 0; --index)
        {
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector3&) ref uiVertex.position).x = (__Null) (double) Mathf.Lerp(((Rect) ref rect).get_xMin(), ((Rect) ref rect).get_xMax(), (float) this.Quads[index].v0.x);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector3&) ref uiVertex.position).y = (__Null) (double) Mathf.Lerp(((Rect) ref rect).get_yMin(), ((Rect) ref rect).get_yMax(), (float) this.Quads[index].v0.y);
          uiVertex.color = (__Null) this.Quads[index].c0;
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector2&) ref uiVertex.uv0).x = (__Null) (double) Mathf.Lerp(((Rect) ref uvRect).get_xMin(), ((Rect) ref uvRect).get_xMax(), (float) this.Quads[index].v0.x);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector2&) ref uiVertex.uv0).y = (__Null) (double) Mathf.Lerp(((Rect) ref uvRect).get_yMin(), ((Rect) ref uvRect).get_yMax(), (float) this.Quads[index].v0.y);
          vh.AddVert(uiVertex);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector3&) ref uiVertex.position).x = (__Null) (double) Mathf.Lerp(((Rect) ref rect).get_xMin(), ((Rect) ref rect).get_xMax(), (float) this.Quads[index].v1.x);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector3&) ref uiVertex.position).y = (__Null) (double) Mathf.Lerp(((Rect) ref rect).get_yMin(), ((Rect) ref rect).get_yMax(), (float) this.Quads[index].v1.y);
          uiVertex.color = (__Null) this.Quads[index].c1;
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector2&) ref uiVertex.uv0).x = (__Null) (double) Mathf.Lerp(((Rect) ref uvRect).get_xMin(), ((Rect) ref uvRect).get_xMax(), (float) this.Quads[index].v1.x);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector2&) ref uiVertex.uv0).y = (__Null) (double) Mathf.Lerp(((Rect) ref uvRect).get_yMin(), ((Rect) ref uvRect).get_yMax(), (float) this.Quads[index].v1.y);
          vh.AddVert(uiVertex);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector3&) ref uiVertex.position).x = (__Null) (double) Mathf.Lerp(((Rect) ref rect).get_xMin(), ((Rect) ref rect).get_xMax(), (float) this.Quads[index].v2.x);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector3&) ref uiVertex.position).y = (__Null) (double) Mathf.Lerp(((Rect) ref rect).get_yMin(), ((Rect) ref rect).get_yMax(), (float) this.Quads[index].v2.y);
          uiVertex.color = (__Null) this.Quads[index].c2;
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector2&) ref uiVertex.uv0).x = (__Null) (double) Mathf.Lerp(((Rect) ref uvRect).get_xMin(), ((Rect) ref uvRect).get_xMax(), (float) this.Quads[index].v2.x);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector2&) ref uiVertex.uv0).y = (__Null) (double) Mathf.Lerp(((Rect) ref uvRect).get_yMin(), ((Rect) ref uvRect).get_yMax(), (float) this.Quads[index].v2.y);
          vh.AddVert(uiVertex);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector3&) ref uiVertex.position).x = (__Null) (double) Mathf.Lerp(((Rect) ref rect).get_xMin(), ((Rect) ref rect).get_xMax(), (float) this.Quads[index].v3.x);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector3&) ref uiVertex.position).y = (__Null) (double) Mathf.Lerp(((Rect) ref rect).get_yMin(), ((Rect) ref rect).get_yMax(), (float) this.Quads[index].v3.y);
          uiVertex.color = (__Null) this.Quads[index].c3;
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector2&) ref uiVertex.uv0).x = (__Null) (double) Mathf.Lerp(((Rect) ref uvRect).get_xMin(), ((Rect) ref uvRect).get_xMax(), (float) this.Quads[index].v3.x);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector2&) ref uiVertex.uv0).y = (__Null) (double) Mathf.Lerp(((Rect) ref uvRect).get_yMin(), ((Rect) ref uvRect).get_yMax(), (float) this.Quads[index].v3.y);
          vh.AddVert(uiVertex);
          vh.AddTriangle(num, num + 1, num + 2);
          vh.AddTriangle(num + 2, num + 3, num);
          num += 4;
        }
      }
      else
      {
        for (int index = this.Quads.Length - 1; index >= 0; --index)
        {
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector3&) ref uiVertex.position).x = (__Null) (double) Mathf.Lerp(((Rect) ref rect).get_xMin(), ((Rect) ref rect).get_xMax(), (float) this.Quads[index].v0.x);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector3&) ref uiVertex.position).y = (__Null) (double) Mathf.Lerp(((Rect) ref rect).get_yMin(), ((Rect) ref rect).get_yMax(), (float) this.Quads[index].v0.y);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Color32&) ref uiVertex.color).r = (__Null) (int) (byte) (this.Quads[index].c0.r * color32.r / (int) byte.MaxValue);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Color32&) ref uiVertex.color).g = (__Null) (int) (byte) (this.Quads[index].c0.g * color32.g / (int) byte.MaxValue);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Color32&) ref uiVertex.color).b = (__Null) (int) (byte) (this.Quads[index].c0.b * color32.b / (int) byte.MaxValue);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Color32&) ref uiVertex.color).a = (__Null) (int) (byte) (this.Quads[index].c0.a * color32.a / (int) byte.MaxValue);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector2&) ref uiVertex.uv0).x = (__Null) (double) Mathf.Lerp(((Rect) ref uvRect).get_xMin(), ((Rect) ref uvRect).get_xMax(), (float) this.Quads[index].v0.x);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector2&) ref uiVertex.uv0).y = (__Null) (double) Mathf.Lerp(((Rect) ref uvRect).get_yMin(), ((Rect) ref uvRect).get_yMax(), (float) this.Quads[index].v0.y);
          vh.AddVert(uiVertex);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector3&) ref uiVertex.position).x = (__Null) (double) Mathf.Lerp(((Rect) ref rect).get_xMin(), ((Rect) ref rect).get_xMax(), (float) this.Quads[index].v1.x);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector3&) ref uiVertex.position).y = (__Null) (double) Mathf.Lerp(((Rect) ref rect).get_yMin(), ((Rect) ref rect).get_yMax(), (float) this.Quads[index].v1.y);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Color32&) ref uiVertex.color).r = (__Null) (int) (byte) (this.Quads[index].c1.r * color32.r / (int) byte.MaxValue);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Color32&) ref uiVertex.color).g = (__Null) (int) (byte) (this.Quads[index].c1.g * color32.g / (int) byte.MaxValue);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Color32&) ref uiVertex.color).b = (__Null) (int) (byte) (this.Quads[index].c1.b * color32.b / (int) byte.MaxValue);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Color32&) ref uiVertex.color).a = (__Null) (int) (byte) (this.Quads[index].c1.a * color32.a / (int) byte.MaxValue);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector2&) ref uiVertex.uv0).x = (__Null) (double) Mathf.Lerp(((Rect) ref uvRect).get_xMin(), ((Rect) ref uvRect).get_xMax(), (float) this.Quads[index].v1.x);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector2&) ref uiVertex.uv0).y = (__Null) (double) Mathf.Lerp(((Rect) ref uvRect).get_yMin(), ((Rect) ref uvRect).get_yMax(), (float) this.Quads[index].v1.y);
          vh.AddVert(uiVertex);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector3&) ref uiVertex.position).x = (__Null) (double) Mathf.Lerp(((Rect) ref rect).get_xMin(), ((Rect) ref rect).get_xMax(), (float) this.Quads[index].v2.x);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector3&) ref uiVertex.position).y = (__Null) (double) Mathf.Lerp(((Rect) ref rect).get_yMin(), ((Rect) ref rect).get_yMax(), (float) this.Quads[index].v2.y);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Color32&) ref uiVertex.color).r = (__Null) (int) (byte) (this.Quads[index].c2.r * color32.r / (int) byte.MaxValue);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Color32&) ref uiVertex.color).g = (__Null) (int) (byte) (this.Quads[index].c2.g * color32.g / (int) byte.MaxValue);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Color32&) ref uiVertex.color).b = (__Null) (int) (byte) (this.Quads[index].c2.b * color32.b / (int) byte.MaxValue);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Color32&) ref uiVertex.color).a = (__Null) (int) (byte) (this.Quads[index].c2.a * color32.a / (int) byte.MaxValue);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector2&) ref uiVertex.uv0).x = (__Null) (double) Mathf.Lerp(((Rect) ref uvRect).get_xMin(), ((Rect) ref uvRect).get_xMax(), (float) this.Quads[index].v2.x);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector2&) ref uiVertex.uv0).y = (__Null) (double) Mathf.Lerp(((Rect) ref uvRect).get_yMin(), ((Rect) ref uvRect).get_yMax(), (float) this.Quads[index].v2.y);
          vh.AddVert(uiVertex);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector3&) ref uiVertex.position).x = (__Null) (double) Mathf.Lerp(((Rect) ref rect).get_xMin(), ((Rect) ref rect).get_xMax(), (float) this.Quads[index].v3.x);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector3&) ref uiVertex.position).y = (__Null) (double) Mathf.Lerp(((Rect) ref rect).get_yMin(), ((Rect) ref rect).get_yMax(), (float) this.Quads[index].v3.y);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Color32&) ref uiVertex.color).r = (__Null) (int) (byte) (this.Quads[index].c3.r * color32.r / (int) byte.MaxValue);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Color32&) ref uiVertex.color).g = (__Null) (int) (byte) (this.Quads[index].c3.g * color32.g / (int) byte.MaxValue);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Color32&) ref uiVertex.color).b = (__Null) (int) (byte) (this.Quads[index].c3.b * color32.b / (int) byte.MaxValue);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Color32&) ref uiVertex.color).a = (__Null) (int) (byte) (this.Quads[index].c3.a * color32.a / (int) byte.MaxValue);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector2&) ref uiVertex.uv0).x = (__Null) (double) Mathf.Lerp(((Rect) ref uvRect).get_xMin(), ((Rect) ref uvRect).get_xMax(), (float) this.Quads[index].v3.x);
          // ISSUE: cast to a reference type
          // ISSUE: explicit reference operation
          (^(Vector2&) ref uiVertex.uv0).y = (__Null) (double) Mathf.Lerp(((Rect) ref uvRect).get_yMin(), ((Rect) ref uvRect).get_yMax(), (float) this.Quads[index].v3.y);
          vh.AddVert(uiVertex);
          vh.AddTriangle(num, num + 1, num + 2);
          vh.AddTriangle(num + 2, num + 3, num);
          num += 4;
        }
      }
    }
  }
}
