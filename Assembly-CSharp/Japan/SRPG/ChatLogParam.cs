﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ChatLogParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class ChatLogParam
  {
    public long id;
    public byte message_type;
    public string fuid;
    public string uid;
    public string icon;
    public string skin_iname;
    public string job_iname;
    public string message;
    public int stamp_id;
    public string name;
    public long posted_at;
    public long gid;
    public string guild_name;
    public int lower_level;
    public int is_auto_approval;
    public string award_id;
    public int report_id;

    public ChatLogParam.eChatMessageType messageType
    {
      get
      {
        return (ChatLogParam.eChatMessageType) this.message_type;
      }
      set
      {
        this.message_type = (byte) value;
      }
    }

    public enum eChatMessageType : byte
    {
      UNKNOWN,
      MESSAGE,
      STAMP,
      SYSTEM,
      GUILD_INVITE,
      SYSTEM_GUILD,
      GUILDRAID_PARTY,
    }
  }
}
