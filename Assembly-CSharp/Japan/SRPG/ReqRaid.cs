﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqRaid
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class ReqRaid : WebAPI
  {
    public ReqRaid(Network.ResponseCallback response)
    {
      this.name = "raidboss";
      this.body = WebAPI.GetRequestString(string.Empty);
      this.callback = response;
    }

    [Serializable]
    public class Response
    {
      public int period_id;
      public int round;
      public int area_id;
      public int is_area_reward;
      public int is_raid_complete_reward;
      public Json_RaidBP bp;
      public JSON_RaidBossData raidboss_current;
      public JSON_RaidBossData rescue_current;
      public JSON_RaidBossInfo[] raidboss_knock_down;
    }
  }
}
