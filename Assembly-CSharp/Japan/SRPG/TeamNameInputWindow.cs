﻿// Decompiled with JetBrains decompiler
// Type: SRPG.TeamNameInputWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class TeamNameInputWindow : MonoBehaviour
  {
    [SerializeField]
    private InputFieldCensorship inputField;

    public TeamNameInputWindow()
    {
      base.\u002Ector();
    }

    public void SetInputName()
    {
      if (string.IsNullOrEmpty(this.inputField.text))
        return;
      string str = this.inputField.text;
      if (str.Length > this.inputField.get_characterLimit())
        str = str.Substring(0, this.inputField.get_characterLimit());
      GlobalVars.TeamName = str;
    }
  }
}
