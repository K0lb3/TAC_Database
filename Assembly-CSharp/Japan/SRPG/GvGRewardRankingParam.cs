﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GvGRewardRankingParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System;
using System.Collections.Generic;

namespace SRPG
{
  public class GvGRewardRankingParam : GvGMasterParam<JSON_GvGRewardRankingParam>
  {
    public int PeriodId { get; private set; }

    public List<GvGRewardRankingDetailParam> RewardDetail { get; private set; }

    public override bool Deserialize(JSON_GvGRewardRankingParam json)
    {
      if (json == null)
        return false;
      this.PeriodId = json.period_id;
      this.RewardDetail = new List<GvGRewardRankingDetailParam>();
      if (json.reward_detail != null)
      {
        for (int index = 0; index < json.reward_detail.Length; ++index)
        {
          GvGRewardRankingDetailParam rankingDetailParam = new GvGRewardRankingDetailParam();
          if (rankingDetailParam.Deserialize(json.reward_detail[index]))
            this.RewardDetail.Add(rankingDetailParam);
        }
      }
      return true;
    }

    public static GvGRewardRankingParam GetRewardRanking(int periodId = 0)
    {
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) MonoSingleton<GameManager>.Instance, (UnityEngine.Object) null))
        return (GvGRewardRankingParam) null;
      if (periodId == 0)
      {
        GvGPeriodParam gvGperiod = GvGPeriodParam.GetGvGPeriod();
        if (gvGperiod == null)
          return (GvGRewardRankingParam) null;
        periodId = gvGperiod.Id;
      }
      List<GvGRewardRankingParam> grewardRankingParam = MonoSingleton<GameManager>.Instance.mGvGRewardRankingParam;
      if (grewardRankingParam != null)
        return grewardRankingParam.Find((Predicate<GvGRewardRankingParam>) (r => r != null && r.PeriodId == periodId));
      DebugUtility.Log("<color=yellow>QuestParam/mGvGRewardRankingParam no data!</color>");
      return (GvGRewardRankingParam) null;
    }
  }
}
