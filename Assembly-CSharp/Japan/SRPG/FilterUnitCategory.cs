﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FilterUnitCategory
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class FilterUnitCategory : MonoBehaviour
  {
    [SerializeField]
    private Text m_HeaderText;
    [SerializeField]
    private GameObject m_ToggleTemplate;

    public FilterUnitCategory()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      GameUtility.SetGameObjectActive(this.m_ToggleTemplate, false);
    }

    public void SetHeaderText(string headerText)
    {
      this.m_HeaderText.set_text(headerText);
    }

    public Toggle CreateFilterButton(FilterUnitConditionParam filterConditionParam)
    {
      GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.m_ToggleTemplate, ((Component) this).get_transform(), false);
      Toggle componentInChildren = (Toggle) gameObject.GetComponentInChildren<Toggle>();
      FilterUtility.FilterBindData data = new FilterUtility.FilterBindData(filterConditionParam.rarity_ini, filterConditionParam.name, ArtifactTypes.None, (byte) 0);
      DataSource.Bind<FilterUtility.FilterBindData>(gameObject, data, false);
      DataSource.Bind<FilterUnitConditionParam>(gameObject, filterConditionParam, false);
      return componentInChildren;
    }
  }
}
