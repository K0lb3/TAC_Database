﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildAttendanceRewardInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;

namespace SRPG
{
  public class GuildAttendanceRewardInfo : MonoBehaviour
  {
    [SerializeField]
    [Header("報酬リストアイテムの親")]
    private Transform RewardListRoot;
    [SerializeField]
    [Header("報酬リストアイテムのテンプレート")]
    private GameObject RewardListItemTemplate;

    public GuildAttendanceRewardInfo()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
      GameUtility.SetGameObjectActive(this.RewardListItemTemplate, false);
    }

    private void Start()
    {
      GuildAttendParam[] guildAttendParams = MonoSingleton<GameManager>.Instance.MasterParam.GuildAttendParams;
      if (guildAttendParams == null)
        return;
      GuildAttendParam guildAttendParam1 = (GuildAttendParam) null;
      long serverTime = Network.GetServerTime();
      for (int index = 0; index < guildAttendParams.Length; ++index)
      {
        GuildAttendParam guildAttendParam2 = guildAttendParams[index];
        if (guildAttendParam2 != null && guildAttendParam2.start_at <= serverTime && serverTime < guildAttendParam2.end_at)
        {
          guildAttendParam1 = guildAttendParam2;
          break;
        }
      }
      this.Refresh(guildAttendParam1);
    }

    private void Refresh(GuildAttendParam param)
    {
      if (param == null || Object.op_Equality((Object) this.RewardListRoot, (Object) null) || (Object.op_Equality((Object) this.RewardListItemTemplate, (Object) null) || param.rewards == null))
        return;
      for (int index = 0; index < param.rewards.Length; ++index)
      {
        GuildAttendRewardDetail reward = param.rewards[index];
        if (reward != null)
        {
          GuildAttendRewardParam attendRewardParam = MonoSingleton<GameManager>.Instance.MasterParam.GetGuildAttendRewardParam(reward.reward_id);
          if (attendRewardParam != null)
          {
            GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.RewardListItemTemplate);
            if (Object.op_Inequality((Object) gameObject, (Object) null))
            {
              gameObject.get_transform().SetParent(this.RewardListRoot, false);
              GuildAttendanceRewardInfoListItem component = (GuildAttendanceRewardInfoListItem) gameObject.GetComponent<GuildAttendanceRewardInfoListItem>();
              if (Object.op_Inequality((Object) component, (Object) null))
                component.Setup(reward.member_cnt, attendRewardParam);
            }
            GameUtility.SetGameObjectActive(gameObject, true);
          }
        }
      }
    }
  }
}
