﻿// Decompiled with JetBrains decompiler
// Type: SRPG.LimitedShopLock
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  [RequireComponent(typeof (Selectable))]
  public class LimitedShopLock : MonoBehaviour
  {
    [SerializeField]
    private GameObject LockObject;
    private Button mButton;

    public LimitedShopLock()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
      Button component = (Button) ((Component) this).GetComponent<Button>();
      if (!Object.op_Inequality((Object) component, (Object) null))
        return;
      this.mButton = component;
    }

    private void Start()
    {
      this.UpdateLockState();
    }

    private void UpdateLockState()
    {
      if (Object.op_Equality((Object) this.mButton, (Object) null))
        return;
      this.LockObject.SetActive(!((Selectable) this.mButton).get_interactable());
    }
  }
}
