﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RaidPeriodTimeScheduleParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;

namespace SRPG
{
  [MessagePackObject(true)]
  public class RaidPeriodTimeScheduleParam
  {
    private string mBegin;
    private string mOpen;

    public string Begin
    {
      get
      {
        return this.mBegin;
      }
    }

    public string Open
    {
      get
      {
        return this.mOpen;
      }
    }

    public bool Deserialize(JSON_RaidPeriodTimeScheduleParam json)
    {
      this.mBegin = json.begin_time;
      this.mOpen = json.open_time;
      return true;
    }
  }
}
