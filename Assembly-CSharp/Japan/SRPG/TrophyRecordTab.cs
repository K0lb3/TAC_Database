﻿// Decompiled with JetBrains decompiler
// Type: SRPG.TrophyRecordTab
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class TrophyRecordTab : MonoBehaviour
  {
    [SerializeField]
    private GameObject Badge;
    [SerializeField]
    private Image Body;
    [SerializeField]
    private Text Text;
    [SerializeField]
    private GameObject Cursor;
    [SerializeField]
    private Image Banner;
    [HideInInspector]
    public TrophyCategoryData CategoryData;
    [HideInInspector]
    public int Index;

    public TrophyRecordTab()
    {
      base.\u002Ector();
    }

    public int HashCode
    {
      get
      {
        return this.CategoryData.Param.hash_code;
      }
    }

    private void Awake()
    {
      if (Object.op_Inequality((Object) this.Badge, (Object) null))
        this.Badge.SetActive(false);
      if (!Object.op_Inequality((Object) this.Cursor, (Object) null))
        return;
      this.Cursor.SetActive(false);
    }

    public void Init(string title, Sprite banner = null)
    {
      ((Component) this).get_gameObject().SetActive(true);
      if (Object.op_Inequality((Object) this.Text, (Object) null))
        this.Text.set_text(title);
      if (Object.op_Inequality((Object) this.Banner, (Object) null) && Object.op_Inequality((Object) banner, (Object) null))
      {
        this.Banner.set_sprite(banner);
        ((Component) this.Banner).get_gameObject().SetActive(true);
        ((Behaviour) this.Body).set_enabled(false);
      }
      else
      {
        ((Component) this.Banner).get_gameObject().SetActive(false);
        ((Behaviour) this.Body).set_enabled(true);
      }
    }

    public void Setup(int _index)
    {
      this.Index = _index;
    }

    public void RefreshDisplayParam()
    {
      int num = 0;
      for (int index = 0; index < this.CategoryData.Trophies.Count; ++index)
      {
        if (this.CategoryData.Trophies[index].IsCompleted)
          ++num;
      }
      if (!Object.op_Inequality((Object) this.Badge, (Object) null))
        return;
      this.Badge.SetActive(num > 0);
    }

    public void SetCategoryData(TrophyCategoryData _category_data)
    {
      this.CategoryData = _category_data;
    }

    public void SetCursor(bool isOn)
    {
      if (!Object.op_Inequality((Object) this.Cursor, (Object) null))
        return;
      this.Cursor.SetActive(isOn);
    }
  }
}
