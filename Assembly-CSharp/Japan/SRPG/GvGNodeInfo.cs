﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GvGNodeInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  [FlowNode.Pin(1, "Initialize", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(11, "Refresh", FlowNode.PinTypes.Input, 11)]
  public class GvGNodeInfo : MonoBehaviour, IFlowInterface
  {
    public const int PIN_INPUT_INIT = 1;
    public const int PIN_INPUT_REFRESH = 11;
    [SerializeField]
    private Text NodeNameText;
    [SerializeField]
    private RewardListItem RewardItem;
    [SerializeField]
    private GameObject MapParent;
    [SerializeField]
    private GameObject GuildParent;
    [SerializeField]
    private Text OccupyDay;
    [SerializeField]
    private Text OccupyPoint;
    [SerializeField]
    private List<ImageArray> ImageArrayList;
    [Space(10f)]
    [SerializeField]
    private List<GameObject> NPCOffObjectList;
    [Space(10f)]
    [SerializeField]
    private Button DeclareButton;
    [SerializeField]
    private Button AttackButton;
    [SerializeField]
    private Button DefenseSetButton;
    [Space(10f)]
    [SerializeField]
    private ImageArray mDefensePartyIcon;
    private GvGNodeData CurrentNode;
    private List<RewardListItem> RewardItemList;

    public GvGNodeInfo()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
      if (!UnityEngine.Object.op_Inequality((UnityEngine.Object) GvGManager.Instance, (UnityEngine.Object) null))
        return;
      GvGManager.Instance.SetAutoRefreshStatus(GvGManager.GvGAutoRefreshState.Stop);
    }

    private void OnDestroy()
    {
      if (!UnityEngine.Object.op_Inequality((UnityEngine.Object) GvGManager.Instance, (UnityEngine.Object) null))
        return;
      GvGManager.Instance.RevertAutoRefreshStatus();
    }

    public void Activated(int pinID)
    {
      if (pinID != 1)
      {
        if (pinID != 11)
          return;
        this.Refresh();
      }
      else
      {
        this.Initialize();
        this.Refresh();
      }
    }

    private void Initialize()
    {
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) MonoSingleton<GameManager>.Instance, (UnityEngine.Object) null) || UnityEngine.Object.op_Equality((UnityEngine.Object) GvGManager.Instance, (UnityEngine.Object) null) || (GvGManager.Instance.NodeDataList == null || GvGManager.Instance.OtherGuildList == null))
        return;
      this.CurrentNode = GvGManager.Instance.NodeDataList.Find((Predicate<GvGNodeData>) (n => n.NodeId == GvGManager.Instance.SelectNodeId));
      if (this.CurrentNode == null || UnityEngine.Object.op_Equality((UnityEngine.Object) this.MapParent, (UnityEngine.Object) null))
        return;
      DataSource.Bind<QuestParam>(this.MapParent, Array.Find<QuestParam>(MonoSingleton<GameManager>.Instance.Quests, (Predicate<QuestParam>) (q => q.iname == this.CurrentNode.NodeParam.QuestId)), false);
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) this.RewardItem, (UnityEngine.Object) null))
        return;
      ((Component) this.RewardItem).get_gameObject().SetActive(false);
      this.RewardItemList.ForEach((Action<RewardListItem>) (ri => UnityEngine.Object.Destroy((UnityEngine.Object) ((Component) ri).get_gameObject())));
      this.RewardItemList.Clear();
      GvGRewardParam rewardNode = this.CurrentNode.NodeParam.GetRewardNode();
      if (rewardNode == null)
        return;
      for (int index = 0; index < rewardNode.Rewards.Count; ++index)
      {
        if (rewardNode.Rewards[index] != null)
        {
          RewardListItem listItem = (RewardListItem) UnityEngine.Object.Instantiate<RewardListItem>((M0) this.RewardItem, ((Component) this.RewardItem).get_transform().get_parent());
          this.SetRewardIcon(rewardNode.Rewards[index], listItem);
          ((Component) listItem).get_gameObject().SetActive(true);
          this.RewardItemList.Add(listItem);
        }
      }
    }

    private void Refresh()
    {
      GvGManager gvgm = GvGManager.Instance;
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) gvgm, (UnityEngine.Object) null))
        return;
      this.ImageArrayList.ForEach((Action<ImageArray>) (imageArray =>
      {
        if (UnityEngine.Object.op_Equality((UnityEngine.Object) imageArray, (UnityEngine.Object) null))
          return;
        imageArray.ImageIndex = Mathf.Clamp((int) gvgm.GetNodeImageIndex(this.CurrentNode), 0, imageArray.Images.Length - 1);
      }));
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.NodeNameText, (UnityEngine.Object) null))
        this.NodeNameText.set_text(this.CurrentNode.NodeParam.Name);
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) this.GuildParent, (UnityEngine.Object) null))
        return;
      DataSource.Bind<ViewGuildData>(this.GuildParent, GvGManager.Instance.FindViewGuild(this.CurrentNode.GuildId), false);
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) this.OccupyDay, (UnityEngine.Object) null))
        return;
      if (this.CurrentNode.GuildId == 0)
      {
        this.OccupyDay.set_text("-");
        this.NPCOffObjectList.ForEach((Action<GameObject>) (go => GameUtility.SetGameObjectActive(go, false)));
      }
      else
      {
        this.OccupyDay.set_text((TimeManager.ServerTime - this.CurrentNode.CaptureTime).Days.ToString());
        this.NPCOffObjectList.ForEach((Action<GameObject>) (go => GameUtility.SetGameObjectActive(go, true)));
      }
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.OccupyPoint, (UnityEngine.Object) null))
        this.OccupyPoint.set_text(this.CurrentNode.NodeParam.Point.ToString());
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mDefensePartyIcon, (UnityEngine.Object) null))
      {
        if (this.CurrentNode.DefensePartyNum == 0)
        {
          GameUtility.SetGameObjectActive((Component) this.mDefensePartyIcon, false);
        }
        else
        {
          GameUtility.SetGameObjectActive((Component) this.mDefensePartyIcon, true);
          this.mDefensePartyIcon.ImageIndex = GvGManager.Instance.GetDefensePartyIconIndex(this.CurrentNode);
        }
      }
      this.RefreshButtons();
    }

    private void SetRewardIcon(GvGRewardDetailParam reward, RewardListItem listItem)
    {
      if (reward == null || UnityEngine.Object.op_Equality((UnityEngine.Object) listItem, (UnityEngine.Object) null))
        return;
      GameManager instance = MonoSingleton<GameManager>.Instance;
      GameObject gameObject = (GameObject) null;
      bool flag = false;
      switch (reward.Type)
      {
        case RaidRewardType.Item:
          ItemParam itemParam = instance.GetItemParam(reward.IName);
          if (itemParam != null)
          {
            gameObject = listItem.RewardItem;
            flag = true;
            DataSource.Bind<ItemParam>(gameObject, itemParam, false);
            break;
          }
          break;
        case RaidRewardType.Gold:
          gameObject = listItem.RewardGold;
          flag = true;
          break;
        case RaidRewardType.Coin:
          gameObject = listItem.RewardCoin;
          flag = true;
          break;
        case RaidRewardType.Award:
          AwardParam awardParam = instance.GetAwardParam(reward.IName);
          if (awardParam == null)
            return;
          gameObject = listItem.RewardAward;
          DataSource.Bind<AwardParam>(gameObject, awardParam, false);
          break;
        case RaidRewardType.Unit:
          UnitParam unitParam = instance.GetUnitParam(reward.IName);
          if (unitParam != null)
          {
            gameObject = listItem.RewardUnit;
            DataSource.Bind<UnitParam>(gameObject, unitParam, false);
            break;
          }
          break;
        case RaidRewardType.ConceptCard:
          ConceptCardData cardDataForDisplay = ConceptCardData.CreateConceptCardDataForDisplay(reward.IName);
          if (cardDataForDisplay != null)
          {
            gameObject = listItem.RewardCard;
            ConceptCardIcon component = (ConceptCardIcon) gameObject.GetComponent<ConceptCardIcon>();
            if (UnityEngine.Object.op_Inequality((UnityEngine.Object) component, (UnityEngine.Object) null))
            {
              component.Setup(cardDataForDisplay);
              break;
            }
            break;
          }
          break;
        case RaidRewardType.Artifact:
          ArtifactParam artifactParam = instance.MasterParam.GetArtifactParam(reward.IName);
          if (artifactParam != null)
          {
            gameObject = listItem.RewardArtifact;
            DataSource.Bind<ArtifactParam>(gameObject, artifactParam, false);
            break;
          }
          break;
      }
      if (flag)
      {
        Transform transform = gameObject.get_transform().Find("amount/Text_amount");
        if (UnityEngine.Object.op_Inequality((UnityEngine.Object) transform, (UnityEngine.Object) null))
        {
          Text component = (Text) ((Component) transform).GetComponent<Text>();
          if (UnityEngine.Object.op_Inequality((UnityEngine.Object) component, (UnityEngine.Object) null))
            component.set_text(reward.Num.ToString());
        }
      }
      if (!UnityEngine.Object.op_Inequality((UnityEngine.Object) gameObject, (UnityEngine.Object) null))
        return;
      gameObject.get_transform().SetParent(listItem.RewardList, false);
      gameObject.SetActive(true);
    }

    private void RefreshButtons()
    {
      GvGManager instance = GvGManager.Instance;
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) instance, (UnityEngine.Object) null))
        return;
      GameUtility.SetGameObjectActive((Component) this.DeclareButton, instance.CanDeclareNode(this.CurrentNode));
      GameUtility.SetGameObjectActive((Component) this.AttackButton, instance.CanAttackNode(this.CurrentNode));
      GameUtility.SetGameObjectActive((Component) this.DefenseSetButton, this.CurrentNode.State == GvGNodeState.OccupySelf || this.CurrentNode.State == GvGNodeState.DeclaredEnemy);
    }
  }
}
