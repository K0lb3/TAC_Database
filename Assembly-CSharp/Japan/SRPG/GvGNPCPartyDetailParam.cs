﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GvGNPCPartyDetailParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class GvGNPCPartyDetailParam
  {
    public int Order { get; private set; }

    public int Unit1Id { get; private set; }

    public int Unit2Id { get; private set; }

    public int Unit3Id { get; private set; }

    public bool Deserialize(JSON_GvGNPCPartyDetailParam json)
    {
      if (json == null)
        return false;
      this.Order = json.order;
      this.Unit1Id = json.unit1_id;
      this.Unit2Id = json.unit2_id;
      this.Unit3Id = json.unit3_id;
      return true;
    }
  }
}
