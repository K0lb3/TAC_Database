﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqDrawCardExec
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Network.Encoding;
using MessagePack;
using System;

namespace SRPG
{
  public class ReqDrawCardExec : WebAPI
  {
    public ReqDrawCardExec(
      int select_card_index,
      SRPG.Network.ResponseCallback response,
      EncodingTypes.ESerializeCompressMethod serializeCompressMethod)
    {
      this.name = "drawcard/exec";
      this.body = WebAPI.GetRequestString<ReqDrawCardExec.RequestParam>(new ReqDrawCardExec.RequestParam()
      {
        select_card_index = select_card_index
      });
      this.callback = response;
      this.serializeCompressMethod = serializeCompressMethod;
    }

    [Serializable]
    public class RequestParam
    {
      public int select_card_index;
    }

    [MessagePackObject(true)]
    [Serializable]
    public class Response
    {
      public ReqDrawCard.CardInfo draw_info;
      public ReqDrawCard.Response.Status drawcard_current_status;
      public ReqDrawCard.CardInfo.Card[] rewards;
      public Json_PlayerData player;
      public Json_Item[] items;
      public Json_Unit[] units;
      public JSON_ConceptCard[] cards;
      public Json_Artifact[] artifacts;
      public JSON_TrophyProgress[] trophyprogs;
      public JSON_TrophyProgress[] bingoprogs;
    }
  }
}
