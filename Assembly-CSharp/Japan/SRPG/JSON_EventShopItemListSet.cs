﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_EventShopItemListSet
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class JSON_EventShopItemListSet
  {
    public int id;
    public int sold;
    public Json_ShopItemDesc item;
    public JSON_EventShopItemListSet.Cost cost;
    public Json_ShopItemDesc[] children;
    public int isreset;
    public long start;
    public long end;

    public class Cost : Json_ShopItemCost
    {
      public string iname;
    }
  }
}
