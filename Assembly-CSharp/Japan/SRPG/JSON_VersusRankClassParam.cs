﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_VersusRankClassParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_VersusRankClassParam
  {
    public int schedule_id;
    public int type;
    public int up_pt;
    public int down_pt;
    public int down_losing_streak;
    public string reward_id;
    public int win_pt_max;
    public int win_pt_min;
    public int lose_pt_max;
    public int lose_pt_min;
  }
}
