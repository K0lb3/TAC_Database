﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqGetConceptCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Text;

namespace SRPG
{
  public class ReqGetConceptCard : WebAPI
  {
    public ReqGetConceptCard(long last_card_iid, Network.ResponseCallback response)
    {
      this.name = "unit/concept";
      StringBuilder stringBuilder = WebAPI.GetStringBuilder();
      stringBuilder.Append("\"last_iid\":");
      stringBuilder.Append(last_card_iid);
      this.body = WebAPI.GetRequestString(stringBuilder.ToString());
      this.callback = response;
    }
  }
}
