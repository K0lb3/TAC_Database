﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_ResetAutoRepeatQuestData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;

namespace SRPG
{
  [FlowNode.NodeType("AutoRepeatQuest/Reset", 32741)]
  [FlowNode.Pin(0, "自動周回データリセット", FlowNode.PinTypes.Input, 0)]
  [FlowNode.Pin(10, "Finish", FlowNode.PinTypes.Output, 10)]
  public class FlowNode_ResetAutoRepeatQuestData : FlowNode
  {
    private const int PIN_IN_RESET = 0;
    private const int PIN_OT_RESET = 10;

    public override void OnActivate(int pinID)
    {
      if (pinID != 0)
        return;
      if (MonoSingleton<GameManager>.Instance.Player != null && MonoSingleton<GameManager>.Instance.Player.AutoRepeatQuestProgress != null)
        MonoSingleton<GameManager>.Instance.Player.AutoRepeatQuestProgress.Reset();
      this.ActivateOutputLinks(10);
    }
  }
}
