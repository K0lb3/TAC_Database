﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_JukeBoxParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_JukeBoxParam
  {
    public string iname;
    public string sheet;
    public string cue;
    public string section;
    public int default_unlock;
    public int external_link;
    public int rate;
    public int range_unit;
    public string[] quests;
    public string cond_quest;
  }
}
