﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildFacilityLvParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class GuildFacilityLvParam
  {
    public int lv;
    public int base_camp;
    public long guild_shop;

    public bool Deserialize(JSON_GuildFacilityLvParam json)
    {
      this.lv = json.lv;
      this.base_camp = json.base_camp;
      this.guild_shop = json.guild_shop;
      return true;
    }
  }
}
