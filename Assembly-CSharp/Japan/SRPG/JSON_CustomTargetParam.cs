﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_CustomTargetParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_CustomTargetParam
  {
    public string iname;
    public string name;
    public string[] units;
    public string[] jobs;
    public string[] unit_groups;
    public string[] job_groups;
    public string[] concept_card_groups;
    public string first_job;
    public string second_job;
    public string third_job;
    public int sex;
    public int birth_id;
    public int fire;
    public int water;
    public int wind;
    public int thunder;
    public int shine;
    public int dark;
  }
}
