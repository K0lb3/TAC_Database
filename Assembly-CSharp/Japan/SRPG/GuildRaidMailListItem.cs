﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRaidMailListItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class GuildRaidMailListItem
  {
    public int MailId { get; private set; }

    public int Round { get; private set; }

    public string Message { get; private set; }

    public int BossId { get; private set; }

    public string RewardId { get; private set; }

    public GuildRaidRewardType RewardType { get; private set; }

    public bool Deserialize(JSON_GuildRaidMailListItem json)
    {
      if (json == null)
        return false;
      this.MailId = json.mid;
      this.Round = json.round;
      this.Message = json.msg;
      this.BossId = json.boss_id;
      this.RewardId = json.reward_id;
      this.RewardType = (GuildRaidRewardType) json.reward_type;
      return true;
    }
  }
}
