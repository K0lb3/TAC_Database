﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GvGBattleResultUnitContentData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class GvGBattleResultUnitContentData
  {
    public Unit mUnit;
    public int mHp;

    public void Deserilize(Unit unit, int hp)
    {
      this.mUnit = unit;
      this.mHp = hp;
    }
  }
}
