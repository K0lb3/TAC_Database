﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_GuildRaidPeriodParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_GuildRaidPeriodParam : JSON_GuildRaidMasterParam
  {
    public int id;
    public int bp;
    public int round_max;
    public int round_buff_max;
    public int heal_ap;
    public int unit_lv_min;
    public string begin_at;
    public string end_at;
    public string reward_end_at;
    public string reward_ranking_begin_at;
    public string reward_ranking_end_at;
    public string reward_ranking_id;
    public JSON_GuildRaidPeriodTime[] schedule;
    public int default_bp;
    public int bp_type;
  }
}
