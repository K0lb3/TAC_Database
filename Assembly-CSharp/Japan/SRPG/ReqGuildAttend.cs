﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqGuildAttend
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Network.Encoding;
using MessagePack;
using System;

namespace SRPG
{
  public class ReqGuildAttend : WebAPI
  {
    public ReqGuildAttend(
      ReqGuildAttend.RequestParam rp,
      SRPG.Network.ResponseCallback response,
      EncodingTypes.ESerializeCompressMethod serializeCompressMethod)
    {
      this.name = "guild/attend";
      this.body = WebAPI.GetRequestString<ReqGuildAttend.RequestParam>(rp);
      this.callback = response;
      this.serializeCompressMethod = serializeCompressMethod;
    }

    [MessagePackObject(true)]
    [Serializable]
    public class RequestParam
    {
      public long gid;

      public RequestParam()
      {
      }

      public RequestParam(long guild_id)
      {
        this.gid = guild_id;
      }
    }

    [MessagePackObject(true)]
    [Serializable]
    public class Response
    {
      public int yesterday_attendance;
      public string[] attend_guild_member_uids;
      public int status;
      public JSON_GuildAttendReward[] rewards;
      public Json_PlayerData player;
      public Json_Item[] items;
      public JSON_ConceptCard[] cards;
      public Json_RuneData[] runes;
      public int rune_storage_used;
    }
  }
}
