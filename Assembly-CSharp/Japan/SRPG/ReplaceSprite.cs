﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReplaceSprite
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System.Collections.Generic;

namespace SRPG
{
  [MessagePackObject(true)]
  public class ReplaceSprite
  {
    public List<ReplacePeriod> mPeriod = new List<ReplacePeriod>();
    public string mIname;

    public static void Deserialize(ref List<ReplaceSprite> ref_params, JSON_ReplaceSprite[] json)
    {
      if (ref_params == null)
        ref_params = new List<ReplaceSprite>();
      ref_params.Clear();
      if (json == null)
        return;
      foreach (JSON_ReplaceSprite json1 in json)
      {
        ReplaceSprite replaceSprite = new ReplaceSprite();
        replaceSprite.Deserialize(json1);
        ref_params.Add(replaceSprite);
      }
    }

    public void Deserialize(JSON_ReplaceSprite json)
    {
      if (json == null || json.periods == null)
        return;
      this.mIname = json.iname;
      foreach (JSON_ReplacePeriod period in json.periods)
      {
        ReplacePeriod replacePeriod = new ReplacePeriod();
        replacePeriod.Deserialize(period);
        this.mPeriod.Add(replacePeriod);
      }
    }
  }
}
