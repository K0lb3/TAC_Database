﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ShopExchangeConfirmWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  [FlowNode.Pin(1, "Refresh", FlowNode.PinTypes.Input, 1)]
  public class ShopExchangeConfirmWindow : MonoBehaviour, IFlowInterface
  {
    private const int PIN_IN_REFRESH = 1;
    [SerializeField]
    private Text ResultText;
    [SerializeField]
    private GameObject ItemIcon;

    public ShopExchangeConfirmWindow()
    {
      base.\u002Ector();
    }

    public void Activated(int pinID)
    {
      if (pinID != 1)
        return;
      this.Refresh();
    }

    private void Refresh()
    {
      if (!(FlowNode_ButtonEvent.currentValue is SerializeValueList currentValue))
        return;
      string key = currentValue.GetString("unit");
      int num = currentValue.GetInt("piecenum");
      string str = (string) null;
      ItemParam itemParam = MonoSingleton<GameManager>.Instance.MasterParam.GetItemParam(key);
      if (itemParam != null)
      {
        DataSource.Bind<ItemParam>(this.ItemIcon, itemParam, false);
        str = itemParam.name;
      }
      this.ResultText.set_text(LocalizedText.Get("sys.PIECE_CONVERT_TO_UNIT_RESULT_MSG", (object) str, (object) num));
      GameParameter.UpdateAll(((Component) this).get_gameObject());
    }
  }
}
