﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RuneDrawEnhanceLevel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class RuneDrawEnhanceLevel : MonoBehaviour
  {
    [SerializeField]
    private Text mLevelNow;
    [SerializeField]
    private Text mLevelNext;
    private BindRuneData mRuneData;
    private int mAddLevel;

    public RuneDrawEnhanceLevel()
    {
      base.\u002Ector();
    }

    public void Awake()
    {
    }

    public void SetDrawParam(BindRuneData rune_data, int add_level = 0)
    {
      this.mRuneData = rune_data;
      this.mAddLevel = add_level;
      this.Refresh();
    }

    public void Refresh()
    {
      if (this.mRuneData == null)
        return;
      if (Object.op_Inequality((Object) this.mLevelNow, (Object) null))
        this.mLevelNow.set_text(this.mRuneData.EnhanceNum.ToString());
      if (!Object.op_Inequality((Object) this.mLevelNext, (Object) null))
        return;
      this.mLevelNext.set_text(Mathf.Min(this.mRuneData.EnhanceNum + this.mAddLevel, MonoSingleton<GameManager>.Instance.MasterParam.FixParam.RuneEnhNextNum).ToString());
    }
  }
}
