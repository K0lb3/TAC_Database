﻿// Decompiled with JetBrains decompiler
// Type: SRPG.SupportSettingUsedList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class SupportSettingUsedList : MonoBehaviour
  {
    [SerializeField]
    private Text mTextForm;
    [SerializeField]
    private Text mTextTimes;
    [SerializeField]
    private Text mTextLast;
    [SerializeField]
    private Text mTextGold;
    [SerializeField]
    private ImageArray mElement;
    [SerializeField]
    private GameObject mLevel;
    [SerializeField]
    private GameObject mOverGold;

    public SupportSettingUsedList()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
      GameUtility.SetGameObjectActive(this.mOverGold, false);
      this.Refresh();
    }

    private void Refresh()
    {
      SupportUnitUsed dataOfClass = DataSource.FindDataOfClass<SupportUnitUsed>(((Component) this).get_gameObject(), (SupportUnitUsed) null);
      if (dataOfClass == null)
        return;
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mTextForm, (UnityEngine.Object) null) && dataOfClass.from != DateTime.MinValue)
        this.mTextForm.set_text(string.Format(LocalizedText.Get("sys.SUPPORT_SET_FROM"), (object) dataOfClass.from.Year, (object) dataOfClass.from.Month, (object) dataOfClass.from.Day));
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mTextTimes, (UnityEngine.Object) null) && dataOfClass.times >= 0)
        this.mTextTimes.set_text(string.Format(LocalizedText.Get("sys.SUPPORT_SET_TIMES"), (object) dataOfClass.times));
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mTextLast, (UnityEngine.Object) null) && dataOfClass.last != DateTime.MinValue)
        this.mTextLast.set_text(string.Format(LocalizedText.Get("sys.SUPPORT_SET_LAST"), (object) dataOfClass.last.Year, (object) dataOfClass.last.Month, (object) dataOfClass.last.Day));
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mTextGold, (UnityEngine.Object) null))
      {
        if (UnityEngine.Object.op_Inequality((UnityEngine.Object) SupportSettingUsedWindow.Instance, (UnityEngine.Object) null) && 999999999 <= dataOfClass.gold)
        {
          this.mTextGold.set_text(999999999.ToString());
          GameUtility.SetGameObjectActive(this.mOverGold, true);
        }
        else
          this.mTextGold.set_text(dataOfClass.gold.ToString());
      }
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mElement, (UnityEngine.Object) null))
        this.mElement.ImageIndex = (int) dataOfClass.element;
      if (dataOfClass.unit != null)
        DataSource.Bind<UnitData>(((Component) this).get_gameObject(), dataOfClass.unit, false);
      else
        GameUtility.SetGameObjectActive(this.mLevel, false);
    }
  }
}
