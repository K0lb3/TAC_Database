﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqGuildRequestEntryApprovalOK
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Text;

namespace SRPG
{
  public class ReqGuildRequestEntryApprovalOK : WebAPI
  {
    public ReqGuildRequestEntryApprovalOK(
      string request_user_uid,
      Network.ResponseCallback response)
    {
      this.name = "guild/apply/accept";
      StringBuilder stringBuilder = WebAPI.GetStringBuilder();
      stringBuilder.Append("\"target_uid\":\"");
      stringBuilder.Append(request_user_uid);
      stringBuilder.Append("\"");
      this.body = WebAPI.GetRequestString(stringBuilder.ToString());
      this.callback = response;
    }
  }
}
