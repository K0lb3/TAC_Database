﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RuneDrawBelongings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class RuneDrawBelongings : MonoBehaviour
  {
    [SerializeField]
    private RawImage mItemImg;
    [SerializeField]
    private Text mItemNum;
    private ItemData mItemData;

    public RuneDrawBelongings()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
    }

    public void SetDrawParam(ItemData item_data)
    {
      this.mItemData = item_data;
      this.Refresh();
    }

    public void Refresh()
    {
      if (this.mItemData == null)
        return;
      if (Object.op_Implicit((Object) this.mItemNum))
        this.mItemNum.set_text(this.mItemData.Num.ToString());
      if (Object.op_Implicit((Object) this.mItemImg))
        DataSource.Bind<ItemParam>(((Component) this.mItemImg).get_gameObject(), this.mItemData.Param, false);
      GameParameter.UpdateAll(((Component) this).get_gameObject());
    }
  }
}
