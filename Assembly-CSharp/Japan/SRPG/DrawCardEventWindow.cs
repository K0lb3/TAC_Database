﻿// Decompiled with JetBrains decompiler
// Type: SRPG.DrawCardEventWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  [FlowNode.Pin(1, "IsEnable", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(101, "Enable", FlowNode.PinTypes.Output, 101)]
  [FlowNode.Pin(111, "Disable", FlowNode.PinTypes.Output, 111)]
  public class DrawCardEventWindow : MonoBehaviour, IFlowInterface
  {
    private const int INPUT_PIN_IS_ENABLE = 1;
    private const int OUTPUT_PIN_ENABLE = 101;
    private const int OUTPUT_PIN_DISABLE = 111;
    [SerializeField]
    private GameObject RuleWindow;
    [SerializeField]
    private GameObject DrawCard;
    [SerializeField]
    private GameObject GameStart;

    public DrawCardEventWindow()
    {
      base.\u002Ector();
    }

    public void Activated(int pinID)
    {
      if (pinID != 1)
        return;
      this.Initialize();
    }

    public void Initialize()
    {
      if (Object.op_Inequality((Object) this.DrawCard, (Object) null))
        this.DrawCard.SetActive(false);
      if (Object.op_Inequality((Object) this.GameStart, (Object) null))
        this.GameStart.SetActive(false);
      if (Object.op_Inequality((Object) this.RuleWindow, (Object) null))
        this.RuleWindow.SetActive(false);
      if (DrawCardParam.DrawCardEnabled)
        FlowNode_GameObject.ActivateOutputLinks((Component) this, 101);
      else
        FlowNode_GameObject.ActivateOutputLinks((Component) this, 111);
    }
  }
}
