﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ItemLimitedIconAttach
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class ItemLimitedIconAttach : MonoBehaviour
  {
    private GameObject mLimitedIcon;

    public ItemLimitedIconAttach()
    {
      base.\u002Ector();
    }

    public void Refresh(ItemParam _item_param = null)
    {
      ItemParam itemParam = _item_param ?? this.GetItemParam();
      if (itemParam == null)
        this.Hide();
      else if (!itemParam.IsLimited)
        this.Hide();
      else
        this.Display();
    }

    private ItemParam GetItemParam()
    {
      ItemParam itemParam = (ItemParam) null;
      if (itemParam == null)
      {
        ItemData dataOfClass = DataSource.FindDataOfClass<ItemData>(((Component) this).get_gameObject(), (ItemData) null);
        if (dataOfClass != null)
          itemParam = dataOfClass.Param;
      }
      if (itemParam == null)
      {
        ItemParam dataOfClass = DataSource.FindDataOfClass<ItemParam>(((Component) this).get_gameObject(), (ItemParam) null);
        if (dataOfClass != null)
          itemParam = dataOfClass;
      }
      return itemParam;
    }

    private void Display()
    {
      if (Object.op_Equality((Object) this.mLimitedIcon, (Object) null))
      {
        GameObject limitedIcon = GameSettings.Instance.ItemIcons.LimitedIcon;
        if (!Object.op_Inequality((Object) limitedIcon, (Object) null))
          return;
        this.mLimitedIcon = (GameObject) Object.Instantiate<GameObject>((M0) limitedIcon);
        this.mLimitedIcon.get_transform().SetParent(((Component) this).get_gameObject().get_transform(), false);
      }
      else
        this.mLimitedIcon.SetActive(true);
    }

    public void Hide()
    {
      if (Object.op_Equality((Object) this.mLimitedIcon, (Object) null))
        return;
      this.mLimitedIcon.SetActive(false);
    }
  }
}
