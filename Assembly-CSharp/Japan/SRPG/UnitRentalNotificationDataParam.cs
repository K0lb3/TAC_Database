﻿// Decompiled with JetBrains decompiler
// Type: SRPG.UnitRentalNotificationDataParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace SRPG
{
  public class UnitRentalNotificationDataParam
  {
    private int[] mPers;
    private string mSysId;

    public List<int> PerList
    {
      get
      {
        return this.mPers != null ? new List<int>((IEnumerable<int>) this.mPers) : new List<int>();
      }
    }

    public string SysId
    {
      get
      {
        return this.mSysId;
      }
    }

    public void Deserialize(JSON_UnitRentalNotificationDataParam json)
    {
      if (json == null)
        return;
      if (json.pers != null)
      {
        this.mPers = new int[json.pers.Length];
        for (int index = 0; index < json.pers.Length; ++index)
          this.mPers[index] = json.pers[index];
      }
      this.mSysId = json.sys_id;
    }
  }
}
