﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RuneStatusList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class RuneStatusList : StatusList
  {
    [SerializeField]
    private ImageSpriteSheet m_SetEffectIcon;
    [SerializeField]
    private Text m_SetEffectName;

    public void SetRuneSetEffect(int iconIndex, string name)
    {
      this.m_SetEffectIcon.SetSprite(iconIndex.ToString());
      this.m_SetEffectName.set_text(name);
    }
  }
}
