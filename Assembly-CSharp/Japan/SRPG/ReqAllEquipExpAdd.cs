﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqAllEquipExpAdd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Network.Encoding;
using MessagePack;
using System;

namespace SRPG
{
  public class ReqAllEquipExpAdd : WebAPI
  {
    public ReqAllEquipExpAdd(
      long iid,
      SRPG.Network.ResponseCallback response,
      EncodingTypes.ESerializeCompressMethod serializeCompressMethod)
    {
      this.name = "unit/job/equip/bulk_enforce_max";
      this.body = WebAPI.GetRequestString<ReqAllEquipExpAdd.RequestParam>(new ReqAllEquipExpAdd.RequestParam()
      {
        iid = iid
      });
      this.callback = response;
      this.serializeCompressMethod = serializeCompressMethod;
    }

    [Serializable]
    public class RequestParam
    {
      public long iid;
    }

    [MessagePackObject(true)]
    [Serializable]
    public class Response
    {
      public Json_PlayerData player;
      public Json_Unit[] units;
      public Json_Item[] items;
    }
  }
}
