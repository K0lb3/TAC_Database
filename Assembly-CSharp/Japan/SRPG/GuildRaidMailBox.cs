﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRaidMailBox
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  [FlowNode.Pin(0, "Refresh", FlowNode.PinTypes.Input, 0)]
  [FlowNode.Pin(1, "Receive All", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(10, "Received", FlowNode.PinTypes.Output, 10)]
  public class GuildRaidMailBox : MonoBehaviour, IFlowInterface
  {
    private const int PIN_IN_REFRESH = 0;
    private const int PIN_IN_RECEIVE_ALL = 1;
    private const int PIN_OUT_RECEIVE = 10;
    [SerializeField]
    private Button ReceiveAllButton;
    [SerializeField]
    private GuildRaidMailBoxItem mTemplate;
    private List<GuildRaidMailBoxItem> mItemList;

    public GuildRaidMailBox()
    {
      base.\u002Ector();
    }

    public void Activated(int pinID)
    {
      if (pinID == 0)
      {
        this.Refresh();
      }
      else
      {
        if (pinID != 1)
          return;
        this.OnReceiveAllItem();
      }
    }

    private void Refresh()
    {
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) GuildRaidManager.Instance, (UnityEngine.Object) null) || GuildRaidManager.Instance.MailCurrentPageItemList == null)
        return;
      this.mItemList.ForEach((Action<GuildRaidMailBoxItem>) (item => UnityEngine.Object.Destroy((UnityEngine.Object) ((Component) item).get_gameObject())));
      this.mItemList.Clear();
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mTemplate, (UnityEngine.Object) null))
      {
        ((Component) this.mTemplate).get_gameObject().SetActive(false);
        GuildRaidManager.Instance.MailCurrentPageItemList.ForEach((Action<GuildRaidMailListItem>) (mail =>
        {
          GuildRaidMailBoxItem guildRaidMailBoxItem = (GuildRaidMailBoxItem) UnityEngine.Object.Instantiate<GuildRaidMailBoxItem>((M0) this.mTemplate, ((Component) this.mTemplate).get_transform().get_parent());
          guildRaidMailBoxItem.Setup(mail, new SRPG_Button.ButtonClickEvent(this.OnReceiveItem));
          ((Component) guildRaidMailBoxItem).get_gameObject().SetActive(true);
          this.mItemList.Add(guildRaidMailBoxItem);
        }));
      }
      if (!UnityEngine.Object.op_Inequality((UnityEngine.Object) this.ReceiveAllButton, (UnityEngine.Object) null))
        return;
      ((Selectable) this.ReceiveAllButton).set_interactable(this.mItemList.Count > 0);
    }

    private void OnReceiveItem(SRPG_Button button)
    {
      GuildRaidMailListItem dataOfClass = DataSource.FindDataOfClass<GuildRaidMailListItem>(((Component) button).get_gameObject(), (GuildRaidMailListItem) null);
      if (dataOfClass == null)
        return;
      GuildRaidManager instance = GuildRaidManager.Instance;
      instance.ResetMailReceivingIdList();
      instance.AddMailReceivingIdList(dataOfClass.MailId);
      FlowNode_GameObject.ActivateOutputLinks((Component) this, 10);
    }

    private void OnReceiveAllItem()
    {
      GuildRaidManager grm = GuildRaidManager.Instance;
      grm.ResetMailReceivingIdList();
      GuildRaidManager.Instance.MailCurrentPageItemList.ForEach((Action<GuildRaidMailListItem>) (mail => grm.AddMailReceivingIdList(mail.MailId)));
      FlowNode_GameObject.ActivateOutputLinks((Component) this, 10);
    }
  }
}
