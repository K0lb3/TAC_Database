﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRaidData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class GuildRaidData
  {
    public int PeriodId { get; private set; }

    public int Round { get; private set; }

    public GuildRaidBattlePoint BP { get; private set; }

    public GuildRaidBossInfo BossInfo { get; private set; }

    public int CanOpenAreaId { get; private set; }

    public int RefreshWaitSec { get; private set; }

    public bool Deserialize(JSON_GuildRaidData json)
    {
      if (json == null || json.current == null)
        return false;
      this.PeriodId = json.current.period_id;
      this.Round = json.current.round;
      if (json.bp == null)
        return false;
      this.BP.Deserialize(json.bp);
      if (json.bossinfo == null)
        return false;
      this.BossInfo = new GuildRaidBossInfo();
      if (!this.BossInfo.Deserialize(json.bossinfo))
        return false;
      this.CanOpenAreaId = json.can_open_area_id;
      this.RefreshWaitSec = json.refresh_wait_sec;
      return true;
    }
  }
}
