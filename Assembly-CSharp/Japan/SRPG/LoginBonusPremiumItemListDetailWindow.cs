﻿// Decompiled with JetBrains decompiler
// Type: SRPG.LoginBonusPremiumItemListDetailWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class LoginBonusPremiumItemListDetailWindow : MonoBehaviour
  {
    [SerializeField]
    private Transform LimitedItemList;
    [SerializeField]
    private GameObject ItemBase;
    [SerializeField]
    private string IconNamePath;
    private Json_PremiumLoginBonus mPremiumLoginBonus;
    private int mLoginBonusIndex;

    public LoginBonusPremiumItemListDetailWindow()
    {
      base.\u002Ector();
    }

    public void Refresh()
    {
      Json_PremiumLoginBonus[] premiumBonuses = MonoSingleton<GameManager>.Instance.Player.PremiumLoginBonus.premium_bonuses;
      this.mLoginBonusIndex = int.Parse(GlobalVars.SelectedItemID);
      this.mPremiumLoginBonus = premiumBonuses[this.mLoginBonusIndex];
      this.MakeTopItemIcon();
      this.MakeItemIconInPacking();
      ((Behaviour) this).set_enabled(true);
    }

    private void MakeTopItemIcon()
    {
      GameManager instance = MonoSingleton<GameManager>.Instance;
      string str = this.mPremiumLoginBonus.icon;
      bool flag = false;
      if (str == null)
      {
        if (this.mPremiumLoginBonus.item != null && this.mPremiumLoginBonus.item.Length > 0)
          str = this.mPremiumLoginBonus.item[0].iname;
        else if (this.mPremiumLoginBonus.coin > 0)
          str = "$COIN";
        else if (this.mPremiumLoginBonus.gold > 0)
          flag = true;
      }
      SetItemObject component = (SetItemObject) ((Component) this).GetComponent<SetItemObject>();
      if (str != null)
      {
        ItemParam itemParam = MonoSingleton<GameManager>.Instance.MasterParam.GetItemParam(str, false);
        if (itemParam != null)
        {
          DataSource.Bind<ItemParam>(((Component) this).get_gameObject(), itemParam, false);
          component.SetIconActive(GiftTypes.Item);
        }
        ConceptCardParam conceptCardParam = instance.MasterParam.GetConceptCardParam(str);
        if (conceptCardParam != null)
        {
          DataSource.Bind<ConceptCardParam>(((Component) this).get_gameObject(), conceptCardParam, false);
          ConceptCardData cardDataForDisplay = ConceptCardData.CreateConceptCardDataForDisplay(str);
          component.SetupConceptCard(cardDataForDisplay);
          component.SetIconActive(GiftTypes.ConceptCard);
        }
        if (instance.MasterParam.GetArtifactParam(str, false) != null)
        {
          ArtifactParam artifactParam = instance.MasterParam.GetArtifactParam(str);
          if (artifactParam != null)
          {
            DataSource.Bind<ArtifactParam>(((Component) this).get_gameObject(), artifactParam, false);
            component.SetIconActive(GiftTypes.Artifact);
            ((Text) ((Component) component.ArtifactIcon.get_transform().Find(this.IconNamePath)).GetComponent<Text>()).set_text(artifactParam.name);
          }
        }
        if (instance.MasterParam.ContainsUnitID(str))
        {
          UnitParam unitParam = instance.MasterParam.GetUnitParam(str);
          if (unitParam != null)
          {
            DataSource.Bind<UnitParam>(((Component) this).get_gameObject(), unitParam, false);
            component.SetIconActive(GiftTypes.Unit);
            ((Text) ((Component) component.UnitIcon.get_transform().Find(this.IconNamePath)).GetComponent<Text>()).set_text(unitParam.name);
          }
        }
        if (instance.MasterParam.ContainsAwardID(str))
        {
          AwardParam awardParam = instance.MasterParam.GetAwardParam(str, true);
          if (awardParam != null)
          {
            DataSource.Bind<AwardParam>(((Component) this).get_gameObject(), awardParam, false);
            component.SetIconActive(GiftTypes.Award);
            ((Text) ((Component) component.AwardIcon.get_transform().Find(this.IconNamePath)).GetComponent<Text>()).set_text(awardParam.name);
          }
        }
      }
      else if (flag)
      {
        component.SetIconActive(GiftTypes.Gold);
        ((Text) ((Component) component.GoldIcon.get_transform().Find(this.IconNamePath)).GetComponent<Text>()).set_text(this.mPremiumLoginBonus.gold.ToString() + LocalizedText.Get("sys.GOLD"));
      }
      GameParameter.UpdateAll(((Component) this).get_gameObject());
    }

    private void MakeItemIconInPacking()
    {
      GameManager instance = MonoSingleton<GameManager>.Instance;
      this.ItemBase.SetActive(false);
      if (this.mPremiumLoginBonus.item != null)
      {
        for (int index = 0; index < this.mPremiumLoginBonus.item.Length; ++index)
        {
          GameObject root = (GameObject) Object.Instantiate<GameObject>((M0) this.ItemBase);
          root.get_transform().SetParent(this.LimitedItemList, false);
          root.SetActive(true);
          bool flag = false;
          SetItemObject component = (SetItemObject) root.GetComponent<SetItemObject>();
          ItemParam itemParam = MonoSingleton<GameManager>.Instance.MasterParam.GetItemParam(this.mPremiumLoginBonus.item[index].iname, false);
          if (itemParam != null)
          {
            DataSource.Bind<ItemParam>(root, itemParam, false);
            component.SetItemDesc(GiftTypes.Item, itemParam.name, this.mPremiumLoginBonus.item[index].num);
            flag = true;
          }
          ConceptCardParam conceptCardParam = instance.MasterParam.GetConceptCardParam(this.mPremiumLoginBonus.item[index].iname);
          if (conceptCardParam != null)
          {
            DataSource.Bind<ConceptCardParam>(root, conceptCardParam, false);
            ConceptCardData cardDataForDisplay = ConceptCardData.CreateConceptCardDataForDisplay(this.mPremiumLoginBonus.item[index].iname);
            component.SetupConceptCard(cardDataForDisplay);
            component.SetItemDesc(GiftTypes.ConceptCard, conceptCardParam.name, this.mPremiumLoginBonus.item[index].num);
            flag = true;
          }
          if (!flag && instance.MasterParam.GetArtifactParam(this.mPremiumLoginBonus.item[index].iname, false) != null)
          {
            ArtifactParam artifactParam = instance.MasterParam.GetArtifactParam(this.mPremiumLoginBonus.item[index].iname);
            if (artifactParam != null)
            {
              DataSource.Bind<ArtifactParam>(root, artifactParam, false);
              component.SetItemDesc(GiftTypes.Artifact, artifactParam.name, this.mPremiumLoginBonus.item[index].num);
              flag = true;
            }
          }
          if (!flag && instance.MasterParam.ContainsUnitID(this.mPremiumLoginBonus.item[index].iname))
          {
            UnitParam unitParam = instance.MasterParam.GetUnitParam(this.mPremiumLoginBonus.item[index].iname);
            if (unitParam != null)
            {
              DataSource.Bind<UnitParam>(root, unitParam, false);
              component.SetItemDesc(GiftTypes.Unit, unitParam.name, this.mPremiumLoginBonus.item[index].num);
              flag = true;
            }
          }
          if (!flag && instance.MasterParam.ContainsAwardID(this.mPremiumLoginBonus.item[index].iname))
          {
            AwardParam awardParam = instance.MasterParam.GetAwardParam(this.mPremiumLoginBonus.item[index].iname, true);
            if (awardParam != null)
            {
              DataSource.Bind<AwardParam>(root, awardParam, false);
              component.SetItemDesc(GiftTypes.Award, awardParam.name, this.mPremiumLoginBonus.item[index].num);
            }
          }
          GameParameter.UpdateAll(root);
        }
      }
      if (this.mPremiumLoginBonus.coin > 0)
      {
        ItemParam itemParam = MonoSingleton<GameManager>.Instance.MasterParam.GetItemParam("$COIN", false);
        if (itemParam != null)
        {
          GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.ItemBase);
          gameObject.get_transform().SetParent(this.LimitedItemList, false);
          gameObject.SetActive(true);
          SetItemObject component = (SetItemObject) gameObject.GetComponent<SetItemObject>();
          DataSource.Bind<ItemParam>(gameObject, itemParam, false);
          component.SetItemDesc(GiftTypes.Coin, string.Empty, this.mPremiumLoginBonus.coin);
        }
      }
      if (this.mPremiumLoginBonus.gold <= 0)
        return;
      GameObject gameObject1 = (GameObject) Object.Instantiate<GameObject>((M0) this.ItemBase);
      gameObject1.get_transform().SetParent(this.LimitedItemList, false);
      gameObject1.SetActive(true);
      ((SetItemObject) gameObject1.GetComponent<SetItemObject>()).SetItemDesc(GiftTypes.Gold, string.Empty, this.mPremiumLoginBonus.gold);
    }
  }
}
