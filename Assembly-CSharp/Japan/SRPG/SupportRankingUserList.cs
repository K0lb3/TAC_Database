﻿// Decompiled with JetBrains decompiler
// Type: SRPG.SupportRankingUserList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class SupportRankingUserList : MonoBehaviour
  {
    [SerializeField]
    private ImageArray mRankImage;
    [SerializeField]
    private Text mRankText;
    [SerializeField]
    private Text mName;
    [SerializeField]
    private Text mLevel;
    [SerializeField]
    private Text mGuildName;
    [SerializeField]
    private Text mScore;
    [SerializeField]
    private GameObject mGuildNone;
    [SerializeField]
    private GameObject mPlayerLock;
    [SerializeField]
    private GameObject mOverGold;
    public int PLAYER_LOCK;
    private const int DEFAULT_RANKING_NOTEXT = 3;

    public SupportRankingUserList()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
      GameUtility.SetGameObjectActive(this.mGuildNone, false);
      GameUtility.SetGameObjectActive(this.mPlayerLock, false);
      GameUtility.SetGameObjectActive(this.mOverGold, false);
      this.Refresh();
    }

    public void Refresh()
    {
      SupportUserRanking dataOfClass = DataSource.FindDataOfClass<SupportUserRanking>(((Component) this).get_gameObject(), (SupportUserRanking) null);
      if (dataOfClass == null)
        return;
      if (dataOfClass.rank > 0)
      {
        if (Object.op_Inequality((Object) this.mRankImage, (Object) null))
        {
          int num = dataOfClass.rank - 1;
          if (num >= this.mRankImage.Images.Length)
            num = this.mRankImage.Images.Length - 1;
          this.mRankImage.ImageIndex = num;
        }
        if (Object.op_Inequality((Object) this.mRankText, (Object) null))
        {
          if (dataOfClass.rank > 3)
            this.mRankText.set_text(string.Format(LocalizedText.Get("sys.SUPPORT_SET_RANK"), (object) dataOfClass.rank.ToString()));
          else
            this.mRankText.set_text(string.Empty);
        }
      }
      if (Object.op_Inequality((Object) this.mName, (Object) null))
        this.mName.set_text(dataOfClass.name);
      if (Object.op_Inequality((Object) this.mLevel, (Object) null))
        this.mLevel.set_text(dataOfClass.lv.ToString());
      if (Object.op_Inequality((Object) this.mGuildName, (Object) null))
        this.mGuildName.set_text(dataOfClass.guildName);
      if (Object.op_Inequality((Object) this.mScore, (Object) null) && dataOfClass.score >= 0)
      {
        if (Object.op_Inequality((Object) SupportRankingWindow.Instance, (Object) null) && 999999999 <= dataOfClass.score)
        {
          this.mScore.set_text(999999999.ToString());
          GameUtility.SetGameObjectActive(this.mOverGold, true);
        }
        else
          this.mScore.set_text(dataOfClass.score.ToString());
      }
      if (dataOfClass.unit != null)
        DataSource.Bind<UnitData>(((Component) this).get_gameObject(), dataOfClass.unit, false);
      if (!string.IsNullOrEmpty(dataOfClass.award))
        DataSource.Bind<AwardParam>(((Component) this).get_gameObject(), MonoSingleton<GameManager>.Instance.GetAwardParam(dataOfClass.award), false);
      if (Object.op_Inequality((Object) this.mGuildNone, (Object) null) && dataOfClass.guildId == 0)
        GameUtility.SetGameObjectActive(this.mGuildNone, true);
      if (!Object.op_Inequality((Object) this.mPlayerLock, (Object) null) || MonoSingleton<GameManager>.Instance.Player.Lv >= this.PLAYER_LOCK)
        return;
      GameUtility.SetGameObjectActive(this.mPlayerLock, true);
    }
  }
}
