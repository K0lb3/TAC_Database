﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_RaidBossData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_RaidBossData
  {
    public string uid;
    public string name;
    public int area_id;
    public JSON_RaidBossInfo boss_info;
    public int sos_status;
    public JSON_RaidSOSMember[] sos_member;
  }
}
