﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GvGBeatRankingData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;

namespace SRPG
{
  public class GvGBeatRankingData
  {
    public int Rank { get; private set; }

    public int BeatNum { get; private set; }

    public string Name { get; private set; }

    public int Level { get; private set; }

    public UnitData Unit { get; private set; }

    public int Role { get; private set; }

    public ViewGuildData Guild { get; private set; }

    public bool Deserialize(JSON_GvGBeatRanking json)
    {
      if (json == null)
        return false;
      this.Rank = json.rank;
      this.BeatNum = json.beat_num;
      this.Name = json.name;
      this.Level = json.level;
      this.Unit = UnitData.CreateUnitDataForDisplay(MonoSingleton<GameManager>.Instance.GetUnitParam(json.unit));
      if (this.Unit != null)
        this.Unit.SetJobSkinAll(json.skin, false);
      this.Role = json.role;
      GvGManager instance = GvGManager.Instance;
      this.Guild = (ViewGuildData) null;
      if (Object.op_Equality((Object) instance, (Object) null))
        return false;
      if (instance.MyGuild.id == json.gid)
      {
        this.Guild = instance.MyGuild;
      }
      else
      {
        for (int index = 0; index < instance.OtherGuildList.Count; ++index)
        {
          if (instance.OtherGuildList[index].id == json.gid)
          {
            this.Guild = instance.OtherGuildList[index];
            break;
          }
        }
      }
      return true;
    }
  }
}
