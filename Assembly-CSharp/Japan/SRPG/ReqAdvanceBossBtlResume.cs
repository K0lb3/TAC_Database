﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqAdvanceBossBtlResume
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class ReqAdvanceBossBtlResume : WebAPI
  {
    public ReqAdvanceBossBtlResume(long btlid, Network.ResponseCallback response)
    {
      this.name = "advance/raidboss/btl/resume";
      this.body = WebAPI.GetRequestString<ReqAdvanceBossBtlResume.RequestParam>(new ReqAdvanceBossBtlResume.RequestParam()
      {
        btlid = btlid
      });
      this.callback = response;
    }

    [Serializable]
    public class RequestParam
    {
      public long btlid;
    }
  }
}
