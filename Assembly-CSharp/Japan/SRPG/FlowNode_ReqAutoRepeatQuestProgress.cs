﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_ReqAutoRepeatQuestProgress
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using Gsc.Network.Encoding;
using MessagePack;
using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("AutoRepeatQuest/ReqProgress", 32741)]
  [FlowNode.Pin(10, "進捗取得開始", FlowNode.PinTypes.Input, 10)]
  [FlowNode.Pin(110, "進捗取得終了", FlowNode.PinTypes.Output, 110)]
  public class FlowNode_ReqAutoRepeatQuestProgress : FlowNode_Network
  {
    private const int PIN_INPUT_GET_PROGRESS_START = 10;
    private const int PIN_OUTPUT_GET_PROGRESS_END = 110;

    public override void OnActivate(int pinID)
    {
      if (pinID == 10)
        this.RequestProgress();
      ((Behaviour) this).set_enabled(true);
    }

    private void RequestProgress()
    {
      if (!MonoSingleton<GameManager>.Instance.Player.AutoRepeatQuestProgress.IsExistRecord)
        return;
      this.ExecRequest((WebAPI) new ReqAutoRepeatQuestProgress(MonoSingleton<GameManager>.Instance.Player.AutoRepeatQuestProgress.CurrentLap, new SRPG.Network.ResponseCallback(((FlowNode_Network) this).ResponseCallback), EncodingTypes.ESerializeCompressMethod.TYPED_MESSAGE_PACK));
    }

    public override void OnSuccess(WWWResult www)
    {
      if (SRPG.Network.IsError)
      {
        int errCode = (int) SRPG.Network.ErrCode;
        FlowNode_Network.Failed();
      }
      else
      {
        ReqAutoRepeatQuestProgress.Response body;
        if (EncodingTypes.IsJsonSerializeCompressSelected(!GlobalVars.SelectedSerializeCompressMethodWasNodeSet ? EncodingTypes.ESerializeCompressMethod.TYPED_MESSAGE_PACK : GlobalVars.SelectedSerializeCompressMethod))
        {
          WebAPI.JSON_BodyResponse<ReqAutoRepeatQuestProgress.Response> jsonObject = JSONParser.parseJSONObject<WebAPI.JSON_BodyResponse<ReqAutoRepeatQuestProgress.Response>>(www.text);
          DebugUtility.Assert(jsonObject != null, "jsonRes == null");
          body = jsonObject.body;
        }
        else
        {
          FlowNode_ReqAutoRepeatQuestProgress.MP_AutoRepeatQuestProgressResponse progressResponse = SerializerCompressorHelper.Decode<FlowNode_ReqAutoRepeatQuestProgress.MP_AutoRepeatQuestProgressResponse>(www.rawResult, true, EncodingTypes.GetCompressModeFromSerializeCompressMethod(EncodingTypes.ESerializeCompressMethod.TYPED_MESSAGE_PACK), false, true);
          DebugUtility.Assert(progressResponse != null, "mpRes == null");
          body = progressResponse.body;
        }
        SRPG.Network.RemoveAPI();
        if (body != null)
        {
          MonoSingleton<GameManager>.Instance.Player.Deserialize(body.auto_repeat, false);
          MonoSingleton<GameManager>.Instance.Player.SetAutoRepeatQuestBox(body.box_extension_count);
          ((Behaviour) this).set_enabled(false);
          this.ActivateOutputLinks(110);
        }
        else
          FlowNode_Network.Failed();
      }
    }

    [MessagePackObject(true)]
    public class MP_AutoRepeatQuestProgressResponse : WebAPI.JSON_BaseResponse
    {
      public ReqAutoRepeatQuestProgress.Response body;
    }
  }
}
