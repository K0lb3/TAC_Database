﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ConceptCardNotSaleCheck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class ConceptCardNotSaleCheck : MonoBehaviour
  {
    [SerializeField]
    private GameObject mCardObjectTemplate;
    [SerializeField]
    private RectTransform mCardObjectParent;

    public ConceptCardNotSaleCheck()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      if (Object.op_Equality((Object) this.mCardObjectTemplate, (Object) null) && Object.op_Equality((Object) this.mCardObjectParent, (Object) null))
      {
        Debug.LogWarning((object) "mCardObject is null");
      }
      else
      {
        this.mCardObjectTemplate.SetActive(false);
        ConceptCardManager instance = ConceptCardManager.Instance;
        if (Object.op_Equality((Object) instance, (Object) null))
          return;
        foreach (ConceptCardData card in instance.SelectedMaterials.GetList())
        {
          if (card.Param.not_sale)
          {
            GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.mCardObjectTemplate);
            gameObject.get_transform().SetParent((Transform) this.mCardObjectParent, false);
            ConceptCardIcon component = (ConceptCardIcon) gameObject.GetComponent<ConceptCardIcon>();
            if (Object.op_Inequality((Object) component, (Object) null))
              component.Setup(card);
            gameObject.SetActive(true);
          }
        }
      }
    }
  }
}
