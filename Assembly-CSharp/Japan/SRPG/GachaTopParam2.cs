﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GachaTopParam2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace SRPG
{
  public class GachaTopParam2
  {
    public string iname;
    public string category;
    public int coin;
    public int coin_p;
    public int gold;
    public int gold_p;
    public List<UnitParam> units;
    public int num;
    public string ticket;
    public int ticket_num;
    public bool step;
    public int step_num;
    public int step_index;
    public bool limit;
    public int limit_num;
    public int limit_stock;
    public string type;
    public string asset_title;
    public string asset_bg;
    public string group;
    public string btext;
    public string confirm;
  }
}
