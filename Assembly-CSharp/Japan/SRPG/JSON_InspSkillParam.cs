﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_InspSkillParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_InspSkillParam
  {
    public string iname;
    public string ability;
    public int gen_id;
    public string[] triggers;
    public JSON_InspSkillDerivation[] derivation;
    public int ctr_min;
    public int ctr_max;
    public int enable_passive;
  }
}
