﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GvGNPCUnitParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System;
using System.Collections.Generic;

namespace SRPG
{
  public class GvGNPCUnitParam : VersusDraftUnitParam
  {
    public bool Deserialize(JSON_GvGNPCUnitParam json)
    {
      if (json == null)
        return false;
      json.draft_unit_id = json.id;
      return this.Deserialize(json.id, (JSON_VersusDraftUnitParam) json);
    }

    public GvGPartyUnit CreateUnitData()
    {
      Json_Unit jsonUnit = this.GetJson_Unit();
      GvGPartyUnit gvGpartyUnit = new GvGPartyUnit();
      gvGpartyUnit.Deserialize(jsonUnit);
      return gvGpartyUnit;
    }

    public static GvGPartyUnit CreateNPCUnitData(int id)
    {
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) MonoSingleton<GameManager>.Instance, (UnityEngine.Object) null))
        return (GvGPartyUnit) null;
      List<GvGNPCUnitParam> mGvGnpcUnitParam = MonoSingleton<GameManager>.Instance.mGvGNPCUnitParam;
      if (mGvGnpcUnitParam == null)
      {
        DebugUtility.Log(string.Format("<color=yellow>QuestParam/mGvGNPCUnitParam no data!</color>"));
        return (GvGPartyUnit) null;
      }
      return mGvGnpcUnitParam.Find((Predicate<GvGNPCUnitParam>) (u => u != null && u.Id == (long) id))?.CreateUnitData();
    }
  }
}
