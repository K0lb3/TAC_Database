﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ConceptCardTrustRewardParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class ConceptCardTrustRewardParam
  {
    public string iname;
    public ConceptCardTrustRewardItemParam[] rewards;

    public bool Deserialize(JSON_ConceptCardTrustRewardParam json)
    {
      this.iname = json.iname;
      if (json.rewards != null)
      {
        this.rewards = new ConceptCardTrustRewardItemParam[json.rewards.Length];
        for (int index = 0; index < json.rewards.Length; ++index)
        {
          ConceptCardTrustRewardItemParam trustRewardItemParam = new ConceptCardTrustRewardItemParam();
          if (!trustRewardItemParam.Deserialize(json.rewards[index]))
            return false;
          this.rewards[index] = trustRewardItemParam;
        }
      }
      return true;
    }
  }
}
