﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqPlayNew
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using Gsc.Network.Encoding;

namespace SRPG
{
  public class ReqPlayNew : WebAPI
  {
    public ReqPlayNew(
      int debugNumber,
      SRPG.Network.ResponseCallback response,
      EncodingTypes.ESerializeCompressMethod serializeCompressMethod)
    {
      this.name = "playnew";
      this.body = string.Empty;
      string str = string.Empty;
      if (debugNumber > 0)
        str = "\"debug\":" + (object) debugNumber + ",";
      this.body += WebAPI.GetRequestString(str + "\"permanent_id\":\"" + MonoSingleton<GameManager>.Instance.UdId + "\"");
      this.callback = response;
      this.serializeCompressMethod = serializeCompressMethod;
    }
  }
}
