﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ConceptCardFilterTab
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class ConceptCardFilterTab : MonoBehaviour
  {
    [SerializeField]
    private GameObject mFilteredObject;
    [SerializeField]
    private GameObject mDefaultObject;
    [SerializeField]
    private Toggle mToggle;
    private bool m_IsFiltered;

    public ConceptCardFilterTab()
    {
      base.\u002Ector();
    }

    public bool isOn
    {
      get
      {
        return !Object.op_Equality((Object) this.mToggle, (Object) null) && this.mToggle.get_isOn();
      }
      set
      {
        if (Object.op_Equality((Object) this.mToggle, (Object) null))
          return;
        this.mToggle.set_isOn(value);
      }
    }

    public void SetIsFiltered(bool isFiltered)
    {
      this.m_IsFiltered = isFiltered;
      GameUtility.SetGameObjectActive(this.mFilteredObject, this.m_IsFiltered);
      GameUtility.SetGameObjectActive(this.mDefaultObject, !this.m_IsFiltered);
    }
  }
}
