﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Flownode_RaidCheckRescueWaitSeconds
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("Raid/CheckRescueWaitSeconds", 32741)]
  [FlowNode.Pin(1, "Check", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(101, "CanReload", FlowNode.PinTypes.Output, 2)]
  [FlowNode.Pin(102, "UseCache", FlowNode.PinTypes.Output, 3)]
  public class Flownode_RaidCheckRescueWaitSeconds : FlowNode
  {
    public override void OnActivate(int pinID)
    {
      if (pinID != 1)
        return;
      RaidManager instance = RaidManager.Instance;
      if (Object.op_Equality((Object) instance, (Object) null))
        this.ActivateOutputLinks(101);
      else if (instance.RaidRescueMemberList == null)
        this.ActivateOutputLinks(101);
      else
        this.ActivateOutputLinks(102);
    }
  }
}
