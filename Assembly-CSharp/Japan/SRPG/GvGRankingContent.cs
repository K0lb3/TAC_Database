﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GvGRankingContent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class GvGRankingContent : MonoBehaviour
  {
    [SerializeField]
    private int DRAW_STRING_UNDER_RANK;
    [SerializeField]
    private ImageArray mRankImages;
    [SerializeField]
    private Text mRankText;
    [SerializeField]
    private Text mScoreText;

    public GvGRankingContent()
    {
      base.\u002Ector();
    }

    public void Setup(GvGRankingGuildData guild)
    {
      if (Object.op_Inequality((Object) this.mRankImages, (Object) null))
        this.mRankImages.ImageIndex = Mathf.Clamp(guild.Rank - 1, 0, this.mRankImages.Images.Length - 1);
      if (Object.op_Inequality((Object) this.mRankText, (Object) null) && guild.Rank >= this.DRAW_STRING_UNDER_RANK)
        this.mRankText.set_text(string.Format(LocalizedText.Get("sys.GVG_RANKING_RANK"), (object) guild.Rank));
      if (Object.op_Inequality((Object) this.mScoreText, (Object) null))
        this.mScoreText.set_text(guild.Point.ToString());
      DataSource.Bind<ViewGuildData>(((Component) this).get_gameObject(), (ViewGuildData) guild, false);
    }
  }
}
