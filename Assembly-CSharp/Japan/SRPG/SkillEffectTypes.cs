﻿// Decompiled with JetBrains decompiler
// Type: SRPG.SkillEffectTypes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public enum SkillEffectTypes : byte
  {
    None,
    Equipment,
    Attack,
    Defend,
    Heal,
    Buff,
    Debuff,
    Revive,
    Shield,
    ReflectDamage,
    DamageControl,
    FailCondition,
    CureCondition,
    DisableCondition,
    GemsGift,
    GemsIncDec,
    Protect,
    Teleport,
    Changing,
    RateHeal,
    RateDamage,
    PerfectAvoid,
    Throw,
    EffReplace,
    SetTrick,
    TransformUnit,
    SetBreakObj,
    ChangeWeather,
    RateDamageCurrent,
    TransformUnitTakeOverHP,
    DynamicTransformUnit,
  }
}
