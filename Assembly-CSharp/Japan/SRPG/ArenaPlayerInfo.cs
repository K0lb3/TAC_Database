﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ArenaPlayerInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class ArenaPlayerInfo : MonoBehaviour
  {
    [Space(10f)]
    public GameObject unit1;
    public GameObject unit2;
    public GameObject unit3;

    public ArenaPlayerInfo()
    {
      base.\u002Ector();
    }

    private void OnEnable()
    {
      this.UpdateValue();
    }

    public void UpdateValue()
    {
      ArenaPlayer dataOfClass = DataSource.FindDataOfClass<ArenaPlayer>(((Component) this).get_gameObject(), (ArenaPlayer) null);
      if (dataOfClass == null)
        return;
      DataSource.Bind<ArenaPlayer>(this.unit1, dataOfClass, false);
      DataSource.Bind<ArenaPlayer>(this.unit2, dataOfClass, false);
      DataSource.Bind<ArenaPlayer>(this.unit3, dataOfClass, false);
      ((BaseIcon) this.unit1.GetComponent<UnitIcon>()).UpdateValue();
      ((BaseIcon) this.unit2.GetComponent<UnitIcon>()).UpdateValue();
      ((BaseIcon) this.unit3.GetComponent<UnitIcon>()).UpdateValue();
    }
  }
}
