﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ArtiFilterItemTab
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SRPG
{
  public class ArtiFilterItemTab : MonoBehaviour
  {
    [SerializeField]
    private Text TextTab;
    [SerializeField]
    private Toggle ToggleTab;
    [SerializeField]
    private GameObject GoFiltered;
    [SerializeField]
    private GameObject GoDefault;
    private int mIndex;
    private FilterArtifactParam mParam;

    public ArtiFilterItemTab()
    {
      base.\u002Ector();
    }

    public int Index
    {
      get
      {
        return this.mIndex;
      }
    }

    public FilterArtifactParam Param
    {
      get
      {
        return this.mParam;
      }
    }

    public bool IsOn
    {
      get
      {
        return Object.op_Implicit((Object) this.ToggleTab) && this.ToggleTab.get_isOn();
      }
      set
      {
        if (!Object.op_Implicit((Object) this.ToggleTab))
          return;
        this.ToggleTab.set_isOn(value);
      }
    }

    public void SetToggleSilent(bool val)
    {
      GameUtility.SetToggle(this.ToggleTab, val);
    }

    public void SetItem(int index, FilterArtifactParam param, UnityAction<bool> action = null)
    {
      if (index < 0 || param == null)
        return;
      this.mIndex = index;
      this.mParam = param;
      if (Object.op_Implicit((Object) this.TextTab))
        this.TextTab.set_text(param.TabName);
      if (action == null || !Object.op_Implicit((Object) this.ToggleTab))
        return;
      ((UnityEvent<bool>) this.ToggleTab.onValueChanged).AddListener(action);
    }

    public void SetIsFiltered(bool is_filtered)
    {
      GameUtility.SetGameObjectActive(this.GoFiltered, is_filtered);
      GameUtility.SetGameObjectActive(this.GoDefault, !is_filtered);
    }
  }
}
