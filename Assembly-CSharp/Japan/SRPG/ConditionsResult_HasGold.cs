﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ConditionsResult_HasGold
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;

namespace SRPG
{
  public class ConditionsResult_HasGold : ConditionsResult
  {
    public ConditionsResult_HasGold(int condsNum)
    {
      this.mCurrentValue = MonoSingleton<GameManager>.Instance.Player.Gold;
      this.mTargetValue = condsNum;
      this.mIsClear = this.mCurrentValue >= this.mTargetValue;
    }

    public override string text
    {
      get
      {
        return string.Empty;
      }
    }

    public override string errorText
    {
      get
      {
        return LocalizedText.Get("sys.GOLD_NOT_ENOUGH");
      }
    }
  }
}
