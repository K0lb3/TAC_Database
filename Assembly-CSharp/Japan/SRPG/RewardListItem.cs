﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RewardListItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class RewardListItem : ListItemEvents
  {
    public GameObject RewardUnit;
    public GameObject RewardItem;
    public GameObject RewardCard;
    public GameObject RewardArtifact;
    public GameObject RewardAward;
    public GameObject RewardGold;
    public GameObject RewardCoin;
    public GameObject RewardEmblem;
    public Transform RewardList;

    public void AllNotActive()
    {
      GameUtility.SetGameObjectActive(this.RewardUnit, false);
      GameUtility.SetGameObjectActive(this.RewardItem, false);
      GameUtility.SetGameObjectActive(this.RewardCard, false);
      GameUtility.SetGameObjectActive(this.RewardArtifact, false);
      GameUtility.SetGameObjectActive(this.RewardAward, false);
      GameUtility.SetGameObjectActive(this.RewardGold, false);
      GameUtility.SetGameObjectActive(this.RewardCoin, false);
      GameUtility.SetGameObjectActive(this.RewardEmblem, false);
    }
  }
}
