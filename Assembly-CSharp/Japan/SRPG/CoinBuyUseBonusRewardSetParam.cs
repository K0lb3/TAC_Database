﻿// Decompiled with JetBrains decompiler
// Type: SRPG.CoinBuyUseBonusRewardSetParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class CoinBuyUseBonusRewardSetParam
  {
    private string iname;
    private CoinBuyUseBonusContentParam[] contents;

    public string Iname
    {
      get
      {
        return this.iname;
      }
    }

    public CoinBuyUseBonusContentParam[] Contents
    {
      get
      {
        return this.contents;
      }
    }

    public void Deserialize(
      JSON_CoinBuyUseBonusRewardSetParam json,
      CoinBuyUseBonusRewardParam[] reward_params)
    {
      this.iname = json.iname;
      if (json.contents == null)
        return;
      this.contents = new CoinBuyUseBonusContentParam[json.contents.Length];
      for (int index = 0; index < json.contents.Length; ++index)
      {
        CoinBuyUseBonusContentParam bonusContentParam = new CoinBuyUseBonusContentParam();
        bonusContentParam.Deserialize(json.contents[index], reward_params);
        this.contents[index] = bonusContentParam;
      }
    }

    public static void Deserialize(
      ref CoinBuyUseBonusRewardSetParam[] param,
      JSON_CoinBuyUseBonusRewardSetParam[] json,
      CoinBuyUseBonusRewardParam[] reward_params)
    {
      if (json == null)
        return;
      param = new CoinBuyUseBonusRewardSetParam[json.Length];
      for (int index = 0; index < json.Length; ++index)
      {
        CoinBuyUseBonusRewardSetParam bonusRewardSetParam = new CoinBuyUseBonusRewardSetParam();
        bonusRewardSetParam.Deserialize(json[index], reward_params);
        param[index] = bonusRewardSetParam;
      }
    }
  }
}
