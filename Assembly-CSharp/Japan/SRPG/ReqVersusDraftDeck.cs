﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqVersusDraftDeck
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class ReqVersusDraftDeck : WebAPI
  {
    public ReqVersusDraftDeck(string token, long deck_id, Network.ResponseCallback response)
    {
      this.name = "vs/draft/deck";
      this.body = WebAPI.GetRequestString<ReqVersusDraftDeck.RequestParam>(new ReqVersusDraftDeck.RequestParam()
      {
        token = token,
        deck_id = deck_id
      });
      this.callback = response;
    }

    [Serializable]
    public class RequestParam
    {
      public string token;
      public long deck_id;
    }
  }
}
