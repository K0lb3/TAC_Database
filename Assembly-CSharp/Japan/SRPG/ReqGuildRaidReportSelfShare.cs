﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqGuildRaidReportSelfShare
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Network.Encoding;
using System;

namespace SRPG
{
  public class ReqGuildRaidReportSelfShare : WebAPI
  {
    public ReqGuildRaidReportSelfShare(
      int gid,
      int report_id,
      SRPG.Network.ResponseCallback response,
      EncodingTypes.ESerializeCompressMethod serializeCompressMethod)
    {
      this.name = "guildraid/report/self/share";
      this.body = WebAPI.GetRequestString<ReqGuildRaidReportSelfShare.RequestParam>(new ReqGuildRaidReportSelfShare.RequestParam()
      {
        gid = gid,
        report_id = report_id
      });
      this.callback = response;
      this.serializeCompressMethod = serializeCompressMethod;
    }

    [Serializable]
    public class RequestParam
    {
      public int gid;
      public int report_id;
    }
  }
}
