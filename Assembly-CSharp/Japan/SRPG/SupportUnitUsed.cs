﻿// Decompiled with JetBrains decompiler
// Type: SRPG.SupportUnitUsed
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class SupportUnitUsed
  {
    public UnitData unit;
    public DateTime from;
    public int times;
    public DateTime last;
    public int gold;
    public EElement element;
  }
}
