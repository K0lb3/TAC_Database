﻿// Decompiled with JetBrains decompiler
// Type: SRPG.LocalVariable
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;
using UnityEngine;

namespace SRPG
{
  [DisallowMultipleComponent]
  public class LocalVariable : MonoBehaviour
  {
    private Dictionary<string, string> mVariables;

    public LocalVariable()
    {
      base.\u002Ector();
    }

    public bool Exists(string key)
    {
      return this.mVariables.ContainsKey(key);
    }

    public void Set(string key, string val)
    {
      if (!this.Exists(key))
        this.mVariables.Add(key, val);
      else
        this.mVariables[key] = val;
    }

    public bool Equal(string key, string val)
    {
      return this.Exists(key) && this.mVariables[key] == val;
    }
  }
}
