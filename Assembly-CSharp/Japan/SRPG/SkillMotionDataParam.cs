﻿// Decompiled with JetBrains decompiler
// Type: SRPG.SkillMotionDataParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace SRPG
{
  public class SkillMotionDataParam
  {
    private List<string> mUnitList;
    private List<string> mJobList;
    private string mMotionId;
    public string mEffectId;
    private SkillMotionDataParam.Flags mFlags;

    public List<string> UnitList
    {
      get
      {
        return this.mUnitList;
      }
    }

    public List<string> JobList
    {
      get
      {
        return this.mJobList;
      }
    }

    public string MotionId
    {
      get
      {
        return this.mMotionId;
      }
    }

    public string EffectId
    {
      get
      {
        return this.mEffectId;
      }
    }

    public bool IsBattleScene
    {
      get
      {
        return (this.mFlags & SkillMotionDataParam.Flags.IsBattleScene) != (SkillMotionDataParam.Flags) 0;
      }
    }

    public void Deserialize(JSON_SkillMotionDataParam json)
    {
      if (json == null)
        return;
      if (json.unit_ids != null)
      {
        this.mUnitList = new List<string>(json.unit_ids.Length);
        for (int index = 0; index < json.unit_ids.Length; ++index)
          this.mUnitList.Add(json.unit_ids[index]);
      }
      if (json.job_ids != null)
      {
        this.mJobList = new List<string>(json.job_ids.Length);
        for (int index = 0; index < json.job_ids.Length; ++index)
          this.mJobList.Add(json.job_ids[index]);
      }
      this.mMotionId = json.motnm;
      this.mEffectId = json.effnm;
      this.mFlags = (SkillMotionDataParam.Flags) 0;
      if (json.isbtl == 0)
        return;
      this.mFlags |= SkillMotionDataParam.Flags.IsBattleScene;
    }

    public enum Flags
    {
      IsBattleScene = 1,
    }
  }
}
