﻿// Decompiled with JetBrains decompiler
// Type: SRPG.BookmarkUnit
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class BookmarkUnit : MonoBehaviour
  {
    public GameObject BookmarkIcon;
    public GameObject Overlay;
    public SRPG_Button Button;
    public Text Challenge;
    public Text Limit;

    public BookmarkUnit()
    {
      base.\u002Ector();
    }
  }
}
