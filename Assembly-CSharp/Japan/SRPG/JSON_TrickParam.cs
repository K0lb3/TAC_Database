﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_TrickParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_TrickParam
  {
    public string iname;
    public string name;
    public string expr;
    public int dmg_type;
    public int dmg_val;
    public int calc;
    public int elem;
    public int atk_det;
    public string buff;
    public string cond;
    public int kb_rate;
    public int kb_val;
    public int target;
    public int visual;
    public int count;
    public int clock;
    public int is_no_ow;
    public string marker;
    public string effect;
    public int eff_target;
    public int eff_shape;
    public int eff_scope;
    public int eff_height;
    public int ig_mt_num;
    public int[] ig_mts;
  }
}
