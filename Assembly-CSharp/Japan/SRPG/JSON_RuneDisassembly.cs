﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_RuneDisassembly
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_RuneDisassembly
  {
    public string success_item;
    public int success_num;
    public string great_item;
    public int great_num;
    public string super_item;
    public int super_num;
  }
}
