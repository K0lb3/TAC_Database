﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqQuestParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Network.Encoding;

namespace SRPG
{
  public class ReqQuestParam : WebAPI
  {
    public ReqQuestParam(
      SRPG.Network.ResponseCallback response,
      EncodingTypes.ESerializeCompressMethod serializeCompressMethod)
    {
      this.name = "mst/10/quest";
      this.callback = response;
      this.serializeCompressMethod = serializeCompressMethod;
    }
  }
}
