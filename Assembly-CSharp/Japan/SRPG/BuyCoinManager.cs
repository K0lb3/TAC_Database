﻿// Decompiled with JetBrains decompiler
// Type: SRPG.BuyCoinManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  [FlowNode.Pin(1, "Initialize", FlowNode.PinTypes.Input, 0)]
  public class BuyCoinManager : MonoBehaviour, IFlowInterface
  {
    public const int PIN_INPUT_INIT = 1;
    [SerializeField]
    private GameObject mTabCoin;
    [SerializeField]
    private GameObject mTabLimited;
    [SerializeField]
    private GameObject mTabCollabo;
    [SerializeField]
    private GameObject mPeriod;
    [SerializeField]
    private Text mPeriodText;
    private static BuyCoinManager mInstance;
    public BuyCoinManager.BuyCoinShopType mNowSelectTab;

    public BuyCoinManager()
    {
      base.\u002Ector();
    }

    public static BuyCoinManager Instance
    {
      get
      {
        return BuyCoinManager.mInstance;
      }
    }

    private void Awake()
    {
      BuyCoinManager.mInstance = this;
    }

    private void OnDestroy()
    {
      BuyCoinManager.mInstance = (BuyCoinManager) null;
    }

    private void Update()
    {
    }

    public void Activated(int pinID)
    {
      if (pinID != 1)
        return;
      this.Init();
    }

    private void Init()
    {
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mPeriod, (UnityEngine.Object) null))
        this.mPeriod.SetActive(false);
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mTabCoin, (UnityEngine.Object) null))
        this.mTabCoin.SetActive(true);
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mTabLimited, (UnityEngine.Object) null))
        this.mTabLimited.SetActive(false);
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mTabCollabo, (UnityEngine.Object) null))
        this.mTabCollabo.SetActive(false);
      List<BuyCoinShopParam> buyCoinShopParam = MonoSingleton<GameManager>.Instance.MasterParam.GetBuyCoinShopParam();
      if (buyCoinShopParam != null)
      {
        for (int index = 0; index < buyCoinShopParam.Count; ++index)
        {
          if (MonoSingleton<GameManager>.Instance.MasterParam.IsBuyCoinShopOpen(buyCoinShopParam[index]))
          {
            GameObject gameObject = (GameObject) null;
            if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mTabCoin, (UnityEngine.Object) null) && buyCoinShopParam[index].ShopType == BuyCoinManager.BuyCoinShopType.COIN)
              gameObject = this.mTabCoin;
            if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mTabLimited, (UnityEngine.Object) null) && buyCoinShopParam[index].ShopType == BuyCoinManager.BuyCoinShopType.LIMITED)
              gameObject = this.mTabLimited;
            if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mTabCollabo, (UnityEngine.Object) null) && buyCoinShopParam[index].ShopType == BuyCoinManager.BuyCoinShopType.COLLABO)
              gameObject = this.mTabCollabo;
            if (UnityEngine.Object.op_Inequality((UnityEngine.Object) gameObject, (UnityEngine.Object) null))
            {
              Text componentInChildren = (Text) gameObject.GetComponentInChildren<Text>();
              if (UnityEngine.Object.op_Inequality((UnityEngine.Object) componentInChildren, (UnityEngine.Object) null) && !string.IsNullOrEmpty(buyCoinShopParam[index].DisplayName))
                componentInChildren.set_text(LocalizedText.Get(buyCoinShopParam[index].DisplayName));
              gameObject.SetActive(true);
              DataSource.Bind<BuyCoinShopParam>(gameObject, buyCoinShopParam[index], false);
            }
          }
        }
      }
      this.mNowSelectTab = BuyCoinManager.BuyCoinShopType.COIN;
    }

    public void OnClickTab(GameObject go)
    {
      BuyCoinShopParam dataOfClass = DataSource.FindDataOfClass<BuyCoinShopParam>(go, (BuyCoinShopParam) null);
      if (dataOfClass == null)
      {
        if (!UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mPeriod, (UnityEngine.Object) null))
          return;
        this.mPeriod.SetActive(false);
      }
      else
      {
        this.mNowSelectTab = dataOfClass.ShopType;
        if (!dataOfClass.AlwaysOpen)
        {
          if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mPeriod, (UnityEngine.Object) null))
          {
            this.mPeriod.SetActive(true);
            DateTime dateTime1 = TimeManager.FromUnixTime(dataOfClass.BeginAt);
            DateTime dateTime2 = TimeManager.FromUnixTime(dataOfClass.EndAt);
            string str1 = LocalizedText.Get("sys.BUYCOIN_PERIODTIME", (object) dateTime1.Year, (object) dateTime1.Month, (object) dateTime1.Day, (object) dateTime1.Hour, (object) dateTime1.Minute);
            string str2 = LocalizedText.Get("sys.BUYCOIN_PERIODTIME", (object) dateTime2.Year, (object) dateTime2.Month, (object) dateTime2.Day, (object) dateTime2.Hour, (object) dateTime2.Minute);
            if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mPeriodText, (UnityEngine.Object) null))
              this.mPeriodText.set_text(LocalizedText.Get("sys.BUYCOIN_PERIOD", (object) str1, (object) str2));
          }
        }
        else if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mPeriod, (UnityEngine.Object) null))
          this.mPeriod.SetActive(false);
        this.GetProduct();
        GlobalEvent.Invoke("CHANGE_TAB", (object) this);
      }
    }

    public PaymentManager.Product[] GetProduct()
    {
      MasterParam masterParam = MonoSingleton<GameManager>.Instance.MasterParam;
      PaymentManager.Product[] products = FlowNode_PaymentGetProducts.Products;
      List<PaymentManager.Product> productList = new List<PaymentManager.Product>();
      if (products == null)
        return products;
      BuyCoinShopParam buyCoinShopParam = masterParam.GetBuyCoinShopParam(this.mNowSelectTab);
      if (buyCoinShopParam != null)
      {
        for (int index = 0; index < products.Length; ++index)
        {
          BuyCoinProductParam coinProductParam = masterParam.GetBuyCoinProductParam(products[index].ID);
          if (coinProductParam != null && coinProductParam.ShopId == buyCoinShopParam.ShopId)
            productList.Add(products[index]);
        }
      }
      return productList.ToArray();
    }

    public bool GetProductBuyConfirm(PaymentManager.Product product)
    {
      return product != null && (product.ID == null || product.remainNum != 0);
    }

    public enum BuyCoinShopType
    {
      COIN,
      LIMITED,
      COLLABO,
      ALL,
    }

    public enum PremiumRewadType
    {
      Item,
      Gold,
      Coin,
      Artifact,
      ConceptCard,
      Unit,
    }

    public enum PremiumRestrictionType
    {
      None,
      AllBuy,
      DayBuy,
    }
  }
}
