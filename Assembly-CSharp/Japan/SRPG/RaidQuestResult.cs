﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RaidQuestResult
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class RaidQuestResult
  {
    public int index;
    public int pexp;
    public int uexp;
    public int gold;
    public QuestResult.DropItemData[] drops;
  }
}
