﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GvGEndWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class GvGEndWindow : MonoBehaviour
  {
    [SerializeField]
    private GvGEndWindow.eGvGEndResultType mResultType;
    [SerializeField]
    private Text mRankText;
    [SerializeField]
    private Text mPointText;
    [SerializeField]
    private Text mCaptureNodeCountText;

    public GvGEndWindow()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
      this.Init();
    }

    private void Init()
    {
      this.Setup(this.GetResultData(this.mResultType));
    }

    private void Setup(GvGResultData result_data)
    {
      if (result_data == null)
        return;
      if (Object.op_Inequality((Object) this.mRankText, (Object) null))
        this.mRankText.set_text(result_data.Rank.ToString());
      if (Object.op_Inequality((Object) this.mPointText, (Object) null))
        this.mPointText.set_text(result_data.Point.ToString());
      if (!Object.op_Inequality((Object) this.mCaptureNodeCountText, (Object) null))
        return;
      this.mCaptureNodeCountText.set_text(result_data.CaptureNodes.Count.ToString());
    }

    private GvGResultData GetResultData(GvGEndWindow.eGvGEndResultType result_type)
    {
      GvGManager instance = GvGManager.Instance;
      if (Object.op_Inequality((Object) instance, (Object) null))
      {
        if (result_type == GvGEndWindow.eGvGEndResultType.Daily)
          return instance.ResultDaily;
        if (result_type == GvGEndWindow.eGvGEndResultType.Season)
          return instance.ResultSeason;
      }
      return (GvGResultData) null;
    }

    public enum eGvGEndResultType
    {
      None,
      Daily,
      Season,
    }
  }
}
