﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqGuildRaidMailRead
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Network.Encoding;
using MessagePack;
using System;

namespace SRPG
{
  public class ReqGuildRaidMailRead : WebAPI
  {
    public ReqGuildRaidMailRead(
      int[] mailids,
      int page,
      int gid,
      SRPG.Network.ResponseCallback response,
      EncodingTypes.ESerializeCompressMethod serializeCompressMethod)
    {
      this.name = "guildraid/mail/read";
      this.body = WebAPI.GetRequestString<ReqGuildRaidMailRead.RequestParam>(new ReqGuildRaidMailRead.RequestParam()
      {
        mailids = mailids,
        page = page,
        gid = gid
      });
      this.callback = response;
      this.serializeCompressMethod = serializeCompressMethod;
    }

    [Serializable]
    public class RequestParam
    {
      public int[] mailids;
      public int page;
      public int gid;
    }

    [MessagePackObject(true)]
    [Serializable]
    public class Response
    {
      public JSON_GuildRaidMail mails;
      public JSON_GuildRaidMailListItem[] processed;
      public JSON_TrophyProgress[] trophyprogs;
      public JSON_TrophyProgress[] bingoprogs;
      public Json_PlayerData player;
      public Json_Item[] items;
      public Json_Unit[] units;
      public Json_Artifact[] artifacts;
      public JSON_ConceptCard[] cards;
      public int[] gift_mailids;
    }
  }
}
