﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_CheckUnitRentalQuestActive
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("UnitRental/Check/RentalQuestActive", 32741)]
  [FlowNode.Pin(1, "IsActive", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(111, "Yes", FlowNode.PinTypes.Output, 111)]
  [FlowNode.Pin(112, "No", FlowNode.PinTypes.Output, 112)]
  public class FlowNode_CheckUnitRentalQuestActive : FlowNode
  {
    private const int PIN_IN_IS_ACTIVE = 1;
    private const int PIN_OUT_YES = 111;
    private const int PIN_OUT_NO = 112;

    public override void OnActivate(int pinID)
    {
      if (pinID != 1)
        return;
      GameManager instance = MonoSingleton<GameManager>.Instance;
      if (Object.op_Implicit((Object) instance))
      {
        UnitData rentalUnit = instance.Player.GetRentalUnit();
        if (rentalUnit != null)
        {
          UnitRentalParam unitRentalParam = UnitRentalParam.GetParam(rentalUnit.RentalIname);
          int rentalFavoritePoint = rentalUnit.RentalFavoritePoint;
          if (unitRentalParam != null)
          {
            foreach (RentalQuestInfo rentalQuestInfo in unitRentalParam.UnitQuestInfo)
            {
              if (rentalQuestInfo != null && rentalQuestInfo.Quest != null && ((int) rentalQuestInfo.Point <= rentalFavoritePoint && rentalQuestInfo.Quest.IsAvailable()) && rentalQuestInfo.Quest.state != QuestStates.Cleared)
              {
                this.ActivateOutputLinks(111);
                return;
              }
            }
          }
        }
      }
      this.ActivateOutputLinks(112);
    }
  }
}
