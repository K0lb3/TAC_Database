﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqGenesisRaidboss
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class ReqGenesisRaidboss : WebAPI
  {
    public ReqGenesisRaidboss(
      string area_id,
      QuestDifficulties difficulty,
      Network.ResponseCallback response)
    {
      this.name = "genesis/raidboss";
      this.body = WebAPI.GetRequestString<ReqGenesisRaidboss.RequestParam>(new ReqGenesisRaidboss.RequestParam()
      {
        area_id = area_id,
        difficulty = (int) difficulty
      });
      this.callback = response;
    }

    [Serializable]
    public class RequestParam
    {
      public string area_id;
      public int difficulty;
    }

    [Serializable]
    public class Response
    {
      public int hp;
      public int round;
    }
  }
}
