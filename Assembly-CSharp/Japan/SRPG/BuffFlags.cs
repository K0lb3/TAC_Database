﻿// Decompiled with JetBrains decompiler
// Type: SRPG.BuffFlags
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public enum BuffFlags
  {
    UpReplenish = 1,
    NoDisabled = 2,
    NoBuffTurn = 4,
    AvoidPerfect = 8,
    AvoidMiss = 16, // 0x00000010
    UpReplenishUseSkillCtr = 32, // 0x00000020
  }
}
