﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqReadTips
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Text;

namespace SRPG
{
  public class ReqReadTips : WebAPI
  {
    public ReqReadTips(
      string tips,
      string trophyprog,
      string bingoprog,
      Network.ResponseCallback response)
    {
      this.name = "tips/end";
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.Append("\"iname\":");
      stringBuilder.Append("\"" + tips + "\"");
      if (!string.IsNullOrEmpty(trophyprog))
      {
        stringBuilder.Append(",");
        stringBuilder.Append(trophyprog);
      }
      if (!string.IsNullOrEmpty(bingoprog))
      {
        stringBuilder.Append(",");
        stringBuilder.Append(bingoprog);
      }
      this.body = WebAPI.GetRequestString(stringBuilder.ToString());
      this.callback = response;
    }
  }
}
