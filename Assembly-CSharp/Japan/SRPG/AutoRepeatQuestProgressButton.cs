﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AutoRepeatQuestProgressButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;

namespace SRPG
{
  public class AutoRepeatQuestProgressButton : MonoBehaviour
  {
    [SerializeField]
    private GameObject mButtonObj;
    [SerializeField]
    private GameObject mBadgeObj;

    public AutoRepeatQuestProgressButton()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
      GameUtility.SetGameObjectActive(this.mButtonObj, false);
      GameUtility.SetGameObjectActive(this.mBadgeObj, false);
    }

    private void Update()
    {
      if (MonoSingleton<GameManager>.Instance.Player.AutoRepeatQuestProgress.IsExistRecord)
      {
        GameUtility.SetGameObjectActive(this.mButtonObj, true);
        if (MonoSingleton<GameManager>.Instance.Player.AutoRepeatQuestProgress.State != AutoRepeatQuestData.eState.AUTO_REPEAT_END)
          return;
        GameUtility.SetGameObjectActive(this.mBadgeObj, true);
      }
      else
      {
        GameUtility.SetGameObjectActive(this.mButtonObj, false);
        GameUtility.SetGameObjectActive(this.mBadgeObj, false);
      }
    }
  }
}
