﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ConceptCardGroupParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace SRPG
{
  public class ConceptCardGroupParam
  {
    public static bool Deserialize(
      JSON_ConceptCardGroup[] json_groups,
      Dictionary<string, ConceptCardParam> concept_card_params,
      ref Dictionary<string, List<ConceptCardParam>> ref_params)
    {
      ref_params = new Dictionary<string, List<ConceptCardParam>>();
      foreach (JSON_ConceptCardGroup jsonGroup in json_groups)
      {
        if (jsonGroup.cards != null && jsonGroup.cards.Length > 0 && !ref_params.ContainsKey(jsonGroup.iname))
        {
          List<ConceptCardParam> conceptCardParamList = new List<ConceptCardParam>();
          for (int index = 0; index < jsonGroup.cards.Length; ++index)
          {
            ConceptCardParam conceptCardParam = (ConceptCardParam) null;
            if (concept_card_params.ContainsKey(jsonGroup.cards[index]))
              conceptCardParam = concept_card_params[jsonGroup.cards[index]];
            if (conceptCardParam != null)
              conceptCardParamList.Add(conceptCardParam);
          }
          if (conceptCardParamList.Count > 0)
            ref_params[jsonGroup.iname] = conceptCardParamList;
        }
      }
      return true;
    }
  }
}
