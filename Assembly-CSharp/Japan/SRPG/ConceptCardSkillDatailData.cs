﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ConceptCardSkillDatailData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class ConceptCardSkillDatailData
  {
    public ConceptCardEquipEffect effect;
    public SkillData skill_data;
    public LearningSkill learning_skill;
    public ConceptCardDetailAbility.ShowType type;

    public ConceptCardSkillDatailData(
      ConceptCardEquipEffect _effect,
      SkillData _data,
      ConceptCardDetailAbility.ShowType _type,
      LearningSkill _learning_skill = null)
    {
      this.skill_data = _data;
      this.effect = _effect;
      this.type = _type;
      this.learning_skill = _learning_skill;
    }
  }
}
