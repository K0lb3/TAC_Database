﻿// Decompiled with JetBrains decompiler
// Type: SRPG.QuestHaveUnitOpenPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;

namespace SRPG
{
  public class QuestHaveUnitOpenPopup : MonoBehaviour
  {
    [SerializeField]
    private string MessageLText;
    private GameObject mMsg;

    public QuestHaveUnitOpenPopup()
    {
      base.\u002Ector();
    }

    public void OnClick(GameObject go)
    {
      if (Object.op_Equality((Object) go, (Object) null) || string.IsNullOrEmpty(this.MessageLText) || Object.op_Inequality((Object) this.mMsg, (Object) null))
        return;
      QuestParam dataOfClass = DataSource.FindDataOfClass<QuestParam>(go, (QuestParam) null);
      if (dataOfClass == null)
        return;
      UnitParam unitParam = MonoSingleton<GameManager>.Instance.MasterParam.GetUnitParam(dataOfClass.OpenUnit);
      if (unitParam == null)
        return;
      this.mMsg = UIUtility.SystemMessage(LocalizedText.Get(this.MessageLText, (object) unitParam.name), new UIUtility.DialogResultEvent(this.OnConfirm), (GameObject) null, false, -1);
    }

    public void OnConfirm(GameObject go)
    {
      this.mMsg = (GameObject) null;
    }
  }
}
