﻿// Decompiled with JetBrains decompiler
// Type: SRPG.VersusMatchingInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;

namespace SRPG
{
  [FlowNode.Pin(1, "Matching", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(2, "DraftMatching", FlowNode.PinTypes.Input, 2)]
  public class VersusMatchingInfo : MonoBehaviour, IFlowInterface
  {
    public VersusMatchingInfo()
    {
      base.\u002Ector();
    }

    public void Start()
    {
      MonoSingleton<GameManager>.Instance.AudienceMode = false;
      GlobalVars.IsVersusDraftMode = false;
    }

    public void Activated(int pinID)
    {
      MonoSingleton<GameManager>.Instance.AudienceMode = false;
      if (pinID == 1)
      {
        GlobalVars.IsVersusDraftMode = false;
        this.StartCoroutine(ProgressWindow.OpenVersusLoadScreenAsync());
      }
      else
      {
        if (pinID != 2)
          return;
        GlobalVars.IsVersusDraftMode = true;
        this.StartCoroutine(ProgressWindow.OpenVersusDraftLoadScreenAsync());
      }
    }
  }
}
