﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_GenesisChapterModeInfoParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_GenesisChapterModeInfoParam
  {
    public string star_id;
    public int liberation_qno;
    public string boss_unit_id;
    public int boss_hp;
    public string boss_ch_item_id;
    public int boss_ch_item_num;
    public string boss_reward_id;
    public int mode_ui_index;
    public string lap_boss_id;
  }
}
