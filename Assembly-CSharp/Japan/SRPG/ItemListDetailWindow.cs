﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ItemListDetailWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;

namespace SRPG
{
  [FlowNode.Pin(1, "Reload", FlowNode.PinTypes.Input, 1)]
  public class ItemListDetailWindow : MonoBehaviour, IFlowInterface
  {
    [SerializeField]
    private GameObject mItemLimitedText;

    public ItemListDetailWindow()
    {
      base.\u002Ector();
    }

    public void Activated(int pinID)
    {
      this.Refresh();
    }

    private void Start()
    {
      this.Refresh();
    }

    private void Refresh()
    {
      ItemData itemDataByItemId = MonoSingleton<GameManager>.Instance.Player.FindItemDataByItemID(GlobalVars.SelectedItemID, false);
      ItemParam itemParam;
      if (itemDataByItemId != null)
      {
        itemParam = itemDataByItemId.Param;
        DataSource.Bind<ItemData>(((Component) this).get_gameObject(), itemDataByItemId, false);
      }
      else
      {
        itemParam = MonoSingleton<GameManager>.Instance.MasterParam.GetItemParam(GlobalVars.SelectedItemID);
        if (itemParam == null)
          return;
        DataSource.Bind<ItemParam>(((Component) this).get_gameObject(), itemParam, false);
      }
      if (itemParam != null)
        this.mItemLimitedText.SetActive(itemParam.IsLimited);
      GameParameter.UpdateAll(((Component) this).get_gameObject());
      ((Behaviour) this).set_enabled(true);
    }
  }
}
