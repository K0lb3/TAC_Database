﻿// Decompiled with JetBrains decompiler
// Type: SRPG.MultiTowerSkipFloorItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SRPG
{
  public class MultiTowerSkipFloorItem : MonoBehaviour
  {
    [SerializeField]
    private Text TextValue;
    [SerializeField]
    private SRPG_Button SelectBtn;
    [HideInInspector]
    public int Floor;

    public MultiTowerSkipFloorItem()
    {
      base.\u002Ector();
    }

    public void SetItem(int floor, UnityAction action)
    {
      this.Floor = floor;
      if (Object.op_Implicit((Object) this.TextValue))
        this.TextValue.set_text(string.Format(LocalizedText.Get("sys.MULTI_TOWER_HIERARCHY"), (object) floor));
      if (action == null || !Object.op_Implicit((Object) this.SelectBtn))
        return;
      ((UnityEvent) this.SelectBtn.get_onClick()).RemoveListener(action);
      ((UnityEvent) this.SelectBtn.get_onClick()).AddListener(action);
    }
  }
}
