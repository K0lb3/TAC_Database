﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_GuildMember
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_GuildMember
  {
    public long gid;
    public string uid;
    public int role_id;
    public string name;
    public int lv;
    public string award_id;
    public Json_Unit units;
    public long applied_at;
    public long joined_at;
    public long leave_at;
    public long lastlogin;
  }
}
