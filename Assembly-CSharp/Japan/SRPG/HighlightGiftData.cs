﻿// Decompiled with JetBrains decompiler
// Type: SRPG.HighlightGiftData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class HighlightGiftData
  {
    public HighlightGiftType type;
    public string item;
    public int num;

    public void Deserialize(JSON_HighlightGiftData json)
    {
      if (json == null)
        return;
      this.type = (HighlightGiftType) json.type;
      this.item = json.item;
      this.num = json.num;
    }
  }
}
