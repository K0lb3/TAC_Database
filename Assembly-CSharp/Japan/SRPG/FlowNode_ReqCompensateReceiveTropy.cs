﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_ReqCompensateReceiveTropy
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System;
using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("Trophy/CompensateReceiveTrophy", 32741)]
  [FlowNode.Pin(1, "Begin", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(101, "End", FlowNode.PinTypes.Output, 101)]
  public class FlowNode_ReqCompensateReceiveTropy : FlowNode_Network
  {
    private const int PIN_INPUT_BEGIN_API = 1;
    private const int PIN_OUTPUT_END_API = 101;

    public override void OnActivate(int pinID)
    {
      if (pinID != 1)
        return;
      ((Behaviour) this).set_enabled(true);
      this.ExecRequest((WebAPI) new ReqCompensateReceiveTrophy(new Network.ResponseCallback(((FlowNode_Network) this).ResponseCallback)));
    }

    public override void OnSuccess(WWWResult www)
    {
      if (Network.IsError)
      {
        int errCode = (int) Network.ErrCode;
        this.OnRetry();
      }
      else
      {
        WebAPI.JSON_BodyResponse<ReqCompensateReceiveTrophy.Response> jsonObject = JSONParser.parseJSONObject<WebAPI.JSON_BodyResponse<ReqCompensateReceiveTrophy.Response>>(www.text);
        DebugUtility.Assert(jsonObject != null, "res == null");
        try
        {
          MonoSingleton<GameManager>.Instance.Player.OverwiteTrophyProgress(jsonObject.body.trophyprogs);
          MonoSingleton<GameManager>.Instance.Player.OverwiteTrophyProgress(jsonObject.body.bingoprogs);
        }
        catch (Exception ex)
        {
          DebugUtility.LogException(ex);
          return;
        }
        Network.RemoveAPI();
        ((Behaviour) this).set_enabled(false);
        this.ActivateOutputLinks(101);
      }
    }
  }
}
