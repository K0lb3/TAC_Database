﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_Friend
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class Json_Friend
  {
    public string uid;
    public string fuid;
    public string name;
    public string type;
    public int lv;
    public long lastlogin;
    public int is_multi_push;
    public string multi_comment;
    public Json_Unit unit;
    public string created_at;
    public int is_favorite;
    public string award;
    public string wish;
    public string status;
    public JSON_ViewGuild guild;
  }
}
