﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_Guild
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_Guild
  {
    public long id;
    public string created_uid;
    public string name;
    public string award_id;
    public string board;
    public int count;
    public int max_count;
    public int submaster_count;
    public JSON_GuildEntryCondition guild_subscription_condition;
    public JSON_GuildMember[] guild_member;
    public string[] have_awards;
    public JSON_GuildFacilityData[] facilities;
    public long created_at;
  }
}
