﻿// Decompiled with JetBrains decompiler
// Type: SRPG.UnitPieceShopGroupCost
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class UnitPieceShopGroupCost
  {
    private int mCost;
    private int mNum;

    public int Cost
    {
      get
      {
        return this.mCost;
      }
    }

    public int Num
    {
      get
      {
        return this.mNum;
      }
    }

    public bool Deserialize(JSON_UnitPieceShopGroupCost json)
    {
      if (json == null)
        return false;
      this.mCost = json.cost;
      this.mNum = json.num;
      return true;
    }
  }
}
