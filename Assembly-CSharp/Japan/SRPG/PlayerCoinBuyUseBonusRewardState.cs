﻿// Decompiled with JetBrains decompiler
// Type: SRPG.PlayerCoinBuyUseBonusRewardState
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class PlayerCoinBuyUseBonusRewardState
  {
    public string iname;
    public int num;
    public DateTime rewarded_at;

    public bool IsReceived
    {
      get
      {
        return this.rewarded_at != DateTime.MinValue;
      }
    }

    public void Deserialize(JSON_PlayerCoinBuyUseBonusRewardState json)
    {
      if (json == null)
        return;
      this.iname = json.iname;
      this.num = json.num;
      this.rewarded_at = DateTime.MinValue;
      if (json.rewarded_at <= 0L)
        return;
      this.rewarded_at = TimeManager.FromUnixTime(json.rewarded_at);
    }
  }
}
