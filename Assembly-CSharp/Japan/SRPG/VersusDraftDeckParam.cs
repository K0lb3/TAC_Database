﻿// Decompiled with JetBrains decompiler
// Type: SRPG.VersusDraftDeckParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class VersusDraftDeckParam
  {
    private int mId;
    private string mName;
    private int mSecretMax;

    public int Id
    {
      get
      {
        return this.mId;
      }
    }

    public string Name
    {
      get
      {
        return this.mName;
      }
    }

    public int SecretMax
    {
      get
      {
        return this.mSecretMax;
      }
    }

    public bool Deserialize(JSON_VersusDraftDeckParam param)
    {
      this.mId = param.id;
      this.mName = param.name;
      this.mSecretMax = param.secret_max;
      return true;
    }
  }
}
