﻿// Decompiled with JetBrains decompiler
// Type: SRPG.HomeGalleryIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;

namespace SRPG
{
  public class HomeGalleryIcon : MonoBehaviour
  {
    [SerializeField]
    private GameObject Banner;
    private float mRefreshInterval;
    private HighlightParam[] mHilights;

    public HomeGalleryIcon()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
      this.Banner.SetActive(false);
      HighlightParam[] highlightParam = MonoSingleton<GameManager>.Instance.MasterParam.HighlightParam;
      if (highlightParam != null && highlightParam.Length > 0)
        this.mHilights = MonoSingleton<GameManager>.Instance.MasterParam.HighlightParam;
      if (this.mHilights == null || this.mHilights.Length < 1 || Object.op_Equality((Object) this.Banner, (Object) null))
        ((Behaviour) this).set_enabled(false);
      else
        this.CheckAvailable();
    }

    private void Update()
    {
      this.mRefreshInterval -= Time.get_unscaledDeltaTime();
      if ((double) this.mRefreshInterval > 0.0)
        return;
      this.CheckAvailable();
      this.mRefreshInterval = 1f;
    }

    private void CheckAvailable()
    {
      bool flag = false;
      for (int index = 0; index < this.mHilights.Length; ++index)
      {
        if (this.mHilights[index].IsAvailable())
        {
          flag = true;
          break;
        }
      }
      this.Banner.SetActive(flag);
    }
  }
}
