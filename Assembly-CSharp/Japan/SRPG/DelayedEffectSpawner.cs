﻿// Decompiled with JetBrains decompiler
// Type: SRPG.DelayedEffectSpawner
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;
using UnityEngine;

namespace SRPG
{
  public class DelayedEffectSpawner : MonoBehaviour
  {
    private float mCurrTime;
    private List<DelayedEffectSpawner.Cue> mCues;
    private Transform mSelfTransform;
    private bool mAllSpawned;

    public DelayedEffectSpawner()
    {
      base.\u002Ector();
    }

    public void Init(SkillEffect.EffectElement[] effects, Vector3 position, Quaternion rotation)
    {
      this.mSelfTransform = ((Component) this).get_transform();
      this.mSelfTransform.set_position(position);
      this.mSelfTransform.set_rotation(rotation);
      if (effects == null || effects.Length <= 0)
        return;
      foreach (SkillEffect.EffectElement effect in effects)
      {
        DelayedEffectSpawner.Cue cue = new DelayedEffectSpawner.Cue();
        cue.delay = effect.Delay;
        if (Object.op_Inequality((Object) effect.Effect, (Object) null))
        {
          cue.particle = (GameObject) Object.Instantiate<GameObject>((M0) effect.Effect, this.mSelfTransform);
          cue.particle.RequireComponent<OneShotParticle>();
          cue.particle.SetActive(false);
        }
        if (!string.IsNullOrEmpty(effect.CueID))
        {
          cue.customSound = (CustomSound) ((Component) this).get_gameObject().AddComponent<CustomSound>();
          cue.customSound.cueID = effect.CueID;
          cue.customSound.type = effect.SoundType;
          cue.customSound.PlayOnAwake = false;
        }
        this.mCues.Add(cue);
      }
      this.mAllSpawned = false;
    }

    private void Update()
    {
      if (this.mCues == null || this.mCues.Count <= 0)
        return;
      if (this.mAllSpawned && this.mSelfTransform.get_childCount() <= 0)
      {
        Object.Destroy((Object) ((Component) this).get_gameObject());
      }
      else
      {
        this.mCurrTime += Time.get_deltaTime();
        this.mAllSpawned = true;
        foreach (DelayedEffectSpawner.Cue mCue in this.mCues)
        {
          if ((double) mCue.delay <= (double) this.mCurrTime && !mCue.spawned)
          {
            mCue.spawned = true;
            if (Object.op_Inequality((Object) mCue.particle, (Object) null))
              mCue.particle.SetActive(true);
            if (Object.op_Inequality((Object) mCue.customSound, (Object) null))
              mCue.customSound.Play();
          }
          if (!mCue.spawned)
            this.mAllSpawned = false;
        }
      }
    }

    public ParticleSystem GetLastDurationParticle()
    {
      if (this.mCues == null || this.mCues.Count <= 0)
        return (ParticleSystem) null;
      ParticleSystem particleSystem1 = (ParticleSystem) null;
      float num1 = 0.0f;
      foreach (DelayedEffectSpawner.Cue mCue in this.mCues)
      {
        if (!Object.op_Equality((Object) mCue.particle, (Object) null))
        {
          ParticleSystem[] componentsInChildren = (ParticleSystem[]) mCue.particle.GetComponentsInChildren<ParticleSystem>(true);
          if (componentsInChildren != null && componentsInChildren.Length > 0)
          {
            foreach (ParticleSystem particleSystem2 in componentsInChildren)
            {
              ParticleSystem.MainModule main = particleSystem2.get_main();
              float num2 = ((ParticleSystem.MainModule) ref main).get_duration() + mCue.delay;
              if ((double) num2 > (double) num1)
              {
                particleSystem1 = particleSystem2;
                num1 = num2;
              }
            }
          }
        }
      }
      return particleSystem1;
    }

    private class Cue
    {
      public GameObject particle;
      public CustomSound customSound;
      public bool spawned;
      public float delay;
    }
  }
}
