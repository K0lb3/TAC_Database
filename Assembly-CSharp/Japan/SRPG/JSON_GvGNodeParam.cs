﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_GvGNodeParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_GvGNodeParam : JSON_GvGMasterParam
  {
    public int id;
    public int period_id;
    public string name;
    public int rank;
    public int defense_max;
    public string quest_id;
    public int point;
    public string reward_id;
    public int npc_party_id;
    public string consecutive_debuff_id;
    public int consecutive_debuff_max;
    public int[] adjacent_node;
  }
}
