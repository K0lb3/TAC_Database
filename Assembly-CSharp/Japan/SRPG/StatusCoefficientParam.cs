﻿// Decompiled with JetBrains decompiler
// Type: SRPG.StatusCoefficientParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;

namespace SRPG
{
  public class StatusCoefficientParam
  {
    private float mHP;
    private float mAttack;
    private float mDefense;
    private float mMagAttack;
    private float mMagDefense;
    private float mDex;
    private float mSpeed;
    private float mCritical;
    private float mLuck;
    private float mCombo;
    private float mMove;
    private float mJump;

    public float HP
    {
      get
      {
        return this.mHP;
      }
    }

    public float Attack
    {
      get
      {
        return this.mAttack;
      }
    }

    public float Defense
    {
      get
      {
        return this.mDefense;
      }
    }

    public float MagAttack
    {
      get
      {
        return this.mMagAttack;
      }
    }

    public float MagDefense
    {
      get
      {
        return this.mMagDefense;
      }
    }

    public float Dex
    {
      get
      {
        return this.mDex;
      }
    }

    public float Speed
    {
      get
      {
        return this.mSpeed;
      }
    }

    public float Critical
    {
      get
      {
        return this.mCritical;
      }
    }

    public float Luck
    {
      get
      {
        return this.mLuck;
      }
    }

    public float Combo
    {
      get
      {
        return this.mCombo;
      }
    }

    public float Move
    {
      get
      {
        return this.mMove;
      }
    }

    public float Jump
    {
      get
      {
        return this.mJump;
      }
    }

    public void Deserialize(JSON_StatusCoefficientParam json)
    {
      if (json == null)
        return;
      this.mHP = json.hp;
      this.mAttack = json.atk;
      this.mDefense = json.def;
      this.mMagAttack = json.matk;
      this.mMagDefense = json.mdef;
      this.mDex = json.dex;
      this.mSpeed = json.spd;
      this.mCritical = json.cri;
      this.mLuck = json.luck;
      this.mCombo = json.cmb;
      this.mMove = json.move;
      this.mJump = json.jmp;
    }

    public static int CalcTotalStatus(UnitData unit)
    {
      return 0 + (int) ((double) (int) unit.Status.param.hp * (double) MonoSingleton<GameManager>.Instance.MasterParam.mStatusCoefficient.HP) + (int) ((double) (int) unit.Status.param.atk * (double) MonoSingleton<GameManager>.Instance.MasterParam.mStatusCoefficient.Attack) + (int) ((double) (int) unit.Status.param.def * (double) MonoSingleton<GameManager>.Instance.MasterParam.mStatusCoefficient.Defense) + (int) ((double) (int) unit.Status.param.mag * (double) MonoSingleton<GameManager>.Instance.MasterParam.mStatusCoefficient.MagAttack) + (int) ((double) (int) unit.Status.param.mnd * (double) MonoSingleton<GameManager>.Instance.MasterParam.mStatusCoefficient.MagDefense) + (int) ((double) (int) unit.Status.param.dex * (double) MonoSingleton<GameManager>.Instance.MasterParam.mStatusCoefficient.Dex) + (int) ((double) (int) unit.Status.param.spd * (double) MonoSingleton<GameManager>.Instance.MasterParam.mStatusCoefficient.Speed) + (int) ((double) (int) unit.Status.param.cri * (double) MonoSingleton<GameManager>.Instance.MasterParam.mStatusCoefficient.Critical) + (int) ((double) (int) unit.Status.param.luk * (double) MonoSingleton<GameManager>.Instance.MasterParam.mStatusCoefficient.Luck) + (int) ((double) unit.GetCombination() * (double) MonoSingleton<GameManager>.Instance.MasterParam.mStatusCoefficient.Combo) + (int) ((double) (int) unit.Status.param.mov * (double) MonoSingleton<GameManager>.Instance.MasterParam.mStatusCoefficient.Move) + (int) ((double) (int) unit.Status.param.jmp * (double) MonoSingleton<GameManager>.Instance.MasterParam.mStatusCoefficient.Jump);
    }
  }
}
