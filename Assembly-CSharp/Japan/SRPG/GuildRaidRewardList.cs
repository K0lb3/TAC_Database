﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRaidRewardList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class GuildRaidRewardList : SRPG_ListBase
  {
    [SerializeField]
    private RaidRewardListItem mListItem;

    protected override void Start()
    {
      base.Start();
      GameManager instance = MonoSingleton<GameManager>.Instance;
      if (instance.Player.mGuildRaidSeasonResult == null)
        return;
      string rewardRankingParam = instance.GetGuildRaidRewardRankingParam(instance.Player.mGuildRaidSeasonResult.Ranking.reward_id, instance.Player.mGuildRaidSeasonResult.Ranking.rank);
      this.CreateList(this.mListItem, instance.GetGuildRaidRewardParam(rewardRankingParam).Rewards);
    }

    private void CreateList(RaidRewardListItem ListItem, List<GuildRaidReward> rewards)
    {
      GameManager gm = MonoSingleton<GameManager>.Instance;
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) ListItem, (UnityEngine.Object) null))
        return;
      ((Component) ListItem).get_gameObject().SetActive(false);
      GameUtility.SetGameObjectActive(ListItem.RewardUnit, false);
      GameUtility.SetGameObjectActive(ListItem.RewardItem, false);
      GameUtility.SetGameObjectActive(ListItem.RewardCard, false);
      GameUtility.SetGameObjectActive(ListItem.RewardArtifact, false);
      GameUtility.SetGameObjectActive(ListItem.RewardAward, false);
      GameUtility.SetGameObjectActive(ListItem.RewardGold, false);
      GameUtility.SetGameObjectActive(ListItem.RewardCoin, false);
      GameUtility.SetGameObjectActive(ListItem.RewardEmblem, false);
      if (rewards == null || rewards.Count == 0)
        return;
      RaidRewardListItem item = ListItem;
      ((Component) item).get_transform().SetParent(((Component) this).get_transform(), false);
      ((Component) item).get_gameObject().SetActive(true);
      rewards.ForEach((Action<GuildRaidReward>) (reward =>
      {
        bool flag = false;
        GameObject gameObject;
        switch (reward.Type)
        {
          case RaidRewardType.Item:
            ItemParam itemParam = gm.GetItemParam(reward.IName);
            if (itemParam == null)
              return;
            gameObject = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) item.RewardItem);
            DataSource.Bind<ItemParam>(gameObject, itemParam, false);
            flag = true;
            break;
          case RaidRewardType.Gold:
            gameObject = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) item.RewardGold);
            flag = true;
            break;
          case RaidRewardType.Coin:
            gameObject = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) item.RewardCoin);
            flag = true;
            break;
          case RaidRewardType.Award:
            AwardParam awardParam = gm.GetAwardParam(reward.IName);
            if (awardParam == null)
              return;
            gameObject = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) item.RewardAward);
            DataSource.Bind<AwardParam>(gameObject, awardParam, false);
            break;
          case RaidRewardType.Unit:
            UnitParam unitParam = gm.GetUnitParam(reward.IName);
            if (unitParam == null)
              return;
            gameObject = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) item.RewardUnit);
            DataSource.Bind<UnitParam>(gameObject, unitParam, false);
            break;
          case RaidRewardType.ConceptCard:
            ConceptCardData cardDataForDisplay = ConceptCardData.CreateConceptCardDataForDisplay(reward.IName);
            if (cardDataForDisplay == null)
              return;
            gameObject = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) item.RewardCard);
            ConceptCardIcon component1 = (ConceptCardIcon) gameObject.GetComponent<ConceptCardIcon>();
            if (UnityEngine.Object.op_Inequality((UnityEngine.Object) component1, (UnityEngine.Object) null))
            {
              component1.Setup(cardDataForDisplay);
              break;
            }
            break;
          case RaidRewardType.Artifact:
            ArtifactParam artifactParam = gm.MasterParam.GetArtifactParam(reward.IName);
            if (artifactParam == null)
              return;
            gameObject = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) item.RewardArtifact);
            DataSource.Bind<ArtifactParam>(gameObject, artifactParam, false);
            break;
          case RaidRewardType.GuildEmblem:
            GuildEmblemParam guildEmbleme = gm.MasterParam.GetGuildEmbleme(reward.IName);
            if (guildEmbleme == null)
              return;
            gameObject = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) item.RewardEmblem);
            this.GetEmblem(gameObject, guildEmbleme.Image);
            break;
          default:
            return;
        }
        if (flag)
        {
          Transform transform = gameObject.get_transform().Find("amount/Text_amount");
          if (UnityEngine.Object.op_Inequality((UnityEngine.Object) transform, (UnityEngine.Object) null))
          {
            Text component2 = (Text) ((Component) transform).GetComponent<Text>();
            if (UnityEngine.Object.op_Inequality((UnityEngine.Object) component2, (UnityEngine.Object) null))
              component2.set_text(reward.Num.ToString());
          }
        }
        gameObject.get_transform().SetParent(item.RewardList, false);
        gameObject.SetActive(true);
      }));
    }

    private void GetEmblem(GameObject obj, string name)
    {
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) obj, (UnityEngine.Object) null))
      {
        obj.SetActive(false);
      }
      else
      {
        Image component = (Image) obj.GetComponent<Image>();
        string name1 = name;
        ViewGuildData dataOfClass = DataSource.FindDataOfClass<ViewGuildData>(((Component) this).get_gameObject(), (ViewGuildData) null);
        if (dataOfClass != null)
          name1 = dataOfClass.award_id;
        if (string.IsNullOrEmpty(name1) || UnityEngine.Object.op_Equality((UnityEngine.Object) component, (UnityEngine.Object) null))
        {
          ((Behaviour) component).set_enabled(false);
        }
        else
        {
          SpriteSheet spriteSheet = AssetManager.Load<SpriteSheet>("GuildEmblemImage/GuildEmblemes");
          if (!UnityEngine.Object.op_Inequality((UnityEngine.Object) spriteSheet, (UnityEngine.Object) null))
            return;
          component.set_sprite(spriteSheet.GetSprite(name1));
          ((Behaviour) component).set_enabled(true);
        }
      }
    }
  }
}
