﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_ArenaPlayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class Json_ArenaPlayer
  {
    public string result;
    public string fuid;
    public string name;
    public int lv;
    public int rank;
    public Json_Unit[] units;
    public long btl_at;
    public string award;
    public JSON_ViewGuild guild;
  }
}
