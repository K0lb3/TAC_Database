﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RuneDrawDisassemblyEffect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class RuneDrawDisassemblyEffect : MonoBehaviour
  {
    [SerializeField]
    private ImageArray[] mTitleImage;
    [SerializeField]
    private Animator mAnimator;
    private readonly string AnimatorTrigger;
    private ReqRuneDisassembly.Response.Result mResult;

    public RuneDrawDisassemblyEffect()
    {
      base.\u002Ector();
    }

    public void Awake()
    {
    }

    public void OnDestroy()
    {
    }

    public void SetDrawParam(ReqRuneDisassembly.Response.Result result)
    {
      this.mResult = result;
      this.Refresh();
    }

    public void Refresh()
    {
      if (Object.op_Implicit((Object) this.mAnimator))
        this.mAnimator.SetTrigger(this.AnimatorTrigger);
      if (this.mTitleImage == null)
        return;
      for (int index = 0; index < this.mTitleImage.Length; ++index)
      {
        if (Object.op_Inequality((Object) this.mTitleImage[index], (Object) null))
          this.mTitleImage[index].ImageIndex = (int) this.mResult;
      }
    }
  }
}
