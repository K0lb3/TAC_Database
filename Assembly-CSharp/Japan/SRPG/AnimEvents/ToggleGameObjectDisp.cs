﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AnimEvents.ToggleGameObjectDisp
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;
using UnityEngine;

namespace SRPG.AnimEvents
{
  public class ToggleGameObjectDisp : AnimEvent
  {
    public string[] NameGameObjects;
    private List<GameObject> mGoTargetLists;

    private List<GameObject> GetGoTargets(GameObject go)
    {
      if (this.mGoTargetLists == null)
      {
        this.mGoTargetLists = new List<GameObject>();
        if (this.NameGameObjects != null)
        {
          for (int index = 0; index < this.NameGameObjects.Length; ++index)
          {
            string nameGameObject = this.NameGameObjects[index];
            if (!string.IsNullOrEmpty(nameGameObject))
            {
              Transform childRecursively = GameUtility.findChildRecursively(go.get_transform(), nameGameObject);
              if (Object.op_Implicit((Object) childRecursively))
                this.mGoTargetLists.Add(((Component) childRecursively).get_gameObject());
            }
          }
        }
        if (this.mGoTargetLists.Count == 0 && Object.op_Implicit((Object) go))
          this.mGoTargetLists.Add(go);
      }
      return this.mGoTargetLists;
    }

    public override void OnStart(GameObject go)
    {
      this.mGoTargetLists = (List<GameObject>) null;
      List<GameObject> goTargets = this.GetGoTargets(go);
      if (goTargets == null)
        return;
      for (int index = 0; index < goTargets.Count; ++index)
        goTargets[index].SetActive(false);
    }

    public override void OnEnd(GameObject go)
    {
      List<GameObject> goTargets = this.GetGoTargets(go);
      if (goTargets == null)
        return;
      for (int index = 0; index < goTargets.Count; ++index)
        goTargets[index].SetActive(true);
      this.mGoTargetLists = (List<GameObject>) null;
    }
  }
}
