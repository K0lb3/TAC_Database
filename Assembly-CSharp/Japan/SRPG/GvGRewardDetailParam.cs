﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GvGRewardDetailParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class GvGRewardDetailParam
  {
    public RaidRewardType Type { get; private set; }

    public string IName { get; private set; }

    public int Num { get; private set; }

    public bool Deserialize(JSON_GvGRewardDetailParam json)
    {
      if (json == null)
        return false;
      this.Type = (RaidRewardType) json.item_type;
      this.IName = json.item_iname;
      this.Num = json.item_num;
      return true;
    }
  }
}
