﻿// Decompiled with JetBrains decompiler
// Type: SRPG.DirectionArrow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class DirectionArrow : MonoBehaviour
  {
    public DirectionArrow.ArrowStates State;
    public EUnitDirection Direction;
    private Animator mAnimator;
    [HelpBox("方向の選択状態にあわせてAnimatorのStateNameを変更します (0=Normal,1=Press,2=Hilit,3=Close)。矢印はアニメーションが停止したら破棄されるので、PressとClose状態以外はループアニメーションにしてください。")]
    public string StateName;

    public DirectionArrow()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      this.mAnimator = (Animator) ((Component) this).GetComponent<Animator>();
    }

    private void Update()
    {
      if (Object.op_Equality((Object) this.mAnimator, (Object) null))
        return;
      this.mAnimator.SetInteger(this.StateName, (int) this.State);
      AnimatorStateInfo animatorStateInfo1 = this.mAnimator.GetCurrentAnimatorStateInfo(0);
      if (((AnimatorStateInfo) ref animatorStateInfo1).get_loop())
        return;
      AnimatorStateInfo animatorStateInfo2 = this.mAnimator.GetCurrentAnimatorStateInfo(0);
      if ((double) ((AnimatorStateInfo) ref animatorStateInfo2).get_normalizedTime() < 1.0 || this.mAnimator.IsInTransition(0))
        return;
      Object.Destroy((Object) ((Component) this).get_gameObject());
    }

    public enum ArrowStates
    {
      Normal,
      Press,
      Hilit,
      Close,
    }
  }
}
