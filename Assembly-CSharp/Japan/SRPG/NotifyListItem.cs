﻿// Decompiled with JetBrains decompiler
// Type: SRPG.NotifyListItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class NotifyListItem : MonoBehaviour
  {
    public Text Message;
    [NonSerialized]
    public float Lifetime;
    [NonSerialized]
    public float Height;

    public NotifyListItem()
    {
      base.\u002Ector();
    }
  }
}
