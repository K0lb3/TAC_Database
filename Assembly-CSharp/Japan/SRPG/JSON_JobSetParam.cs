﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_JobSetParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_JobSetParam
  {
    public string iname;
    public string job;
    public int lrare;
    public int lplus;
    public string ljob1;
    public int llv1;
    public string ljob2;
    public int llv2;
    public string ljob3;
    public int llv3;
    public string cjob;
    public string target_unit;
    public int joblv_opened;
  }
}
