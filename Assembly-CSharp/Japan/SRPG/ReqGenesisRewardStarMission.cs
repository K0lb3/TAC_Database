﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqGenesisRewardStarMission
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class ReqGenesisRewardStarMission : WebAPI
  {
    public ReqGenesisRewardStarMission(
      string area_id,
      QuestDifficulties difficult,
      int star_index,
      Network.ResponseCallback response)
    {
      this.name = "genesis/reward/star_mission";
      this.body = WebAPI.GetRequestString<ReqGenesisRewardStarMission.RequestParam>(new ReqGenesisRewardStarMission.RequestParam()
      {
        area_id = area_id,
        difficulty = (int) difficult,
        star_index = star_index
      });
      this.callback = response;
    }

    [Serializable]
    public class RequestParam
    {
      public string area_id;
      public int difficulty;
      public int star_index;
    }

    [Serializable]
    public class Response
    {
      public ReqBtlCom.GenesisStar[] genesis_stars;
      public Json_Gift[] reward;
      public Json_PlayerData player;
      public Json_Item[] items;
      public Json_Unit[] units;
      public JSON_ConceptCard[] cards;
      public Json_Artifact[] artifacts;
      public JSON_TrophyProgress[] trophyprogs;
      public JSON_TrophyProgress[] bingoprogs;
    }
  }
}
