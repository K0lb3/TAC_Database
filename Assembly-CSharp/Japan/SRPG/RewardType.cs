﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RewardType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public enum RewardType
  {
    Item = 0,
    Gold = 1,
    Coin = 2,
    Artifact = 3,
    Unit = 4,
    Award = 5,
    ConceptCard = 6,
    Nothing = 100, // 0x00000064
  }
}
