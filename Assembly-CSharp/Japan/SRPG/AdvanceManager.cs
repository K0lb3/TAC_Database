﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AdvanceManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

namespace SRPG
{
  public class AdvanceManager : MonoBehaviour
  {
    private static AdvanceManager mInstance;
    private static AdvanceEventParam mCurrentEventParam;
    private static ChapterParam mCurrentChapterParam;

    public AdvanceManager()
    {
      base.\u002Ector();
    }

    public static AdvanceManager Instance
    {
      get
      {
        return AdvanceManager.mInstance;
      }
    }

    private void OnEnable()
    {
      if (!Object.op_Equality((Object) AdvanceManager.mInstance, (Object) null))
        return;
      AdvanceManager.mInstance = this;
    }

    private void OnDisable()
    {
      if (!Object.op_Equality((Object) AdvanceManager.mInstance, (Object) this))
        return;
      AdvanceManager.mInstance = (AdvanceManager) null;
    }

    public static AdvanceEventParam CurrentEventParam
    {
      get
      {
        return AdvanceManager.mCurrentEventParam;
      }
      set
      {
        AdvanceManager.mCurrentEventParam = value;
      }
    }

    public static ChapterParam CurrentChapterParam
    {
      get
      {
        return AdvanceManager.mCurrentChapterParam;
      }
      set
      {
        AdvanceManager.mCurrentChapterParam = value;
      }
    }

    public bool LoadAssets<T>(string name, AdvanceManager.LoadAssetCallback<T> callback) where T : Object
    {
      if (callback == null)
        return false;
      this.StartCoroutine(this.DownloadCoroutine<T>(name, callback));
      return true;
    }

    [DebuggerHidden]
    private IEnumerator DownloadCoroutine<T>(
      string name,
      AdvanceManager.LoadAssetCallback<T> callback)
      where T : Object
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new AdvanceManager.\u003CDownloadCoroutine\u003Ec__Iterator0<T>()
      {
        name = name,
        callback = callback
      };
    }

    public static void SetStarMissionInfo(ReqBtlCom.AdvanceStar[] genesis_stars)
    {
      if (genesis_stars == null)
        return;
      GameManager instance = MonoSingleton<GameManager>.Instance;
      if (!Object.op_Implicit((Object) instance))
        return;
      for (int index1 = 0; index1 < genesis_stars.Length; ++index1)
      {
        ReqBtlCom.AdvanceStar genesisStar = genesis_stars[index1];
        if (genesisStar != null && genesisStar.mode != null)
        {
          AdvanceEventParam eventParamFromAreaId = instance.GetAdvanceEventParamFromAreaId(genesisStar.area_id);
          if (eventParamFromAreaId != null)
          {
            for (int index2 = 0; index2 < genesisStar.mode.Length; ++index2)
            {
              ReqBtlCom.AdvanceStar.Mode mode = genesisStar.mode[index2];
              if (mode != null && mode.is_reward != null)
              {
                AdvanceEventModeInfoParam modeInfo = eventParamFromAreaId.GetModeInfo((QuestDifficulties) index2);
                if (modeInfo != null && modeInfo.StarParam != null)
                {
                  for (int index3 = 0; index3 < mode.is_reward.Length && index3 < modeInfo.StarParam.StarList.Count; ++index3)
                    modeInfo.StarParam.StarList[index3].IsReward = mode.is_reward[index3] != 0;
                }
              }
            }
          }
        }
      }
    }

    public delegate void LoadAssetCallback<T>(T obj) where T : Object;
  }
}
