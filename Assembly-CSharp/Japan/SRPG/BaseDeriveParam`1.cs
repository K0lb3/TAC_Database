﻿// Decompiled with JetBrains decompiler
// Type: SRPG.BaseDeriveParam`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class BaseDeriveParam<T>
  {
    public SkillAbilityDeriveParam m_SkillAbilityDeriveParam;
    public T m_BaseParam;
    public T m_DeriveParam;

    public int MasterIndex
    {
      get
      {
        return this.m_SkillAbilityDeriveParam.m_OriginIndex;
      }
    }
  }
}
