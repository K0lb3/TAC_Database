﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqProductChargePrepare
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Network.Encoding;

namespace SRPG
{
  public class ReqProductChargePrepare : WebAPI
  {
    public ReqProductChargePrepare(
      string ID,
      SRPG.Network.ResponseCallback response,
      EncodingTypes.ESerializeCompressMethod serializeCompressMethod)
    {
      this.name = "charge/prepare";
      this.body = string.Empty;
      ReqProductChargePrepare productChargePrepare = this;
      productChargePrepare.body = productChargePrepare.body + "\"id\":\"" + ID + "\"";
      this.body = WebAPI.GetRequestString(this.body);
      this.callback = response;
      this.serializeCompressMethod = serializeCompressMethod;
    }
  }
}
