﻿// Decompiled with JetBrains decompiler
// Type: SRPG.DebugMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class DebugMenu : MonoBehaviour
  {
    public DebugMenu()
    {
      base.\u002Ector();
    }

    public static void Start()
    {
    }

    public static void Log(string tag, string text, string trace)
    {
    }

    public static void Log(string tag, string text)
    {
    }

    public static void Log(string text)
    {
    }

    public static void LogSystem(string tag, string text, string trace)
    {
    }

    public static void LogSystem(string tag, string text)
    {
    }

    public static void LogSystem(string text)
    {
    }

    public static void LogWarning(string tag, string text, string trace)
    {
    }

    public static void LogWarning(string tag, string text)
    {
    }

    public static void LogWarning(string text)
    {
    }

    public static void LogError(string tag, string text, string trace)
    {
    }

    public static void LogError(string tag, string text)
    {
    }

    public static void LogError(string text)
    {
    }
  }
}
