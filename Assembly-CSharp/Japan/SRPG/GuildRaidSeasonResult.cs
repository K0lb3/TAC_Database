﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRaidSeasonResult
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class GuildRaidSeasonResult
  {
    public int mId;
    public JSON_GuildRaidGuildData mGuild;
    public JSON_GuildRaidRankingRewardData mRanking;

    public int Id
    {
      get
      {
        return this.mId;
      }
    }

    public JSON_GuildRaidGuildData Guild
    {
      get
      {
        return this.mGuild;
      }
    }

    public JSON_GuildRaidRankingRewardData Ranking
    {
      get
      {
        return this.mRanking;
      }
    }

    public void Deserialize(ReqGuildRaidRankingReward.Response res)
    {
      this.mId = res.period_id;
      this.mGuild = res.my_guild_info.guild;
      this.mRanking = res.my_guild_info.ranking;
    }
  }
}
