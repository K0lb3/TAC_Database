﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ConvertUnitPieceExcludeParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class ConvertUnitPieceExcludeParam
  {
    public int id;
    public string unit_piece_iname;

    public void Deserialize(JSON_ConvertUnitPieceExcludeParam json)
    {
      if (json == null)
        return;
      this.id = json.id;
      this.unit_piece_iname = json.unit_piece_iname;
    }
  }
}
