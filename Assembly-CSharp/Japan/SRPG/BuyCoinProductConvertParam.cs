﻿// Decompiled with JetBrains decompiler
// Type: SRPG.BuyCoinProductConvertParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class BuyCoinProductConvertParam
  {
    private string mName;
    private string mConvert;

    public string Name
    {
      get
      {
        return this.mName;
      }
    }

    public string Convert
    {
      get
      {
        return this.mConvert;
      }
    }

    public bool Deserialize(JSON_BuyCoinProductConvertParam json)
    {
      if (json == null)
        return false;
      this.mName = json.name;
      this.mConvert = json.convert;
      return true;
    }
  }
}
