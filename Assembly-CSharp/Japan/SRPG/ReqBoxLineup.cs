﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqBoxLineup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class ReqBoxLineup : WebAPI
  {
    public ReqBoxLineup(string box_iname, Network.ResponseCallback response)
    {
      this.name = "box_lottery/lineup";
      this.body = WebAPI.GetRequestString<ReqBoxLineup.RequestParam>(new ReqBoxLineup.RequestParam()
      {
        box_iname = box_iname
      });
      this.callback = response;
    }

    [Serializable]
    public class RequestParam
    {
      public string box_iname;
    }

    [Serializable]
    public class Response
    {
      public string box_iname;
      public int total_step;
      public JSON_BoxGachaSteps[] steps;
    }
  }
}
