﻿// Decompiled with JetBrains decompiler
// Type: SRPG.MultiTowerRoundParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace SRPG
{
  public class MultiTowerRoundParam
  {
    public int Now;
    public List<int> Round;
  }
}
