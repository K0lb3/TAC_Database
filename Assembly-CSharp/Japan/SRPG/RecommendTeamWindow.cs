﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RecommendTeamWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SRPG
{
  [FlowNode.Pin(0, "設定保存", FlowNode.PinTypes.Input, 0)]
  [FlowNode.Pin(100, "保存完了", FlowNode.PinTypes.Output, 100)]
  [FlowNode.Pin(101, "保存完了(念装自動装備ON)", FlowNode.PinTypes.Output, 101)]
  public class RecommendTeamWindow : MonoBehaviour, IFlowInterface
  {
    private const int INPUT_SAVE_CHANGES = 0;
    private const int OUTPUT_SAVE_CHANGES = 100;
    private const int OUTPUT_SAVE_CHANGES_WITH_CARD_EQIUP = 101;
    [SerializeField]
    private ScrollablePulldown TypePullDown;
    [SerializeField]
    private ElementDropdown ElemmentPullDown;
    [SerializeField]
    private Toggle SelectAutoEquipConceptCard;
    private readonly RecommendTeamWindow.TypeAndStr[] items;
    private readonly RecommendTeamWindow.ElemAndStr[] elements;
    private int currentTypeIndex;
    private int currentElemmentIndex;

    public RecommendTeamWindow()
    {
      base.\u002Ector();
    }

    public void Activated(int pinID)
    {
      if (pinID != 0)
        return;
      this.SaveSettings();
    }

    private void Awake()
    {
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.SelectAutoEquipConceptCard, (UnityEngine.Object) null))
      {
        this.SelectAutoEquipConceptCard.set_isOn(GlobalVars.IsAutoEquipConceptCard);
        // ISSUE: method pointer
        ((UnityEvent<bool>) this.SelectAutoEquipConceptCard.onValueChanged).AddListener(new UnityAction<bool>((object) this, __methodptr(OnValueChange)));
      }
      this.TypePullDown.OnSelectionChangeDelegate = new ScrollablePulldownBase.SelectItemEvent(this.OnTypeItemSelect);
      this.ElemmentPullDown.OnSelectionChangeDelegate = new Pulldown.SelectItemEvent(this.OnElemmentItemSelect);
      if (GlobalVars.RecommendTeamSettingValue != null)
      {
        this.currentTypeIndex = PartyUtility.RecommendTypeToComparatorOrder(GlobalVars.RecommendTeamSettingValue.recommendedType);
        this.currentElemmentIndex = (int) GlobalVars.RecommendTeamSettingValue.recommendedElement;
      }
      else
      {
        this.currentTypeIndex = 0;
        this.currentElemmentIndex = 0;
      }
      this.Refresh();
    }

    private void OnTypeItemSelect(int value)
    {
      if (value < 0 || value >= this.items.Length || value == this.currentTypeIndex)
        return;
      this.currentTypeIndex = value;
    }

    private void OnElemmentItemSelect(int value)
    {
      if (value == this.currentElemmentIndex)
        return;
      this.currentElemmentIndex = value;
    }

    private void Refresh()
    {
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.TypePullDown, (UnityEngine.Object) null))
      {
        this.TypePullDown.ClearItems();
        for (int index = 0; index < this.items.Length; ++index)
          this.TypePullDown.AddItem(LocalizedText.Get(this.items[index].title), index);
        this.TypePullDown.Selection = this.currentTypeIndex;
        ((Component) this.TypePullDown).get_gameObject().SetActive(true);
      }
      if (!UnityEngine.Object.op_Inequality((UnityEngine.Object) this.ElemmentPullDown, (UnityEngine.Object) null))
        return;
      this.ElemmentPullDown.ClearItems();
      GameSettings instance = GameSettings.Instance;
      for (int index = 0; index < this.elements.Length; ++index)
      {
        Sprite sprite = (Sprite) null;
        if (index < instance.Elements_IconSmall.Length && index != 0)
          sprite = instance.Elements_IconSmall[(int) this.elements[index].element];
        this.ElemmentPullDown.AddItem(LocalizedText.Get(this.elements[index].title), sprite, index);
      }
      this.ElemmentPullDown.Selection = this.currentElemmentIndex;
      ((Component) this.ElemmentPullDown).get_gameObject().SetActive(true);
    }

    private void SaveSettings()
    {
      GlobalVars.RecommendType type = this.items[this.currentTypeIndex].type;
      EElement element = this.elements[this.currentElemmentIndex].element;
      GlobalVars.RecommendTeamSettingValue = !Enum.IsDefined(typeof (GlobalVars.RecommendType), (object) type) || !Enum.IsDefined(typeof (EElement), (object) element) ? (GlobalVars.RecommendTeamSetting) null : new GlobalVars.RecommendTeamSetting(type, element);
      if (GlobalVars.IsAutoEquipConceptCard)
        FlowNode_GameObject.ActivateOutputLinks((Component) this, 101);
      else
        FlowNode_GameObject.ActivateOutputLinks((Component) this, 100);
    }

    public void Cancel()
    {
      GlobalVars.RecommendTeamSettingValue = (GlobalVars.RecommendTeamSetting) null;
    }

    private void OnValueChange(bool value)
    {
      GlobalVars.IsAutoEquipConceptCard = value;
    }

    private struct TypeAndStr
    {
      public readonly GlobalVars.RecommendType type;
      public readonly string title;

      public TypeAndStr(GlobalVars.RecommendType type, string title)
      {
        this.type = type;
        this.title = title;
      }
    }

    private struct ElemAndStr
    {
      public readonly EElement element;
      public readonly string title;

      public ElemAndStr(EElement element, string title)
      {
        this.element = element;
        this.title = title;
      }
    }
  }
}
