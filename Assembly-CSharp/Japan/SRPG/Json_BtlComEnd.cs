﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_BtlComEnd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class Json_BtlComEnd : Json_PlayerDataAll
  {
    public JSON_QuestProgress[] quests;
    public Json_BtlQuestRanking quest_ranking;
    public Json_FirstClearItem[] fclr_items;
    public Json_BtlRewardConceptCard[] cards;
    public int is_mail_cards;
    public int is_quest_out_of_period;
    public BattleCore.Json_BtlInspSlot[] sins;
    public BattleCore.Json_BtlInsp[] levelup_sins;
    public int guildraid_bp_charge;
  }
}
