﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_SetForceSceneChange
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  [FlowNode.NodeType("Scene/SetForceSceneChange", 32741)]
  [FlowNode.Pin(0, "True", FlowNode.PinTypes.Input, 0)]
  [FlowNode.Pin(1, "False", FlowNode.PinTypes.Input, 0)]
  [FlowNode.Pin(100, "Out", FlowNode.PinTypes.Output, 0)]
  public class FlowNode_SetForceSceneChange : FlowNode
  {
    private const int PIN_IN_TRUE = 0;
    private const int PIN_IN_FALSE = 1;
    private const int PIN_OUT = 100;

    public override void OnActivate(int pinID)
    {
      GlobalVars.ForceSceneChange = pinID == 0;
      this.ActivateOutputLinks(100);
    }
  }
}
