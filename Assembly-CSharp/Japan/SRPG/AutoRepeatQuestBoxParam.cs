﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AutoRepeatQuestBoxParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class AutoRepeatQuestBoxParam
  {
    private int mSize;
    private int mCoin;

    public int Size
    {
      get
      {
        return this.mSize;
      }
    }

    public int Coin
    {
      get
      {
        return this.mCoin;
      }
    }

    public void Deserialize(JSON_AutoRepeatQuestBoxParam json)
    {
      if (json == null)
        return;
      this.mSize = json.size;
      this.mCoin = json.coin;
    }

    public static void Deserialize(
      ref AutoRepeatQuestBoxParam[] param,
      JSON_AutoRepeatQuestBoxParam[] json)
    {
      if (json == null)
        return;
      param = new AutoRepeatQuestBoxParam[json.Length];
      for (int index = 0; index < json.Length; ++index)
      {
        AutoRepeatQuestBoxParam repeatQuestBoxParam = new AutoRepeatQuestBoxParam();
        repeatQuestBoxParam.Deserialize(json[index]);
        param[index] = repeatQuestBoxParam;
      }
    }
  }
}
