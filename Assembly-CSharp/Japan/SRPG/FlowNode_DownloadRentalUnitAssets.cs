﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_DownloadRentalUnitAssets
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("UnitRental/Download", 32741)]
  [FlowNode.Pin(1, "Start", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(111, "End", FlowNode.PinTypes.Output, 111)]
  public class FlowNode_DownloadRentalUnitAssets : FlowNode
  {
    private const int PIN_IN_START = 1;
    private const int PIN_OUT_END = 111;

    public override void OnActivate(int pinID)
    {
      if (pinID != 1)
        return;
      GameManager instance = MonoSingleton<GameManager>.Instance;
      if (Object.op_Equality((Object) instance, (Object) null))
      {
        this.ActivateOutputLinks(111);
      }
      else
      {
        UnitData rentalUnit = instance.Player.GetRentalUnit();
        if (rentalUnit == null)
        {
          this.ActivateOutputLinks(111);
        }
        else
        {
          DownloadUtility.DownloadUnit(rentalUnit.UnitParam, (JobData[]) null);
          this.StartCoroutine(this.Download());
        }
      }
    }

    [DebuggerHidden]
    private IEnumerator Download()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new FlowNode_DownloadRentalUnitAssets.\u003CDownload\u003Ec__Iterator0()
      {
        \u0024this = this
      };
    }
  }
}
