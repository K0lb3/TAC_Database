﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_QuestPartyParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_QuestPartyParam
  {
    public string iname;
    public int type_1;
    public int type_2;
    public int type_3;
    public int type_4;
    public int support_type;
    public int subtype_1;
    public int subtype_2;
    public string unit_1;
    public string unit_2;
    public string unit_3;
    public string unit_4;
    public string subunit_1;
    public string subunit_2;
    public int l_npc_rare;
  }
}
