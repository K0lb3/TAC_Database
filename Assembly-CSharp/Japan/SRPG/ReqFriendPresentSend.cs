﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqFriendPresentSend
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Text;

namespace SRPG
{
  public class ReqFriendPresentSend : WebAPI
  {
    public ReqFriendPresentSend(
      string url,
      Network.ResponseCallback response,
      string text,
      string trophyprog = null,
      string bingoprog = null)
    {
      StringBuilder stringBuilder = WebAPI.GetStringBuilder();
      stringBuilder.Append(text);
      if (!string.IsNullOrEmpty(trophyprog))
        stringBuilder.Append(trophyprog);
      if (!string.IsNullOrEmpty(bingoprog))
      {
        if (!string.IsNullOrEmpty(trophyprog))
          stringBuilder.Append(",");
        stringBuilder.Append(bingoprog);
      }
      this.name = url;
      this.body = WebAPI.GetRequestString(stringBuilder.ToString());
      this.callback = response;
    }
  }
}
