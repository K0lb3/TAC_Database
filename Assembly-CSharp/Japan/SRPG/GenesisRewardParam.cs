﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GenesisRewardParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace SRPG
{
  public class GenesisRewardParam
  {
    private string mIname;
    private GenesisRewardDataParam[] mRewards;

    public string Iname
    {
      get
      {
        return this.mIname;
      }
    }

    public List<GenesisRewardDataParam> RewardList
    {
      get
      {
        return this.mRewards != null ? new List<GenesisRewardDataParam>((IEnumerable<GenesisRewardDataParam>) this.mRewards) : new List<GenesisRewardDataParam>();
      }
    }

    public void Deserialize(JSON_GenesisRewardParam json)
    {
      if (json == null)
        return;
      this.mIname = json.iname;
      this.mRewards = (GenesisRewardDataParam[]) null;
      if (json.rewards == null || json.rewards.Length == 0)
        return;
      this.mRewards = new GenesisRewardDataParam[json.rewards.Length];
      for (int index = 0; index < json.rewards.Length; ++index)
      {
        this.mRewards[index] = new GenesisRewardDataParam();
        this.mRewards[index].Deserialize(json.rewards[index]);
      }
    }

    public static void Deserialize(
      ref List<GenesisRewardParam> list,
      JSON_GenesisRewardParam[] json)
    {
      if (json == null)
        return;
      if (list == null)
        list = new List<GenesisRewardParam>(json.Length);
      list.Clear();
      for (int index = 0; index < json.Length; ++index)
      {
        GenesisRewardParam genesisRewardParam = new GenesisRewardParam();
        genesisRewardParam.Deserialize(json[index]);
        list.Add(genesisRewardParam);
      }
    }
  }
}
