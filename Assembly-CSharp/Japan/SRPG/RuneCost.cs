﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RuneCost
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class RuneCost
  {
    public string iname;
    public string use_item;
    public int use_zeny;
    public int use_num;
    public int use_coin;

    public bool Deserialize(JSON_RuneCost json)
    {
      this.iname = json.iname;
      this.use_item = json.use_item;
      this.use_zeny = json.use_gold;
      this.use_num = json.use_item_num;
      this.use_coin = json.use_coin;
      return true;
    }

    public bool IsPlayerAmountEnough()
    {
      return (string.IsNullOrEmpty(this.use_item) || MonoSingleton<GameManager>.Instance.Player.GetItemAmount(this.use_item) >= this.use_num) && (MonoSingleton<GameManager>.Instance.Player.Gold >= this.use_zeny && MonoSingleton<GameManager>.Instance.Player.Coin >= this.use_coin);
    }
  }
}
