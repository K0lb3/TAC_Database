﻿// Decompiled with JetBrains decompiler
// Type: SRPG.StopWatch
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Diagnostics;

namespace SRPG
{
  public class StopWatch : Stopwatch
  {
    private int milliSec = (int) ((double) TimeManager.FPS * 1000.0);

    public void ReStart()
    {
      this.Reset();
      this.Start();
    }

    public void SetMilliSec(int time)
    {
      this.milliSec = time;
    }

    public bool IsElapsec()
    {
      return this.ElapsedMilliseconds > (long) this.milliSec;
    }
  }
}
