﻿// Decompiled with JetBrains decompiler
// Type: SRPG.MultiTowerQuestInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class MultiTowerQuestInfo : MonoBehaviour
  {
    [SerializeField]
    private GameObject EnemyTemplate;
    [SerializeField]
    private GameObject EnemyRoot;
    [SerializeField]
    private Text QuestTitle;
    [SerializeField]
    private Text RecommendLv;
    [SerializeField]
    private GameObject DetailObject;
    [SerializeField]
    private GameObject RewardTemplate;
    [SerializeField]
    private GameObject RewardRoot;
    [SerializeField]
    private MultiTowerQuestInfo.eRewardShowType RewardShowType;
    private GameObject Detail;
    private List<GameObject> mEnemyObject;
    private List<GameObject> mRewardObject;

    public MultiTowerQuestInfo()
    {
      base.\u002Ector();
    }

    public void Refresh()
    {
      GameUtility.SetGameObjectActive(this.RewardTemplate, false);
      MultiTowerFloorParam dataOfClass = DataSource.FindDataOfClass<MultiTowerFloorParam>(((Component) this).get_gameObject(), (MultiTowerFloorParam) null);
      if (dataOfClass == null)
        return;
      this.SetEnemy(dataOfClass);
      if (Object.op_Inequality((Object) this.QuestTitle, (Object) null))
        this.QuestTitle.set_text(dataOfClass.title + " " + dataOfClass.name);
      if (Object.op_Inequality((Object) this.RecommendLv, (Object) null))
        this.RecommendLv.set_text(string.Format(LocalizedText.Get("sys.MULTI_TOWER_RECOMMEND"), (object) dataOfClass.lv, (object) dataOfClass.joblv));
      this.SetReward(dataOfClass);
    }

    private void SetEnemy(MultiTowerFloorParam param)
    {
      int index1 = 0;
      if (param.map == null)
        return;
      string src = AssetManager.LoadTextData(AssetPath.LocalMap(param.map[0].mapSetName));
      if (src == null)
        return;
      JSON_MapUnit jsonObject = JSONParser.parseJSONObject<JSON_MapUnit>(src);
      if (jsonObject == null || !Object.op_Inequality((Object) this.EnemyTemplate, (Object) null))
        return;
      for (int index2 = 0; index2 < jsonObject.enemy.Length; ++index2)
      {
        NPCSetting npcSetting = new NPCSetting(jsonObject.enemy[index2]);
        Unit data = new Unit();
        if (data != null && data.Setup((UnitData) null, (UnitSetting) npcSetting, (Unit.DropItem) null, (Unit.DropItem) null) && !data.IsGimmick)
        {
          GameObject root;
          if (index1 + 1 > this.mEnemyObject.Count)
          {
            root = (GameObject) Object.Instantiate<GameObject>((M0) this.EnemyTemplate);
            if (!Object.op_Equality((Object) root, (Object) null))
              this.mEnemyObject.Add(root);
            else
              continue;
          }
          else
            root = this.mEnemyObject[index1];
          DataSource.Bind<Unit>(root, data, false);
          GameParameter.UpdateAll(root);
          if (Object.op_Inequality((Object) this.EnemyRoot, (Object) null))
            root.get_transform().SetParent(this.EnemyRoot.get_transform(), false);
          root.SetActive(true);
          ++index1;
        }
      }
      for (int index2 = index1; index2 < this.mEnemyObject.Count; ++index2)
        this.mEnemyObject[index2].SetActive(false);
      this.EnemyTemplate.SetActive(false);
    }

    private void SetReward(MultiTowerFloorParam param)
    {
      GameManager instance = MonoSingleton<GameManager>.Instance;
      List<MultiTowerRewardItem> mtFloorReward = instance.GetMTFloorReward(param.reward_id, instance.GetMTRound((int) param.floor));
      if (mtFloorReward == null || Object.op_Equality((Object) this.RewardTemplate, (Object) null))
        return;
      for (int index = this.mRewardObject.Count - 1; index >= 0; --index)
        Object.Destroy((Object) this.mRewardObject[index]);
      if (this.RewardShowType == MultiTowerQuestInfo.eRewardShowType.TopOnly)
      {
        MultiTowerRewardItem reward = mtFloorReward.Count <= 0 ? (MultiTowerRewardItem) null : mtFloorReward[0];
        if (!Object.op_Inequality((Object) this.RewardTemplate, (Object) null) || reward == null)
          return;
        this.RewardTemplate.SetActive(true);
        this.BindData(this.RewardTemplate, reward);
      }
      else
      {
        if (this.RewardShowType != MultiTowerQuestInfo.eRewardShowType.All)
          return;
        for (int index = 0; index < mtFloorReward.Count; ++index)
        {
          GameObject rewardObject = this.CreateRewardObject();
          if (!Object.op_Equality((Object) rewardObject, (Object) null))
          {
            this.mRewardObject.Add(rewardObject);
            this.BindData(rewardObject, mtFloorReward[index]);
          }
        }
      }
    }

    private GameObject CreateRewardObject()
    {
      if (Object.op_Equality((Object) this.RewardTemplate, (Object) null))
        return (GameObject) null;
      if (Object.op_Equality((Object) this.RewardRoot, (Object) null))
      {
        DebugUtility.LogError("CreateRewardObject -> RewardRoot == null");
        return (GameObject) null;
      }
      GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.RewardTemplate);
      gameObject.get_transform().SetParent(this.RewardRoot.get_transform(), false);
      gameObject.SetActive(true);
      return gameObject;
    }

    private void BindData(GameObject obj, MultiTowerRewardItem reward)
    {
      DataSource.Bind<MultiTowerRewardItem>(obj, reward, false);
      MultiTowerRewardInfo component = (MultiTowerRewardInfo) obj.GetComponent<MultiTowerRewardInfo>();
      if (!Object.op_Inequality((Object) component, (Object) null))
        return;
      component.Refresh();
    }

    public enum eRewardShowType
    {
      TopOnly,
      All,
    }
  }
}
