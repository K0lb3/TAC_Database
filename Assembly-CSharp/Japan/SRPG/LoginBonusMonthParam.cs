﻿// Decompiled with JetBrains decompiler
// Type: SRPG.LoginBonusMonthParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class LoginBonusMonthParam
  {
    private LoginBonusMonthState m_State;
    private int m_Day;

    public LoginBonusMonthParam(LoginBonusMonthState state, int day)
    {
      this.m_State = state;
      this.m_Day = day;
    }

    public LoginBonusMonthState State
    {
      get
      {
        return this.m_State;
      }
    }

    public int Day
    {
      get
      {
        return this.m_Day;
      }
    }
  }
}
