﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_GuildRaidBossParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_GuildRaidBossParam : JSON_GuildRaidMasterParam
  {
    public int id;
    public int period_id;
    public int area_no;
    public string name;
    public int hp;
    public int hp_warning;
    public string unit_iname;
    public string quest_iname;
    public int score_id;
    public string buff_id;
    public string mob_buff_id;
    public string beat_reward_id;
    public string damage_ranking_reward_id;
    public string damage_ratio_reward_id;
    public string lastatk_reward_id;
  }
}
