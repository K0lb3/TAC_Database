﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_BoxGachaItems
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_BoxGachaItems
  {
    public int is_feature_item;
    public string itype;
    public string iname;
    public int num;
    public int total_num;
    public int remain_num;
    public int coin;
    public int gold;
    public string box_set_iname;
    public string box_set_name;
  }
}
