﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ResultOrdeal
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class ResultOrdeal : MonoBehaviour
  {
    public ResultOrdeal()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      SceneBattle instance = SceneBattle.Instance;
      if (!Object.op_Implicit((Object) instance) || instance.ResultData == null)
        return;
      DataSource.Bind<BattleCore.Record>(((Component) this).get_gameObject(), instance.ResultData.Record, false);
      GameParameter.UpdateAll(((Component) this).get_gameObject());
    }
  }
}
