﻿// Decompiled with JetBrains decompiler
// Type: SRPG.TargetMarker
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class TargetMarker : MonoBehaviour
  {
    private Transform m_Transform;

    public TargetMarker()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
      this.m_Transform = (Transform) ((Component) this).GetComponent<Transform>();
    }

    private void LateUpdate()
    {
      SceneBattle instance = SceneBattle.Instance;
      Vector3 zero = Vector3.get_zero();
      if (Object.op_Inequality((Object) instance, (Object) null) && instance.isUpView)
      {
        ref Vector3 local = ref zero;
        local.y = (__Null) (local.y + 0.649999976158142);
      }
      this.m_Transform.set_localPosition(zero);
    }
  }
}
