﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GenesisChapterSelectorItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SRPG
{
  public class GenesisChapterSelectorItem : MonoBehaviour
  {
    [SerializeField]
    private Text TextTitle;
    [SerializeField]
    private Transform TrParentBanner;
    [SerializeField]
    private GameObject GoLock;
    [Space(5f)]
    [SerializeField]
    private SRPG_Button SelectBtn;
    private GenesisChapterParam mChapterParam;
    private bool mIsOutOfPeriod;
    private bool mIsLiberation;

    public GenesisChapterSelectorItem()
    {
      base.\u002Ector();
    }

    public GenesisChapterParam ChapterParam
    {
      get
      {
        return this.mChapterParam;
      }
    }

    public bool IsOutOfPeriod
    {
      get
      {
        return this.mIsOutOfPeriod;
      }
    }

    public bool IsLiberation
    {
      get
      {
        return this.mIsLiberation;
      }
    }

    public void SetItem(
      GenesisChapterParam chapter_param,
      UnityAction action,
      bool is_out_of_period,
      bool is_liberation)
    {
      if (chapter_param == null)
        return;
      this.mChapterParam = chapter_param;
      this.mIsOutOfPeriod = is_out_of_period;
      this.mIsLiberation = is_liberation;
      if (Object.op_Implicit((Object) this.TextTitle))
        this.TextTitle.set_text(chapter_param.Name);
      if (Object.op_Implicit((Object) this.TrParentBanner) && !string.IsNullOrEmpty(chapter_param.ChapterBanner) && Object.op_Implicit((Object) GenesisManager.Instance))
        GenesisManager.Instance.LoadAssets<GameObject>(chapter_param.ChapterBanner, (GenesisManager.LoadAssetCallback<GameObject>) (prefab =>
        {
          if (Object.op_Equality((Object) prefab, (Object) null))
          {
            DebugUtility.LogError("GenesisChapterSelectorItem/AssetLoad Error! name=" + this.mChapterParam.ChapterBanner);
          }
          else
          {
            prefab.get_gameObject().SetActive(true);
            Object.Instantiate<GameObject>((M0) prefab, this.TrParentBanner);
          }
        }));
      if (Object.op_Implicit((Object) this.GoLock))
        this.GoLock.SetActive(is_out_of_period || !is_liberation);
      if (action == null || !Object.op_Implicit((Object) this.SelectBtn))
        return;
      ((UnityEvent) this.SelectBtn.get_onClick()).RemoveListener(action);
      ((UnityEvent) this.SelectBtn.get_onClick()).AddListener(action);
    }
  }
}
