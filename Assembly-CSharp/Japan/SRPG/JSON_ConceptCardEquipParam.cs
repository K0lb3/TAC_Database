﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_ConceptCardEquipParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_ConceptCardEquipParam
  {
    public string cnds_iname;
    public string card_skill;
    public string add_card_skill_buff_awake;
    public string add_card_skill_buff_lvmax;
    public string abil_iname;
    public string abil_iname_lvmax;
    public string statusup_skill;
    public string skin;
    public int is_decrease_eff;
  }
}
