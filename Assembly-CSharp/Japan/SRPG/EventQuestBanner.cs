﻿// Decompiled with JetBrains decompiler
// Type: SRPG.EventQuestBanner
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class EventQuestBanner : MonoBehaviour, IGameParameter
  {
    public GameObject Lock;
    public GameObject Counter;
    public Text CounterText;
    public Text CounterLimitText;
    private bool m_HideChallengeCounter;

    public EventQuestBanner()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      this.UpdateValue();
    }

    public void UpdateValue()
    {
      ChapterParam dataOfClass = DataSource.FindDataOfClass<ChapterParam>(((Component) this).get_gameObject(), (ChapterParam) null);
      if (dataOfClass == null)
        return;
      if (dataOfClass.challengeLimit <= 0 || this.m_HideChallengeCounter)
      {
        if (Object.op_Inequality((Object) this.Lock, (Object) null))
          this.Lock.SetActive(false);
        if (!Object.op_Inequality((Object) this.Counter, (Object) null))
          return;
        this.Counter.SetActive(false);
      }
      else
      {
        ChapterParam chapter;
        bool flag = dataOfClass.CheckEnableChallange(out chapter);
        if (Object.op_Inequality((Object) this.Lock, (Object) null))
          this.Lock.SetActive(!flag);
        if (Object.op_Inequality((Object) this.Counter, (Object) null))
          this.Counter.SetActive(true);
        if (Object.op_Inequality((Object) this.CounterText, (Object) null))
          this.CounterText.set_text(chapter.challengeCount.ToString());
        if (!Object.op_Inequality((Object) this.CounterLimitText, (Object) null))
          return;
        this.CounterLimitText.set_text(chapter.challengeLimit.ToString());
      }
    }

    public void SetHideChallengeCounter(bool hide)
    {
      this.m_HideChallengeCounter = hide;
    }
  }
}
