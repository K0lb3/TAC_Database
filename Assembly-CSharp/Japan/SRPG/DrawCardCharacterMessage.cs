﻿// Decompiled with JetBrains decompiler
// Type: SRPG.DrawCardCharacterMessage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class DrawCardCharacterMessage : MonoBehaviour
  {
    private static DrawCardCharacterMessage mInstance;
    [SerializeField]
    private GameObject mMessageParent;
    [SerializeField]
    private Text mMessageText;
    private const float MESSAGE_SPEED = 0.05f;
    private string mMessageString;
    private int mMessageIndex;
    private float mMessageSeconds;

    public DrawCardCharacterMessage()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
      DrawCardCharacterMessage.mInstance = this;
      if (Object.op_Inequality((Object) this.mMessageParent, (Object) null))
        this.mMessageParent.SetActive(false);
      if (Object.op_Inequality((Object) this.mMessageText, (Object) null))
        this.mMessageText.set_text(string.Empty);
      this.mMessageString = string.Empty;
      this.mMessageIndex = 0;
      this.mMessageSeconds = 0.0f;
    }

    private void Update()
    {
      if (string.IsNullOrEmpty(this.mMessageString) || this.mMessageString.Length <= this.mMessageIndex)
        return;
      if (Input.get_anyKeyDown())
      {
        this.mMessageIndex = this.mMessageString.Length;
      }
      else
      {
        this.mMessageSeconds += Time.get_deltaTime();
        if ((double) this.mMessageSeconds >= 0.0500000007450581)
        {
          this.mMessageSeconds -= 0.05f;
          ++this.mMessageIndex;
        }
      }
      if (Object.op_Equality((Object) this.mMessageText, (Object) null))
        return;
      this.mMessageText.set_text(this.mMessageString.Substring(0, this.mMessageIndex));
    }

    public static bool IsMessaging
    {
      get
      {
        return !Object.op_Equality((Object) DrawCardCharacterMessage.mInstance, (Object) null) && !string.IsNullOrEmpty(DrawCardCharacterMessage.mInstance.mMessageString) && DrawCardCharacterMessage.mInstance.mMessageString.Length > DrawCardCharacterMessage.mInstance.mMessageIndex;
      }
    }

    public static void ShowMessage(string message)
    {
      if (Object.op_Equality((Object) DrawCardCharacterMessage.mInstance, (Object) null))
        DebugUtility.LogError("It is uninitialized.");
      else
        DrawCardCharacterMessage.mInstance._showMessage(message);
    }

    public static void HiddenMessage()
    {
      if (Object.op_Equality((Object) DrawCardCharacterMessage.mInstance, (Object) null))
      {
        DebugUtility.LogError("It is uninitialized.");
      }
      else
      {
        if (Object.op_Equality((Object) DrawCardCharacterMessage.mInstance.mMessageParent, (Object) null))
          return;
        DrawCardCharacterMessage.mInstance.mMessageParent.SetActive(false);
      }
    }

    private void _showMessage(string message)
    {
      if (Object.op_Equality((Object) this.mMessageParent, (Object) null))
        return;
      this.mMessageParent.SetActive(true);
      if (Object.op_Equality((Object) this.mMessageText, (Object) null))
        return;
      this.mMessageText.set_text(string.Empty);
      this.mMessageString = LocalizedText.Get(message);
      this.mMessageIndex = 1;
      this.mMessageSeconds = 0.0f;
    }
  }
}
