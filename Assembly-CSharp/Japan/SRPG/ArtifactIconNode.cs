﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ArtifactIconNode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class ArtifactIconNode : ContentNode
  {
    [SerializeField]
    public ArtifactIcon Icon;
    [SerializeField]
    public GameObject RecommendObject;
    [SerializeField]
    public GameObject SelectObject;
    [SerializeField]
    public GameObject EmptyObject;

    public void Setup(ArtifactData arti_data)
    {
      if (Object.op_Equality((Object) this.Icon, (Object) null) || arti_data == null)
        return;
      ((Component) this.Icon).get_gameObject().SetActive(true);
      DataSource.Bind<ArtifactData>(((Component) this.Icon).get_gameObject(), arti_data, false);
      GameParameter.UpdateAll(((Component) this.Icon).get_gameObject());
    }

    public void Setup(ArtifactParam arti_param)
    {
      if (Object.op_Equality((Object) this.Icon, (Object) null) || arti_param == null)
        return;
      ((Component) this.Icon).get_gameObject().SetActive(true);
      DataSource.Bind<ArtifactParam>(((Component) this.Icon).get_gameObject(), arti_param, false);
      GameParameter.UpdateAll(((Component) this.Icon).get_gameObject());
    }

    public void Empty(bool is_enmpty)
    {
      if (Object.op_Equality((Object) this.EmptyObject, (Object) null) || Object.op_Equality((Object) this.Icon, (Object) null))
        return;
      ((Component) this.Icon).get_gameObject().SetActive(!is_enmpty);
      this.EmptyObject.SetActive(is_enmpty);
    }

    public void Enable(bool enable)
    {
      if (Object.op_Equality((Object) this.Icon, (Object) null) || !((Component) this.Icon).get_gameObject().get_activeSelf())
        return;
      this.Icon.EquipForceMask = !enable;
      Button component = (Button) ((Component) this.Icon).GetComponent<Button>();
      if (!Object.op_Inequality((Object) component, (Object) null))
        return;
      ((Selectable) component).set_interactable(enable);
    }

    public void Select(bool select)
    {
      if (Object.op_Equality((Object) this.Icon, (Object) null) || !((Component) this.Icon).get_gameObject().get_activeSelf() || Object.op_Equality((Object) this.SelectObject, (Object) null))
        return;
      this.SelectObject.SetActive(select);
    }

    public void Recommend(bool is_recommend)
    {
      if (Object.op_Equality((Object) this.Icon, (Object) null) || !((Component) this.Icon).get_gameObject().get_activeSelf() || Object.op_Equality((Object) this.RecommendObject, (Object) null))
        return;
      this.RecommendObject.SetActive(is_recommend);
    }
  }
}
