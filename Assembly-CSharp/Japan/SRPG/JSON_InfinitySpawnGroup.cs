﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_InfinitySpawnGroup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_InfinitySpawnGroup
  {
    public int tag;
    public int deck;
    public int interval;
    public int spawn_max;
    public int spawn_unit_num;
    public int is_spawn_at_start;
    public int is_skip_empty_at_start;
  }
}
