﻿// Decompiled with JetBrains decompiler
// Type: SRPG.QuestCampaignInspSkill
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class QuestCampaignInspSkill
  {
    private string mIname;
    private string mInspirationSkillIname;
    private int mRate;

    public string Iname
    {
      get
      {
        return this.mIname;
      }
    }

    public string InspirationSkillIname
    {
      get
      {
        return this.mInspirationSkillIname;
      }
    }

    public int Rate
    {
      get
      {
        return this.mRate;
      }
    }

    public bool Deserialize(JSON_QuestCampaignInspSkill json)
    {
      this.mIname = json.children_iname;
      this.mInspirationSkillIname = json.insp;
      this.mRate = json.rate;
      return true;
    }
  }
}
