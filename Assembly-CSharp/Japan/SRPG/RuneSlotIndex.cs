﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RuneSlotIndex
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public struct RuneSlotIndex
  {
    private byte value;
    public const byte All = 255;
    public const byte Min = 0;
    public const byte Max = 5;
    public const byte MaxCount = 6;

    public RuneSlotIndex(byte value)
    {
      if (value != byte.MaxValue && ((byte) 0 > value || value > (byte) 5))
      {
        DebugUtility.LogError("適正でないスロット番号「value=" + (object) value + "」が設定されようとしています");
        value = (byte) 0;
      }
      this.value = value;
    }

    public static implicit operator RuneSlotIndex(byte value)
    {
      return new RuneSlotIndex(value);
    }

    public static implicit operator byte(RuneSlotIndex value)
    {
      return value.value;
    }

    public static RuneSlotIndex CreateSlotToIndex(int slot)
    {
      return new RuneSlotIndex((byte) (slot - 1));
    }

    public static int IndexToSlot(int index)
    {
      return index + 1;
    }
  }
}
