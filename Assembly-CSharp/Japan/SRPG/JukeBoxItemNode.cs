﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JukeBoxItemNode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace SRPG
{
  public class JukeBoxItemNode : ContentNode, IGameParameter, IPointerDownHandler, IHoldGesture, IEventSystemHandler
  {
    [SerializeField]
    private Text TextMusic;
    [Space(5f)]
    [SerializeField]
    private GameObject GoActive;
    [SerializeField]
    private GameObject NewBadge;
    [SerializeField]
    private GameObject Mylist;
    [Space(5f)]
    [SerializeField]
    private SRPG_Button BtnSelect;
    [Space(5f)]
    [SerializeField]
    private GameObject GoLocked;
    [SerializeField]
    private SRPG_Button BtnLock;
    private JukeBoxItemParam mParam;

    public JukeBoxItemParam Param
    {
      get
      {
        return this.mParam;
      }
    }

    public void Setup(
      JukeBoxItemParam param,
      bool is_current,
      bool is_new,
      bool is_mylist,
      UnityAction action = null,
      UnityAction lock_action = null,
      UnityAction long_tap_action = null)
    {
      if (param == null || param.ItemData == null || param.ItemData.param == null)
        return;
      this.mParam = param;
      if (Object.op_Implicit((Object) this.TextMusic))
        this.TextMusic.set_text(param.ItemData.param.Title);
      if (Object.op_Implicit((Object) this.GoLocked))
      {
        this.GoLocked.SetActive(!param.ItemData.is_unlock);
        if (lock_action != null && Object.op_Implicit((Object) this.BtnSelect))
        {
          ((UnityEventBase) this.BtnSelect.get_onClick()).RemoveAllListeners();
          ((UnityEvent) this.BtnSelect.get_onClick()).AddListener(lock_action);
        }
      }
      if (action != null && param.ItemData.is_unlock && Object.op_Implicit((Object) this.BtnSelect))
      {
        ((UnityEventBase) this.BtnSelect.get_onClick()).RemoveAllListeners();
        ((UnityEvent) this.BtnSelect.get_onClick()).AddListener(action);
      }
      if (Object.op_Implicit((Object) this.GoActive))
        this.GoActive.SetActive(false);
      if (Object.op_Implicit((Object) this.NewBadge))
        this.NewBadge.SetActive(false);
      if (Object.op_Implicit((Object) this.Mylist))
        this.Mylist.SetActive(false);
      this.SetCurrent(is_current);
      this.SetNewBadge(is_new);
      this.SetMylist(is_mylist);
    }

    public void SetCurrent(bool is_active)
    {
      if (!Object.op_Implicit((Object) this.GoActive))
        return;
      this.GoActive.SetActive(is_active);
    }

    public bool IsCurrent()
    {
      if (Object.op_Implicit((Object) this.GoActive))
        this.GoActive.GetActive();
      return false;
    }

    public void SetNewBadge(bool is_new)
    {
      if (!Object.op_Implicit((Object) this.NewBadge))
        return;
      this.NewBadge.SetActive(is_new);
    }

    public void SetMylist(bool is_mylist)
    {
      if (!Object.op_Implicit((Object) this.Mylist))
        return;
      this.Mylist.SetActive(is_mylist);
    }

    public virtual bool HasTooltip
    {
      get
      {
        return true;
      }
    }

    protected void ShowTooltip(Vector2 screen)
    {
      if (this.mParam == null || !this.mParam.ItemData.is_unlock || string.IsNullOrEmpty(this.mParam.ItemData.param.Situation))
        return;
      this.mParam.LongTapAction.Invoke();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
      HoldGestureObserver.StartHoldGesture((IHoldGesture) this);
    }

    public void OnPointerHoldStart()
    {
      if (!this.HasTooltip)
        return;
      RectTransform transform = (RectTransform) ((Component) this).get_transform();
      Vector2 screen = Vector2.op_Implicit(((Transform) transform).TransformPoint(Vector2.op_Implicit(Vector2.get_zero())));
      CanvasScaler componentInParent = (CanvasScaler) ((Component) transform).GetComponentInParent<CanvasScaler>();
      if (Object.op_Inequality((Object) componentInParent, (Object) null))
      {
        Vector3 localScale = ((Component) componentInParent).get_transform().get_localScale();
        ref Vector2 local1 = ref screen;
        local1.x = local1.x / localScale.x;
        ref Vector2 local2 = ref screen;
        local2.y = local2.y / localScale.y;
      }
      this.ShowTooltip(screen);
    }

    public void OnPointerHoldEnd()
    {
    }

    public void UpdateValue()
    {
    }
  }
}
