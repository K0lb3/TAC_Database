﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_AdvanceEventParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_AdvanceEventParam
  {
    public string iname;
    public int trans_type;
    public int priority;
    public string area_id;
    public string name;
    public string box_iname;
    public int event_ui_index;
    public string event_banner;
    public string event_detail_url;
    public string boss_hint_url;
    public JSON_AdvanceEventModeInfoParam[] mode_info;
  }
}
