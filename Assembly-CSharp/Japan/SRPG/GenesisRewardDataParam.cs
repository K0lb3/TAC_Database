﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GenesisRewardDataParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class GenesisRewardDataParam
  {
    public int ItemType;
    public string ItemIname;
    public int ItemNum;

    public void Deserialize(JSON_GenesisRewardDataParam json)
    {
      if (json == null)
        return;
      this.ItemType = json.item_type;
      this.ItemIname = json.item_iname;
      this.ItemNum = json.item_num;
    }
  }
}
