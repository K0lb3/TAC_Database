﻿// Decompiled with JetBrains decompiler
// Type: SRPG.SupportElementList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System;
using UnityEngine;

namespace SRPG
{
  public class SupportElementList : MonoBehaviour
  {
    [StringIsResourcePath(typeof (GameObject))]
    [SerializeField]
    private string CARDLIST_WINDOW_PATH;
    [SerializeField]
    private GameObject[] m_SupportUnits;
    [SerializeField]
    private GenericSlot[] mConceptCards;
    private GameObject mCardSelectWindow;

    public SupportElementList()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
      for (int index = 0; index < this.mConceptCards.Length; ++index)
      {
        if (!UnityEngine.Object.op_Equality((UnityEngine.Object) this.mConceptCards[index], (UnityEngine.Object) null))
        {
          PartySlotData data = new PartySlotData(PartySlotType.Free, (string) null, PartySlotIndex.Main1, false);
          this.mConceptCards[index].SetSlotData<PartySlotData>(data);
        }
      }
    }

    public void Clear()
    {
      if (this.m_SupportUnits == null)
      {
        DebugUtility.LogError("m_SupportUnitsがnullです。");
      }
      else
      {
        for (int element = 0; element < this.m_SupportUnits.Length; ++element)
          this.Refresh(element, (UnitData) null);
      }
    }

    public void Refresh(int element, UnitData unit)
    {
      if (this.m_SupportUnits == null)
        DebugUtility.LogError("m_SupportUnitsがnullです。");
      else if (this.m_SupportUnits.Length < Enum.GetValues(typeof (EElement)).Length)
        DebugUtility.LogError("m_SupportUnitsの数が足りません。Inspectorからの設定を確認してください。");
      else if (element >= this.m_SupportUnits.Length)
      {
        DebugUtility.LogError("unitsの数が足りません。");
      }
      else
      {
        if (unit != null)
        {
          UnitData unit1 = new UnitData();
          unit1.Setup(unit);
          unit1.TempFlags |= UnitData.TemporaryFlags.TemporaryUnitData | UnitData.TemporaryFlags.AllowJobChange;
          eOverWritePartyType party_type = UnitOverWriteUtility.Element2OverWritePartyType((EElement) element);
          unit = UnitOverWriteUtility.Apply(unit1, party_type, true);
        }
        if (!UnityEngine.Object.op_Inequality((UnityEngine.Object) this.m_SupportUnits[element], (UnityEngine.Object) null))
          return;
        GameObject gameObject = this.m_SupportUnits[element].get_gameObject();
        if (!UnityEngine.Object.op_Inequality((UnityEngine.Object) gameObject, (UnityEngine.Object) null))
          return;
        SerializeValueBehaviour component = (SerializeValueBehaviour) gameObject.GetComponent<SerializeValueBehaviour>();
        DataSource dataSource = DataSource.Create(gameObject);
        if (!UnityEngine.Object.op_Inequality((UnityEngine.Object) dataSource, (UnityEngine.Object) null))
          return;
        dataSource.Clear();
        if (unit != null)
        {
          dataSource.Add(typeof (UnitData), (object) unit);
          if (UnityEngine.Object.op_Inequality((UnityEngine.Object) component, (UnityEngine.Object) null))
          {
            component.list.SetInteractable("btn", true);
            component.list.SetActive(1, true);
          }
        }
        else if (UnityEngine.Object.op_Inequality((UnityEngine.Object) component, (UnityEngine.Object) null))
        {
          component.list.SetInteractable("btn", false);
          component.list.SetActive(1, false);
        }
        ConceptCardData card = unit == null ? (ConceptCardData) null : unit.MainConceptCard;
        ConceptCardIcon componentInChildren = (ConceptCardIcon) gameObject.GetComponentInChildren<ConceptCardIcon>();
        if (UnityEngine.Object.op_Inequality((UnityEngine.Object) componentInChildren, (UnityEngine.Object) null))
          componentInChildren.Setup(card);
        if (element <= this.mConceptCards.Length - 1)
          this.mConceptCards[element].OnSelect = new GenericSlot.SelectEvent(this.OnCardSlotClick);
        eOverWritePartyType data = UnitOverWriteUtility.Element2OverWritePartyType((EElement) element);
        DataSource.Bind<eOverWritePartyType>(gameObject, data, false);
        GameParameter.UpdateAll(gameObject);
      }
    }

    public void Refresh(long[] uniqs)
    {
      if (this.m_SupportUnits == null)
        DebugUtility.LogError("m_SupportUnitsがnullです。");
      else if (uniqs == null)
        DebugUtility.LogError("unitsがnullです。");
      else if (this.m_SupportUnits.Length < Enum.GetValues(typeof (EElement)).Length)
        DebugUtility.LogError("m_SupportUnitsの数が足りません。Inspectorからの設定を確認してください。");
      else if (uniqs.Length < this.m_SupportUnits.Length)
      {
        DebugUtility.LogError("unitsの数が足りません。");
      }
      else
      {
        for (int element = 0; element < uniqs.Length; ++element)
          this.Refresh(element, MonoSingleton<GameManager>.Instance.Player.FindUnitDataByUniqueID(uniqs[element]));
        GameParameter.UpdateAll(((Component) this).get_gameObject());
      }
    }

    public void Refresh(UnitData[] units)
    {
      if (this.m_SupportUnits == null)
        DebugUtility.LogError("m_SupportUnitsがnullです。");
      else if (units == null)
        DebugUtility.LogError("unitsがnullです。");
      else if (this.m_SupportUnits.Length < Enum.GetValues(typeof (EElement)).Length)
        DebugUtility.LogError("m_SupportUnitsの数が足りません。Inspectorからの設定を確認してください。");
      else if (units.Length < this.m_SupportUnits.Length)
      {
        DebugUtility.LogError("unitsの数が足りません。");
      }
      else
      {
        for (int element = 0; element < units.Length; ++element)
          this.Refresh(element, units[element]);
        GameParameter.UpdateAll(((Component) this).get_gameObject());
      }
    }

    private void OnCardSlotClick(GenericSlot slot, bool interactable)
    {
      int element_index = Array.IndexOf<GenericSlot>(this.mConceptCards, slot);
      UnitData unit_data = DataSource.FindDataOfClass<UnitData>(((Component) slot).get_gameObject(), (UnitData) null);
      if (unit_data == null)
        return;
      GameObject gameObject = AssetManager.Load<GameObject>(this.CARDLIST_WINDOW_PATH);
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) gameObject, (UnityEngine.Object) null))
        return;
      this.mCardSelectWindow = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) gameObject);
      ConceptCardEquipWindow component = (ConceptCardEquipWindow) this.mCardSelectWindow.GetComponent<ConceptCardEquipWindow>();
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) component, (UnityEngine.Object) null))
        return;
      eOverWritePartyType overWritePartyType = UnitOverWriteUtility.Element2OverWritePartyType((EElement) element_index);
      GlobalVars.OverWritePartyType.Set(overWritePartyType);
      component.Init(unit_data, 0);
      component.OnChangeAction = (Action) (() => ((DestroyEventListener) this.mCardSelectWindow.AddComponent<DestroyEventListener>()).Listeners += (DestroyEventListener.DestroyEvent) (go =>
      {
        GlobalVars.OverWritePartyType.Set(eOverWritePartyType.None);
        this.Refresh(element_index, unit_data);
      }));
    }
  }
}
