﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqAutoRepeatQuestProgress
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Network.Encoding;
using MessagePack;
using System;

namespace SRPG
{
  public class ReqAutoRepeatQuestProgress : WebAPI
  {
    public ReqAutoRepeatQuestProgress(
      int current_lap,
      SRPG.Network.ResponseCallback response,
      EncodingTypes.ESerializeCompressMethod serializeCompressMethod)
    {
      this.name = "btl/auto_repeat/progress";
      this.body = WebAPI.GetRequestString<ReqAutoRepeatQuestProgress.RequestParam>(new ReqAutoRepeatQuestProgress.RequestParam()
      {
        current_lap_num = current_lap
      });
      this.callback = response;
      this.serializeCompressMethod = serializeCompressMethod;
    }

    [Serializable]
    public class RequestParam
    {
      public int current_lap_num;
    }

    [MessagePackObject(true)]
    [Serializable]
    public class Response
    {
      public Json_AutoRepeatQuestData auto_repeat;
      public int box_extension_count;
    }
  }
}
