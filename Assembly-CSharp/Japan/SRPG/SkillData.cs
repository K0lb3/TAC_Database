﻿// Decompiled with JetBrains decompiler
// Type: SRPG.SkillData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using MessagePack;
using System;
using System.Collections.Generic;

namespace SRPG
{
  [MessagePackObject(true)]
  public class SkillData
  {
    private OInt mRank = (OInt) 1;
    private OInt mRankCap = (OInt) 1;
    private SkillParam mSkillParam;
    private OInt mCastSpeed;
    private OInt mEffectRate;
    private OInt mEffectValue;
    private OInt mEffectRange;
    private OInt mElementValue;
    private OInt mControlDamageRate;
    private OInt mControlDamageValue;
    private OInt mControlChargeTimeRate;
    private OInt mControlChargeTimeValue;
    private OInt mShieldTurn;
    private OInt mShieldValue;
    public BuffEffect mTargetBuffEffect;
    public CondEffect mTargetCondEffect;
    public BuffEffect mSelfBuffEffect;
    public CondEffect mSelfCondEffect;
    private OInt mUseRate;
    private SkillLockCondition mUseCondition;
    private bool mCheckCount;
    private OBool mIsCollabo;
    private string mReplaceSkillId;
    public string m_BaseSkillIname;
    private AbilityData m_OwnerAbility;
    private SkillDeriveParam m_SkillDeriveParam;
    private ConceptCardData m_ConceptCardData;
    private int mProtectValue;
    private bool m_IsDecreaseEffectOnSubSlot;
    private DependStateSpcEffParam mDependStateSpcEffParam;
    private DependStateSpcEffParam mDependStateSpcEffParamSelf;

    public SkillParam SkillParam
    {
      get
      {
        return this.mSkillParam;
      }
    }

    public string SkillID
    {
      get
      {
        return this.mSkillParam != null ? this.mSkillParam.iname : (string) null;
      }
    }

    public int Rank
    {
      get
      {
        return (int) this.mRank;
      }
    }

    public string Name
    {
      get
      {
        return this.mSkillParam.name;
      }
    }

    public ESkillType SkillType
    {
      get
      {
        return this.mSkillParam.type;
      }
    }

    public ESkillTarget Target
    {
      get
      {
        return this.mSkillParam.target;
      }
    }

    public ESkillTiming Timing
    {
      get
      {
        return this.mSkillParam.timing;
      }
    }

    public ESkillCondition Condition
    {
      get
      {
        return this.mSkillParam.condition;
      }
    }

    public int Cost
    {
      get
      {
        return this.SkillParam.cost;
      }
    }

    public ELineType LineType
    {
      get
      {
        return this.mSkillParam.line_type;
      }
    }

    public int EnableAttackGridHeight
    {
      get
      {
        return this.mSkillParam.effect_height;
      }
    }

    public int RangeMin
    {
      get
      {
        return this.mSkillParam.range_min;
      }
    }

    public int RangeMax
    {
      get
      {
        return this.mSkillParam.range_max;
      }
    }

    public int Scope
    {
      get
      {
        return this.mSkillParam.scope;
      }
    }

    public int HpCostRate
    {
      get
      {
        return this.mSkillParam.hp_cost_rate;
      }
    }

    public ECastTypes CastType
    {
      get
      {
        return this.mSkillParam.cast_type;
      }
    }

    public OInt CastSpeed
    {
      get
      {
        return this.mCastSpeed;
      }
    }

    public SkillEffectTypes EffectType
    {
      get
      {
        return this.mSkillParam.effect_type;
      }
    }

    public OInt EffectRate
    {
      get
      {
        return this.mEffectRate;
      }
    }

    public OInt EffectValue
    {
      get
      {
        return this.mEffectValue;
      }
    }

    public OInt EffectRange
    {
      get
      {
        return this.mEffectRange;
      }
    }

    public OInt EffectHpMaxRate
    {
      get
      {
        return (OInt) this.mSkillParam.effect_hprate;
      }
    }

    public OInt EffectGemsMaxRate
    {
      get
      {
        return (OInt) this.mSkillParam.effect_mprate;
      }
    }

    public SkillParamCalcTypes EffectCalcType
    {
      get
      {
        return this.mSkillParam.effect_calc;
      }
    }

    public EElement ElementType
    {
      get
      {
        return this.mSkillParam.element_type;
      }
    }

    public OInt ElementValue
    {
      get
      {
        return this.mElementValue;
      }
    }

    public AttackTypes AttackType
    {
      get
      {
        return this.mSkillParam.attack_type;
      }
    }

    public AttackDetailTypes AttackDetailType
    {
      get
      {
        return this.mSkillParam.attack_detail;
      }
    }

    public int BackAttackDefenseDownRate
    {
      get
      {
        return this.mSkillParam.back_defrate;
      }
    }

    public int SideAttackDefenseDownRate
    {
      get
      {
        return this.mSkillParam.side_defrate;
      }
    }

    public DamageTypes ReactionDamageType
    {
      get
      {
        return this.mSkillParam.reaction_damage_type;
      }
    }

    public int DamageAbsorbRate
    {
      get
      {
        return this.mSkillParam.absorb_damage_rate;
      }
    }

    public OInt ControlDamageRate
    {
      get
      {
        return this.mControlDamageRate;
      }
    }

    public OInt ControlDamageValue
    {
      get
      {
        return this.mControlDamageValue;
      }
    }

    public SkillParamCalcTypes ControlDamageCalcType
    {
      get
      {
        return this.mSkillParam.control_damage_calc;
      }
    }

    public OInt ControlChargeTimeRate
    {
      get
      {
        return this.mControlChargeTimeRate;
      }
    }

    public OInt ControlChargeTimeValue
    {
      get
      {
        return this.mControlChargeTimeValue;
      }
    }

    public SkillParamCalcTypes ControlChargeTimeCalcType
    {
      get
      {
        return this.mSkillParam.control_ct_calc;
      }
    }

    public ShieldTypes ShieldType
    {
      get
      {
        return this.mSkillParam.shield_type;
      }
    }

    public DamageTypes ShieldDamageType
    {
      get
      {
        return this.mSkillParam.shield_damage_type;
      }
    }

    public OInt ShieldTurn
    {
      get
      {
        return this.mShieldTurn;
      }
    }

    public OInt ShieldValue
    {
      get
      {
        return this.mShieldValue;
      }
    }

    public OInt UseRate
    {
      get
      {
        return this.mUseRate;
      }
      set
      {
        this.mUseRate = value;
      }
    }

    public SkillLockCondition UseCondition
    {
      get
      {
        return this.mUseCondition;
      }
      set
      {
        if (value == null)
          return;
        if (this.mUseCondition == null)
          this.mUseCondition = new SkillLockCondition();
        value.CopyTo(this.mUseCondition);
      }
    }

    public bool CheckCount
    {
      get
      {
        return this.mCheckCount;
      }
      set
      {
        this.mCheckCount = value;
      }
    }

    public int DuplicateCount
    {
      get
      {
        return this.mSkillParam.DuplicateCount;
      }
    }

    public OBool IsCollabo
    {
      get
      {
        return this.mIsCollabo;
      }
      set
      {
        this.mIsCollabo = value;
      }
    }

    public string ReplaceSkillId
    {
      get
      {
        return this.mReplaceSkillId;
      }
      set
      {
        this.mReplaceSkillId = value;
      }
    }

    public eTeleportType TeleportType
    {
      get
      {
        return this.mSkillParam.TeleportType;
      }
    }

    public ESkillTarget TeleportTarget
    {
      get
      {
        return this.mSkillParam.TeleportTarget;
      }
    }

    public int TeleportHeight
    {
      get
      {
        return this.mSkillParam.TeleportHeight;
      }
    }

    public bool TeleportIsMove
    {
      get
      {
        return this.mSkillParam.TeleportIsMove;
      }
    }

    public eTeleportSkillPos TeleportSkillPos
    {
      get
      {
        return this.mSkillParam.TeleportSkillPos;
      }
    }

    public OInt KnockBackRate
    {
      get
      {
        return (OInt) this.mSkillParam.KnockBackRate;
      }
    }

    public OInt KnockBackVal
    {
      get
      {
        return (OInt) this.mSkillParam.KnockBackVal;
      }
    }

    public eKnockBackDir KnockBackDir
    {
      get
      {
        return this.mSkillParam.KnockBackDir;
      }
    }

    public eKnockBackDs KnockBackDs
    {
      get
      {
        return this.mSkillParam.KnockBackDs;
      }
    }

    public int WeatherRate
    {
      get
      {
        return this.mSkillParam.WeatherRate;
      }
    }

    public string WeatherId
    {
      get
      {
        return this.mSkillParam.WeatherId;
      }
    }

    public int ElementSpcAtkRate
    {
      get
      {
        return this.mSkillParam.ElementSpcAtkRate;
      }
    }

    public int MaxDamageValue
    {
      get
      {
        return this.mSkillParam.MaxDamageValue;
      }
    }

    public string CutInConceptCardId
    {
      get
      {
        return this.mSkillParam.CutInConceptCardId;
      }
    }

    public int JumpSpcAtkRate
    {
      get
      {
        return this.mSkillParam.JumpSpcAtkRate;
      }
    }

    public AbilityData OwnerAbiliy
    {
      get
      {
        return this.m_OwnerAbility;
      }
    }

    public bool IsDerivedSkill
    {
      get
      {
        return !string.IsNullOrEmpty(this.m_BaseSkillIname);
      }
    }

    public SkillDeriveParam DeriveParam
    {
      get
      {
        return this.m_SkillDeriveParam;
      }
    }

    public bool IsCreatedByConceptCard
    {
      get
      {
        return this.m_ConceptCardData != null;
      }
    }

    public ConceptCardData ConceptCard
    {
      get
      {
        return this.m_ConceptCardData;
      }
    }

    public bool IsDecreaseEffectOnSubSlot
    {
      get
      {
        return this.m_IsDecreaseEffectOnSubSlot;
      }
    }

    public int DecreaseEffectRate
    {
      get
      {
        return !this.IsCreatedByConceptCard ? 0 : this.m_ConceptCardData.CurrentDecreaseEffectRate;
      }
    }

    public ProtectSkillParam ProtectSkill
    {
      get
      {
        return this.mSkillParam != null ? this.mSkillParam.ProtectSkill : (ProtectSkillParam) null;
      }
    }

    public ProtectTypes ProtectType
    {
      get
      {
        return this.mSkillParam != null && this.mSkillParam.ProtectSkill != null ? this.mSkillParam.ProtectSkill.Type : ProtectTypes.None;
      }
    }

    public DamageTypes ProtectDamageType
    {
      get
      {
        return this.mSkillParam != null && this.mSkillParam.ProtectSkill != null ? this.mSkillParam.ProtectSkill.DamageType : DamageTypes.None;
      }
    }

    public OInt ProtectRange
    {
      get
      {
        return this.mSkillParam != null ? (OInt) (this.mSkillParam.ProtectSkill == null ? 0 : this.mSkillParam.ProtectSkill.Range) : (OInt) 0;
      }
    }

    public OInt ProtectHeight
    {
      get
      {
        return this.mSkillParam != null ? (OInt) (this.mSkillParam.ProtectSkill == null ? 0 : this.mSkillParam.ProtectSkill.Height) : (OInt) 0;
      }
    }

    public int ProtectValue
    {
      get
      {
        return this.mProtectValue;
      }
    }

    public void Setup(
      string iname,
      int rank,
      int rankcap = 1,
      MasterParam master = null,
      ConceptCardEffectDecreaseInfo decreaseInfo = null)
    {
      if (string.IsNullOrEmpty(iname))
      {
        this.Reset();
      }
      else
      {
        if (master == null)
          master = MonoSingleton<GameManager>.GetInstanceDirect().MasterParam;
        this.mSkillParam = master.GetSkillParam(iname);
        if (decreaseInfo != null)
        {
          this.m_ConceptCardData = decreaseInfo.m_ConceptCardData;
          this.m_IsDecreaseEffectOnSubSlot = decreaseInfo.m_IsDecreaseEffect;
        }
        else
        {
          this.m_ConceptCardData = (ConceptCardData) null;
          this.m_IsDecreaseEffectOnSubSlot = false;
        }
        this.mRankCap = this.mSkillParam.lvcap != 0 ? (OInt) Math.Max(this.mSkillParam.lvcap, 1) : (OInt) Math.Max(rankcap, 1);
        this.mRank = (OInt) Math.Min(rank, (int) this.mRankCap);
        this.mTargetBuffEffect = BuffEffect.CreateBuffEffect(master.GetBuffEffectParam(this.SkillParam.target_buff_iname), (int) this.mRank, (int) this.mRankCap);
        this.mSelfBuffEffect = BuffEffect.CreateBuffEffect(master.GetBuffEffectParam(this.SkillParam.self_buff_iname), (int) this.mRank, (int) this.mRankCap);
        this.mTargetCondEffect = CondEffect.CreateCondEffect(master.GetCondEffectParam(this.SkillParam.target_cond_iname), (int) this.mRank, (int) this.mRankCap);
        this.mSelfCondEffect = CondEffect.CreateCondEffect(master.GetCondEffectParam(this.SkillParam.self_cond_iname), (int) this.mRank, (int) this.mRankCap);
        this.UpdateParam();
      }
    }

    private void Reset()
    {
      this.mSkillParam = (SkillParam) null;
      this.mRank = (OInt) 1;
    }

    public bool IsValid()
    {
      return this.mSkillParam != null;
    }

    public int GetRankCap()
    {
      return (int) this.mRankCap;
    }

    public void UpdateParam()
    {
      if (this.SkillParam == null)
        return;
      int rank = this.Rank;
      int rankCap = this.GetRankCap();
      this.mCastSpeed = (OInt) this.SkillParam.CalcCurrentRankValueShort(rank, rankCap, this.SkillParam.cast_speed);
      this.mEffectRate = (OInt) this.SkillParam.CalcCurrentRankValueShort(rank, rankCap, this.SkillParam.effect_rate);
      this.mEffectValue = (OInt) this.SkillParam.CalcCurrentRankValue(rank, rankCap, this.SkillParam.effect_value);
      this.mEffectRange = (OInt) this.SkillParam.CalcCurrentRankValueShort(rank, rankCap, this.SkillParam.effect_range);
      this.mElementValue = (OInt) 0;
      this.mControlDamageRate = (OInt) this.SkillParam.CalcCurrentRankValueShort(rank, rankCap, this.SkillParam.control_damage_rate);
      this.mControlDamageValue = (OInt) this.SkillParam.CalcCurrentRankValueShort(rank, rankCap, this.SkillParam.control_damage_value);
      this.mControlChargeTimeRate = (OInt) this.SkillParam.CalcCurrentRankValueShort(rank, rankCap, this.SkillParam.control_ct_rate);
      this.mControlChargeTimeValue = (OInt) this.SkillParam.CalcCurrentRankValueShort(rank, rankCap, this.SkillParam.control_ct_value);
      this.mShieldTurn = (OInt) this.SkillParam.CalcCurrentRankValueShort(rank, rankCap, this.SkillParam.shield_turn);
      this.mShieldValue = (OInt) this.SkillParam.CalcCurrentRankValue(rank, rankCap, this.SkillParam.shield_value);
      if (this.SkillParam.ProtectSkill != null)
        this.mProtectValue = this.SkillParam.CalcCurrentRankValue(rank, rankCap, this.SkillParam.ProtectSkill.Value);
      if (this.mTargetBuffEffect != null)
        this.mTargetBuffEffect.UpdateCurrentValues(rank, rankCap);
      if (this.mTargetCondEffect != null)
        this.mTargetCondEffect.UpdateCurrentValues(rank, rankCap);
      if (this.mSelfBuffEffect != null)
        this.mSelfBuffEffect.UpdateCurrentValues(rank, rankCap);
      if (this.mSelfCondEffect == null)
        return;
      this.mSelfCondEffect.UpdateCurrentValues(rank, rankCap);
    }

    public bool IsNormalAttack()
    {
      return this.SkillParam != null && this.SkillParam.type == ESkillType.Attack;
    }

    public bool IsPassiveSkill()
    {
      if (this.SkillParam == null)
        return false;
      if (this.SkillType == ESkillType.Passive)
        return true;
      return this.SkillType == ESkillType.Item && this.EffectType == SkillEffectTypes.Equipment;
    }

    public bool IsItem()
    {
      return this.SkillParam != null && this.SkillParam.type == ESkillType.Item;
    }

    public bool IsReactionSkill()
    {
      return this.SkillParam != null && this.SkillParam.IsReactionSkill();
    }

    public bool IsEnableChangeRange()
    {
      return this.SkillParam != null && this.SkillParam.IsEnableChangeRange();
    }

    public bool IsEnableHeightRangeBonus()
    {
      return this.SkillParam != null && this.SkillParam.IsEnableHeightRangeBonus();
    }

    public bool IsEnableHeightParamAdjust()
    {
      return this.SkillParam != null && this.SkillParam.IsEnableHeightParamAdjust();
    }

    public bool IsEnableUnitLockTarget()
    {
      return this.SkillParam != null && this.SkillParam.IsEnableUnitLockTarget();
    }

    public bool IsDamagedSkill()
    {
      return this.SkillParam != null && this.SkillParam.IsDamagedSkill();
    }

    public bool IsHealSkill()
    {
      return this.SkillParam != null && this.SkillParam.IsHealSkill();
    }

    public bool IsPierce()
    {
      return this.SkillParam != null && this.SkillParam.IsPierce();
    }

    public bool IsJewelAttack()
    {
      return this.SkillParam != null && this.SkillParam.IsJewelAttack();
    }

    public bool IsForceHit()
    {
      return this.SkillParam != null && this.SkillParam.IsForceHit();
    }

    public bool IsSuicide()
    {
      return this.SkillParam != null && this.SkillParam.IsSuicide();
    }

    public bool IsSubActuate()
    {
      return this.SkillParam != null && this.SkillParam.IsSubActuate();
    }

    public bool IsFixedDamage()
    {
      return this.SkillParam != null && this.SkillParam.IsFixedDamage();
    }

    public bool IsForceUnitLock()
    {
      return this.SkillParam != null && this.SkillParam.IsForceUnitLock();
    }

    public bool IsAllDamageReaction()
    {
      return this.SkillParam != null && this.SkillParam.IsAllDamageReaction();
    }

    public bool IsIgnoreElement()
    {
      return this.SkillParam != null && this.SkillParam.IsIgnoreElement();
    }

    public bool IsPrevApply()
    {
      return this.SkillParam != null && this.SkillParam.IsPrevApply();
    }

    public bool IsMhmDamage()
    {
      return this.SkillParam != null && this.SkillParam.IsMhmDamage();
    }

    public bool IsCastBreak()
    {
      return this.SkillParam != null && this.SkillParam.IsCastBreak();
    }

    public bool IsCastSkill()
    {
      return this.SkillParam != null && this.SkillParam.IsCastSkill();
    }

    public bool IsCutin()
    {
      return this.SkillParam != null && this.SkillParam.IsCutin();
    }

    public bool IsMapSkill()
    {
      return this.SkillParam == null || this.SkillParam.IsMapSkill();
    }

    public bool IsBattleSkill()
    {
      return this.SkillParam != null && this.SkillParam.IsBattleSkill();
    }

    public bool IsAreaSkill()
    {
      return this.SkillParam != null && this.SkillParam.IsAreaSkill();
    }

    public bool IsAllEffect()
    {
      return this.SkillParam != null && this.SkillParam.IsAllEffect();
    }

    public bool IsLongRangeSkill()
    {
      return this.SkillParam != null && this.SkillParam.IsLongRangeSkill();
    }

    public bool IsSupportSkill()
    {
      return this.SkillParam != null && this.SkillParam.IsSupportSkill();
    }

    public bool IsTrickSkill()
    {
      return this.SkillParam != null && this.SkillParam.IsTrickSkill();
    }

    public bool IsTransformSkill()
    {
      return this.SkillParam != null && this.SkillParam.IsTransformSkill();
    }

    public bool IsDynamicTransformSkill()
    {
      return this.SkillParam != null && this.SkillParam.IsDynamicTransformSkill();
    }

    public bool IsSetBreakObjSkill()
    {
      return this.SkillParam != null && this.SkillParam.IsSetBreakObjSkill();
    }

    public bool IsChangeWeatherSkill()
    {
      return this.SkillParam != null && this.SkillParam.IsChangeWeatherSkill();
    }

    public bool IsJudgeHp(Unit unit)
    {
      return this.SkillParam != null && this.SkillParam.IsJudgeHp(unit);
    }

    public bool IsConditionSkill()
    {
      return this.SkillParam != null && this.SkillParam.IsConditionSkill();
    }

    public bool IsPhysicalAttack()
    {
      return this.SkillParam != null && this.SkillParam.attack_type == AttackTypes.PhyAttack;
    }

    public bool IsMagicalAttack()
    {
      return this.SkillParam != null && this.SkillParam.attack_type == AttackTypes.MagAttack;
    }

    public bool IsSelfTargetSelect()
    {
      return this.SkillParam.IsSelfTargetSelect();
    }

    public bool IsAdvantage()
    {
      return this.SkillParam != null && this.SkillParam.IsAdvantage();
    }

    public bool IsAiNoAutoTiming()
    {
      return this.SkillParam != null && this.SkillParam.IsAiNoAutoTiming();
    }

    public bool IsMpUseAfter()
    {
      return this.SkillParam != null && this.SkillParam.IsMpUseAfter();
    }

    public bool IsForcedTargeting
    {
      get
      {
        return this.SkillParam != null && this.SkillParam.ForcedTargetingTurn > 0;
      }
    }

    public int ForcedTargetingTurn
    {
      get
      {
        return this.SkillParam != null ? this.SkillParam.ForcedTargetingTurn : 0;
      }
    }

    public bool IsForcedTargetingSkillEffect
    {
      get
      {
        return this.SkillParam != null && this.SkillParam.IsForcedTargetingSkillEffect;
      }
    }

    public bool CheckGridLineSkill()
    {
      SkillParam skillParam = this.SkillParam;
      return skillParam != null && skillParam.line_type != ELineType.None && this.RangeMax > 1;
    }

    public bool CheckUnitSkillTarget()
    {
      switch (this.Target)
      {
        case ESkillTarget.Self:
        case ESkillTarget.SelfSide:
        case ESkillTarget.EnemySide:
        case ESkillTarget.UnitAll:
        case ESkillTarget.NotSelf:
        case ESkillTarget.SelfSideNotSelf:
          return true;
        default:
          return false;
      }
    }

    public int GetHpCost(Unit self)
    {
      int hpCost = this.SkillParam.hp_cost;
      int num = hpCost + hpCost * (int) self.CurrentStatus[BattleBonus.HpCostRate] * 100 / 10000;
      return (int) self.MaximumStatus.param.hp * num * 100 / 10000;
    }

    public BuffEffect GetBuffEffect(SkillEffectTargets target = SkillEffectTargets.Target)
    {
      if (target == SkillEffectTargets.Target)
        return this.mTargetBuffEffect;
      return target == SkillEffectTargets.Self ? this.mSelfBuffEffect : (BuffEffect) null;
    }

    public CondEffect GetCondEffect(SkillEffectTargets target = SkillEffectTargets.Target)
    {
      if (target == SkillEffectTargets.Target)
        return this.mTargetCondEffect;
      return target == SkillEffectTargets.Self ? this.mSelfCondEffect : (CondEffect) null;
    }

    public int CalcBuffEffectValue(
      ESkillTiming timing,
      ParamTypes type,
      int src,
      SkillEffectTargets target = SkillEffectTargets.Target)
    {
      return timing != this.Timing ? src : this.CalcBuffEffectValue(type, src, target);
    }

    public int CalcBuffEffectValue(ParamTypes type, int src, SkillEffectTargets target = SkillEffectTargets.Target)
    {
      BuffEffect buffEffect = this.GetBuffEffect(target);
      if (buffEffect == null)
        return src;
      BuffEffect.BuffTarget buffTarget = buffEffect[type];
      return buffTarget == null ? src : SkillParam.CalcSkillEffectValue(buffTarget.calcType, (int) buffTarget.value, src);
    }

    public int GetBuffEffectValue(ParamTypes type, SkillEffectTargets target = SkillEffectTargets.Target)
    {
      BuffEffect buffEffect = this.GetBuffEffect(target);
      if (buffEffect == null)
        return 0;
      BuffEffect.BuffTarget buffTarget = buffEffect[type];
      return buffTarget == null ? 0 : (int) buffTarget.value;
    }

    public bool BuffSkill(
      ESkillTiming timing,
      EElement element,
      BaseStatus buff,
      BaseStatus buff_negative,
      BaseStatus buff_scale,
      BaseStatus debuff,
      BaseStatus debuff_negative,
      BaseStatus debuff_scale,
      RandXorshift rand = null,
      SkillEffectTargets buff_target = SkillEffectTargets.Target,
      bool is_resume = false,
      List<BuffEffect.BuffValues> list = null,
      bool calcDecreaseEffect = false)
    {
      if (this.Timing != timing)
        return false;
      BuffEffect buffEffect = this.GetBuffEffect(buff_target);
      if (buffEffect == null)
        return false;
      if (!is_resume)
      {
        int rate = (int) buffEffect.param.rate;
        if (rate > 0 && rate < 100)
        {
          DebugUtility.Assert(rand != null, "発動確率が設定されているスキルを正規タイミングで発動させたにも関わらず乱数生成器の設定がされていない");
          if ((int) (rand.Get() % 100U) > rate)
            return false;
        }
      }
      if (list == null)
      {
        if (buff != null)
          this.InternalBuffSkill(buffEffect, element, BuffTypes.Buff, true, false, SkillParamCalcTypes.Add, buff, (List<BuffEffect.BuffValues>) null, calcDecreaseEffect);
        if (buff_negative != null)
          this.InternalBuffSkill(buffEffect, element, BuffTypes.Buff, true, true, SkillParamCalcTypes.Add, buff_negative, (List<BuffEffect.BuffValues>) null, calcDecreaseEffect);
        if (buff_scale != null)
          this.InternalBuffSkill(buffEffect, element, BuffTypes.Buff, false, false, SkillParamCalcTypes.Scale, buff_scale, (List<BuffEffect.BuffValues>) null, calcDecreaseEffect);
        if (debuff != null)
          this.InternalBuffSkill(buffEffect, element, BuffTypes.Debuff, true, false, SkillParamCalcTypes.Add, debuff, (List<BuffEffect.BuffValues>) null, calcDecreaseEffect);
        if (debuff_negative != null)
          this.InternalBuffSkill(buffEffect, element, BuffTypes.Debuff, true, true, SkillParamCalcTypes.Add, debuff_negative, (List<BuffEffect.BuffValues>) null, calcDecreaseEffect);
        if (debuff_scale != null)
          this.InternalBuffSkill(buffEffect, element, BuffTypes.Debuff, false, false, SkillParamCalcTypes.Scale, debuff_scale, (List<BuffEffect.BuffValues>) null, calcDecreaseEffect);
      }
      else
      {
        this.InternalBuffSkill(buffEffect, element, BuffTypes.Buff, false, false, SkillParamCalcTypes.Add, (BaseStatus) null, list, calcDecreaseEffect);
        this.InternalBuffSkill(buffEffect, element, BuffTypes.Buff, false, false, SkillParamCalcTypes.Scale, (BaseStatus) null, list, calcDecreaseEffect);
        this.InternalBuffSkill(buffEffect, element, BuffTypes.Debuff, false, false, SkillParamCalcTypes.Add, (BaseStatus) null, list, calcDecreaseEffect);
        this.InternalBuffSkill(buffEffect, element, BuffTypes.Debuff, false, false, SkillParamCalcTypes.Scale, (BaseStatus) null, list, calcDecreaseEffect);
      }
      return true;
    }

    private void InternalBuffSkill(
      BuffEffect effect,
      EElement element,
      BuffTypes buffType,
      bool is_check_negative_value,
      bool is_negative_value_is_buff,
      SkillParamCalcTypes calcType,
      BaseStatus status,
      List<BuffEffect.BuffValues> list = null,
      bool calcDecreaseEffect = false)
    {
      for (int index1 = 0; index1 < effect.targets.Count; ++index1)
      {
        BuffEffect.BuffTarget target = effect.targets[index1];
        if (target != null && target.buffType == buffType && (!is_check_negative_value || BuffEffectParam.IsNegativeValueIsBuff(target.paramType) == is_negative_value_is_buff) && target.calcType == calcType && (element == EElement.None || !BuffEffect.IsElementBuff(target.paramType) || BuffEffect.IsMatchElementBuff(element, target.paramType)))
        {
          BuffMethodTypes buffMethodType = this.GetBuffMethodType(target.buffType, calcType);
          ParamTypes paramType = target.paramType;
          int num = (int) target.value;
          if ((bool) effect.param.mIsUpBuff)
            num = 0;
          if (calcDecreaseEffect && this.IsNeedDecreaseEffect())
            num = GameUtility.CalcSubRateRoundDown((long) num, (long) this.m_ConceptCardData.CurrentDecreaseEffectRate);
          string tokkou = (string) target.tokkou;
          if (list == null)
          {
            BuffEffect.SetBuffValues(paramType, buffMethodType, ref status, num, tokkou);
          }
          else
          {
            bool flag = true;
            for (int index2 = 0; index2 < list.Count; ++index2)
            {
              BuffEffect.BuffValues buffValues = list[index2];
              if (buffValues.param_type == paramType && buffValues.method_type == buffMethodType)
              {
                buffValues.value += num;
                list[index2] = buffValues;
                flag = false;
                break;
              }
            }
            if (flag)
              list.Add(new BuffEffect.BuffValues()
              {
                param_type = paramType,
                method_type = buffMethodType,
                value = num
              });
          }
        }
      }
    }

    private BuffMethodTypes GetBuffMethodType(
      BuffTypes buff,
      SkillParamCalcTypes calc)
    {
      if (this.Timing == ESkillTiming.Passive || calc != SkillParamCalcTypes.Scale)
        return BuffMethodTypes.Add;
      return buff == BuffTypes.Buff ? BuffMethodTypes.Highest : BuffMethodTypes.Lowest;
    }

    public bool IsReactionDet(AttackDetailTypes atk_detail_type)
    {
      return this.mSkillParam != null && this.mSkillParam.IsReactionDet(atk_detail_type);
    }

    public static void GetHomePassiveBuffStatus(
      SkillData skill,
      EElement element,
      ref BaseStatus status,
      ref BaseStatus scale_status,
      bool calcDecreaseEffect = false)
    {
      SkillData.GetHomePassiveBuffStatus(skill, element, ref status, ref status, ref status, ref status, ref scale_status, calcDecreaseEffect);
    }

    public static void GetHomePassiveBuffStatus(
      SkillData skill,
      EElement element,
      ref BaseStatus status,
      ref BaseStatus negative_status,
      ref BaseStatus debuff_status,
      ref BaseStatus negative_debuff_status,
      ref BaseStatus scale_status,
      bool calcDecreaseEffect = false)
    {
      if (skill == null || !skill.IsPassiveSkill() || (skill.Target != ESkillTarget.Self || skill.Condition != ESkillCondition.None) || !string.IsNullOrEmpty(skill.SkillParam.tokkou))
        return;
      BuffEffect buffEffect1 = skill.GetBuffEffect(SkillEffectTargets.Target);
      if (buffEffect1 != null && buffEffect1.param != null && (buffEffect1.param.cond == ESkillCondition.None && buffEffect1.param.mAppType == EAppType.Standard) && (buffEffect1.param.mEffRange == EEffRange.None && !(bool) buffEffect1.param.mIsUpBuff))
        skill.BuffSkill(ESkillTiming.Passive, element, status, negative_status, scale_status, debuff_status, negative_debuff_status, scale_status, (RandXorshift) null, SkillEffectTargets.Target, false, (List<BuffEffect.BuffValues>) null, calcDecreaseEffect);
      BuffEffect buffEffect2 = skill.GetBuffEffect(SkillEffectTargets.Self);
      if (buffEffect2 == null || buffEffect2.param == null || (buffEffect2.param.cond != ESkillCondition.None || buffEffect2.param.mAppType != EAppType.Standard) || (buffEffect2.param.mEffRange != EEffRange.None || (bool) buffEffect2.param.mIsUpBuff))
        return;
      skill.BuffSkill(ESkillTiming.Passive, element, status, negative_status, scale_status, debuff_status, negative_debuff_status, scale_status, (RandXorshift) null, SkillEffectTargets.Self, false, (List<BuffEffect.BuffValues>) null, calcDecreaseEffect);
    }

    public static void GetPassiveBuffStatus(
      SkillData skill,
      BuffEffect[] add_buff_effects,
      EElement element,
      ref BaseStatus status,
      ref BaseStatus scale_status)
    {
      SkillData.GetPassiveBuffStatus(skill, add_buff_effects, element, ref status, ref status, ref status, ref status, ref scale_status);
    }

    public static void GetPassiveBuffStatus(
      SkillData skill,
      BuffEffect[] add_buff_effects,
      EElement element,
      ref BaseStatus status,
      ref BaseStatus negative_status,
      ref BaseStatus debuff_status,
      ref BaseStatus negative_debuff_status,
      ref BaseStatus scale_status)
    {
      if (skill == null || !skill.IsPassiveSkill())
        return;
      BuffEffect buffEffect1 = skill.GetBuffEffect(SkillEffectTargets.Target);
      if (buffEffect1 != null && buffEffect1.param != null && (buffEffect1.param.mAppType == EAppType.Standard && buffEffect1.param.mEffRange == EEffRange.None) && !(bool) buffEffect1.param.mIsUpBuff)
        skill.BuffSkill(ESkillTiming.Passive, element, status, negative_status, scale_status, debuff_status, negative_debuff_status, scale_status, (RandXorshift) null, SkillEffectTargets.Target, false, (List<BuffEffect.BuffValues>) null, false);
      BuffEffect buffEffect2 = skill.GetBuffEffect(SkillEffectTargets.Self);
      if (buffEffect2 == null || buffEffect2.param == null || (buffEffect2.param.mAppType != EAppType.Standard || buffEffect2.param.mEffRange != EEffRange.None) || (bool) buffEffect2.param.mIsUpBuff)
        return;
      skill.BuffSkill(ESkillTiming.Passive, element, status, negative_status, scale_status, debuff_status, negative_debuff_status, scale_status, (RandXorshift) null, SkillEffectTargets.Self, false, (List<BuffEffect.BuffValues>) null, false);
    }

    public bool IsTargetGridNoUnit
    {
      get
      {
        return this.SkillParam != null && this.SkillParam.IsTargetGridNoUnit;
      }
    }

    public bool IsTargetValidGrid
    {
      get
      {
        return this.SkillParam != null && this.SkillParam.IsTargetValidGrid;
      }
    }

    public bool IsSkillCountNoLimit
    {
      get
      {
        return this.SkillParam != null && this.SkillParam.IsSkillCountNoLimit;
      }
    }

    public bool IsTargetTeleport
    {
      get
      {
        return this.SkillParam != null && this.SkillParam.IsTargetTeleport;
      }
    }

    public void SetOwnerAbility(AbilityData owner)
    {
      this.m_OwnerAbility = owner;
    }

    public SkillData CreateDeriveSkill(SkillDeriveParam skillDeriveParam)
    {
      SkillData skillData = new SkillData();
      skillData.Setup(skillDeriveParam.DeriveSkillIname, (int) this.mRank, (int) this.mRankCap, (MasterParam) null, (ConceptCardEffectDecreaseInfo) null);
      skillData.m_OwnerAbility = this.m_OwnerAbility;
      skillData.m_BaseSkillIname = this.SkillID;
      skillData.m_SkillDeriveParam = skillDeriveParam;
      return skillData;
    }

    public DependStateSpcEffParam GetDependStateSpcEffParam()
    {
      if (this.SkillParam != null && !string.IsNullOrEmpty(this.SkillParam.DependStateSpcEffId) && this.mDependStateSpcEffParam == null)
        this.mDependStateSpcEffParam = MonoSingleton<GameManager>.Instance.MasterParam.GetDependStateSpcEffParam(this.SkillParam.DependStateSpcEffId);
      return this.mDependStateSpcEffParam;
    }

    public DependStateSpcEffParam GetDependStateSpcEffParamSelf()
    {
      if (this.SkillParam != null && !string.IsNullOrEmpty(this.SkillParam.DependStateSpcEffSelfId) && this.mDependStateSpcEffParamSelf == null)
        this.mDependStateSpcEffParamSelf = MonoSingleton<GameManager>.Instance.MasterParam.GetDependStateSpcEffParam(this.SkillParam.DependStateSpcEffSelfId);
      return this.mDependStateSpcEffParamSelf;
    }

    public bool IsNeedDecreaseEffect()
    {
      return this.IsCreatedByConceptCard && this.m_ConceptCardData.IsEquipedSubSlot && this.IsDecreaseEffectOnSubSlot;
    }

    public bool IsProtectSkill()
    {
      return this.SkillParam != null && this.SkillParam.IsProtectSkill();
    }

    public bool IsProtectReactionSkill()
    {
      return this.SkillParam != null && this.SkillParam.IsProtectReactionSkill();
    }
  }
}
