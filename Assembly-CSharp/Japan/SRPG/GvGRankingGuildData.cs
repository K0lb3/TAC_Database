﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GvGRankingGuildData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class GvGRankingGuildData : ViewGuildData
  {
    public int mRank;
    public int mPoint;

    public int Rank
    {
      get
      {
        return this.mRank;
      }
    }

    public int Point
    {
      get
      {
        return this.mPoint;
      }
    }

    public void Deserialize(JSON_GvGRankingData json)
    {
      if (json == null)
        return;
      this.Deserialize((JSON_ViewGuild) json);
      this.mRank = json.rank;
      this.mPoint = json.point;
    }
  }
}
