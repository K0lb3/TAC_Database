﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GvGBattleResultUnitContent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class GvGBattleResultUnitContent : MonoBehaviour
  {
    [SerializeField]
    private Slider mGauge;
    [SerializeField]
    private GameObject mDead;

    public GvGBattleResultUnitContent()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
      GvGBattleResultUnitContentData dataOfClass = DataSource.FindDataOfClass<GvGBattleResultUnitContentData>(((Component) this).get_gameObject(), (GvGBattleResultUnitContentData) null);
      if (dataOfClass == null)
        return;
      DataSource.Bind<Unit>(((Component) this).get_gameObject(), dataOfClass.mUnit, false);
      GameUtility.SetGameObjectActive(this.mDead, dataOfClass.mHp == 0);
      if (!Object.op_Inequality((Object) this.mGauge, (Object) null) || dataOfClass.mUnit.MaximumStatusHp <= 0)
        return;
      this.mGauge.set_value((float) dataOfClass.mHp * 100f / (float) dataOfClass.mUnit.MaximumStatusHp);
    }
  }
}
