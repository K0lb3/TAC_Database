﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_ReqRaidRescueSend
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  [FlowNode.NodeType("Raid/Req/Rescue/Send", 32741)]
  public class FlowNode_ReqRaidRescueSend : FlowNode_ReqRaidBase
  {
    public override WebAPI GenerateWebAPI()
    {
      RaidBossData currentRaidBossData = RaidManager.Instance.CurrentRaidBossData;
      int is_send_guild = !RaidManager.Instance.RescueReqOptionGuild ? 0 : 1;
      int is_send_friend = !RaidManager.Instance.RescueReqOptionFriend ? 0 : 1;
      return (WebAPI) new ReqRaidRescueSend(currentRaidBossData.AreaId, currentRaidBossData.RaidBossInfo.BossId, currentRaidBossData.RaidBossInfo.Round, is_send_guild, is_send_friend, new Network.ResponseCallback(((FlowNode_Network) this).ResponseCallback));
    }

    public override bool Success(WWWResult www)
    {
      RaidManager.Instance.CurrentRaidBossData.SOSDone();
      return true;
    }
  }
}
