﻿// Decompiled with JetBrains decompiler
// Type: SRPG.HighlightGift
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class HighlightGift
  {
    public string iname;
    public string message;
    public HighlightGiftData[] gifts;

    public void Deserialize(JSON_HighlightGift json)
    {
      if (json == null)
        return;
      this.iname = json.iname;
      this.message = json.message;
      HighlightGiftData[] highlightGiftDataArray = new HighlightGiftData[json.gifts.Length];
      for (int index = 0; index < json.gifts.Length; ++index)
      {
        HighlightGiftData highlightGiftData = new HighlightGiftData();
        highlightGiftData.Deserialize(json.gifts[index]);
        highlightGiftDataArray[index] = highlightGiftData;
      }
      this.gifts = highlightGiftDataArray;
    }
  }
}
