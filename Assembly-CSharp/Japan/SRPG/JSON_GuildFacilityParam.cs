﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_GuildFacilityParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_GuildFacilityParam
  {
    public string iname;
    public string name;
    public string image;
    public int type;
    public int enhance;
    public int increment;
    public long day_limit;
    public int rel_cnds_type;
    public string rel_cnds_val1;
    public int rel_cnds_val2;
    public JSON_GuildFacilityEffectParam[] effects;
  }
}
