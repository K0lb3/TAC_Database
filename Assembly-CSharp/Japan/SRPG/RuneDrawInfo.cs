﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RuneDrawInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class RuneDrawInfo : MonoBehaviour
  {
    [SerializeField]
    private Text mRuneName;
    [SerializeField]
    private ImageArray mRuneSlotNumber;
    [SerializeField]
    private Text mRuneSetEffName;
    [SerializeField]
    private Text mRuneSlotNumberText;
    private BindRuneData mRuneData;

    public RuneDrawInfo()
    {
      base.\u002Ector();
    }

    public void Awake()
    {
    }

    public void SetDrawParam(BindRuneData rune_data)
    {
      if (rune_data == null)
        return;
      this.mRuneData = rune_data;
      this.Refresh();
    }

    public void Refresh()
    {
      if (this.mRuneData == null)
        return;
      RuneData rune = this.mRuneData.Rune;
      if (rune == null)
        return;
      RuneParam runeParam = rune.RuneParam;
      if (runeParam == null)
        return;
      ItemParam itemParam = runeParam.ItemParam;
      if (itemParam == null)
        return;
      RuneSetEff runeSetEff = runeParam.RuneSetEff;
      if (runeSetEff == null)
        return;
      if (Object.op_Implicit((Object) this.mRuneName))
        this.mRuneName.set_text(itemParam.name);
      if (Object.op_Implicit((Object) this.mRuneSlotNumber))
        this.mRuneSlotNumber.ImageIndex = (int) (byte) runeParam.slot_index;
      if (Object.op_Implicit((Object) this.mRuneSetEffName))
        this.mRuneSetEffName.set_text(runeSetEff.name);
      if (!Object.op_Implicit((Object) this.mRuneSlotNumberText))
        return;
      this.mRuneSlotNumberText.set_text(LocalizedText.Get("sys.RUNE_SLOT_TEXT_" + (object) RuneSlotIndex.IndexToSlot((int) (byte) runeParam.slot_index)));
    }
  }
}
