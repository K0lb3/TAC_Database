﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FilterConceptCardParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class FilterConceptCardParam
  {
    public string iname;
    public string tab_name;
    public string name;
    public eConceptCardFilterTypes filter_type;
    public FilterConceptCardConditionParam[] conditions;

    public bool IsEnableFilterType(eConceptCardFilterTypes type)
    {
      return this.filter_type == type;
    }

    public void Deserialize(JSON_FilterConceptCardParam json)
    {
      this.iname = json.iname;
      this.tab_name = json.tab_name;
      this.name = json.name;
      this.filter_type = (eConceptCardFilterTypes) json.filter_type;
      if (json.cnds == null)
        return;
      this.conditions = new FilterConceptCardConditionParam[json.cnds.Length];
      for (int index = 0; index < json.cnds.Length; ++index)
      {
        FilterConceptCardConditionParam cardConditionParam = new FilterConceptCardConditionParam(this);
        cardConditionParam.Deserialize(json.cnds[index]);
        this.conditions[index] = cardConditionParam;
      }
    }

    public static void Deserialize(
      ref FilterConceptCardParam[] param,
      JSON_FilterConceptCardParam[] json)
    {
      if (json == null)
        return;
      param = new FilterConceptCardParam[json.Length];
      for (int index = 0; index < json.Length; ++index)
      {
        FilterConceptCardParam conceptCardParam = new FilterConceptCardParam();
        conceptCardParam.Deserialize(json[index]);
        param[index] = conceptCardParam;
      }
    }
  }
}
