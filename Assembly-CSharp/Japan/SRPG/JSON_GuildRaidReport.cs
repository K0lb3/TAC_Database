﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_GuildRaidReport
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_GuildRaidReport
  {
    public int report_id;
    public int boss_id;
    public int round;
    public int damage;
    public int posted_at;
    public JSON_GuildRaidDeck deck;
  }
}
