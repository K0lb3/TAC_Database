﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Event2dAction_StandFrontMove
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  [EventActionInfo("New/立ち絵2/最前面移動(2D)", "指定した立ち絵を最前面に持ってきます", 5592405, 4473992)]
  public class Event2dAction_StandFrontMove : EventAction
  {
    public string CharaID;
    private EventDialogBubbleCustom mBubble;

    public override void PreStart()
    {
    }

    public override void OnActivate()
    {
      if (!string.IsNullOrEmpty(this.CharaID) && EventStandCharaController2.Instances != null && EventStandCharaController2.Instances.Count > 0)
      {
        foreach (EventStandCharaController2 instance in EventStandCharaController2.Instances)
        {
          if (instance.CharaID == this.CharaID)
          {
            if (!instance.IsClose)
            {
              ((Component) instance).get_transform().SetAsLastSibling();
              ((Component) instance).get_transform().SetSiblingIndex(((Component) instance).get_transform().GetSiblingIndex() - 1);
            }
          }
          else if (!instance.IsClose)
            ;
        }
      }
      this.ActivateNext();
    }
  }
}
