﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ConceptCardListItemBadge
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SRPG
{
  public class ConceptCardListItemBadge : MonoBehaviour
  {
    private const float CHANGE_INTERBAL = 2f;
    [CustomGroup("バッジ")]
    [CustomField("クエスト", CustomFieldAttribute.Type.GameObject)]
    public GameObject m_Badge;
    [CustomGroup("バッジ")]
    [CustomField("闘技場", CustomFieldAttribute.Type.GameObject)]
    public GameObject m_Arena;
    [CustomGroup("バッジ")]
    [CustomField("傭兵", CustomFieldAttribute.Type.GameObject)]
    public GameObject m_Support;
    private ConceptCardData m_CardData;
    private List<GameObject> m_DispOn;
    private List<GameObject> m_DispOff;
    private int m_Index;
    private float m_Time;
    private List<UnitData> mQuestUnits;
    private List<UnitData> mArenaUnits;
    private List<UnitData> mArenaDefUnits;
    private List<UnitData> mSupportUnits;

    public ConceptCardListItemBadge()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
      this.mQuestUnits = MonoSingleton<GameManager>.Instance.Player.Units;
      this.mArenaUnits = UnitOverWriteUtility.GetPartyUnits(eOverWritePartyType.Arena, false);
      this.mArenaDefUnits = UnitOverWriteUtility.GetPartyUnits(eOverWritePartyType.ArenaDef, false);
      this.mSupportUnits = UnitOverWriteUtility.GetSupportAllUnits(false);
    }

    private void Update()
    {
      this.UpdateBadge();
    }

    private void UpdateBadge()
    {
      if (this.m_DispOn.Count <= 1)
        return;
      this.m_Time += Time.get_deltaTime();
      if ((double) this.m_Time <= 2.0)
        return;
      this.m_Time -= 2f;
      ++this.m_Index;
      if (this.m_Index >= this.m_DispOn.Count)
        this.m_Index = 0;
      this.UpdateBadgeAlternate(this.m_Index);
    }

    private void UpdateBadgeAlternate(int index)
    {
      for (int index1 = 0; index1 < this.m_DispOff.Count; ++index1)
      {
        if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.m_DispOff[index1], (UnityEngine.Object) null) && this.m_DispOff[index1].get_activeSelf())
          this.m_DispOff[index1].SetActive(false);
      }
      for (int index1 = 0; index1 < this.m_DispOn.Count; ++index1)
      {
        if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.m_DispOn[index1], (UnityEngine.Object) null))
        {
          bool flag = index == index1;
          if (this.m_DispOn[index1].get_activeSelf() != flag)
            this.m_DispOn[index1].SetActive(flag);
        }
      }
    }

    public void Refresh()
    {
      this.m_DispOn.Clear();
      this.m_DispOff.Clear();
      this.m_CardData = DataSource.FindDataOfClass<ConceptCardData>(((Component) this).get_gameObject(), (ConceptCardData) null);
      if (this.m_CardData != null)
      {
        if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.m_Badge, (UnityEngine.Object) null))
        {
          if (this.mQuestUnits.FindIndex((Predicate<UnitData>) (u => u.IsEquipConceptCard((long) this.m_CardData.UniqueID))) >= 0)
            this.m_DispOn.Add(this.m_Badge);
          else
            this.m_DispOff.Add(this.m_Badge);
        }
        if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.m_Arena, (UnityEngine.Object) null))
        {
          bool flag1 = this.mArenaUnits.FindIndex((Predicate<UnitData>) (u => u.IsEquipConceptCard((long) this.m_CardData.UniqueID))) >= 0;
          bool flag2 = this.mArenaDefUnits.FindIndex((Predicate<UnitData>) (u => u.IsEquipConceptCard((long) this.m_CardData.UniqueID))) >= 0;
          if (flag1 || flag2)
            this.m_DispOn.Add(this.m_Arena);
          else
            this.m_DispOff.Add(this.m_Arena);
        }
        if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.m_Support, (UnityEngine.Object) null))
        {
          if (this.mSupportUnits.FindIndex((Predicate<UnitData>) (u => u.IsEquipConceptCard((long) this.m_CardData.UniqueID))) >= 0)
            this.m_DispOn.Add(this.m_Support);
          else
            this.m_DispOff.Add(this.m_Support);
        }
      }
      else
      {
        if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.m_Badge, (UnityEngine.Object) null))
          this.m_DispOff.Add(this.m_Badge);
        if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.m_Arena, (UnityEngine.Object) null))
          this.m_DispOff.Add(this.m_Arena);
        if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.m_Support, (UnityEngine.Object) null))
          this.m_DispOff.Add(this.m_Support);
      }
      this.m_Time = 0.0f;
      this.m_Index = 0;
      this.UpdateBadgeAlternate(0);
    }

    private void OnEnable()
    {
      this.Refresh();
    }

    private void OnDisable()
    {
    }
  }
}
