﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_DrawCardCharacterMessage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  [FlowNode.NodeType("DrawCard/Character/Message", 32741)]
  [FlowNode.Pin(1, "Start", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(2, "Hidden", FlowNode.PinTypes.Input, 2)]
  [FlowNode.Pin(10, "Output", FlowNode.PinTypes.Output, 10)]
  public class FlowNode_DrawCardCharacterMessage : FlowNode
  {
    [FlowNode.ShowInInfo]
    public string Message = "Empty";
    private const int PIN_IN_MESS_SHOW = 1;
    private const int PIN_IN_MESS_HIDDEN = 2;
    private const int PIN_OUT_END = 10;

    public override void OnActivate(int pinID)
    {
      switch (pinID)
      {
        case 1:
          DrawCardCharacterMessage.ShowMessage(this.Message);
          break;
        case 2:
          DrawCardCharacterMessage.HiddenMessage();
          break;
      }
      this.ActivateOutputLinks(10);
    }
  }
}
