﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_ReqRaidRescueCancel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  [FlowNode.NodeType("Raid/Req/Rescue/Cancel", 32741)]
  public class FlowNode_ReqRaidRescueCancel : FlowNode_ReqRaidBase
  {
    public override WebAPI GenerateWebAPI()
    {
      RaidBossData rescueRaidBossData = RaidManager.Instance.RescueRaidBossData;
      return (WebAPI) new ReqRaidRescueCancel(rescueRaidBossData.OwnerUID, rescueRaidBossData.AreaId, rescueRaidBossData.RaidBossInfo.BossId, rescueRaidBossData.RaidBossInfo.Round, new Network.ResponseCallback(((FlowNode_Network) this).ResponseCallback));
    }

    public override bool Success(WWWResult www)
    {
      return true;
    }
  }
}
