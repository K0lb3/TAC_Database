﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RaidDamageAmountRewardWeightParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class RaidDamageAmountRewardWeightParam
  {
    private int mDamageAmount;
    private string mRewardId;

    public int DamageAmount
    {
      get
      {
        return this.mDamageAmount;
      }
    }

    public string RewardId
    {
      get
      {
        return this.mRewardId;
      }
    }

    public bool Deserialize(JSON_RaidDamageAmountRewardAmountParam json)
    {
      if (json == null)
        return false;
      this.mDamageAmount = json.damage_Amount;
      this.mRewardId = json.reward_id;
      return true;
    }
  }
}
