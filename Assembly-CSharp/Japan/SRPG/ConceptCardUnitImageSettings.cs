﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ConceptCardUnitImageSettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class ConceptCardUnitImageSettings : ScriptableObject
  {
    private const string ASSET_NAME = "ConceptCardSettings/ConceptCardUnitImageSettings";
    public UnitImageSetting UnitImageSetting;
    private static ConceptCardUnitImageSettings mInstance;

    public ConceptCardUnitImageSettings()
    {
      base.\u002Ector();
    }

    public static ConceptCardUnitImageSettings Instance
    {
      get
      {
        if (Object.op_Equality((Object) ConceptCardUnitImageSettings.mInstance, (Object) null))
          ConceptCardUnitImageSettings.mInstance = AssetManager.Load<ConceptCardUnitImageSettings>("ConceptCardSettings/ConceptCardUnitImageSettings");
        return ConceptCardUnitImageSettings.mInstance;
      }
    }

    public static void ComposeUnitConceptCardImage(
      ConceptCardParam param,
      RawImage bgImage,
      GameObject imageTemplate,
      GameObject msg,
      Text msgText)
    {
      string path1 = string.IsNullOrEmpty(param.bg_image) ? AssetPath.ConceptCard(param.icon) : AssetPath.ConceptCard(param.bg_image);
      if (Object.op_Inequality((Object) bgImage, (Object) null))
      {
        string fileName = Path.GetFileName(path1);
        if (((Object) ((Graphic) bgImage).get_mainTexture()).get_name() != fileName)
        {
          bgImage.set_texture((Texture) null);
          MonoSingleton<GameManager>.Instance.ApplyTextureAsync(bgImage, path1);
        }
      }
      if (param.unit_images != null && param.unit_images.Length > 0)
      {
        Dictionary<string, UnitImageSetting.Vector2AndFloat> table = ConceptCardUnitImageSettings.Instance.UnitImageSetting.GetTable();
        foreach (string unitImage in param.unit_images)
        {
          if (!Object.op_Equality((Object) imageTemplate, (Object) null))
          {
            GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) imageTemplate);
            RawImage component = (RawImage) gameObject.GetComponent<RawImage>();
            string path2 = AssetPath.UnitImage(unitImage);
            component.set_texture((Texture) null);
            MonoSingleton<GameManager>.Instance.ApplyTextureAsync(component, path2);
            gameObject.get_transform().SetParent(imageTemplate.get_transform().get_parent(), false);
            gameObject.SetActive(true);
            RectTransform transform = gameObject.get_transform() as RectTransform;
            transform.set_pivot(new Vector2(0.0f, 1f));
            UnitImageSetting.Vector2AndFloat vector2AndFloat;
            if (table != null && table.TryGetValue(unitImage, out vector2AndFloat))
            {
              Vector2 vector2;
              ((Vector2) ref vector2).\u002Ector((float) (transform.get_sizeDelta().x * vector2AndFloat.Offset.x), (float) (transform.get_sizeDelta().y * vector2AndFloat.Offset.y));
              RectTransform rectTransform = transform;
              rectTransform.set_anchoredPosition(Vector2.op_Addition(rectTransform.get_anchoredPosition(), vector2));
              ((Transform) transform).set_localScale(new Vector3(vector2AndFloat.Scale, vector2AndFloat.Scale, vector2AndFloat.Scale));
            }
          }
        }
        if (Object.op_Inequality((Object) msg, (Object) null))
          msg.SetActive(true);
        if (!Object.op_Inequality((Object) msgText, (Object) null))
          return;
        msgText.set_text(param.GetLocalizedTextMessage());
      }
      else
      {
        if (Object.op_Inequality((Object) msg, (Object) null))
          msg.SetActive(false);
        if (!Object.op_Inequality((Object) msgText, (Object) null))
          return;
        msgText.set_text((string) null);
      }
    }
  }
}
