﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_SelectGachaInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  [FlowNode.NodeType("SRPG/Gacha/SelectGachaInfo", 32741)]
  [FlowNode.Pin(0, "Input", FlowNode.PinTypes.Input, 0)]
  [FlowNode.Pin(10, "SelectDetailOnly", FlowNode.PinTypes.Output, 10)]
  [FlowNode.Pin(11, "SelectDetailAndRate", FlowNode.PinTypes.Output, 11)]
  public class FlowNode_SelectGachaInfo : FlowNode
  {
    private const int PIN_IN_INPUT = 0;
    private const int PIN_OT_SELECT_DETAIL_ONLY = 10;
    private const int PIN_OT_SELECT_DETAIL_RATE = 11;

    public override void OnActivate(int pinID)
    {
      if (pinID != 0)
        return;
      bool isRateView = GachaWindow.IsRateView;
      FlowNode_Variable.Set("SHARED_WEBWINDOW_URL", FlowNode_Variable.Get("SHARED_WEBWINDOW_URL2"));
      if (!isRateView)
        this.ActivateOutputLinks(10);
      else
        this.ActivateOutputLinks(11);
    }
  }
}
