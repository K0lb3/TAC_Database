﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_RaidRescueMember
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_RaidRescueMember
  {
    public string uid;
    public string name;
    public int lv;
    public int member_type;
    public Json_Unit unit;
    public string selected_award;
    public int lastlogin;
    public int area_id;
    public int boss_id;
    public int round;
    public int current_hp;
    public long start_time;
  }
}
