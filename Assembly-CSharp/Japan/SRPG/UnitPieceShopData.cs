﻿// Decompiled with JetBrains decompiler
// Type: SRPG.UnitPieceShopData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace SRPG
{
  public class UnitPieceShopData
  {
    public string ShopIName { get; private set; }

    public string CostIName { get; private set; }

    public List<UnitPieceShopItem> ShopItems { get; private set; }

    public bool Deserialize(ReqUnitPieceShopItemList.Response json)
    {
      if (json == null)
        return false;
      this.ShopIName = json.shop_iname;
      this.CostIName = json.cost_iname;
      this.ShopItems = new List<UnitPieceShopItem>();
      for (int index = 0; index < json.shopitems.Length; ++index)
      {
        UnitPieceShopItem unitPieceShopItem = new UnitPieceShopItem();
        if (unitPieceShopItem.Deserialize(json.shopitems[index]))
          this.ShopItems.Add(unitPieceShopItem);
      }
      return true;
    }

    public bool Deserialize(ReqUnitPieceShopBuypaid.Response json)
    {
      if (json == null)
        return false;
      this.ShopItems = new List<UnitPieceShopItem>();
      for (int index = 0; index < json.shopitems.Length; ++index)
      {
        UnitPieceShopItem unitPieceShopItem = new UnitPieceShopItem();
        if (unitPieceShopItem.Deserialize(json.shopitems[index]))
          this.ShopItems.Add(unitPieceShopItem);
      }
      return true;
    }
  }
}
