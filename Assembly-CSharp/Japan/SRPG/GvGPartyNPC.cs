﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GvGPartyNPC
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class GvGPartyNPC
  {
    public long UniqueID { get; private set; }

    public int HP { get; private set; }

    public bool Deserialize(JSON_GvGPartyNPC json)
    {
      if (json == null)
        return false;
      this.UniqueID = (long) json.iid;
      this.HP = json.hp;
      return true;
    }
  }
}
