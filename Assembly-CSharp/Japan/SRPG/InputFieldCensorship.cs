﻿// Decompiled with JetBrains decompiler
// Type: SRPG.InputFieldCensorship
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using UnityEngine.UI;

namespace SRPG
{
  public class InputFieldCensorship : SRPG_InputField
  {
    public bool CheckCharacterLimitAtEnded;
    public int CheckCharacterLimitAtEndedCount;

    public string text
    {
      get
      {
        return base.get_text();
      }
      set
      {
        if (this.CheckCharacterLimitAtEnded && value.Length > this.CheckCharacterLimitAtEndedCount)
          base.set_text(value.Substring(0, this.CheckCharacterLimitAtEndedCount));
        else
          base.set_text(value);
      }
    }

    protected virtual void Start()
    {
      InputFieldCensorship inputFieldCensorship = this;
      // ISSUE: method pointer
      inputFieldCensorship.set_onValidateInput((InputField.OnValidateInput) System.Delegate.Combine((System.Delegate) inputFieldCensorship.get_onValidateInput(), (System.Delegate) new InputField.OnValidateInput((object) this, __methodptr(MyValidate))));
    }

    private char MyValidate(string input, int charIndex, char addedChar)
    {
      GameSettings instance = GameSettings.Instance;
      return UnityEngine.Object.op_Equality((UnityEngine.Object) instance, (UnityEngine.Object) null) || Array.IndexOf<char>(instance.ValidInputChars, addedChar) < 0 ? char.MinValue : addedChar;
    }

    public void EndEdit(string text)
    {
      if (text.Length > this.get_characterLimit())
        text = text.Substring(0, this.get_characterLimit());
      this.text = text;
    }

    [Serializable]
    public class ValidCodeSegment
    {
      public int Start;
      public int End;
    }
  }
}
