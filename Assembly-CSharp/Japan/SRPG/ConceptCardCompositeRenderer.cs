﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ConceptCardCompositeRenderer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

namespace SRPG
{
  public class ConceptCardCompositeRenderer : MonoBehaviour
  {
    [SerializeField]
    private RawImage_Transparent Image;
    [SerializeField]
    private GameObject Message;
    [SerializeField]
    private Text MessageText;
    [SerializeField]
    private GameObject OverlayImageTemplate;
    [SerializeField]
    private Camera Camera;
    private RenderTexture mRenderTexture;

    public ConceptCardCompositeRenderer()
    {
      base.\u002Ector();
    }

    public RenderTexture RenderTexture
    {
      get
      {
        if (Object.op_Equality((Object) this.mRenderTexture, (Object) null))
        {
          if (Object.op_Equality((Object) this.Camera, (Object) null))
            return (RenderTexture) null;
          this.mRenderTexture = RenderTexture.GetTemporary(1024, 1024, 24, (RenderTextureFormat) 0, (RenderTextureReadWrite) 1);
          this.mRenderTexture.set_antiAliasing(1);
          ((Texture) this.mRenderTexture).set_dimension((TextureDimension) 2);
          ((Texture) this.mRenderTexture).set_wrapMode((TextureWrapMode) 1);
          ((Texture) this.mRenderTexture).set_filterMode((FilterMode) 1);
          this.mRenderTexture.set_autoGenerateMips(false);
          this.Camera.set_targetTexture(this.mRenderTexture);
        }
        return this.mRenderTexture;
      }
    }

    public void Setup(ConceptCardParam param)
    {
      if (Object.op_Inequality((Object) this.OverlayImageTemplate, (Object) null))
        this.OverlayImageTemplate.get_gameObject().SetActive(false);
      if (Object.op_Inequality((Object) this.Message, (Object) null))
        this.Message.get_gameObject().SetActive(false);
      if (!Object.op_Inequality((Object) this.Image, (Object) null) || param == null)
        return;
      ConceptCardUnitImageSettings.ComposeUnitConceptCardImage(param, (RawImage) this.Image, this.OverlayImageTemplate, this.Message, this.MessageText);
    }

    private void OnDestroy()
    {
      if (!Object.op_Inequality((Object) this.mRenderTexture, (Object) null))
        return;
      RenderTexture.ReleaseTemporary(this.mRenderTexture);
      this.mRenderTexture = (RenderTexture) null;
    }
  }
}
