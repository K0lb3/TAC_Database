﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Flownode_GuildRaidBattlePoint
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("GuildRaid/BattlePoint", 32741)]
  [FlowNode.Pin(1, "Start", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(101, "Today End", FlowNode.PinTypes.Output, 101)]
  [FlowNode.Pin(102, "No Challenge", FlowNode.PinTypes.Output, 102)]
  [FlowNode.Pin(103, "Challenge", FlowNode.PinTypes.Output, 103)]
  [FlowNode.Pin(900, "Error", FlowNode.PinTypes.Output, 900)]
  public class Flownode_GuildRaidBattlePoint : FlowNode
  {
    public override void OnActivate(int pinID)
    {
      if (pinID != 1)
        return;
      GuildRaidManager instance = GuildRaidManager.Instance;
      if (Object.op_Equality((Object) instance, (Object) null))
        this.ActivateOutputLinks(900);
      else if (instance.ChallengedBp == instance.MaxBp)
        this.ActivateOutputLinks(101);
      else if (instance.ChallengedBp == 0)
        this.ActivateOutputLinks(102);
      else
        this.ActivateOutputLinks(103);
    }
  }
}
