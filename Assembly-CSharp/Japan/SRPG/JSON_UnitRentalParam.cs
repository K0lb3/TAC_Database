﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_UnitRentalParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_UnitRentalParam
  {
    public string iname;
    public string unit;
    public string begin_at;
    public string end_at;
    public int pt_max;
    public int ptup_lv;
    public int ptup_evol;
    public int ptup_awake;
    public int ptup_job_lv;
    public int ptup_ability_lv;
    public int ptup_quest_main;
    public int ptup_quest_sub;
    public string notification;
    public JSON_UnitRentalParam.QuestInfo[] quest_infos;

    [MessagePackObject(true)]
    [Serializable]
    public class QuestInfo
    {
      public int point;
      public string quest_id;
    }
  }
}
