﻿// Decompiled with JetBrains decompiler
// Type: SRPG.DrawCardInfoParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class DrawCardInfoParam
  {
    public OInt CardNum;
    public OInt MissNum;
    public string DcRewardId;
    private DrawCardRewardParam mDcReward;

    public DrawCardRewardParam DcReward
    {
      get
      {
        if (this.mDcReward == null && !string.IsNullOrEmpty(this.DcRewardId))
          this.mDcReward = DrawCardRewardParam.GetParam(this.DcRewardId);
        return this.mDcReward;
      }
    }

    public void Deserialize(JSON_DrawCardParam.DrawInfo json)
    {
      if (json == null)
        return;
      this.CardNum = (OInt) json.card_num;
      this.MissNum = (OInt) json.miss_num;
      this.DcRewardId = json.dc_reward_id;
    }
  }
}
