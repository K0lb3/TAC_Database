﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ButtonHoldObserver
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.EventSystems;

namespace SRPG
{
  public class ButtonHoldObserver : MonoBehaviour, IPointerDownHandler, IEventSystemHandler
  {
    private float[] HoldSpan;
    private float HoldDuration;
    private bool Holding;
    private int ActionCount;
    private Vector2 mDragStartPos;
    public ButtonHoldObserver.DelegateOnHoldEvent OnHoldStart;
    public ButtonHoldObserver.DelegateOnHoldEvent OnHoldEnd;
    public ButtonHoldObserver.DelegateOnHoldEvent OnHoldUpdate;

    public ButtonHoldObserver()
    {
      base.\u002Ector();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
      if (this.OnHoldStart == null)
        return;
      this.OnHoldStart();
      this.Holding = true;
      this.mDragStartPos = eventData.get_position();
    }

    public void OnPointerUp()
    {
      if (this.OnHoldEnd != null)
        this.OnHoldEnd();
      this.StatusReset();
    }

    public void StatusReset()
    {
      this.Holding = false;
      this.ActionCount = 0;
      this.HoldDuration = 0.0f;
      ((Vector2) ref this.mDragStartPos).Set(0.0f, 0.0f);
    }

    public void Update()
    {
      if (this.OnHoldUpdate == null)
        return;
      float unscaledDeltaTime = Time.get_unscaledDeltaTime();
      if (this.Holding && !Input.GetMouseButton(0))
      {
        this.OnPointerUp();
      }
      else
      {
        GameSettings instance = GameSettings.Instance;
        float num = (float) (instance.HoldMargin * instance.HoldMargin);
        Vector2 vector2 = Vector2.op_Subtraction(this.mDragStartPos, Vector2.op_Implicit(Input.get_mousePosition()));
        bool flag = (double) ((Vector2) ref vector2).get_sqrMagnitude() > (double) num;
        if ((double) this.HoldDuration < (double) this.HoldSpan[this.ActionCount] && this.ActionCount < 1 && flag)
        {
          this.StatusReset();
        }
        else
        {
          if (!this.Holding)
            return;
          this.HoldDuration += unscaledDeltaTime;
          if ((double) this.HoldDuration < (double) this.HoldSpan[this.ActionCount])
            return;
          this.HoldDuration -= this.HoldSpan[this.ActionCount];
          this.OnHoldUpdate();
          if (this.ActionCount >= this.HoldSpan.Length - 1)
            return;
          ++this.ActionCount;
        }
      }
    }

    public void OnDestroy()
    {
      this.StatusReset();
      this.OnHoldStart = (ButtonHoldObserver.DelegateOnHoldEvent) null;
      this.OnHoldEnd = (ButtonHoldObserver.DelegateOnHoldEvent) null;
    }

    public delegate void DelegateOnHoldEvent();
  }
}
