﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RankMatchRankingTab
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;

namespace SRPG
{
  public class RankMatchRankingTab : SRPG_ListBase
  {
    [SerializeField]
    private GameObject PlayerGO;
    [SerializeField]
    private GameObject PlayerUnit;
    [SerializeField]
    private ListItemEvents ListItem;

    protected override void Start()
    {
      base.Start();
      if (Object.op_Inequality((Object) this.PlayerUnit, (Object) null) && Object.op_Inequality((Object) this.PlayerGO, (Object) null))
      {
        PlayerData player = MonoSingleton<GameManager>.Instance.Player;
        DataSource.Bind<PlayerData>(this.PlayerGO, player, false);
        DataSource.Bind<UnitData>(this.PlayerUnit, player.FindUnitDataByUniqueID((long) GlobalVars.SelectedSupportUnitUniqueID), false);
        GameParameter.UpdateAll(this.PlayerUnit);
      }
      if (Object.op_Equality((Object) this.ListItem, (Object) null))
        return;
      this.ClearItems();
      ((Component) this.ListItem).get_gameObject().SetActive(false);
      Network.RequestAPI((WebAPI) new ReqRankMatchRanking(new Network.ResponseCallback(this.ResponseCallback)), false);
    }

    private void ResponseCallback(WWWResult www)
    {
      if (FlowNode_Network.HasCommonError(www))
        return;
      if (Network.IsError)
      {
        Network.EErrCode errCode = Network.ErrCode;
        switch (errCode)
        {
          case Network.EErrCode.MultiMaintenance:
          case Network.EErrCode.VsMaintenance:
          case Network.EErrCode.MultiVersionMaintenance:
          case Network.EErrCode.MultiTowerMaintenance:
            Network.RemoveAPI();
            ((Behaviour) this).set_enabled(false);
            break;
          default:
            if (errCode != Network.EErrCode.OutOfDateQuest)
            {
              if (errCode == Network.EErrCode.MultiVersionMismatch || errCode == Network.EErrCode.VS_Version)
              {
                Network.RemoveAPI();
                Network.ResetError();
                ((Behaviour) this).set_enabled(false);
                break;
              }
              FlowNode_Network.Retry();
              break;
            }
            Network.RemoveAPI();
            Network.ResetError();
            ((Behaviour) this).set_enabled(false);
            break;
        }
      }
      else
      {
        WebAPI.JSON_BodyResponse<ReqRankMatchRanking.Response> jsonBodyResponse = (WebAPI.JSON_BodyResponse<ReqRankMatchRanking.Response>) JsonUtility.FromJson<WebAPI.JSON_BodyResponse<ReqRankMatchRanking.Response>>(www.text);
        DebugUtility.Assert(jsonBodyResponse != null, "res == null");
        if (jsonBodyResponse.body == null)
        {
          Network.RemoveAPI();
        }
        else
        {
          if (jsonBodyResponse.body.rankings == null)
            return;
          for (int index = 0; index < jsonBodyResponse.body.rankings.Length; ++index)
          {
            ReqRankMatchRanking.ResponceRanking ranking = jsonBodyResponse.body.rankings[index];
            ListItemEvents listItemEvents = (ListItemEvents) Object.Instantiate<ListItemEvents>((M0) this.ListItem);
            DataSource.Bind<ReqRankMatchRanking.ResponceRanking>(((Component) listItemEvents).get_gameObject(), ranking, false);
            FriendData data = new FriendData();
            data.Deserialize(ranking.enemy);
            DataSource.Bind<FriendData>(((Component) listItemEvents).get_gameObject(), data, false);
            DataSource.Bind<ViewGuildData>(((Component) listItemEvents).get_gameObject(), data.ViewGuild, false);
            DataSource.Bind<UnitData>(((Component) listItemEvents).get_gameObject(), data.Unit, false);
            SerializeValueBehaviour component = (SerializeValueBehaviour) ((Component) listItemEvents).GetComponent<SerializeValueBehaviour>();
            if (Object.op_Inequality((Object) component, (Object) null))
            {
              long num = data.ViewGuild == null ? 0L : (long) data.ViewGuild.id;
              component.list.SetField(GuildSVB_Key.GUILD_ID, (int) num);
            }
            this.AddItem(listItemEvents);
            ((Component) listItemEvents).get_transform().SetParent(((Component) this).get_transform(), false);
            ((Component) listItemEvents).get_gameObject().SetActive(true);
          }
          Network.RemoveAPI();
        }
      }
    }
  }
}
