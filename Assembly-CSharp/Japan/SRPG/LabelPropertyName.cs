﻿// Decompiled with JetBrains decompiler
// Type: SRPG.LabelPropertyName
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class LabelPropertyName : PropertyAttribute
  {
    public string Name;

    public LabelPropertyName(string name)
    {
      this.\u002Ector();
      this.Name = name;
    }
  }
}
