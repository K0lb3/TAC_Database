﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_ShopStepupMessage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("Shop/ShopStepupMessage", 32741)]
  [FlowNode.Pin(1, "Start", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(2, "Finished", FlowNode.PinTypes.Output, 2)]
  public class FlowNode_ShopStepupMessage : FlowNode
  {
    public override void OnActivate(int pinID)
    {
      if (pinID != 1)
        return;
      if (!ShopBuyStepupInfoData.IsSet)
      {
        this.ActivateOutputLinks(2);
      }
      else
      {
        ShopBuyStepupInfoData.Reset();
        if (string.IsNullOrEmpty(ShopBuyStepupInfoData.ItemName))
          this.ActivateOutputLinks(2);
        else
          UIUtility.SystemMessage(string.Format(LocalizedText.Get("sys.SHOP_STEP_UP_MESSAGE"), (object) ShopBuyStepupInfoData.SoldCount, (object) ShopBuyStepupInfoData.ItemName, (object) ShopBuyStepupInfoData.PriceBefore, (object) ShopBuyStepupInfoData.PriceAfter, (object) ShopBuyStepupInfoData.CurrencyUnit, (object) ShopBuyStepupInfoData.Currency), (UIUtility.DialogResultEvent) (go => this.ActivateOutputLinks(2)), (GameObject) null, true, -1);
      }
    }
  }
}
