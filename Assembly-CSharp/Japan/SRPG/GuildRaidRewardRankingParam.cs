﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRaidRewardRankingParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace SRPG
{
  public class GuildRaidRewardRankingParam : GuildRaidMasterParam<JSON_GuildRaidRewardRankingParam>
  {
    public string Id { get; private set; }

    public List<GuildRaidRewardRankingDataParam> Ranking { get; private set; }

    public override bool Deserialize(JSON_GuildRaidRewardRankingParam json)
    {
      if (json == null)
        return false;
      this.Id = json.id;
      if (json.ranking == null)
        return false;
      this.Ranking = new List<GuildRaidRewardRankingDataParam>();
      for (int index = 0; index < json.ranking.Length; ++index)
      {
        GuildRaidRewardRankingDataParam rankingDataParam = new GuildRaidRewardRankingDataParam();
        if (rankingDataParam.Deserialize(json.ranking[index]))
          this.Ranking.Add(rankingDataParam);
      }
      return true;
    }
  }
}
