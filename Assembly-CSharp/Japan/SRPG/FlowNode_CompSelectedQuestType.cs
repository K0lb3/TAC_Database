﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_CompSelectedQuestType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("System/CompSelectedQuestType", 32741)]
  [FlowNode.Pin(1, "Comp", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(111, "Yes", FlowNode.PinTypes.Output, 111)]
  [FlowNode.Pin(112, "No", FlowNode.PinTypes.Output, 112)]
  public class FlowNode_CompSelectedQuestType : FlowNode
  {
    [SerializeField]
    private QuestTypes[] mCompQuestTypes;
    private const int PIN_IN_COMP = 1;
    private const int PIN_OUT_YES = 111;
    private const int PIN_OUT_NO = 112;

    public override void OnActivate(int pinID)
    {
      if (pinID != 1)
        return;
      QuestParam quest = MonoSingleton<GameManager>.Instance.FindQuest(GlobalVars.SelectedQuestID);
      if (quest != null && this.mCompQuestTypes != null && this.mCompQuestTypes.Length != 0)
      {
        for (int index = 0; index < this.mCompQuestTypes.Length; ++index)
        {
          if (quest.type == this.mCompQuestTypes[index])
          {
            this.ActivateOutputLinks(111);
            return;
          }
        }
      }
      this.ActivateOutputLinks(112);
    }
  }
}
