﻿// Decompiled with JetBrains decompiler
// Type: SRPG.eInspSkillTriggerType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public enum eInspSkillTriggerType
  {
    UNIT,
    UNIT_ELEMENT,
    UNIT_TYPE,
    UNIT_JOB,
    UNIT_BIRTH,
    USE_SKILL,
    TARGET_UNIT,
    TARGET_UNIT_ELEMENT,
    TARGET_UNIT_TYPE,
    TARGET_UNIT_JOB,
    TARGET_UNIT_BIRTH,
    UNIT_LEVEL,
    TARGET_UNIT_LEVEL,
  }
}
