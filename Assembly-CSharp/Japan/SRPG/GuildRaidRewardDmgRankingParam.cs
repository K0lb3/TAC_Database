﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRaidRewardDmgRankingParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace SRPG
{
  public class GuildRaidRewardDmgRankingParam : GuildRaidMasterParam<JSON_GuildRaidRewardDmgRankingParam>
  {
    public string Id { get; private set; }

    public List<GuildRaidRewardDmgRankingRankParam> Ranking { get; private set; }

    public override bool Deserialize(JSON_GuildRaidRewardDmgRankingParam json)
    {
      if (json == null)
        return false;
      this.Id = json.id;
      if (json.ranking == null)
        return false;
      this.Ranking = new List<GuildRaidRewardDmgRankingRankParam>();
      for (int index = 0; index < json.ranking.Length; ++index)
      {
        GuildRaidRewardDmgRankingRankParam rankingRankParam = new GuildRaidRewardDmgRankingRankParam();
        if (rankingRankParam.Deserialize(json.ranking[index]))
          this.Ranking.Add(rankingRankParam);
      }
      return true;
    }
  }
}
