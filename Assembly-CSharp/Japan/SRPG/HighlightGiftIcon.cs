﻿// Decompiled with JetBrains decompiler
// Type: SRPG.HighlightGiftIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class HighlightGiftIcon : MonoBehaviour
  {
    [SerializeField]
    private GameObject AnimationStamp;
    [SerializeField]
    private GameObject Stamp;

    public HighlightGiftIcon()
    {
      base.\u002Ector();
    }

    public void Initialize()
    {
      if (Object.op_Inequality((Object) this.AnimationStamp, (Object) null))
        this.AnimationStamp.SetActive(false);
      if (!Object.op_Inequality((Object) this.Stamp, (Object) null))
        return;
      this.Stamp.SetActive(false);
    }

    public void SetStamp(bool playAnimation)
    {
      if (playAnimation)
      {
        if (Object.op_Inequality((Object) this.AnimationStamp, (Object) null))
          this.AnimationStamp.SetActive(true);
        if (!Object.op_Inequality((Object) this.Stamp, (Object) null))
          return;
        this.Stamp.SetActive(false);
      }
      else
      {
        if (Object.op_Inequality((Object) this.AnimationStamp, (Object) null))
          this.AnimationStamp.SetActive(false);
        if (!Object.op_Inequality((Object) this.Stamp, (Object) null))
          return;
        this.Stamp.SetActive(true);
      }
    }
  }
}
