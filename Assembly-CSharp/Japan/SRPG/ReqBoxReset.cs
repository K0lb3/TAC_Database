﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqBoxReset
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class ReqBoxReset : WebAPI
  {
    public ReqBoxReset(string box_iname, Network.ResponseCallback response)
    {
      this.name = "box_lottery/next_step";
      this.body = WebAPI.GetRequestString<ReqBoxReset.RequestParam>(new ReqBoxReset.RequestParam()
      {
        box_iname = box_iname
      });
      this.callback = response;
    }

    [Serializable]
    public class RequestParam
    {
      public string box_iname;
    }

    [Serializable]
    public class Response
    {
      public string box_iname;
      public int step;
      public int total_num;
      public int remain_num;
    }
  }
}
