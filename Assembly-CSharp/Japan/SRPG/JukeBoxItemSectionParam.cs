﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JukeBoxItemSectionParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;
using UnityEngine.Events;

namespace SRPG
{
  public class JukeBoxItemSectionParam : ContentSource.Param
  {
    private List<JukeBoxItemSectionNode> mNodeList = new List<JukeBoxItemSectionNode>();
    public JukeBoxSectionParam SectionParam;
    public UnityAction OnClickAction;
    private bool mIsCurrent;
    private bool mNewFlag;

    public override void OnEnable(ContentNode node)
    {
      base.OnEnable(node);
      this.mNodeList.Add(node as JukeBoxItemSectionNode);
      this.Refresh();
    }

    public override void OnDisable(ContentNode node)
    {
      base.OnDisable(node);
      this.mNodeList.Remove(node as JukeBoxItemSectionNode);
      if (this.mNodeList.Count > 0)
        return;
      this.mNodeList.Clear();
    }

    public void Refresh()
    {
      if (this.mNodeList.Count <= 0 || this.SectionParam == null)
        return;
      foreach (JukeBoxItemSectionNode mNode in this.mNodeList)
        mNode.Setup(this, this.mIsCurrent, this.mNewFlag, this.OnClickAction);
    }

    public void SetCurrent(bool is_active)
    {
      this.mIsCurrent = is_active;
      if (this.mNodeList.Count <= 0)
        return;
      foreach (JukeBoxItemSectionNode mNode in this.mNodeList)
        mNode.SetCurrent(is_active);
    }

    public bool IsCurrent()
    {
      return this.mIsCurrent;
    }

    public void SetNewBadge(bool is_new)
    {
      this.mNewFlag = is_new;
      if (this.mNodeList.Count <= 0)
        return;
      foreach (JukeBoxItemSectionNode mNode in this.mNodeList)
        mNode.SetNewBadge(is_new);
    }
  }
}
