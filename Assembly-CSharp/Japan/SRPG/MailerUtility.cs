﻿// Decompiled with JetBrains decompiler
// Type: SRPG.MailerUtility
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using DeviceKit;

namespace SRPG
{
  public class MailerUtility
  {
    public static void Launch(string mailto, string subject, string body)
    {
      App.LaunchMailer(mailto, subject, body.Replace("\n", "%0A"));
    }
  }
}
