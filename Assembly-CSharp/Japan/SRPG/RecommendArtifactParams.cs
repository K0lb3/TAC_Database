﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RecommendArtifactParams
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using System.Collections.Generic;

namespace SRPG
{
  public class RecommendArtifactParams
  {
    private List<ArtifactParam> mUnitJobArtifacts = new List<ArtifactParam>();
    private List<ArtifactParam> mUnitArtifacts = new List<ArtifactParam>();
    private List<ArtifactParam> mAbilityArtifacts = new List<ArtifactParam>();

    public List<ArtifactParam> UnitJobArtifacts
    {
      get
      {
        return this.mUnitJobArtifacts;
      }
    }

    public List<ArtifactParam> UnitArtifacts
    {
      get
      {
        return this.mUnitArtifacts;
      }
    }

    public List<ArtifactParam> AbilityArtifacts
    {
      get
      {
        return this.mAbilityArtifacts;
      }
    }

    public List<ArtifactParam> GetAll()
    {
      List<ArtifactParam> artifactParamList = new List<ArtifactParam>();
      Dictionary<ArtifactTypes, List<ArtifactParam>> dictionary = new Dictionary<ArtifactTypes, List<ArtifactParam>>();
      dictionary.Add(ArtifactTypes.Arms, new List<ArtifactParam>());
      dictionary.Add(ArtifactTypes.Armor, new List<ArtifactParam>());
      dictionary.Add(ArtifactTypes.Accessory, new List<ArtifactParam>());
      if (this.mUnitJobArtifacts != null)
      {
        for (int index = 0; index < this.mUnitJobArtifacts.Count; ++index)
        {
          ArtifactTypes type = this.mUnitJobArtifacts[index].type;
          if (!dictionary[type].Contains(this.mUnitJobArtifacts[index]))
            dictionary[type].Add(this.mUnitJobArtifacts[index]);
        }
      }
      if (this.mUnitArtifacts != null)
      {
        for (int index = 0; index < this.mUnitArtifacts.Count; ++index)
        {
          ArtifactTypes type = this.mUnitArtifacts[index].type;
          if (!dictionary[type].Contains(this.mUnitArtifacts[index]))
            dictionary[type].Add(this.mUnitArtifacts[index]);
        }
      }
      if (this.mAbilityArtifacts != null)
      {
        for (int index = 0; index < this.mAbilityArtifacts.Count; ++index)
        {
          ArtifactTypes type = this.mAbilityArtifacts[index].type;
          if (!dictionary[type].Contains(this.mAbilityArtifacts[index]))
            dictionary[type].Add(this.mAbilityArtifacts[index]);
        }
      }
      artifactParamList.AddRange((IEnumerable<ArtifactParam>) dictionary[ArtifactTypes.Arms]);
      artifactParamList.AddRange((IEnumerable<ArtifactParam>) dictionary[ArtifactTypes.Armor]);
      artifactParamList.AddRange((IEnumerable<ArtifactParam>) dictionary[ArtifactTypes.Accessory]);
      return artifactParamList;
    }

    public List<ArtifactData> GetRecommendArtifacts(
      List<ArtifactData> player_artifacts,
      ArtifactTypes type,
      int num)
    {
      List<ArtifactData> artifactDataList = new List<ArtifactData>();
      int num1 = num;
      List<ArtifactData> all = player_artifacts.FindAll((Predicate<ArtifactData>) (a => a.ArtifactParam.type == type));
      if (all == null || all.Count <= 0)
        return artifactDataList;
      List<ArtifactData> tmp = all.FindAll((Predicate<ArtifactData>) (data => this.mUnitJobArtifacts.Contains(data.ArtifactParam)));
      for (int i = 0; i < tmp.Count && num1 > 0; ++i)
      {
        if (artifactDataList.FindIndex((Predicate<ArtifactData>) (r => r.ArtifactParam.iname == tmp[i].ArtifactParam.iname)) < 0)
        {
          artifactDataList.Add(tmp[i]);
          --num1;
        }
      }
      if (num1 > 0)
      {
        tmp = all.FindAll((Predicate<ArtifactData>) (data => this.mUnitArtifacts.Contains(data.ArtifactParam)));
        for (int i = 0; i < tmp.Count && num1 > 0; ++i)
        {
          if (artifactDataList.FindIndex((Predicate<ArtifactData>) (r => r.ArtifactParam.iname == tmp[i].ArtifactParam.iname)) < 0)
          {
            artifactDataList.Add(tmp[i]);
            --num1;
          }
        }
      }
      return artifactDataList;
    }

    public List<ArtifactData> GetMasterAbilityArtifacts(
      List<ArtifactData> player_artifacts,
      ArtifactTypes type,
      int num)
    {
      List<ArtifactData> artifactDataList = new List<ArtifactData>();
      int num1 = num;
      List<ArtifactData> all = player_artifacts.FindAll((Predicate<ArtifactData>) (a => a.ArtifactParam.type == type));
      if (all == null || all.Count <= 0 || num1 <= 0)
        return artifactDataList;
      List<ArtifactData> tmp = all.FindAll((Predicate<ArtifactData>) (data => this.mAbilityArtifacts.Contains(data.ArtifactParam)));
      for (int i = 0; i < tmp.Count && num1 > 0; ++i)
      {
        if (artifactDataList.FindIndex((Predicate<ArtifactData>) (r => r.ArtifactParam.iname == tmp[i].ArtifactParam.iname)) < 0)
        {
          artifactDataList.Add(tmp[i]);
          --num1;
        }
      }
      return artifactDataList;
    }
  }
}
