﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GlobalEventTrigger
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  [AddComponentMenu("Event/Global Event Trigger")]
  [DisallowMultipleComponent]
  public class GlobalEventTrigger : MonoBehaviour
  {
    public GlobalEventTrigger()
    {
      base.\u002Ector();
    }

    public void Trigger(string eventName)
    {
      GlobalEvent.Invoke(eventName, (object) ((Component) this).get_gameObject());
    }
  }
}
