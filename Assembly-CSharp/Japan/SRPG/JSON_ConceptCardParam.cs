﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_ConceptCardParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_ConceptCardParam
  {
    public string iname;
    public string name;
    public string expr;
    public int type;
    public string icon;
    public int rare;
    public int lvcap;
    public int sell;
    public int coin_item;
    public int en_cost;
    public int en_exp;
    public int en_trust;
    public string trust_reward;
    public string first_get_unit;
    public JSON_ConceptCardEquipParam[] effects;
    public int not_sale;
    public int birth_id;
    public string[] concept_card_groups;
    public string leader_skill;
    public int gallery_view;
    public int is_other;
    public string bg_image;
    public string[] unit_images;
  }
}
