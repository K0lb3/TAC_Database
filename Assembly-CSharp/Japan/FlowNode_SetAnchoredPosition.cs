﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_SetAnchoredPosition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

[FlowNode.NodeType("UI/SetAnchoredPosition", 32741)]
[FlowNode.Pin(0, "In", FlowNode.PinTypes.Input, 0)]
[FlowNode.Pin(1, "Out", FlowNode.PinTypes.Output, 1)]
public class FlowNode_SetAnchoredPosition : FlowNode
{
  [FlowNode.DropTarget(typeof (RectTransform), true)]
  [FlowNode.ShowInInfo]
  public RectTransform Target;
  public Vector2 TargetPosition;

  public override void OnActivate(int pinID)
  {
    if (pinID != 0)
      return;
    if (Object.op_Inequality((Object) this.Target, (Object) null))
      this.Target.set_anchoredPosition(this.TargetPosition);
    this.ActivateOutputLinks(1);
  }
}
