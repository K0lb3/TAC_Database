﻿// Decompiled with JetBrains decompiler
// Type: System.Reflection.CustomAttributeExtensions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;
using System.Linq;

namespace System.Reflection
{
  public static class CustomAttributeExtensions
  {
    public static T GetCustomAttribute<T>(MemberInfo memberInfo, bool inherit)
    {
      return (T) ((IEnumerable<object>) memberInfo.GetCustomAttributes(typeof (T), inherit)).FirstOrDefault<object>();
    }
  }
}
