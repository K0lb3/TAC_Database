﻿// Decompiled with JetBrains decompiler
// Type: System.Reflection.TypeInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;
using System.Linq;

namespace System.Reflection
{
  public class TypeInfo
  {
    private readonly Type type;

    public TypeInfo(Type type)
    {
      this.type = type;
    }

    public string Name
    {
      get
      {
        return this.type.Name;
      }
    }

    public TypeAttributes Attributes
    {
      get
      {
        return this.type.Attributes;
      }
    }

    public bool IsClass
    {
      get
      {
        return this.type.IsClass;
      }
    }

    public bool IsPublic
    {
      get
      {
        return this.type.IsPublic;
      }
    }

    public bool IsInterface
    {
      get
      {
        return this.type.IsInterface;
      }
    }

    public bool IsAbstract
    {
      get
      {
        return this.type.IsAbstract;
      }
    }

    public bool IsArray
    {
      get
      {
        return this.type.IsArray;
      }
    }

    public bool IsValueType
    {
      get
      {
        return this.type.IsValueType;
      }
    }

    public bool IsNestedPublic
    {
      get
      {
        return this.type.IsNestedPublic;
      }
    }

    public IEnumerable<ConstructorInfo> DeclaredConstructors
    {
      get
      {
        return ((IEnumerable<ConstructorInfo>) this.type.GetConstructors()).AsEnumerable<ConstructorInfo>();
      }
    }

    public bool IsGenericType
    {
      get
      {
        return this.type.IsGenericType;
      }
    }

    public Type GetGenericTypeDefinition()
    {
      return this.type.GetGenericTypeDefinition();
    }

    public Type AsType()
    {
      return this.type;
    }

    public MethodInfo GetDeclaredMethod(string name)
    {
      return this.type.GetMethod(name);
    }

    public IEnumerable<MethodInfo> GetDeclaredMethods(string name)
    {
      return ((IEnumerable<MethodInfo>) this.type.GetMethods()).Where<MethodInfo>((Func<MethodInfo, bool>) (x => x.Name == name));
    }

    public Type[] GenericTypeArguments
    {
      get
      {
        return this.type.GetGenericArguments();
      }
    }

    public bool IsEnum
    {
      get
      {
        return this.type.IsEnum;
      }
    }

    public bool IsConstructedGenericType()
    {
      return this.type.IsGenericType && !this.type.IsGenericTypeDefinition;
    }

    public Type[] ImplementedInterfaces
    {
      get
      {
        return this.type.GetInterfaces();
      }
    }

    public MethodInfo[] GetRuntimeMethods()
    {
      return this.type.GetMethods();
    }

    public bool IsAssignableFrom(TypeInfo c)
    {
      return this.type.IsAssignableFrom(c.AsType());
    }

    public PropertyInfo GetDeclaredProperty(string name)
    {
      return this.type.GetProperty(name);
    }

    public FieldInfo GetField(string name, BindingFlags flags)
    {
      return this.type.GetField(name, flags);
    }

    public PropertyInfo GetProperty(string name, BindingFlags flags)
    {
      return this.type.GetProperty(name, flags);
    }

    public T GetCustomAttribute<T>(bool inherit = true) where T : Attribute
    {
      return this.type.GetCustomAttributes(inherit).OfType<T>().FirstOrDefault<T>();
    }

    public IEnumerable<T> GetCustomAttributes<T>(bool inherit = true) where T : Attribute
    {
      return this.type.GetCustomAttributes(inherit).OfType<T>();
    }
  }
}
