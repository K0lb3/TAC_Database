﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_Output
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using UnityEngine;

[FlowNode.NodeType("Event/Output", 32741)]
[FlowNode.Pin(1, "", FlowNode.PinTypes.Input, 0)]
[FlowNode.Pin(10, "", FlowNode.PinTypes.Output, 10)]
public class FlowNode_Output : FlowNode
{
  public string PinName;
  [NonSerialized]
  public FlowNode_ExternalLink TargetNode;
  [NonSerialized]
  public int TargetPinID;

  public override string GetCaption()
  {
    return base.GetCaption() + ":" + this.PinName;
  }

  public override void OnActivate(int pinID)
  {
    if (Object.op_Inequality((Object) this.TargetNode, (Object) null))
      this.TargetNode.ActivateOutputLinks(this.TargetPinID);
    this.ActivateOutputLinks(10);
  }
}
