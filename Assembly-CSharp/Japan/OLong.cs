﻿// Decompiled with JetBrains decompiler
// Type: OLong
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using CodeStage.AntiCheat.ObscuredTypes;
using MessagePack;

[MessagePackObject(true)]
public struct OLong
{
  private ObscuredLong value;

  public OLong(long value)
  {
    this.value = (ObscuredLong) value;
  }

  public static implicit operator OLong(long value)
  {
    return new OLong(value);
  }

  public static implicit operator long(OLong value)
  {
    return (long) value.value;
  }

  public static OLong operator ++(OLong value)
  {
    ref OLong local = ref value;
    local.value = (ObscuredLong) ((long) local.value + 1L);
    return value;
  }

  public static OLong operator --(OLong value)
  {
    ref OLong local = ref value;
    local.value = (ObscuredLong) ((long) local.value - 1L);
    return value;
  }

  public override string ToString()
  {
    return this.value.ToString();
  }
}
