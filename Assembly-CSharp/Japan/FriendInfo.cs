﻿// Decompiled with JetBrains decompiler
// Type: FriendInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

public class FriendInfo
{
  [Obsolete("Use UserId.")]
  public string Name
  {
    get
    {
      return this.UserId;
    }
  }

  public string UserId { get; protected internal set; }

  public bool IsOnline { get; protected internal set; }

  public string Room { get; protected internal set; }

  public bool IsInRoom
  {
    get
    {
      return this.IsOnline && !string.IsNullOrEmpty(this.Room);
    }
  }

  public override string ToString()
  {
    return string.Format("{0}\t is: {1}", (object) this.UserId, this.IsOnline ? (!this.IsInRoom ? (object) "on master" : (object) "playing") : (object) "offline");
  }
}
