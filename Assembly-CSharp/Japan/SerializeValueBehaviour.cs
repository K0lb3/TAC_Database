﻿// Decompiled with JetBrains decompiler
// Type: SerializeValueBehaviour
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

public class SerializeValueBehaviour : MonoBehaviour
{
  [SerializeField]
  private SerializeValueList m_List;

  public SerializeValueBehaviour()
  {
    base.\u002Ector();
  }

  public SerializeValueList list
  {
    get
    {
      return this.m_List;
    }
  }

  private void Awake()
  {
    this.m_List.Initialize();
  }
}
