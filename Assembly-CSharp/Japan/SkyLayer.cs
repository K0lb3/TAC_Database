﻿// Decompiled with JetBrains decompiler
// Type: SkyLayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

[RequireComponent(typeof (Renderer))]
[AddComponentMenu("Rendering/Sky Layer")]
public class SkyLayer : MonoBehaviour
{
  public float SkyDistance;
  public Vector3 SkyOffset;
  private bool mVisible;
  private Vector3 mOldPosition;
  private Camera mCamera;

  public SkyLayer()
  {
    base.\u002Ector();
  }

  private void OnEnable()
  {
    CameraHook.AddPreCullEventListener(new CameraHook.PreCullEvent(this.OnCameraPreCull));
  }

  private void OnDisable()
  {
    CameraHook.RemovePreCullEventListener(new CameraHook.PreCullEvent(this.OnCameraPreCull));
  }

  private void OnCameraPreCull(Camera camera)
  {
    this.OnWillRenderObject();
  }

  private void OnWillRenderObject()
  {
    Transform transform1 = ((Component) Camera.get_current()).get_transform();
    Transform transform2 = ((Component) this).get_transform();
    this.mVisible = true;
    this.mOldPosition = transform2.get_position();
    transform2.set_position(Vector3.op_Addition(Vector3.op_Multiply(Vector3.get_forward(), (float) transform1.get_position().z + this.SkyDistance), this.SkyOffset));
  }

  private void OnRenderObject()
  {
    if (!this.mVisible)
      return;
    ((Component) this).get_transform().set_position(this.mOldPosition);
    this.mVisible = false;
  }
}
