﻿// Decompiled with JetBrains decompiler
// Type: UVScroll
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

[RequireComponent(typeof (Renderer))]
[DisallowMultipleComponent]
[AddComponentMenu("Rendering/UVScroll")]
public class UVScroll : MonoBehaviour
{
  public Vector2 Speed;
  private Vector2 mOffset;
  public Vector2 Limit;

  public UVScroll()
  {
    base.\u002Ector();
  }

  private void Update()
  {
    UVScroll uvScroll = this;
    uvScroll.mOffset = Vector2.op_Addition(uvScroll.mOffset, Vector2.op_Multiply(Time.get_deltaTime(), this.Speed));
    if (this.Limit.x != 0.0 && (double) Mathf.Abs((float) this.mOffset.x) >= this.Limit.x)
    {
      ref Vector2 local = ref this.mOffset;
      local.x = local.x % this.Limit.x;
    }
    if (this.Limit.y != 0.0 && (double) Mathf.Abs((float) this.mOffset.y) >= this.Limit.y)
    {
      ref Vector2 local = ref this.mOffset;
      local.y = local.y % this.Limit.y;
    }
    ((Renderer) ((Component) this).GetComponent<Renderer>()).get_material().set_mainTextureOffset(this.mOffset);
  }
}
