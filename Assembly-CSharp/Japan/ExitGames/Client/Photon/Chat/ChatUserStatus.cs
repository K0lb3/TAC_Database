﻿// Decompiled with JetBrains decompiler
// Type: ExitGames.Client.Photon.Chat.ChatUserStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace ExitGames.Client.Photon.Chat
{
  public static class ChatUserStatus
  {
    public const int Offline = 0;
    public const int Invisible = 1;
    public const int Online = 2;
    public const int Away = 3;
    public const int DND = 4;
    public const int LFG = 5;
    public const int Playing = 6;
  }
}
