﻿// Decompiled with JetBrains decompiler
// Type: ExitGames.Client.Photon.Chat.ChatEventCode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace ExitGames.Client.Photon.Chat
{
  public class ChatEventCode
  {
    public const byte ChatMessages = 0;
    public const byte Users = 1;
    public const byte PrivateMessage = 2;
    public const byte FriendsList = 3;
    public const byte StatusUpdate = 4;
    public const byte Subscribe = 5;
    public const byte Unsubscribe = 6;
    public const byte UserSubscribed = 8;
    public const byte UserUnsubscribed = 9;
  }
}
