﻿// Decompiled with JetBrains decompiler
// Type: OIntVector2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;

[MessagePackObject(true)]
public struct OIntVector2
{
  public OInt x;
  public OInt y;

  public OIntVector2(int a, int b)
  {
    this.x = (OInt) a;
    this.y = (OInt) b;
  }

  public override string ToString()
  {
    return string.Format("[OIntVector2] {0}, {1}", (object) this.x, (object) this.y);
  }

  public static bool operator ==(OIntVector2 a, OIntVector2 b)
  {
    return (int) a.x == (int) b.x && (int) a.y == (int) b.y;
  }

  public static bool operator !=(OIntVector2 a, OIntVector2 b)
  {
    return (int) a.x != (int) b.x || (int) a.y != (int) b.y;
  }

  public override bool Equals(object obj)
  {
    return obj is OIntVector2 ointVector2 && ointVector2 == this;
  }

  public override int GetHashCode()
  {
    return base.GetHashCode();
  }
}
