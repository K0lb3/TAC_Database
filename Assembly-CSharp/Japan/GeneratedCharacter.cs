﻿// Decompiled with JetBrains decompiler
// Type: GeneratedCharacter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using SRPG;
using System.Collections.Generic;
using UnityEngine;

public class GeneratedCharacter : MonoBehaviour
{
  private FaceAnimation mFaceAnimation;
  private bool mPlayingFaceAnimation;
  public GeneratedCharacter.OnDestroyCharacter mDestroyCharacter;
  private Color32 VesselColor;
  private Color UnitColor;
  private bool mEnableStaticLight;
  private List<Material> mCharacterMaterials;
  private GameObject mPrimaryEquipment;
  private GameObject mSecondaryEquipment;
  private List<GameObject> PrimaryEquipmentChangeLists;
  private List<GameObject> SecondaryEquipmentChangeLists;
  private GameObject mPrimaryEquipmentDefault;
  private GameObject mSecondaryEquipmentDefault;
  private GameObject mSubPrimaryEquipment;
  private GameObject mSubSecondaryEquipment;
  private GameObject mSubPrimaryEquipmentDefault;
  private GameObject mSubSecondaryEquipmentDefault;

  public GeneratedCharacter()
  {
    base.\u002Ector();
  }

  private void Start()
  {
  }

  private void Update()
  {
  }

  public void SetVesselColor(Color32 color)
  {
    this.VesselColor = color;
  }

  public void EnableStaticLight(bool enable)
  {
    this.mEnableStaticLight = enable;
  }

  private List<Material> CharacterMaterials
  {
    get
    {
      if (this.mCharacterMaterials == null)
        this.FindCharacterMaterials();
      return this.mCharacterMaterials;
    }
  }

  private void DestroyMaterials()
  {
    if (this.mCharacterMaterials == null)
      return;
    using (List<Material>.Enumerator enumerator = this.mCharacterMaterials.GetEnumerator())
    {
      while (enumerator.MoveNext())
        Object.Destroy((Object) enumerator.Current);
    }
    this.mCharacterMaterials = (List<Material>) null;
  }

  public void SetEquip(EquipmentSet Equip)
  {
    CharacterSettings component = (CharacterSettings) ((Component) this).GetComponent<CharacterSettings>();
    EquipmentSet equipmentSet = Equip;
    List<GameObject> primaryHandChangeLists = equipmentSet.PrimaryHandChangeLists;
    GameObject primaryHand;
    if (Object.op_Inequality((Object) equipmentSet, (Object) null) && equipmentSet.PrimaryForceOverride)
    {
      primaryHand = equipmentSet.PrimaryHand;
      primaryHandChangeLists = equipmentSet.PrimaryHandChangeLists;
    }
    else
      primaryHand = equipmentSet.PrimaryHand;
    GameObject secondaryHand1 = equipmentSet.SecondaryHand;
    List<GameObject> secondaryHandChangeLists = equipmentSet.SecondaryHandChangeLists;
    GameObject secondaryHand2;
    if (Object.op_Inequality((Object) equipmentSet, (Object) null) && equipmentSet.PrimaryForceOverride)
    {
      secondaryHand2 = equipmentSet.SecondaryHand;
      secondaryHandChangeLists = equipmentSet.SecondaryHandChangeLists;
    }
    else
      secondaryHand2 = equipmentSet.SecondaryHand;
    if (Object.op_Inequality((Object) primaryHand, (Object) null) && !string.IsNullOrEmpty(component.Rig.RightHand))
    {
      Transform childRecursively = GameUtility.findChildRecursively(((Component) this).get_transform(), component.Rig.RightHand);
      if (Object.op_Inequality((Object) childRecursively, (Object) null))
      {
        this.mPrimaryEquipment = (GameObject) Object.Instantiate<GameObject>((M0) primaryHand, primaryHand.get_transform().get_position(), primaryHand.get_transform().get_rotation());
        this.mPrimaryEquipment.get_transform().SetParent(childRecursively, false);
        this.mPrimaryEquipment.get_transform().set_localScale(Vector3.op_Multiply(Vector3.get_one(), component.Rig.EquipmentScale));
        if (equipmentSet.PrimaryHidden)
          this.SetPrimaryEquipmentsVisible(false);
      }
      else
        Debug.LogError((object) ("Node " + component.Rig.RightHand + " not found."));
    }
    if (primaryHandChangeLists != null && !string.IsNullOrEmpty(component.Rig.RightHand))
    {
      Transform childRecursively = GameUtility.findChildRecursively(((Component) this).get_transform(), component.Rig.RightHand);
      if (Object.op_Implicit((Object) childRecursively))
      {
        this.mPrimaryEquipmentDefault = this.mPrimaryEquipment;
        this.mPrimaryEquipmentChangeLists.Clear();
        using (List<GameObject>.Enumerator enumerator = primaryHandChangeLists.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            GameObject current = enumerator.Current;
            GameObject gameObject = (GameObject) null;
            if (Object.op_Implicit((Object) current))
            {
              gameObject = (GameObject) Object.Instantiate<GameObject>((M0) current, current.get_transform().get_position(), current.get_transform().get_rotation());
              if (Object.op_Implicit((Object) gameObject))
              {
                gameObject.get_transform().SetParent(childRecursively, false);
                gameObject.get_transform().set_localScale(Vector3.op_Multiply(Vector3.get_one(), component.Rig.EquipmentScale));
                gameObject.get_gameObject().SetActive(false);
              }
            }
            this.mPrimaryEquipmentChangeLists.Add(gameObject);
          }
        }
      }
    }
    if (Object.op_Inequality((Object) secondaryHand2, (Object) null) && !string.IsNullOrEmpty(component.Rig.LeftHand))
    {
      Transform childRecursively = GameUtility.findChildRecursively(((Component) this).get_transform(), component.Rig.LeftHand);
      if (Object.op_Inequality((Object) childRecursively, (Object) null))
      {
        this.mSecondaryEquipment = (GameObject) Object.Instantiate<GameObject>((M0) secondaryHand2, secondaryHand2.get_transform().get_position(), secondaryHand2.get_transform().get_rotation());
        this.mSecondaryEquipment.get_transform().SetParent(childRecursively, false);
        this.mSecondaryEquipment.get_transform().set_localScale(Vector3.op_Multiply(Vector3.get_one(), component.Rig.EquipmentScale));
        if (equipmentSet.SecondaryHidden)
          this.SetSecondaryEquipmentsVisible(false);
      }
      else
        Debug.LogError((object) ("Node " + component.Rig.LeftHand + " not found."));
    }
    if (secondaryHandChangeLists != null && !string.IsNullOrEmpty(component.Rig.LeftHand))
    {
      Transform childRecursively = GameUtility.findChildRecursively(((Component) this).get_transform(), component.Rig.LeftHand);
      if (Object.op_Implicit((Object) childRecursively))
      {
        this.mSecondaryEquipmentDefault = this.mSecondaryEquipment;
        this.mSecondaryEquipmentChangeLists.Clear();
        using (List<GameObject>.Enumerator enumerator = secondaryHandChangeLists.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            GameObject current = enumerator.Current;
            GameObject gameObject = (GameObject) null;
            if (Object.op_Implicit((Object) current))
            {
              gameObject = (GameObject) Object.Instantiate<GameObject>((M0) current, current.get_transform().get_position(), current.get_transform().get_rotation());
              if (Object.op_Implicit((Object) gameObject))
              {
                gameObject.get_transform().SetParent(childRecursively, false);
                gameObject.get_transform().set_localScale(Vector3.op_Multiply(Vector3.get_one(), component.Rig.EquipmentScale));
                gameObject.get_gameObject().SetActive(false);
              }
            }
            this.mSecondaryEquipmentChangeLists.Add(gameObject);
          }
        }
      }
    }
    this.ResetEquipmentLists(GeneratedCharacter.EquipmentType.PRIMARY);
    this.ResetEquipmentLists(GeneratedCharacter.EquipmentType.SECONDARY);
  }

  public void ResetEquipmentLists(GeneratedCharacter.EquipmentType type)
  {
    switch (type)
    {
      case GeneratedCharacter.EquipmentType.PRIMARY:
        if (!Object.op_Inequality((Object) this.mPrimaryEquipment, (Object) this.mPrimaryEquipmentDefault))
          break;
        if (Object.op_Implicit((Object) this.mPrimaryEquipment))
          this.mPrimaryEquipment.SetActive(false);
        this.mPrimaryEquipment = this.mPrimaryEquipmentDefault;
        if (!Object.op_Implicit((Object) this.mPrimaryEquipment))
          break;
        this.mPrimaryEquipment.SetActive(true);
        break;
      case GeneratedCharacter.EquipmentType.SECONDARY:
        if (!Object.op_Inequality((Object) this.mSecondaryEquipment, (Object) this.mSecondaryEquipmentDefault))
          break;
        bool flag = !Object.op_Implicit((Object) this.mSecondaryEquipment) || this.mSecondaryEquipment.get_activeSelf();
        if (Object.op_Implicit((Object) this.mSecondaryEquipment))
          this.mSecondaryEquipment.SetActive(false);
        this.mSecondaryEquipment = this.mSecondaryEquipmentDefault;
        if (!Object.op_Implicit((Object) this.mSecondaryEquipment))
          break;
        this.mSecondaryEquipment.SetActive(flag);
        break;
    }
  }

  private void FindCharacterMaterials()
  {
    Renderer[] componentsInChildren = (Renderer[]) ((Component) this).get_gameObject().GetComponentsInChildren<Renderer>(true);
    if (componentsInChildren == null)
      return;
    List<Renderer> rendererList = new List<Renderer>((IEnumerable<Renderer>) componentsInChildren);
    Renderer component = (Renderer) ((Component) this).get_gameObject().GetComponent<Renderer>();
    if (Object.op_Inequality((Object) component, (Object) null))
      rendererList.Add(component);
    for (int index = rendererList.Count - 1; index >= 0; --index)
    {
      Material material = rendererList[index].get_material();
      if (!string.IsNullOrEmpty(material.GetTag("Character", false)) || !string.IsNullOrEmpty(material.GetTag("CharacterSimple", false)))
      {
        if (this.mCharacterMaterials == null)
          this.mCharacterMaterials = new List<Material>();
        this.mCharacterMaterials.Add(material);
      }
    }
  }

  private void OnDestroy()
  {
    if (this.mDestroyCharacter != null)
      this.mDestroyCharacter();
    this.DestroyMaterials();
    using (List<GameObject>.Enumerator enumerator = this.mPrimaryEquipmentChangeLists.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if (Object.op_Implicit((Object) current))
          Object.Destroy((Object) current.get_gameObject());
      }
    }
    this.mPrimaryEquipmentChangeLists.Clear();
    using (List<GameObject>.Enumerator enumerator = this.mSecondaryEquipmentChangeLists.GetEnumerator())
    {
      while (enumerator.MoveNext())
      {
        GameObject current = enumerator.Current;
        if (Object.op_Implicit((Object) current))
          Object.Destroy((Object) current.get_gameObject());
      }
    }
    this.mSecondaryEquipmentChangeLists.Clear();
  }

  public void SetLim(bool enable)
  {
    if (this.CharacterMaterials == null)
      return;
    if (!enable)
    {
      for (int index = 0; index < this.CharacterMaterials.Count; ++index)
        this.CharacterMaterials[index].EnableKeyword("WITHOUT_LIM");
    }
    else
    {
      for (int index = 0; index < this.CharacterMaterials.Count; ++index)
        this.CharacterMaterials[index].DisableKeyword("WITHOUT_LIM");
      Vector3 position = ((Component) this).get_transform().get_position();
      StaticLightVolume volume = StaticLightVolume.FindVolume(position);
      Color directLit;
      Color indirectLit;
      if (Object.op_Equality((Object) volume, (Object) null) || !this.mEnableStaticLight)
      {
        GameSettings instance = GameSettings.Instance;
        directLit = instance.Character_DefaultDirectLitColor;
        indirectLit = instance.Character_DefaultIndirectLitColor;
      }
      else
        volume.CalcLightColor(position, out directLit, out indirectLit);
      for (int index = 0; index < this.CharacterMaterials.Count; ++index)
      {
        this.CharacterMaterials[index].SetColor("_directLitColor", directLit);
        this.CharacterMaterials[index].SetColor("_indirectLitColor", indirectLit);
      }
    }
  }

  private void SetMaterialColor(List<Material> materials, Color color)
  {
    for (int index = 0; index < materials.Count; ++index)
    {
      materials[index].EnableKeyword("USE_FADE_COLOR");
      materials[index].SetColor("_fadeColor", color);
    }
  }

  public void SetVisible(bool visible)
  {
    GameUtility.SetLayer((Component) this, !visible ? GameUtility.LayerHidden : GameUtility.LayerCH, true);
  }

  public Color GetColor()
  {
    return this.UnitColor;
  }

  public void SetColor(Color color)
  {
    if (color.r >= 1.0 && color.g >= 1.0 && color.b >= 1.0)
      color = Color.get_white();
    this.UnitColor = color;
    if (this.CharacterMaterials == null)
      return;
    for (int index = 0; index < this.CharacterMaterials.Count; ++index)
    {
      if (!Object.op_Equality((Object) this.CharacterMaterials[index], (Object) null))
        this.SetMaterialColor(this.CharacterMaterials, color);
    }
  }

  public void SetVesselStrength(float strength, GameObject go, float VesselStrength)
  {
    if ((double) strength > 1.0)
      strength = 1f;
    if ((double) strength < 0.0)
      strength = 0.0f;
    if (this.CharacterMaterials == null)
      return;
    for (int index = 0; index < this.CharacterMaterials.Count; ++index)
    {
      Material characterMaterial = this.CharacterMaterials[index];
      if (!Object.op_Equality((Object) characterMaterial, (Object) null))
      {
        if (!string.IsNullOrEmpty(characterMaterial.GetTag("Bloom", false, (string) null)))
        {
          float num = 0.003921569f;
          characterMaterial.EnableKeyword("MODE_BLOOM");
          characterMaterial.DisableKeyword("MODE_DEFAULT");
          characterMaterial.SetVector("_glowColor", new Vector4((float) this.VesselColor.r * num, (float) this.VesselColor.g * num, (float) this.VesselColor.b * num, Mathf.Pow(strength, 0.7f)));
          characterMaterial.SetFloat("_colorMultipler", (float) (1.0 - (double) strength * 0.400000005960464));
          characterMaterial.SetFloat("_glowStrength", Mathf.Pow(strength, 1.5f) * VesselStrength);
        }
        else
        {
          characterMaterial.EnableKeyword("MODE_DEFAULT");
          characterMaterial.DisableKeyword("MODE_BLOOM");
          characterMaterial.SetFloat("_colorMultipler", 1f);
        }
      }
    }
  }

  private List<GameObject> mPrimaryEquipmentChangeLists
  {
    get
    {
      return this.PrimaryEquipmentChangeLists;
    }
  }

  private List<GameObject> mSecondaryEquipmentChangeLists
  {
    get
    {
      return this.SecondaryEquipmentChangeLists;
    }
  }

  public void SetEquipmentsVisible(bool visible)
  {
    if (Object.op_Inequality((Object) this.mPrimaryEquipment, (Object) null))
      this.mPrimaryEquipment.get_gameObject().SetActive(visible);
    if (!Object.op_Inequality((Object) this.mSecondaryEquipment, (Object) null))
      return;
    this.mSecondaryEquipment.get_gameObject().SetActive(visible);
  }

  public void SetPrimaryEquipmentsVisible(bool visible)
  {
    if (!Object.op_Inequality((Object) this.mPrimaryEquipment, (Object) null))
      return;
    this.mPrimaryEquipment.get_gameObject().SetActive(visible);
  }

  public void SetSecondaryEquipmentsVisible(bool visible)
  {
    if (!Object.op_Inequality((Object) this.mSecondaryEquipment, (Object) null))
      return;
    this.mSecondaryEquipment.get_gameObject().SetActive(visible);
  }

  public void SwitchEquipmentLists(GeneratedCharacter.EquipmentType type, int no)
  {
    if (no <= 0)
      return;
    int index = no - 1;
    switch (type)
    {
      case GeneratedCharacter.EquipmentType.PRIMARY:
        if (index >= this.mPrimaryEquipmentChangeLists.Count)
          break;
        bool flag1 = !Object.op_Implicit((Object) this.mPrimaryEquipment) || this.mPrimaryEquipment.get_activeSelf();
        if (Object.op_Implicit((Object) this.mPrimaryEquipment))
          this.mPrimaryEquipment.SetActive(false);
        this.mPrimaryEquipment = this.mPrimaryEquipmentChangeLists[index];
        if (!Object.op_Implicit((Object) this.mPrimaryEquipment))
          break;
        this.mPrimaryEquipment.SetActive(flag1);
        break;
      case GeneratedCharacter.EquipmentType.SECONDARY:
        if (index >= this.mSecondaryEquipmentChangeLists.Count)
          break;
        bool flag2 = !Object.op_Implicit((Object) this.mSecondaryEquipment) || this.mSecondaryEquipment.get_activeSelf();
        if (Object.op_Implicit((Object) this.mSecondaryEquipment))
          this.mSecondaryEquipment.SetActive(false);
        this.mSecondaryEquipment = this.mSecondaryEquipmentChangeLists[index];
        if (!Object.op_Implicit((Object) this.mSecondaryEquipment))
          break;
        this.mSecondaryEquipment.SetActive(flag2);
        break;
    }
  }

  public delegate void OnDestroyCharacter();

  public enum EquipmentType
  {
    PRIMARY,
    SECONDARY,
  }
}
