﻿// Decompiled with JetBrains decompiler
// Type: AdjustCamera
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AdjustCamera : MonoBehaviour
{
  private static Dictionary<Camera, AdjustCamera> s_ComponentCache = new Dictionary<Camera, AdjustCamera>();
  [SerializeField]
  private bool m_IsIgnoreAutoAdjust;
  private Camera m_TargetCamera;

  public AdjustCamera()
  {
    base.\u002Ector();
  }

  [RuntimeInitializeOnLoadMethod]
  private static void InitializeOnLoad()
  {
    // ISSUE: variable of the null type
    __Null onPreCull = Camera.onPreCull;
    // ISSUE: reference to a compiler-generated field
    if (AdjustCamera.\u003C\u003Ef__mg\u0024cache0 == null)
    {
      // ISSUE: reference to a compiler-generated field
      // ISSUE: method pointer
      AdjustCamera.\u003C\u003Ef__mg\u0024cache0 = new Camera.CameraCallback((object) null, __methodptr(onPreCull));
    }
    // ISSUE: reference to a compiler-generated field
    Camera.CameraCallback fMgCache0 = AdjustCamera.\u003C\u003Ef__mg\u0024cache0;
    Camera.onPreCull = (__Null) Delegate.Combine((Delegate) onPreCull, (Delegate) fMgCache0);
  }

  private void Start()
  {
    this.m_TargetCamera = (Camera) ((Component) this).GetComponent<Camera>();
    if (this.m_IsIgnoreAutoAdjust)
      return;
    AdjustCamera.SetViewportRect(this.m_TargetCamera);
  }

  private static void onPreCull(Camera camera)
  {
    AdjustCamera adjustCamera1 = (AdjustCamera) null;
    if (AdjustCamera.s_ComponentCache.TryGetValue(camera, out adjustCamera1))
    {
      if (!Object.op_Equality((Object) adjustCamera1, (Object) null))
        return;
      AdjustCamera adjustCamera2 = (AdjustCamera) ((Component) camera).get_gameObject().GetComponent<AdjustCamera>();
      if (Object.op_Equality((Object) adjustCamera2, (Object) null))
        adjustCamera2 = (AdjustCamera) ((Component) camera).get_gameObject().AddComponent<AdjustCamera>();
      AdjustCamera.s_ComponentCache[camera] = adjustCamera2;
    }
    else
    {
      AdjustCamera adjustCamera2 = (AdjustCamera) ((Component) camera).GetComponent<AdjustCamera>();
      if (Object.op_Equality((Object) adjustCamera2, (Object) null))
        adjustCamera2 = (AdjustCamera) ((Component) camera).get_gameObject().AddComponent<AdjustCamera>();
      AdjustCamera.s_ComponentCache.Add(camera, adjustCamera2);
      Camera[] array = ((IEnumerable<Camera>) AdjustCamera.s_ComponentCache.Keys).ToArray<Camera>();
      for (int index = 0; index < array.Length; ++index)
      {
        if (Object.op_Equality((Object) array[index], (Object) null))
          AdjustCamera.s_ComponentCache.Remove(array[index]);
      }
    }
  }

  private static void SetViewportRect(Camera camera)
  {
    if (Object.op_Equality((Object) camera, (Object) null))
      return;
    camera.set_rect(SetCanvasBounds.GetCameraViewport());
  }
}
