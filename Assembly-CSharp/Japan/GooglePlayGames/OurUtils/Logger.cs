﻿// Decompiled with JetBrains decompiler
// Type: GooglePlayGames.OurUtils.Logger
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using UnityEngine;

namespace GooglePlayGames.OurUtils
{
  public class Logger
  {
    private static bool warningLogEnabled = true;
    private static bool debugLogEnabled;

    public static bool DebugLogEnabled
    {
      get
      {
        return Logger.debugLogEnabled;
      }
      set
      {
        Logger.debugLogEnabled = value;
      }
    }

    public static bool WarningLogEnabled
    {
      get
      {
        return Logger.warningLogEnabled;
      }
      set
      {
        Logger.warningLogEnabled = value;
      }
    }

    public static void d(string msg)
    {
      if (!Logger.debugLogEnabled)
        return;
      PlayGamesHelperObject.RunOnGameThread((Action) (() => Debug.Log((object) Logger.ToLogMessage(string.Empty, "DEBUG", msg))));
    }

    public static void w(string msg)
    {
      if (!Logger.warningLogEnabled)
        return;
      PlayGamesHelperObject.RunOnGameThread((Action) (() => Debug.LogWarning((object) Logger.ToLogMessage("!!!", "WARNING", msg))));
    }

    public static void e(string msg)
    {
      if (!Logger.warningLogEnabled)
        return;
      PlayGamesHelperObject.RunOnGameThread((Action) (() => Debug.LogWarning((object) Logger.ToLogMessage("***", "ERROR", msg))));
    }

    public static string describe(byte[] b)
    {
      return b == null ? "(null)" : "byte[" + (object) b.Length + "]";
    }

    private static string ToLogMessage(string prefix, string logType, string msg)
    {
      string empty;
      try
      {
        empty = DateTime.Now.ToString("MM/dd/yy H:mm:ss zzz");
      }
      catch (Exception ex)
      {
        PlayGamesHelperObject.RunOnGameThread((Action) (() => Debug.LogWarning((object) "*** [Play Games Plugin DLL] ERROR: Failed to format DateTime.Now")));
        empty = string.Empty;
      }
      return string.Format("{0} [Play Games Plugin DLL] {1} {2}: {3}", (object) prefix, (object) empty, (object) logType, (object) msg);
    }
  }
}
