﻿// Decompiled with JetBrains decompiler
// Type: OnJoinedInstantiate
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

public class OnJoinedInstantiate : MonoBehaviour
{
  public Transform SpawnPosition;
  public float PositionOffset;
  public GameObject[] PrefabsToInstantiate;

  public OnJoinedInstantiate()
  {
    base.\u002Ector();
  }

  public void OnJoinedRoom()
  {
    if (this.PrefabsToInstantiate == null)
      return;
    foreach (GameObject gameObject in this.PrefabsToInstantiate)
    {
      Debug.Log((object) ("Instantiating: " + ((Object) gameObject).get_name()));
      Vector3 vector3_1 = Vector3.get_up();
      if (Object.op_Inequality((Object) this.SpawnPosition, (Object) null))
        vector3_1 = this.SpawnPosition.get_position();
      Vector3 vector3_2 = Random.get_insideUnitSphere();
      vector3_2.y = (__Null) 0.0;
      vector3_2 = ((Vector3) ref vector3_2).get_normalized();
      Vector3 position = Vector3.op_Addition(vector3_1, Vector3.op_Multiply(this.PositionOffset, vector3_2));
      PhotonNetwork.Instantiate(((Object) gameObject).get_name(), position, Quaternion.get_identity(), (byte) 0);
    }
  }
}
