﻿// Decompiled with JetBrains decompiler
// Type: NativePlugin
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using System.Runtime.InteropServices;

public static class NativePlugin
{
  [DllImport("NativePlugin", CharSet = CharSet.Ansi)]
  public static extern IntPtr DecompressFile(string path, out int size);

  [DllImport("NativePlugin", CharSet = CharSet.Ansi)]
  public static extern void FreePtr(IntPtr ptr);
}
