﻿// Decompiled with JetBrains decompiler
// Type: AdjustRectScale
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

public class AdjustRectScale : MonoBehaviour
{
  [SerializeField]
  private float SetScale;
  private Rect lastSafeArea;
  private Vector3 initScale;
  private float lastSetScale;

  public AdjustRectScale()
  {
    base.\u002Ector();
  }

  private void Start()
  {
    RectTransform component = (RectTransform) ((Component) this).GetComponent<RectTransform>();
    if (!Object.op_Inequality((Object) component, (Object) null))
      return;
    this.initScale = ((Component) component).get_transform().get_localScale();
    this.ApplySafeAreaScale(SetCanvasBounds.GetSafeArea(true));
  }

  private void ApplySafeAreaScale(Rect area)
  {
    RectTransform component = (RectTransform) ((Component) this).GetComponent<RectTransform>();
    if (Object.op_Inequality((Object) component, (Object) null))
    {
      if ((double) (((Rect) ref area).get_width() / (float) Screen.get_width()) < 1.0)
        ((Transform) component).set_localScale(new Vector3(this.SetScale, this.SetScale, this.SetScale));
      else
        ((Transform) component).set_localScale(this.initScale);
    }
    this.lastSafeArea = area;
    this.lastSetScale = this.SetScale;
  }
}
