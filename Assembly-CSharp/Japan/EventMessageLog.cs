﻿// Decompiled with JetBrains decompiler
// Type: EventMessageLog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using SRPG;
using UnityEngine;

public class EventMessageLog : MonoBehaviour
{
  public EventMessageLog()
  {
    base.\u002Ector();
  }

  private void Awake()
  {
    RectTransform component = (RectTransform) ((Component) this).GetComponent<RectTransform>();
    Rect safeArea = SetCanvasBounds.GetSafeArea(false);
    if ((double) (((Rect) ref safeArea).get_width() / (float) Screen.get_width()) >= 1.0)
      return;
    float num = (float) Screen.get_height() - ((Rect) ref safeArea).get_height();
    component.set_anchoredPosition(new Vector2((float) component.get_anchoredPosition().x, (float) component.get_anchoredPosition().y + num));
  }

  public void OnClick()
  {
    if (EventScript.OnBackLogButtonClicked != null)
      EventScript.OnBackLogButtonClicked();
    EventScript.OpenBackLog();
  }

  private void OnDisable()
  {
    EventScript.CloseBackLog();
  }
}
