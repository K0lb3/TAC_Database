﻿// Decompiled with JetBrains decompiler
// Type: ScoreExtensions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using ExitGames.Client.Photon;
using System.Collections.Generic;

public static class ScoreExtensions
{
  public static void SetScore(this PhotonPlayer player, int newScore)
  {
    Hashtable propertiesToSet = new Hashtable();
    propertiesToSet.set_Item((object) "score", (object) newScore);
    player.SetCustomProperties(propertiesToSet, (Hashtable) null, false);
  }

  public static void AddScore(this PhotonPlayer player, int scoreToAddToCurrent)
  {
    int num = player.GetScore() + scoreToAddToCurrent;
    Hashtable propertiesToSet = new Hashtable();
    propertiesToSet.set_Item((object) "score", (object) num);
    player.SetCustomProperties(propertiesToSet, (Hashtable) null, false);
  }

  public static int GetScore(this PhotonPlayer player)
  {
    object obj;
    return ((Dictionary<object, object>) player.CustomProperties).TryGetValue((object) "score", out obj) ? (int) obj : 0;
  }
}
