﻿// Decompiled with JetBrains decompiler
// Type: Slack.PostMessageData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace Slack
{
  [Serializable]
  public class PostMessageData
  {
    public string token = string.Empty;
    public string channel = string.Empty;
    public string text = string.Empty;
    public string parse = string.Empty;
    public string link_names = string.Empty;
    public string username = string.Empty;
    public string icon_url = string.Empty;
    public string icon_emoji = string.Empty;
  }
}
