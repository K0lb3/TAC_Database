﻿// Decompiled with JetBrains decompiler
// Type: Slack.UploadData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace Slack
{
  [Serializable]
  public class UploadData
  {
    public string token = string.Empty;
    public string filename = string.Empty;
    public string title = string.Empty;
    public string initial_comment = string.Empty;
    public string channels = string.Empty;
  }
}
