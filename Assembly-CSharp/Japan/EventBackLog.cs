﻿// Decompiled with JetBrains decompiler
// Type: EventBackLog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using SRPG;
using System.Collections.Generic;
using UnityEngine;

public class EventBackLog : MonoBehaviour
{
  private List<EventBackLogContent> mBackLogItems;
  [SerializeField]
  private Transform mBackLogItemParent;
  [SerializeField]
  private EventBackLogContent mBackLogItemPrefab;
  [SerializeField]
  private SRPG_ScrollRect mScrollRect;
  private bool mIsUpdate;

  public EventBackLog()
  {
    base.\u002Ector();
  }

  public void Add(string name, string text)
  {
    EventBackLogContent eventBackLogContent = (EventBackLogContent) Object.Instantiate<EventBackLogContent>((M0) this.mBackLogItemPrefab, this.mBackLogItemParent);
    eventBackLogContent.SetBackLogText(name, text);
    ((Component) eventBackLogContent).get_gameObject().SetActive(true);
    this.mBackLogItems.Add(eventBackLogContent);
    this.mIsUpdate = true;
  }

  public bool CanOpen { get; set; }

  public int Count
  {
    get
    {
      return this.mBackLogItems.Count;
    }
  }

  private void LateUpdate()
  {
    if (!this.mIsUpdate)
      return;
    this.mScrollRect.set_verticalNormalizedPosition(0.0f);
    this.mIsUpdate = false;
  }

  public void Open()
  {
    if (!this.CanOpen || this.mBackLogItems == null || this.mBackLogItems.Count <= 0)
      return;
    EventScript.IsMessageAuto = false;
    if (Object.op_Equality((Object) EventScript.BackLogCanvas, (Object) null))
      return;
    ((Component) EventScript.BackLogCanvas).get_gameObject().SetActive(true);
    ((Component) this).get_gameObject().SetActive(true);
    this.mScrollRect.set_verticalNormalizedPosition(0.0f);
  }

  public void Close()
  {
    if (Object.op_Inequality((Object) EventScript.BackLogCanvas, (Object) null))
      ((Component) EventScript.BackLogCanvas).get_gameObject().SetActive(false);
    ((Component) this).get_gameObject().SetActive(false);
  }

  public void Clear()
  {
    for (int index = this.mBackLogItems.Count - 1; index >= 0; --index)
    {
      Object.Destroy((Object) ((Component) this.mBackLogItems[index]).get_gameObject());
      this.mBackLogItems.Remove(this.mBackLogItems[index]);
    }
  }

  private void Start()
  {
  }

  private void Update()
  {
  }

  private void OnDestroy()
  {
  }
}
