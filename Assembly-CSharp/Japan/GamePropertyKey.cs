﻿// Decompiled with JetBrains decompiler
// Type: GamePropertyKey
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

public class GamePropertyKey
{
  public const byte MaxPlayers = 255;
  public const byte IsVisible = 254;
  public const byte IsOpen = 253;
  public const byte PlayerCount = 252;
  public const byte Removed = 251;
  public const byte PropsListedInLobby = 250;
  public const byte CleanupCacheOnLeave = 249;
  public const byte MasterClientId = 248;
  public const byte ExpectedUsers = 247;
  public const byte PlayerTtl = 246;
  public const byte EmptyRoomTtl = 245;
}
