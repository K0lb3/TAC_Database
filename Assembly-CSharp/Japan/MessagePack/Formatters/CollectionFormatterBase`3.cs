﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.CollectionFormatterBase`3
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace MessagePack.Formatters
{
  public abstract class CollectionFormatterBase<TElement, TIntermediate, TCollection> : CollectionFormatterBase<TElement, TIntermediate, IEnumerator<TElement>, TCollection>
    where TCollection : IEnumerable<TElement>
  {
    protected override IEnumerator<TElement> GetSourceEnumerator(TCollection source)
    {
      return source.GetEnumerator();
    }
  }
}
