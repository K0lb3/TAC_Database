﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.IgnoreFormatter`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Formatters
{
  public sealed class IgnoreFormatter<T> : IMessagePackFormatter<T>, IMessagePackFormatter
  {
    public int Serialize(
      ref byte[] bytes,
      int offset,
      T value,
      IFormatterResolver formatterResolver)
    {
      return MessagePackBinary.WriteNil(ref bytes, offset);
    }

    public T Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      readSize = MessagePackBinary.ReadNextBlock(bytes, offset);
      return default (T);
    }
  }
}
