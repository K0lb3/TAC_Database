﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.Int16Formatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Formatters
{
  public sealed class Int16Formatter : IMessagePackFormatter<short>, IMessagePackFormatter
  {
    public static readonly Int16Formatter Instance = new Int16Formatter();

    private Int16Formatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      short value,
      IFormatterResolver formatterResolver)
    {
      return MessagePackBinary.WriteInt16(ref bytes, offset, value);
    }

    public short Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      return MessagePackBinary.ReadInt16(bytes, offset, out readSize);
    }
  }
}
