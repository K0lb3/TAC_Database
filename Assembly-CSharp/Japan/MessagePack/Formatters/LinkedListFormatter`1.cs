﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.LinkedListFormatter`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace MessagePack.Formatters
{
  public sealed class LinkedListFormatter<T> : CollectionFormatterBase<T, LinkedList<T>, LinkedList<T>.Enumerator, LinkedList<T>>
  {
    protected override void Add(LinkedList<T> collection, int index, T value)
    {
      collection.AddLast(value);
    }

    protected override LinkedList<T> Complete(LinkedList<T> intermediateCollection)
    {
      return intermediateCollection;
    }

    protected override LinkedList<T> Create(int count)
    {
      return new LinkedList<T>();
    }

    protected override LinkedList<T>.Enumerator GetSourceEnumerator(LinkedList<T> source)
    {
      return source.GetEnumerator();
    }
  }
}
