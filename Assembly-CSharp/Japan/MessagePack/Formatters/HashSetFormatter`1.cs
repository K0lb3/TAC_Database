﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.HashSetFormatter`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace MessagePack.Formatters
{
  public sealed class HashSetFormatter<T> : CollectionFormatterBase<T, HashSet<T>, HashSet<T>.Enumerator, HashSet<T>>
  {
    protected override int? GetCount(HashSet<T> sequence)
    {
      return new int?(sequence.Count);
    }

    protected override void Add(HashSet<T> collection, int index, T value)
    {
      collection.Add(value);
    }

    protected override HashSet<T> Complete(HashSet<T> intermediateCollection)
    {
      return intermediateCollection;
    }

    protected override HashSet<T> Create(int count)
    {
      return new HashSet<T>();
    }

    protected override HashSet<T>.Enumerator GetSourceEnumerator(HashSet<T> source)
    {
      return source.GetEnumerator();
    }
  }
}
