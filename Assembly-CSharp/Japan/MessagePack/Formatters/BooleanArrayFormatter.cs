﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.BooleanArrayFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Formatters
{
  public sealed class BooleanArrayFormatter : IMessagePackFormatter<bool[]>, IMessagePackFormatter
  {
    public static readonly BooleanArrayFormatter Instance = new BooleanArrayFormatter();

    private BooleanArrayFormatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      bool[] value,
      IFormatterResolver formatterResolver)
    {
      if (value == null)
        return MessagePackBinary.WriteNil(ref bytes, offset);
      int num = offset;
      offset += MessagePackBinary.WriteArrayHeader(ref bytes, offset, value.Length);
      for (int index = 0; index < value.Length; ++index)
        offset += MessagePackBinary.WriteBoolean(ref bytes, offset, value[index]);
      return offset - num;
    }

    public bool[] Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      if (MessagePackBinary.IsNil(bytes, offset))
      {
        readSize = 1;
        return (bool[]) null;
      }
      int num = offset;
      int length = MessagePackBinary.ReadArrayHeader(bytes, offset, out readSize);
      offset += readSize;
      bool[] flagArray = new bool[length];
      for (int index = 0; index < flagArray.Length; ++index)
      {
        flagArray[index] = MessagePackBinary.ReadBoolean(bytes, offset, out readSize);
        offset += readSize;
      }
      readSize = offset - num;
      return flagArray;
    }
  }
}
