﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.SRPG.ReqGvG_ResponseFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack.Internal;
using SRPG;
using System;

namespace MessagePack.Formatters.SRPG
{
  public sealed class ReqGvG_ResponseFormatter : IMessagePackFormatter<ReqGvG.Response>, IMessagePackFormatter
  {
    private readonly AutomataDictionary ____keyMapping;
    private readonly byte[][] ____stringByteKeys;

    public ReqGvG_ResponseFormatter()
    {
      this.____keyMapping = new AutomataDictionary()
      {
        {
          "nodes",
          0
        },
        {
          "matching_order",
          1
        },
        {
          "guilds",
          2
        },
        {
          "my_guild",
          3
        },
        {
          "used_units",
          4
        },
        {
          "declare_num",
          5
        },
        {
          "refresh_wait_sec",
          6
        },
        {
          "auto_refresh_wait_sec",
          7
        },
        {
          "result_daily",
          8
        },
        {
          "result",
          9
        }
      };
      this.____stringByteKeys = new byte[10][]
      {
        MessagePackBinary.GetEncodedStringBytes("nodes"),
        MessagePackBinary.GetEncodedStringBytes("matching_order"),
        MessagePackBinary.GetEncodedStringBytes("guilds"),
        MessagePackBinary.GetEncodedStringBytes("my_guild"),
        MessagePackBinary.GetEncodedStringBytes("used_units"),
        MessagePackBinary.GetEncodedStringBytes("declare_num"),
        MessagePackBinary.GetEncodedStringBytes("refresh_wait_sec"),
        MessagePackBinary.GetEncodedStringBytes("auto_refresh_wait_sec"),
        MessagePackBinary.GetEncodedStringBytes("result_daily"),
        MessagePackBinary.GetEncodedStringBytes("result")
      };
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      ReqGvG.Response value,
      IFormatterResolver formatterResolver)
    {
      if (value == null)
        return MessagePackBinary.WriteNil(ref bytes, offset);
      int num = offset;
      offset += MessagePackBinary.WriteFixedMapHeaderUnsafe(ref bytes, offset, 10);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[0]);
      offset += formatterResolver.GetFormatterWithVerify<JSON_GvGNodeData[]>().Serialize(ref bytes, offset, value.nodes, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[1]);
      offset += formatterResolver.GetFormatterWithVerify<int[]>().Serialize(ref bytes, offset, value.matching_order, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[2]);
      offset += formatterResolver.GetFormatterWithVerify<JSON_ViewGuild[]>().Serialize(ref bytes, offset, value.guilds, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[3]);
      offset += formatterResolver.GetFormatterWithVerify<JSON_ViewGuild>().Serialize(ref bytes, offset, value.my_guild, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[4]);
      offset += formatterResolver.GetFormatterWithVerify<long[]>().Serialize(ref bytes, offset, value.used_units, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[5]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.declare_num);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[6]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.refresh_wait_sec);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[7]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.auto_refresh_wait_sec);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[8]);
      offset += formatterResolver.GetFormatterWithVerify<JSON_GvGResult>().Serialize(ref bytes, offset, value.result_daily, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[9]);
      offset += formatterResolver.GetFormatterWithVerify<JSON_GvGResult>().Serialize(ref bytes, offset, value.result, formatterResolver);
      return offset - num;
    }

    public ReqGvG.Response Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      if (MessagePackBinary.IsNil(bytes, offset))
      {
        readSize = 1;
        return (ReqGvG.Response) null;
      }
      int num1 = offset;
      int num2 = MessagePackBinary.ReadMapHeader(bytes, offset, out readSize);
      offset += readSize;
      JSON_GvGNodeData[] jsonGvGnodeDataArray = (JSON_GvGNodeData[]) null;
      int[] numArray1 = (int[]) null;
      JSON_ViewGuild[] jsonViewGuildArray = (JSON_ViewGuild[]) null;
      JSON_ViewGuild jsonViewGuild = (JSON_ViewGuild) null;
      long[] numArray2 = (long[]) null;
      int num3 = 0;
      int num4 = 0;
      int num5 = 0;
      JSON_GvGResult jsonGvGresult1 = (JSON_GvGResult) null;
      JSON_GvGResult jsonGvGresult2 = (JSON_GvGResult) null;
      for (int index = 0; index < num2; ++index)
      {
        ArraySegment<byte> key = MessagePackBinary.ReadStringSegment(bytes, offset, out readSize);
        offset += readSize;
        int num6;
        if (!this.____keyMapping.TryGetValueSafe(key, out num6))
        {
          readSize = MessagePackBinary.ReadNextBlock(bytes, offset);
        }
        else
        {
          switch (num6)
          {
            case 0:
              jsonGvGnodeDataArray = formatterResolver.GetFormatterWithVerify<JSON_GvGNodeData[]>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 1:
              numArray1 = formatterResolver.GetFormatterWithVerify<int[]>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 2:
              jsonViewGuildArray = formatterResolver.GetFormatterWithVerify<JSON_ViewGuild[]>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 3:
              jsonViewGuild = formatterResolver.GetFormatterWithVerify<JSON_ViewGuild>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 4:
              numArray2 = formatterResolver.GetFormatterWithVerify<long[]>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 5:
              num3 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 6:
              num4 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 7:
              num5 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 8:
              jsonGvGresult1 = formatterResolver.GetFormatterWithVerify<JSON_GvGResult>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 9:
              jsonGvGresult2 = formatterResolver.GetFormatterWithVerify<JSON_GvGResult>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            default:
              readSize = MessagePackBinary.ReadNextBlock(bytes, offset);
              break;
          }
        }
        offset += readSize;
      }
      readSize = offset - num1;
      return new ReqGvG.Response()
      {
        nodes = jsonGvGnodeDataArray,
        matching_order = numArray1,
        guilds = jsonViewGuildArray,
        my_guild = jsonViewGuild,
        used_units = numArray2,
        declare_num = num3,
        refresh_wait_sec = num4,
        auto_refresh_wait_sec = num5,
        result_daily = jsonGvGresult1,
        result = jsonGvGresult2
      };
    }
  }
}
