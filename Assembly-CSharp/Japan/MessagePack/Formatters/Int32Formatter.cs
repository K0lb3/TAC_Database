﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.Int32Formatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Formatters
{
  public sealed class Int32Formatter : IMessagePackFormatter<int>, IMessagePackFormatter
  {
    public static readonly Int32Formatter Instance = new Int32Formatter();

    private Int32Formatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      int value,
      IFormatterResolver formatterResolver)
    {
      return MessagePackBinary.WriteInt32(ref bytes, offset, value);
    }

    public int Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      return MessagePackBinary.ReadInt32(bytes, offset, out readSize);
    }
  }
}
