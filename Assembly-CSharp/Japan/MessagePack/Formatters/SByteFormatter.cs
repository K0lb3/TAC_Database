﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.SByteFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Formatters
{
  public sealed class SByteFormatter : IMessagePackFormatter<sbyte>, IMessagePackFormatter
  {
    public static readonly SByteFormatter Instance = new SByteFormatter();

    private SByteFormatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      sbyte value,
      IFormatterResolver formatterResolver)
    {
      return MessagePackBinary.WriteSByte(ref bytes, offset, value);
    }

    public sbyte Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      return MessagePackBinary.ReadSByte(bytes, offset, out readSize);
    }
  }
}
