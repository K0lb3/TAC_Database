﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.UInt64Formatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Formatters
{
  public sealed class UInt64Formatter : IMessagePackFormatter<ulong>, IMessagePackFormatter
  {
    public static readonly UInt64Formatter Instance = new UInt64Formatter();

    private UInt64Formatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      ulong value,
      IFormatterResolver formatterResolver)
    {
      return MessagePackBinary.WriteUInt64(ref bytes, offset, value);
    }

    public ulong Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      return MessagePackBinary.ReadUInt64(bytes, offset, out readSize);
    }
  }
}
