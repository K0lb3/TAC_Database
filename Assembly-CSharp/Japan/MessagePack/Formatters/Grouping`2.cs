﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.Grouping`2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace MessagePack.Formatters
{
  internal class Grouping<TKey, TElement> : IGrouping<TKey, TElement>, IEnumerable, IEnumerable<TElement>
  {
    private readonly TKey key;
    private readonly IEnumerable<TElement> elements;

    public Grouping(TKey key, IEnumerable<TElement> elements)
    {
      this.key = key;
      this.elements = elements;
    }

    public TKey Key
    {
      get
      {
        return this.key;
      }
    }

    public IEnumerator<TElement> GetEnumerator()
    {
      return this.elements.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return (IEnumerator) this.elements.GetEnumerator();
    }
  }
}
