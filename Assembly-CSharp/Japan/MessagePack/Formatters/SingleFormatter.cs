﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.SingleFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Formatters
{
  public sealed class SingleFormatter : IMessagePackFormatter<float>, IMessagePackFormatter
  {
    public static readonly SingleFormatter Instance = new SingleFormatter();

    private SingleFormatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      float value,
      IFormatterResolver formatterResolver)
    {
      return MessagePackBinary.WriteSingle(ref bytes, offset, value);
    }

    public float Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      return MessagePackBinary.ReadSingle(bytes, offset, out readSize);
    }
  }
}
