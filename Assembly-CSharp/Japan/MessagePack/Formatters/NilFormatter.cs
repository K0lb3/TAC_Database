﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.NilFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Formatters
{
  public class NilFormatter : IMessagePackFormatter<Nil>, IMessagePackFormatter
  {
    public static readonly IMessagePackFormatter<Nil> Instance = (IMessagePackFormatter<Nil>) new NilFormatter();

    private NilFormatter()
    {
    }

    public int Serialize(ref byte[] bytes, int offset, Nil value, IFormatterResolver typeResolver)
    {
      return MessagePackBinary.WriteNil(ref bytes, offset);
    }

    public Nil Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver typeResolver,
      out int readSize)
    {
      return MessagePackBinary.ReadNil(bytes, offset, out readSize);
    }
  }
}
