﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.NonGenericListFormatter`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using System.Collections;

namespace MessagePack.Formatters
{
  public sealed class NonGenericListFormatter<T> : IMessagePackFormatter<T>, IMessagePackFormatter
    where T : class, IList, new()
  {
    public int Serialize(
      ref byte[] bytes,
      int offset,
      T value,
      IFormatterResolver formatterResolver)
    {
      if ((object) value == null)
      {
        MessagePackBinary.WriteNil(ref bytes, offset);
        return 1;
      }
      IMessagePackFormatter<object> formatterWithVerify = formatterResolver.GetFormatterWithVerify<object>();
      int num = offset;
      offset += MessagePackBinary.WriteArrayHeader(ref bytes, offset, value.Count);
      IEnumerator enumerator = value.GetEnumerator();
      try
      {
        while (enumerator.MoveNext())
        {
          object current = enumerator.Current;
          offset += formatterWithVerify.Serialize(ref bytes, offset, current, formatterResolver);
        }
      }
      finally
      {
        if (enumerator is IDisposable disposable)
          disposable.Dispose();
      }
      return offset - num;
    }

    public T Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      if (MessagePackBinary.IsNil(bytes, offset))
      {
        readSize = 1;
        return (T) null;
      }
      IMessagePackFormatter<object> formatterWithVerify = formatterResolver.GetFormatterWithVerify<object>();
      int num1 = offset;
      int num2 = MessagePackBinary.ReadArrayHeader(bytes, offset, out readSize);
      offset += readSize;
      T obj = new T();
      for (int index = 0; index < num2; ++index)
      {
        obj.Add(formatterWithVerify.Deserialize(bytes, offset, formatterResolver, out readSize));
        offset += readSize;
      }
      readSize = offset - num1;
      return obj;
    }
  }
}
