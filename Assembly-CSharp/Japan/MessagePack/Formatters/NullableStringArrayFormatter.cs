﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.NullableStringArrayFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Formatters
{
  public sealed class NullableStringArrayFormatter : IMessagePackFormatter<string[]>, IMessagePackFormatter
  {
    public static readonly NullableStringArrayFormatter Instance = new NullableStringArrayFormatter();

    private NullableStringArrayFormatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      string[] value,
      IFormatterResolver typeResolver)
    {
      if (value == null)
        return MessagePackBinary.WriteNil(ref bytes, offset);
      int num = offset;
      offset += MessagePackBinary.WriteArrayHeader(ref bytes, offset, value.Length);
      for (int index = 0; index < value.Length; ++index)
        offset += MessagePackBinary.WriteString(ref bytes, offset, value[index]);
      return offset - num;
    }

    public string[] Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver typeResolver,
      out int readSize)
    {
      if (MessagePackBinary.IsNil(bytes, offset))
      {
        readSize = 1;
        return (string[]) null;
      }
      int num = offset;
      int length = MessagePackBinary.ReadArrayHeader(bytes, offset, out readSize);
      offset += readSize;
      string[] strArray = new string[length];
      for (int index = 0; index < strArray.Length; ++index)
      {
        strArray[index] = MessagePackBinary.ReadString(bytes, offset, out readSize);
        offset += readSize;
      }
      readSize = offset - num;
      return strArray;
    }
  }
}
