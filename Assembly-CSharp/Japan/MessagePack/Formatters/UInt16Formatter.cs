﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.UInt16Formatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Formatters
{
  public sealed class UInt16Formatter : IMessagePackFormatter<ushort>, IMessagePackFormatter
  {
    public static readonly UInt16Formatter Instance = new UInt16Formatter();

    private UInt16Formatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      ushort value,
      IFormatterResolver formatterResolver)
    {
      return MessagePackBinary.WriteUInt16(ref bytes, offset, value);
    }

    public ushort Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      return MessagePackBinary.ReadUInt16(bytes, offset, out readSize);
    }
  }
}
