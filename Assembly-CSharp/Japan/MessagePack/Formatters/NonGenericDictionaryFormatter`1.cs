﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.NonGenericDictionaryFormatter`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using System.Collections;

namespace MessagePack.Formatters
{
  public sealed class NonGenericDictionaryFormatter<T> : IMessagePackFormatter<T>, IMessagePackFormatter
    where T : class, IDictionary, new()
  {
    public int Serialize(
      ref byte[] bytes,
      int offset,
      T value,
      IFormatterResolver formatterResolver)
    {
      if ((object) value == null)
      {
        MessagePackBinary.WriteNil(ref bytes, offset);
        return 1;
      }
      IMessagePackFormatter<object> formatterWithVerify = formatterResolver.GetFormatterWithVerify<object>();
      int num = offset;
      offset += MessagePackBinary.WriteMapHeader(ref bytes, offset, value.Count);
      IDictionaryEnumerator enumerator = value.GetEnumerator();
      try
      {
        while (enumerator.MoveNext())
        {
          DictionaryEntry current = (DictionaryEntry) enumerator.Current;
          offset += formatterWithVerify.Serialize(ref bytes, offset, current.Key, formatterResolver);
          offset += formatterWithVerify.Serialize(ref bytes, offset, current.Value, formatterResolver);
        }
      }
      finally
      {
        if (enumerator is IDisposable disposable)
          disposable.Dispose();
      }
      return offset - num;
    }

    public T Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      if (MessagePackBinary.IsNil(bytes, offset))
      {
        readSize = 1;
        return (T) null;
      }
      IMessagePackFormatter<object> formatterWithVerify = formatterResolver.GetFormatterWithVerify<object>();
      int num1 = offset;
      int num2 = MessagePackBinary.ReadMapHeader(bytes, offset, out readSize);
      offset += readSize;
      T obj1 = new T();
      for (int index = 0; index < num2; ++index)
      {
        object key = formatterWithVerify.Deserialize(bytes, offset, formatterResolver, out readSize);
        offset += readSize;
        object obj2 = formatterWithVerify.Deserialize(bytes, offset, formatterResolver, out readSize);
        offset += readSize;
        obj1.Add(key, obj2);
      }
      readSize = offset - num1;
      return obj1;
    }
  }
}
