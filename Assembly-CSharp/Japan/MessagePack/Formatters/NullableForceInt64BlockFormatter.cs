﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.NullableForceInt64BlockFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Formatters
{
  public sealed class NullableForceInt64BlockFormatter : IMessagePackFormatter<long?>, IMessagePackFormatter
  {
    public static readonly NullableForceInt64BlockFormatter Instance = new NullableForceInt64BlockFormatter();

    private NullableForceInt64BlockFormatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      long? value,
      IFormatterResolver formatterResolver)
    {
      return !value.HasValue ? MessagePackBinary.WriteNil(ref bytes, offset) : MessagePackBinary.WriteInt64ForceInt64Block(ref bytes, offset, value.Value);
    }

    public long? Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      if (!MessagePackBinary.IsNil(bytes, offset))
        return new long?(MessagePackBinary.ReadInt64(bytes, offset, out readSize));
      readSize = 1;
      return new long?();
    }
  }
}
