﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.DateTimeFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace MessagePack.Formatters
{
  public sealed class DateTimeFormatter : IMessagePackFormatter<DateTime>, IMessagePackFormatter
  {
    public static readonly DateTimeFormatter Instance = new DateTimeFormatter();

    private DateTimeFormatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      DateTime value,
      IFormatterResolver formatterResolver)
    {
      return MessagePackBinary.WriteDateTime(ref bytes, offset, value);
    }

    public DateTime Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      return MessagePackBinary.ReadDateTime(bytes, offset, out readSize);
    }
  }
}
