﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.ForceUInt64BlockFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Formatters
{
  public sealed class ForceUInt64BlockFormatter : IMessagePackFormatter<ulong>, IMessagePackFormatter
  {
    public static readonly ForceUInt64BlockFormatter Instance = new ForceUInt64BlockFormatter();

    private ForceUInt64BlockFormatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      ulong value,
      IFormatterResolver formatterResolver)
    {
      return MessagePackBinary.WriteUInt64ForceUInt64Block(ref bytes, offset, value);
    }

    public ulong Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      return MessagePackBinary.ReadUInt64(bytes, offset, out readSize);
    }
  }
}
