﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.NullableUInt16Formatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Formatters
{
  public sealed class NullableUInt16Formatter : IMessagePackFormatter<ushort?>, IMessagePackFormatter
  {
    public static readonly NullableUInt16Formatter Instance = new NullableUInt16Formatter();

    private NullableUInt16Formatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      ushort? value,
      IFormatterResolver formatterResolver)
    {
      return !value.HasValue ? MessagePackBinary.WriteNil(ref bytes, offset) : MessagePackBinary.WriteUInt16(ref bytes, offset, value.Value);
    }

    public ushort? Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      if (!MessagePackBinary.IsNil(bytes, offset))
        return new ushort?(MessagePackBinary.ReadUInt16(bytes, offset, out readSize));
      readSize = 1;
      return new ushort?();
    }
  }
}
