﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.NullableForceByteBlockFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Formatters
{
  public sealed class NullableForceByteBlockFormatter : IMessagePackFormatter<byte?>, IMessagePackFormatter
  {
    public static readonly NullableForceByteBlockFormatter Instance = new NullableForceByteBlockFormatter();

    private NullableForceByteBlockFormatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      byte? value,
      IFormatterResolver formatterResolver)
    {
      return !value.HasValue ? MessagePackBinary.WriteNil(ref bytes, offset) : MessagePackBinary.WriteByteForceByteBlock(ref bytes, offset, value.Value);
    }

    public byte? Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      if (!MessagePackBinary.IsNil(bytes, offset))
        return new byte?(MessagePackBinary.ReadByte(bytes, offset, out readSize));
      readSize = 1;
      return new byte?();
    }
  }
}
