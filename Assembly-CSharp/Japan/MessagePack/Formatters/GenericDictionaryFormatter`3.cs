﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.GenericDictionaryFormatter`3
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace MessagePack.Formatters
{
  public sealed class GenericDictionaryFormatter<TKey, TValue, TDictionary> : DictionaryFormatterBase<TKey, TValue, TDictionary, TDictionary>
    where TDictionary : IDictionary<TKey, TValue>, new()
  {
    protected override void Add(TDictionary collection, int index, TKey key, TValue value)
    {
      collection.Add(key, value);
    }

    protected override TDictionary Complete(TDictionary intermediateCollection)
    {
      return intermediateCollection;
    }

    protected override TDictionary Create(int count)
    {
      return new TDictionary();
    }
  }
}
