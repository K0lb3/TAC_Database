﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.ForceInt16BlockFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Formatters
{
  public sealed class ForceInt16BlockFormatter : IMessagePackFormatter<short>, IMessagePackFormatter
  {
    public static readonly ForceInt16BlockFormatter Instance = new ForceInt16BlockFormatter();

    private ForceInt16BlockFormatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      short value,
      IFormatterResolver formatterResolver)
    {
      return MessagePackBinary.WriteInt16ForceInt16Block(ref bytes, offset, value);
    }

    public short Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      return MessagePackBinary.ReadInt16(bytes, offset, out readSize);
    }
  }
}
