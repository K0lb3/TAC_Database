﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.DecimalFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using System.Globalization;

namespace MessagePack.Formatters
{
  public sealed class DecimalFormatter : IMessagePackFormatter<Decimal>, IMessagePackFormatter
  {
    public static readonly DecimalFormatter Instance = new DecimalFormatter();

    private DecimalFormatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      Decimal value,
      IFormatterResolver formatterResolver)
    {
      return MessagePackBinary.WriteString(ref bytes, offset, value.ToString((IFormatProvider) CultureInfo.InvariantCulture));
    }

    public Decimal Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      return Decimal.Parse(MessagePackBinary.ReadString(bytes, offset, out readSize), (IFormatProvider) CultureInfo.InvariantCulture);
    }
  }
}
