﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.StringEncoding
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Text;

namespace MessagePack
{
  internal static class StringEncoding
  {
    public static readonly Encoding UTF8 = (Encoding) new UTF8Encoding(false);
  }
}
