﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Unity.Vector3IntFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack.Formatters;
using System;
using UnityEngine;

namespace MessagePack.Unity
{
  public sealed class Vector3IntFormatter : IMessagePackFormatter<Vector3Int>, IMessagePackFormatter
  {
    public int Serialize(
      ref byte[] bytes,
      int offset,
      Vector3Int value,
      IFormatterResolver formatterResolver)
    {
      int num = offset;
      offset += MessagePackBinary.WriteFixedArrayHeaderUnsafe(ref bytes, offset, 3);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, ((Vector3Int) ref value).get_x());
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, ((Vector3Int) ref value).get_y());
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, ((Vector3Int) ref value).get_z());
      return offset - num;
    }

    public Vector3Int Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      if (MessagePackBinary.IsNil(bytes, offset))
        throw new InvalidOperationException("typecode is null, struct not supported");
      int num1 = offset;
      int num2 = MessagePackBinary.ReadArrayHeader(bytes, offset, out readSize);
      offset += readSize;
      int num3 = 0;
      int num4 = 0;
      int num5 = 0;
      for (int index = 0; index < num2; ++index)
      {
        switch (index)
        {
          case 0:
            num3 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
            break;
          case 1:
            num4 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
            break;
          case 2:
            num5 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
            break;
          default:
            readSize = MessagePackBinary.ReadNextBlock(bytes, offset);
            break;
        }
        offset += readSize;
      }
      readSize = offset - num1;
      Vector3Int vector3Int;
      ((Vector3Int) ref vector3Int).\u002Ector(num3, num4, num5);
      ((Vector3Int) ref vector3Int).set_x(num3);
      ((Vector3Int) ref vector3Int).set_y(num4);
      ((Vector3Int) ref vector3Int).set_z(num5);
      return vector3Int;
    }
  }
}
