﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Unity.RectOffsetFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack.Formatters;
using UnityEngine;

namespace MessagePack.Unity
{
  public sealed class RectOffsetFormatter : IMessagePackFormatter<RectOffset>, IMessagePackFormatter
  {
    public int Serialize(
      ref byte[] bytes,
      int offset,
      RectOffset value,
      IFormatterResolver formatterResolver)
    {
      if (value == null)
        return MessagePackBinary.WriteNil(ref bytes, offset);
      int num = offset;
      offset += MessagePackBinary.WriteFixedArrayHeaderUnsafe(ref bytes, offset, 4);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.get_left());
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.get_right());
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.get_top());
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.get_bottom());
      return offset - num;
    }

    public RectOffset Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      if (MessagePackBinary.IsNil(bytes, offset))
      {
        readSize = 1;
        return (RectOffset) null;
      }
      int num1 = offset;
      int num2 = MessagePackBinary.ReadArrayHeader(bytes, offset, out readSize);
      offset += readSize;
      int num3 = 0;
      int num4 = 0;
      int num5 = 0;
      int num6 = 0;
      for (int index = 0; index < num2; ++index)
      {
        switch (index)
        {
          case 0:
            num3 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
            break;
          case 1:
            num4 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
            break;
          case 2:
            num5 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
            break;
          case 3:
            num6 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
            break;
          default:
            readSize = MessagePackBinary.ReadNextBlock(bytes, offset);
            break;
        }
        offset += readSize;
      }
      readSize = offset - num1;
      RectOffset rectOffset = new RectOffset();
      rectOffset.set_left(num3);
      rectOffset.set_right(num4);
      rectOffset.set_top(num5);
      rectOffset.set_bottom(num6);
      return rectOffset;
    }
  }
}
