﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Unity.UnityResolver
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack.Formatters;

namespace MessagePack.Unity
{
  public class UnityResolver : IFormatterResolver
  {
    public static IFormatterResolver Instance = (IFormatterResolver) new UnityResolver();

    private UnityResolver()
    {
    }

    public IMessagePackFormatter<T> GetFormatter<T>()
    {
      return UnityResolver.FormatterCache<T>.formatter;
    }

    private static class FormatterCache<T>
    {
      public static readonly IMessagePackFormatter<T> formatter = (IMessagePackFormatter<T>) UnityResolveryResolverGetFormatterHelper.GetFormatter(typeof (T));
    }
  }
}
