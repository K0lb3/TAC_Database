﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Unity.BoundsIntFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack.Formatters;
using System;
using UnityEngine;

namespace MessagePack.Unity
{
  public sealed class BoundsIntFormatter : IMessagePackFormatter<BoundsInt>, IMessagePackFormatter
  {
    public int Serialize(
      ref byte[] bytes,
      int offset,
      BoundsInt value,
      IFormatterResolver formatterResolver)
    {
      int num = offset;
      offset += MessagePackBinary.WriteFixedArrayHeaderUnsafe(ref bytes, offset, 2);
      offset += formatterResolver.GetFormatterWithVerify<Vector3Int>().Serialize(ref bytes, offset, ((BoundsInt) ref value).get_position(), formatterResolver);
      offset += formatterResolver.GetFormatterWithVerify<Vector3Int>().Serialize(ref bytes, offset, ((BoundsInt) ref value).get_size(), formatterResolver);
      return offset - num;
    }

    public BoundsInt Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      if (MessagePackBinary.IsNil(bytes, offset))
        throw new InvalidOperationException("typecode is null, struct not supported");
      int num1 = offset;
      int num2 = MessagePackBinary.ReadArrayHeader(bytes, offset, out readSize);
      offset += readSize;
      Vector3Int vector3Int1 = (Vector3Int) null;
      Vector3Int vector3Int2 = (Vector3Int) null;
      for (int index = 0; index < num2; ++index)
      {
        switch (index)
        {
          case 0:
            vector3Int1 = formatterResolver.GetFormatterWithVerify<Vector3Int>().Deserialize(bytes, offset, formatterResolver, out readSize);
            break;
          case 1:
            vector3Int2 = formatterResolver.GetFormatterWithVerify<Vector3Int>().Deserialize(bytes, offset, formatterResolver, out readSize);
            break;
          default:
            readSize = MessagePackBinary.ReadNextBlock(bytes, offset);
            break;
        }
        offset += readSize;
      }
      readSize = offset - num1;
      BoundsInt boundsInt;
      ((BoundsInt) ref boundsInt).\u002Ector(vector3Int1, vector3Int2);
      ((BoundsInt) ref boundsInt).set_position(vector3Int1);
      ((BoundsInt) ref boundsInt).set_size(vector3Int2);
      return boundsInt;
    }
  }
}
