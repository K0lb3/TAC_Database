﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Unity.KeyframeFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack.Formatters;
using System;
using UnityEngine;

namespace MessagePack.Unity
{
  public sealed class KeyframeFormatter : IMessagePackFormatter<Keyframe>, IMessagePackFormatter
  {
    public int Serialize(
      ref byte[] bytes,
      int offset,
      Keyframe value,
      IFormatterResolver formatterResolver)
    {
      int num = offset;
      offset += MessagePackBinary.WriteFixedArrayHeaderUnsafe(ref bytes, offset, 4);
      offset += MessagePackBinary.WriteSingle(ref bytes, offset, ((Keyframe) ref value).get_time());
      offset += MessagePackBinary.WriteSingle(ref bytes, offset, ((Keyframe) ref value).get_value());
      offset += MessagePackBinary.WriteSingle(ref bytes, offset, ((Keyframe) ref value).get_inTangent());
      offset += MessagePackBinary.WriteSingle(ref bytes, offset, ((Keyframe) ref value).get_outTangent());
      return offset - num;
    }

    public Keyframe Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      if (MessagePackBinary.IsNil(bytes, offset))
        throw new InvalidOperationException("typecode is null, struct not supported");
      int num1 = offset;
      int num2 = MessagePackBinary.ReadArrayHeader(bytes, offset, out readSize);
      offset += readSize;
      float num3 = 0.0f;
      float num4 = 0.0f;
      float num5 = 0.0f;
      float num6 = 0.0f;
      for (int index = 0; index < num2; ++index)
      {
        switch (index)
        {
          case 0:
            num3 = MessagePackBinary.ReadSingle(bytes, offset, out readSize);
            break;
          case 1:
            num4 = MessagePackBinary.ReadSingle(bytes, offset, out readSize);
            break;
          case 2:
            num5 = MessagePackBinary.ReadSingle(bytes, offset, out readSize);
            break;
          case 3:
            num6 = MessagePackBinary.ReadSingle(bytes, offset, out readSize);
            break;
          default:
            readSize = MessagePackBinary.ReadNextBlock(bytes, offset);
            break;
        }
        offset += readSize;
      }
      readSize = offset - num1;
      Keyframe keyframe;
      ((Keyframe) ref keyframe).\u002Ector(num3, num4, num5, num6);
      ((Keyframe) ref keyframe).set_time(num3);
      ((Keyframe) ref keyframe).set_value(num4);
      ((Keyframe) ref keyframe).set_inTangent(num5);
      ((Keyframe) ref keyframe).set_outTangent(num6);
      return keyframe;
    }
  }
}
