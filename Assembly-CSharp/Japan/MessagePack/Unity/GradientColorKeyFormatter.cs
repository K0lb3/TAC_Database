﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Unity.GradientColorKeyFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack.Formatters;
using System;
using UnityEngine;

namespace MessagePack.Unity
{
  public sealed class GradientColorKeyFormatter : IMessagePackFormatter<GradientColorKey>, IMessagePackFormatter
  {
    public int Serialize(
      ref byte[] bytes,
      int offset,
      GradientColorKey value,
      IFormatterResolver formatterResolver)
    {
      int num = offset;
      offset += MessagePackBinary.WriteFixedArrayHeaderUnsafe(ref bytes, offset, 2);
      offset += formatterResolver.GetFormatterWithVerify<Color>().Serialize(ref bytes, offset, (Color) value.color, formatterResolver);
      offset += MessagePackBinary.WriteSingle(ref bytes, offset, (float) value.time);
      return offset - num;
    }

    public GradientColorKey Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      if (MessagePackBinary.IsNil(bytes, offset))
        throw new InvalidOperationException("typecode is null, struct not supported");
      int num1 = offset;
      int num2 = MessagePackBinary.ReadArrayHeader(bytes, offset, out readSize);
      offset += readSize;
      Color color = (Color) null;
      float num3 = 0.0f;
      for (int index = 0; index < num2; ++index)
      {
        switch (index)
        {
          case 0:
            color = formatterResolver.GetFormatterWithVerify<Color>().Deserialize(bytes, offset, formatterResolver, out readSize);
            break;
          case 1:
            num3 = MessagePackBinary.ReadSingle(bytes, offset, out readSize);
            break;
          default:
            readSize = MessagePackBinary.ReadNextBlock(bytes, offset);
            break;
        }
        offset += readSize;
      }
      readSize = offset - num1;
      GradientColorKey gradientColorKey;
      ((GradientColorKey) ref gradientColorKey).\u002Ector(color, num3);
      gradientColorKey.color = (__Null) color;
      gradientColorKey.time = (__Null) (double) num3;
      return gradientColorKey;
    }
  }
}
