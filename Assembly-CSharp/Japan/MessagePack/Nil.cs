﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Nil
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using System.Runtime.InteropServices;

namespace MessagePack
{
  [StructLayout(LayoutKind.Sequential, Size = 1)]
  public struct Nil : IEquatable<Nil>
  {
    public static readonly Nil Default = new Nil();

    public override bool Equals(object obj)
    {
      return obj is Nil;
    }

    public bool Equals(Nil other)
    {
      return true;
    }

    public override int GetHashCode()
    {
      return 0;
    }

    public override string ToString()
    {
      return "()";
    }
  }
}
