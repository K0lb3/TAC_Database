﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Resolvers.PrimitiveObjectResolver
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack.Formatters;

namespace MessagePack.Resolvers
{
  public sealed class PrimitiveObjectResolver : IFormatterResolver
  {
    public static IFormatterResolver Instance = (IFormatterResolver) new PrimitiveObjectResolver();

    private PrimitiveObjectResolver()
    {
    }

    public IMessagePackFormatter<T> GetFormatter<T>()
    {
      return PrimitiveObjectResolver.FormatterCache<T>.formatter;
    }

    private static class FormatterCache<T>
    {
      public static readonly IMessagePackFormatter<T> formatter = typeof (T) != typeof (object) ? (IMessagePackFormatter<T>) null : (IMessagePackFormatter<T>) PrimitiveObjectFormatter.Instance;
    }
  }
}
