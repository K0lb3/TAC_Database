﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Resolvers.AttributeFormatterResolver
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack.Formatters;
using System;
using System.Reflection;

namespace MessagePack.Resolvers
{
  public sealed class AttributeFormatterResolver : IFormatterResolver
  {
    public static IFormatterResolver Instance = (IFormatterResolver) new AttributeFormatterResolver();

    private AttributeFormatterResolver()
    {
    }

    public IMessagePackFormatter<T> GetFormatter<T>()
    {
      return AttributeFormatterResolver.FormatterCache<T>.formatter;
    }

    private static class FormatterCache<T>
    {
      public static readonly IMessagePackFormatter<T> formatter;

      static FormatterCache()
      {
        MessagePackFormatterAttribute customAttribute = typeof (T).GetTypeInfo().GetCustomAttribute<MessagePackFormatterAttribute>(true);
        if (customAttribute == null)
          return;
        if (customAttribute.Arguments == null)
          AttributeFormatterResolver.FormatterCache<T>.formatter = (IMessagePackFormatter<T>) Activator.CreateInstance(customAttribute.FormatterType);
        else
          AttributeFormatterResolver.FormatterCache<T>.formatter = (IMessagePackFormatter<T>) Activator.CreateInstance(customAttribute.FormatterType, customAttribute.Arguments);
      }
    }
  }
}
