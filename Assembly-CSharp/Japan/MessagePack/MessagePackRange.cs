﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.MessagePackRange
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack
{
  public static class MessagePackRange
  {
    public const int MinFixNegativeInt = -32;
    public const int MaxFixNegativeInt = -1;
    public const int MaxFixPositiveInt = 127;
    public const int MinFixStringLength = 0;
    public const int MaxFixStringLength = 31;
    public const int MaxFixMapCount = 15;
    public const int MaxFixArrayCount = 15;
  }
}
