﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Internal.RuntimeTypeHandleEqualityComparer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using System.Collections.Generic;

namespace MessagePack.Internal
{
  public class RuntimeTypeHandleEqualityComparer : IEqualityComparer<RuntimeTypeHandle>
  {
    public static IEqualityComparer<RuntimeTypeHandle> Default = (IEqualityComparer<RuntimeTypeHandle>) new RuntimeTypeHandleEqualityComparer();

    private RuntimeTypeHandleEqualityComparer()
    {
    }

    public bool Equals(RuntimeTypeHandle x, RuntimeTypeHandle y)
    {
      return x.Equals(y);
    }

    public int GetHashCode(RuntimeTypeHandle obj)
    {
      return obj.GetHashCode();
    }
  }
}
