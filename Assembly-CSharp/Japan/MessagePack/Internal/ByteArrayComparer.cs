﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Internal.ByteArrayComparer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Internal
{
  public static class ByteArrayComparer
  {
    public static bool Equals(byte[] xs, int xsOffset, int xsCount, byte[] ys)
    {
      if (xs == null || ys == null || xsCount != ys.Length)
        return false;
      for (int index = 0; index < ys.Length; ++index)
      {
        if ((int) xs[xsOffset++] != (int) ys[index])
          return false;
      }
      return true;
    }

    public static bool Equals(
      byte[] xs,
      int xsOffset,
      int xsCount,
      byte[] ys,
      int ysOffset,
      int ysCount)
    {
      if (xs == null || ys == null || xsCount != ysCount)
        return false;
      for (int index = 0; index < xsCount; ++index)
      {
        if ((int) xs[xsOffset++] != (int) ys[ysOffset++])
          return false;
      }
      return true;
    }
  }
}
