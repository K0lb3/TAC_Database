﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Internal.DynamicGenericResolverGetFormatterHelper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack.Formatters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;

namespace MessagePack.Internal
{
  internal static class DynamicGenericResolverGetFormatterHelper
  {
    private static readonly Dictionary<Type, Type> formatterMap = new Dictionary<Type, Type>()
    {
      {
        typeof (List<>),
        typeof (ListFormatter<>)
      },
      {
        typeof (LinkedList<>),
        typeof (LinkedListFormatter<>)
      },
      {
        typeof (Queue<>),
        typeof (QeueueFormatter<>)
      },
      {
        typeof (Stack<>),
        typeof (StackFormatter<>)
      },
      {
        typeof (HashSet<>),
        typeof (HashSetFormatter<>)
      },
      {
        typeof (ReadOnlyCollection<>),
        typeof (ReadOnlyCollectionFormatter<>)
      },
      {
        typeof (IList<>),
        typeof (InterfaceListFormatter<>)
      },
      {
        typeof (ICollection<>),
        typeof (InterfaceCollectionFormatter<>)
      },
      {
        typeof (IEnumerable<>),
        typeof (InterfaceEnumerableFormatter<>)
      },
      {
        typeof (Dictionary<,>),
        typeof (DictionaryFormatter<,>)
      },
      {
        typeof (IDictionary<,>),
        typeof (InterfaceDictionaryFormatter<,>)
      },
      {
        typeof (SortedDictionary<,>),
        typeof (SortedDictionaryFormatter<,>)
      },
      {
        typeof (SortedList<,>),
        typeof (SortedListFormatter<,>)
      },
      {
        typeof (ILookup<,>),
        typeof (InterfaceLookupFormatter<,>)
      },
      {
        typeof (IGrouping<,>),
        typeof (InterfaceGroupingFormatter<,>)
      }
    };

    internal static object GetFormatter(Type t)
    {
      TypeInfo typeInfo = t.GetTypeInfo();
      if (t.IsArray)
      {
        switch (t.GetArrayRank())
        {
          case 1:
            if (t.GetElementType() == typeof (byte))
              return (object) ByteArrayFormatter.Instance;
            return Activator.CreateInstance(typeof (ArrayFormatter<>).MakeGenericType(t.GetElementType()));
          case 2:
            return Activator.CreateInstance(typeof (TwoDimentionalArrayFormatter<>).MakeGenericType(t.GetElementType()));
          case 3:
            return Activator.CreateInstance(typeof (ThreeDimentionalArrayFormatter<>).MakeGenericType(t.GetElementType()));
          case 4:
            return Activator.CreateInstance(typeof (FourDimentionalArrayFormatter<>).MakeGenericType(t.GetElementType()));
          default:
            return (object) null;
        }
      }
      else
      {
        if (typeInfo.IsGenericType)
        {
          Type genericTypeDefinition = typeInfo.GetGenericTypeDefinition();
          bool flag = genericTypeDefinition.GetTypeInfo().IsNullable();
          Type type = !flag ? (Type) null : typeInfo.GenericTypeArguments[0];
          if (genericTypeDefinition == typeof (KeyValuePair<,>))
            return DynamicGenericResolverGetFormatterHelper.CreateInstance(typeof (KeyValuePairFormatter<,>), typeInfo.GenericTypeArguments);
          if (flag && type.GetTypeInfo().IsConstructedGenericType() && type.GetGenericTypeDefinition() == typeof (KeyValuePair<,>))
            return DynamicGenericResolverGetFormatterHelper.CreateInstance(typeof (NullableFormatter<>), new Type[1]
            {
              type
            });
          if (genericTypeDefinition == typeof (ArraySegment<>))
            return typeInfo.GenericTypeArguments[0] == typeof (byte) ? (object) ByteArraySegmentFormatter.Instance : DynamicGenericResolverGetFormatterHelper.CreateInstance(typeof (ArraySegmentFormatter<>), typeInfo.GenericTypeArguments);
          if (flag && type.GetTypeInfo().IsConstructedGenericType() && type.GetGenericTypeDefinition() == typeof (ArraySegment<>))
          {
            if (type == typeof (ArraySegment<byte>))
              return (object) new StaticNullableFormatter<ArraySegment<byte>>((IMessagePackFormatter<ArraySegment<byte>>) ByteArraySegmentFormatter.Instance);
            return DynamicGenericResolverGetFormatterHelper.CreateInstance(typeof (NullableFormatter<>), new Type[1]
            {
              type
            });
          }
          Type genericType;
          if (DynamicGenericResolverGetFormatterHelper.formatterMap.TryGetValue(genericTypeDefinition, out genericType))
            return DynamicGenericResolverGetFormatterHelper.CreateInstance(genericType, typeInfo.GenericTypeArguments);
          if (typeInfo.GenericTypeArguments.Length == 1 && ((IEnumerable<Type>) typeInfo.ImplementedInterfaces).Any<Type>((Func<Type, bool>) (x => x.GetTypeInfo().IsConstructedGenericType() && x.GetGenericTypeDefinition() == typeof (ICollection<>))) && typeInfo.DeclaredConstructors.Any<ConstructorInfo>((Func<ConstructorInfo, bool>) (x => x.GetParameters().Length == 0)))
            return DynamicGenericResolverGetFormatterHelper.CreateInstance(typeof (GenericCollectionFormatter<,>), new Type[2]
            {
              typeInfo.GenericTypeArguments[0],
              t
            });
          if (typeInfo.GenericTypeArguments.Length == 2 && ((IEnumerable<Type>) typeInfo.ImplementedInterfaces).Any<Type>((Func<Type, bool>) (x => x.GetTypeInfo().IsConstructedGenericType() && x.GetGenericTypeDefinition() == typeof (IDictionary<,>))) && typeInfo.DeclaredConstructors.Any<ConstructorInfo>((Func<ConstructorInfo, bool>) (x => x.GetParameters().Length == 0)))
            return DynamicGenericResolverGetFormatterHelper.CreateInstance(typeof (GenericDictionaryFormatter<,,>), new Type[3]
            {
              typeInfo.GenericTypeArguments[0],
              typeInfo.GenericTypeArguments[1],
              t
            });
        }
        else
        {
          if (t == typeof (IList))
            return (object) NonGenericInterfaceListFormatter.Instance;
          if (t == typeof (IDictionary))
            return (object) NonGenericInterfaceDictionaryFormatter.Instance;
          if (typeof (IList).GetTypeInfo().IsAssignableFrom(typeInfo) && typeInfo.DeclaredConstructors.Any<ConstructorInfo>((Func<ConstructorInfo, bool>) (x => x.GetParameters().Length == 0)))
            return Activator.CreateInstance(typeof (NonGenericListFormatter<>).MakeGenericType(t));
          if (typeof (IDictionary).GetTypeInfo().IsAssignableFrom(typeInfo) && typeInfo.DeclaredConstructors.Any<ConstructorInfo>((Func<ConstructorInfo, bool>) (x => x.GetParameters().Length == 0)))
            return Activator.CreateInstance(typeof (NonGenericDictionaryFormatter<>).MakeGenericType(t));
        }
        return (object) null;
      }
    }

    private static object CreateInstance(
      Type genericType,
      Type[] genericTypeArguments,
      params object[] arguments)
    {
      return Activator.CreateInstance(genericType.MakeGenericType(genericTypeArguments), arguments);
    }
  }
}
