﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Internal.DynamicAssembly
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using System.Reflection;
using System.Reflection.Emit;

namespace MessagePack.Internal
{
  internal class DynamicAssembly
  {
    private readonly object gate = new object();
    private readonly AssemblyBuilder assemblyBuilder;
    private readonly ModuleBuilder moduleBuilder;

    public DynamicAssembly(string moduleName)
    {
      this.assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(new AssemblyName(moduleName), AssemblyBuilderAccess.Run);
      this.moduleBuilder = this.assemblyBuilder.DefineDynamicModule(moduleName);
    }

    public TypeBuilder DefineType(string name, TypeAttributes attr)
    {
      lock (this.gate)
        return this.moduleBuilder.DefineType(name, attr);
    }

    public TypeBuilder DefineType(string name, TypeAttributes attr, Type parent)
    {
      lock (this.gate)
        return this.moduleBuilder.DefineType(name, attr, parent);
    }

    public TypeBuilder DefineType(
      string name,
      TypeAttributes attr,
      Type parent,
      Type[] interfaces)
    {
      lock (this.gate)
        return this.moduleBuilder.DefineType(name, attr, parent, interfaces);
    }
  }
}
