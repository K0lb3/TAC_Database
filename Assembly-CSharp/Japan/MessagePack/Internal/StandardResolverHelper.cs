﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Internal.StandardResolverHelper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack.Resolvers;
using MessagePack.Unity;

namespace MessagePack.Internal
{
  internal static class StandardResolverHelper
  {
    public static readonly IFormatterResolver[] DefaultResolvers = new IFormatterResolver[6]
    {
      BuiltinResolver.Instance,
      AttributeFormatterResolver.Instance,
      UnityResolver.Instance,
      (IFormatterResolver) DynamicEnumResolver.Instance,
      DynamicGenericResolver.Instance,
      (IFormatterResolver) DynamicUnionResolver.Instance
    };
  }
}
