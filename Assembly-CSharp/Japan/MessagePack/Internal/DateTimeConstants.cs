﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Internal.DateTimeConstants
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace MessagePack.Internal
{
  internal static class DateTimeConstants
  {
    internal static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
    internal const long BclSecondsAtUnixEpoch = 62135596800;
    internal const int NanosecondsPerTick = 100;
  }
}
