﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.MessagePackType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack
{
  public enum MessagePackType : byte
  {
    Unknown,
    Integer,
    Nil,
    Boolean,
    Float,
    String,
    Binary,
    Array,
    Map,
    Extension,
  }
}
