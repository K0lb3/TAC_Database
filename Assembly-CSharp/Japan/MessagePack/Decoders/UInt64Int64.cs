﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Decoders.UInt64Int64
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Decoders
{
  internal sealed class UInt64Int64 : IInt64Decoder
  {
    internal static readonly IInt64Decoder Instance = (IInt64Decoder) new UInt64Int64();

    private UInt64Int64()
    {
    }

    public long Read(byte[] bytes, int offset, out int readSize)
    {
      readSize = 9;
      return (long) bytes[checked (offset + 1)] << 56 | (long) bytes[checked (offset + 2)] << 48 | (long) bytes[checked (offset + 3)] << 40 | (long) bytes[checked (offset + 4)] << 32 | (long) bytes[checked (offset + 5)] << 24 | (long) bytes[checked (offset + 6)] << 16 | (long) bytes[checked (offset + 7)] << 8 | (long) bytes[checked (offset + 8)];
    }
  }
}
