﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Decoders.FixExt1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Decoders
{
  internal sealed class FixExt1 : IExtDecoder
  {
    internal static readonly IExtDecoder Instance = (IExtDecoder) new FixExt1();

    private FixExt1()
    {
    }

    public ExtensionResult Read(byte[] bytes, int offset, out int readSize)
    {
      readSize = 3;
      return new ExtensionResult((sbyte) bytes[offset + 1], new byte[1]
      {
        bytes[offset + 2]
      });
    }
  }
}
