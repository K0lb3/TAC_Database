﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Decoders.ReadNext3
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Decoders
{
  internal sealed class ReadNext3 : IReadNextDecoder
  {
    internal static readonly IReadNextDecoder Instance = (IReadNextDecoder) new ReadNext3();

    private ReadNext3()
    {
    }

    public int Read(byte[] bytes, int offset)
    {
      return 3;
    }
  }
}
