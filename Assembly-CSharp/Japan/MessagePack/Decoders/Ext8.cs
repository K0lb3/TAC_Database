﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Decoders.Ext8
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace MessagePack.Decoders
{
  internal sealed class Ext8 : IExtDecoder
  {
    internal static readonly IExtDecoder Instance = (IExtDecoder) new Ext8();

    private Ext8()
    {
    }

    public ExtensionResult Read(byte[] bytes, int offset, out int readSize)
    {
      byte num = bytes[offset + 1];
      sbyte typeCode = (sbyte) bytes[offset + 2];
      byte[] data = new byte[(int) num];
      readSize = (int) num + 3;
      Buffer.BlockCopy((Array) bytes, offset + 3, (Array) data, 0, (int) num);
      return new ExtensionResult(typeCode, data);
    }
  }
}
