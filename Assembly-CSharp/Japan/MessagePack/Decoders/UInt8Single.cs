﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Decoders.UInt8Single
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Decoders
{
  internal sealed class UInt8Single : ISingleDecoder
  {
    internal static readonly ISingleDecoder Instance = (ISingleDecoder) new UInt8Single();

    private UInt8Single()
    {
    }

    public float Read(byte[] bytes, int offset, out int readSize)
    {
      return (float) UInt8Byte.Instance.Read(bytes, offset, out readSize);
    }
  }
}
