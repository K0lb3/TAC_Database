﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Decoders.ReadNextArray
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Decoders
{
  internal sealed class ReadNextArray : IReadNextDecoder
  {
    internal static readonly IReadNextDecoder Instance = (IReadNextDecoder) new ReadNextArray();

    private ReadNextArray()
    {
    }

    public int Read(byte[] bytes, int offset)
    {
      int num1 = offset;
      int readSize;
      int num2 = MessagePackBinary.ReadArrayHeader(bytes, offset, out readSize);
      offset += readSize;
      for (int index = 0; index < num2; ++index)
        offset += MessagePackBinary.ReadNext(bytes, offset);
      return offset - num1;
    }
  }
}
