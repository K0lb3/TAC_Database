﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Decoders.FixInt32
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Decoders
{
  internal sealed class FixInt32 : IInt32Decoder
  {
    internal static readonly IInt32Decoder Instance = (IInt32Decoder) new FixInt32();

    private FixInt32()
    {
    }

    public int Read(byte[] bytes, int offset, out int readSize)
    {
      readSize = 1;
      return (int) bytes[offset];
    }
  }
}
