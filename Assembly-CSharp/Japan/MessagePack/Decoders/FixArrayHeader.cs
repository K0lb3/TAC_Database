﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Decoders.FixArrayHeader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Decoders
{
  internal sealed class FixArrayHeader : IArrayHeaderDecoder
  {
    internal static readonly IArrayHeaderDecoder Instance = (IArrayHeaderDecoder) new FixArrayHeader();

    private FixArrayHeader()
    {
    }

    public uint Read(byte[] bytes, int offset, out int readSize)
    {
      readSize = 1;
      return (uint) bytes[offset] & 15U;
    }
  }
}
