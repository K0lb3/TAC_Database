﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Decoders.Str16String
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Decoders
{
  internal sealed class Str16String : IStringDecoder
  {
    internal static readonly IStringDecoder Instance = (IStringDecoder) new Str16String();

    private Str16String()
    {
    }

    public string Read(byte[] bytes, int offset, out int readSize)
    {
      int count = ((int) bytes[offset + 1] << 8) + (int) bytes[offset + 2];
      readSize = count + 3;
      return StringEncoding.UTF8.GetString(bytes, offset + 3, count);
    }
  }
}
