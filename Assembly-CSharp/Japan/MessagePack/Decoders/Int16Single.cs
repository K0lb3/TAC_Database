﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Decoders.Int16Single
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Decoders
{
  internal sealed class Int16Single : ISingleDecoder
  {
    internal static readonly ISingleDecoder Instance = (ISingleDecoder) new Int16Single();

    private Int16Single()
    {
    }

    public float Read(byte[] bytes, int offset, out int readSize)
    {
      return (float) Int16Int16.Instance.Read(bytes, offset, out readSize);
    }
  }
}
