﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Decoders.ReadNext1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Decoders
{
  internal sealed class ReadNext1 : IReadNextDecoder
  {
    internal static readonly IReadNextDecoder Instance = (IReadNextDecoder) new ReadNext1();

    private ReadNext1()
    {
    }

    public int Read(byte[] bytes, int offset)
    {
      return 1;
    }
  }
}
