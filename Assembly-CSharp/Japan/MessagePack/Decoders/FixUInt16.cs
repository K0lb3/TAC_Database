﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Decoders.FixUInt16
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Decoders
{
  internal sealed class FixUInt16 : IUInt16Decoder
  {
    internal static readonly IUInt16Decoder Instance = (IUInt16Decoder) new FixUInt16();

    private FixUInt16()
    {
    }

    public ushort Read(byte[] bytes, int offset, out int readSize)
    {
      readSize = 1;
      return (ushort) bytes[offset];
    }
  }
}
