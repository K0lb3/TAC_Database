﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Decoders.Ext32
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace MessagePack.Decoders
{
  internal sealed class Ext32 : IExtDecoder
  {
    internal static readonly IExtDecoder Instance = (IExtDecoder) new Ext32();

    private Ext32()
    {
    }

    public ExtensionResult Read(byte[] bytes, int offset, out int readSize)
    {
      uint num = (uint) ((int) bytes[offset + 1] << 24 | (int) bytes[offset + 2] << 16 | (int) bytes[offset + 3] << 8) | (uint) bytes[offset + 4];
      sbyte typeCode = (sbyte) bytes[offset + 5];
      byte[] data = new byte[(IntPtr) num];
      readSize = checked ((int) num + 6);
      Buffer.BlockCopy((Array) bytes, checked (offset + 6), (Array) data, 0, checked ((int) num));
      return new ExtensionResult(typeCode, data);
    }
  }
}
