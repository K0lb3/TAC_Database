﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Decoders.Bin8BytesSegment
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace MessagePack.Decoders
{
  internal sealed class Bin8BytesSegment : IBytesSegmentDecoder
  {
    internal static readonly IBytesSegmentDecoder Instance = (IBytesSegmentDecoder) new Bin8BytesSegment();

    private Bin8BytesSegment()
    {
    }

    public ArraySegment<byte> Read(byte[] bytes, int offset, out int readSize)
    {
      byte num = bytes[offset + 1];
      readSize = (int) num + 2;
      return new ArraySegment<byte>(bytes, offset + 2, (int) num);
    }
  }
}
