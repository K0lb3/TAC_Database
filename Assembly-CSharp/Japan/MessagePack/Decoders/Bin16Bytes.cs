﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Decoders.Bin16Bytes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace MessagePack.Decoders
{
  internal sealed class Bin16Bytes : IBytesDecoder
  {
    internal static readonly IBytesDecoder Instance = (IBytesDecoder) new Bin16Bytes();

    private Bin16Bytes()
    {
    }

    public byte[] Read(byte[] bytes, int offset, out int readSize)
    {
      int count = ((int) bytes[offset + 1] << 8) + (int) bytes[offset + 2];
      byte[] numArray = new byte[count];
      Buffer.BlockCopy((Array) bytes, offset + 3, (Array) numArray, 0, count);
      readSize = count + 3;
      return numArray;
    }
  }
}
