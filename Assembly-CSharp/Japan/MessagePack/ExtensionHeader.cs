﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.ExtensionHeader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack
{
  public struct ExtensionHeader
  {
    public ExtensionHeader(sbyte typeCode, uint length)
    {
      this.TypeCode = typeCode;
      this.Length = length;
    }

    public sbyte TypeCode { get; private set; }

    public uint Length { get; private set; }
  }
}
