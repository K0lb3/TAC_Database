﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.ExtensionResult
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack
{
  public struct ExtensionResult
  {
    public ExtensionResult(sbyte typeCode, byte[] data)
    {
      this.TypeCode = typeCode;
      this.Data = data;
    }

    public sbyte TypeCode { get; private set; }

    public byte[] Data { get; private set; }
  }
}
