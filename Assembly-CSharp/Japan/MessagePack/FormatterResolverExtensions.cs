﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.FormatterResolverExtensions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack.Formatters;
using System;
using System.Reflection;

namespace MessagePack
{
  public static class FormatterResolverExtensions
  {
    public static IMessagePackFormatter<T> GetFormatterWithVerify<T>(
      this IFormatterResolver resolver)
    {
      IMessagePackFormatter<T> formatter;
      try
      {
        formatter = resolver.GetFormatter<T>();
      }
      catch (TypeInitializationException ex)
      {
        Exception exception = (Exception) ex;
        while (exception.InnerException != null)
          exception = exception.InnerException;
        throw exception;
      }
      if (formatter == null)
        throw new FormatterNotRegisteredException(typeof (T).FullName + " is not registered in this resolver. resolver:" + resolver.GetType().Name);
      return formatter;
    }

    public static object GetFormatterDynamic(this IFormatterResolver resolver, Type type)
    {
      return typeof (IFormatterResolver).GetRuntimeMethod("GetFormatter", Type.EmptyTypes).MakeGenericMethod(type).Invoke((object) resolver, (object[]) null);
    }
  }
}
