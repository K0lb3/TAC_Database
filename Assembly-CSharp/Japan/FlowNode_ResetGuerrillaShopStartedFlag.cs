﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_ResetGuerrillaShopStartedFlag
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using SRPG;

[FlowNode.NodeType("System/Shop/ResetGuerrillaShopStartedFlag", 32741)]
[FlowNode.Pin(0, "Reset", FlowNode.PinTypes.Input, 0)]
[FlowNode.Pin(1000, "Exit", FlowNode.PinTypes.Output, 1000)]
public class FlowNode_ResetGuerrillaShopStartedFlag : FlowNode
{
  public override void OnActivate(int pinID)
  {
    if (pinID != 0)
      return;
    MonoSingleton<GameManager>.Instance.Player.IsGuerrillaShopStarted = false;
    this.ActivateOutputLinks(1000);
  }
}
