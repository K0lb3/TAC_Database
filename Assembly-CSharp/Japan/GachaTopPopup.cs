﻿// Decompiled with JetBrains decompiler
// Type: GachaTopPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using SRPG;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;

public class GachaTopPopup : MonoBehaviour
{
  private static readonly string HOST_URL = string.Empty;
  private static readonly string GACHA_DETAIL_TITLE = "sys.TITLE_POPUP_GACHA_DETAIL";
  private static readonly string GACHA_DESCRIPTION_TITLE = "sys.TITLE_POPUP_GACHA_DESCRIPTION";
  public GameObject TextTemplate;
  public GameObject ImageTemplate;
  public GameObject Contents;
  public GameObject Title;
  private GachaTopPopup.PopupType popupType;
  private GachaTopParam mCurrentGachaTopParam;
  private string mCurrentGachaIname;

  public GachaTopPopup()
  {
    base.\u002Ector();
  }

  private void Start()
  {
    if (Object.op_Inequality((Object) this.TextTemplate, (Object) null))
      this.TextTemplate.SetActive(false);
    if (Object.op_Inequality((Object) this.ImageTemplate, (Object) null))
      this.ImageTemplate.SetActive(false);
    if (Object.op_Equality((Object) this.Contents, (Object) null) || Object.op_Equality((Object) this.Title, (Object) null))
      return;
    Text component = (Text) ((Component) this.Title.get_transform().Find("Text")).GetComponent<Text>();
    this.popupType = (GachaTopPopup.PopupType) int.Parse(FlowNode_Variable.Get(nameof (GachaTopPopup)));
    string key = this.popupType != GachaTopPopup.PopupType.DETAIL ? GachaTopPopup.GACHA_DESCRIPTION_TITLE : GachaTopPopup.GACHA_DETAIL_TITLE;
    if (Object.op_Inequality((Object) component, (Object) null))
      component.set_text(LocalizedText.Get(key));
    if (this.popupType == GachaTopPopup.PopupType.DETAIL)
    {
      this.mCurrentGachaIname = FlowNode_Variable.Get("GachaDetailSelectIname");
      if (string.IsNullOrEmpty(this.mCurrentGachaIname))
        return;
    }
    this.CreateContents();
  }

  public List<GachaDetailParam> GetGachaDetailData()
  {
    List<GachaDetailParam> gachaDetailParamList = new List<GachaDetailParam>();
    string empty = string.Empty;
    foreach (JSON_GachaDetailParam json in JSONParser.parseJSONArray<JSON_GachaDetailParam>(this.popupType != GachaTopPopup.PopupType.DETAIL ? AssetManager.LoadTextData("Gachas/gacha_description") : AssetManager.LoadTextData("Gachas/gacha_detail")))
    {
      GachaDetailParam gachaDetailParam = new GachaDetailParam();
      if (gachaDetailParam.Deserialize(json))
        gachaDetailParamList.Add(gachaDetailParam);
    }
    return gachaDetailParamList;
  }

  private void CreateContents()
  {
    List<GachaDetailParam> gachaDetailData = this.GetGachaDetailData();
    if (gachaDetailData == null)
      return;
    foreach (GachaDetailParam gachaDetailParam in gachaDetailData)
    {
      if (this.popupType != GachaTopPopup.PopupType.DETAIL || !(this.mCurrentGachaIname != gachaDetailParam.gname))
      {
        if (gachaDetailParam.type == 1)
        {
          GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.TextTemplate);
          gameObject.get_transform().SetParent(this.Contents.get_transform(), false);
          Text component = (Text) ((Component) gameObject.get_transform().Find("Text")).GetComponent<Text>();
          if (Object.op_Inequality((Object) component, (Object) null))
            component.set_text(LocalizedText.Get(gachaDetailParam.text));
          gameObject.SetActive(true);
        }
        if (gachaDetailParam.type == 2)
        {
          GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.ImageTemplate);
          gameObject.get_transform().SetParent(this.Contents.get_transform(), false);
          RawImage component = (RawImage) ((Component) gameObject.get_transform().Find("Image")).GetComponent<RawImage>();
          string url = GachaTopPopup.HOST_URL + "/images/gacha/" + gachaDetailParam.image;
          if (Object.op_Inequality((Object) component, (Object) null))
            this.StartCoroutine(this.GetWWWImage(((Component) component).get_gameObject(), url, gachaDetailParam.width, gachaDetailParam.height, 0));
          gameObject.SetActive(true);
        }
      }
    }
  }

  [DebuggerHidden]
  private IEnumerator GetWWWImage(
    GameObject image,
    string url,
    int continue_count = 0,
    int height = 0,
    int width = 0)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new GachaTopPopup.\u003CGetWWWImage\u003Ec__Iterator0()
    {
      url = url,
      image = image
    };
  }

  public enum PopupType
  {
    DETAIL,
    DESCRIPTION,
    ALL,
  }
}
