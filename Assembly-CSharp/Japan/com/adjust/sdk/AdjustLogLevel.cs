﻿// Decompiled with JetBrains decompiler
// Type: com.adjust.sdk.AdjustLogLevel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace com.adjust.sdk
{
  public enum AdjustLogLevel
  {
    Verbose = 1,
    Debug = 2,
    Info = 3,
    Warn = 4,
    Error = 5,
    Assert = 6,
    Suppress = 7,
  }
}
