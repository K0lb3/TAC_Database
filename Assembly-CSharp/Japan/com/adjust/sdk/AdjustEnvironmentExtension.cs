﻿// Decompiled with JetBrains decompiler
// Type: com.adjust.sdk.AdjustEnvironmentExtension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace com.adjust.sdk
{
  public static class AdjustEnvironmentExtension
  {
    public static string ToLowercaseString(this AdjustEnvironment adjustEnvironment)
    {
      if (adjustEnvironment == AdjustEnvironment.Sandbox)
        return "sandbox";
      return adjustEnvironment == AdjustEnvironment.Production ? "production" : "unknown";
    }
  }
}
