﻿// Decompiled with JetBrains decompiler
// Type: IntVector2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

[Serializable]
public struct IntVector2
{
  public int x;
  public int y;

  public IntVector2(int a, int b)
  {
    this.x = a;
    this.y = b;
  }

  public override string ToString()
  {
    return string.Format("[IntVector2] {0}, {1}", (object) this.x, (object) this.y);
  }

  public static bool operator ==(IntVector2 a, IntVector2 b)
  {
    return a.x == b.x && a.y == b.y;
  }

  public static bool operator !=(IntVector2 a, IntVector2 b)
  {
    return a.x != b.x || a.y != b.y;
  }

  public override bool Equals(object obj)
  {
    return obj is IntVector2 intVector2 && intVector2 == this;
  }

  public override int GetHashCode()
  {
    return base.GetHashCode();
  }
}
