﻿// Decompiled with JetBrains decompiler
// Type: UniWebDemo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

public class UniWebDemo : MonoBehaviour
{
  public GameObject cubePrefab;
  public TextMesh tipTextMesh;

  public UniWebDemo()
  {
    base.\u002Ector();
  }

  private void Start()
  {
    Debug.LogWarning((object) "UniWebView only works on iOS/Android/WP8. Please switch to these platforms in Build Settings.");
  }
}
