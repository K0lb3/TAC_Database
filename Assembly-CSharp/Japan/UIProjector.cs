﻿// Decompiled with JetBrains decompiler
// Type: UIProjector
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

[AddComponentMenu("UI/Link UI Position")]
public class UIProjector : MonoBehaviour
{
  public Camera ProjectCamera;
  public RectTransform UIObject;
  public string UIObjectID;
  private Canvas mCanvas;
  public bool AutoDestroyUIObject;
  public Vector3 LocalOffset;

  public UIProjector()
  {
    base.\u002Ector();
  }

  public void SetCanvas(Canvas canvas)
  {
    this.mCanvas = canvas;
    if (!Object.op_Inequality((Object) this.UIObject, (Object) null))
      return;
    ((Transform) this.UIObject).SetParent(!Object.op_Inequality((Object) this.mCanvas, (Object) null) ? (Transform) null : ((Component) this.mCanvas).get_transform(), false);
  }

  protected virtual void Awake()
  {
    this.AutoDestroyUIObject = Object.op_Equality((Object) this.UIObject, (Object) null);
  }

  protected virtual void Start()
  {
    if (Object.op_Equality((Object) this.UIObject, (Object) null) && !string.IsNullOrEmpty(this.UIObjectID))
    {
      GameObject gameObject = GameObjectID.FindGameObject(this.UIObjectID);
      if (Object.op_Inequality((Object) gameObject, (Object) null))
        this.UIObject = (RectTransform) gameObject.GetComponent<RectTransform>();
    }
    if (Object.op_Equality((Object) this.mCanvas, (Object) null) && Object.op_Inequality((Object) this.UIObject, (Object) null))
      this.mCanvas = (Canvas) ((Component) this.UIObject).GetComponentInParent<Canvas>();
    if (Object.op_Inequality((Object) this.mCanvas, (Object) null) && Object.op_Equality((Object) this.UIObject, (Object) null))
    {
      this.UIObject = new GameObject(((Object) ((Component) this).get_gameObject()).get_name(), new System.Type[1]
      {
        typeof (RectTransform)
      }).get_transform() as RectTransform;
      ((Transform) this.UIObject).SetParent(((Component) this.mCanvas).get_transform(), false);
    }
    CameraHook.AddPreCullEventListener(new CameraHook.PreCullEvent(this.PreCull));
  }

  protected virtual void OnDestroy()
  {
    CameraHook.RemovePreCullEventListener(new CameraHook.PreCullEvent(this.PreCull));
    if (!this.AutoDestroyUIObject)
      return;
    GameUtility.DestroyGameObject((Component) this.UIObject);
  }

  public void PreCull(Camera camera)
  {
    if (Object.op_Inequality((Object) this.ProjectCamera, (Object) null) && Object.op_Inequality((Object) camera, (Object) this.ProjectCamera))
      return;
    Rect cameraViewport = SetCanvasBounds.GetCameraViewport();
    if (!Object.op_Inequality((Object) this.UIObject, (Object) null))
      return;
    Transform transform1 = ((Component) this).get_transform();
    Vector3 vector3 = Vector3.op_Addition(transform1.get_position(), Vector3.op_Addition(Vector3.op_Addition(Vector3.op_Multiply((float) this.LocalOffset.x, transform1.get_right()), Vector3.op_Multiply((float) this.LocalOffset.y, transform1.get_up())), Vector3.op_Multiply((float) this.LocalOffset.z, transform1.get_forward())));
    RectTransform transform2 = ((Component) this.mCanvas).get_transform() as RectTransform;
    Vector3 screenPoint = camera.WorldToScreenPoint(vector3);
    ref Vector3 local1 = ref screenPoint;
    local1.x = (__Null) (local1.x / (double) Screen.get_width());
    ref Vector3 local2 = ref screenPoint;
    local2.y = (__Null) (local2.y / (double) Screen.get_height());
    this.UIObject.set_anchorMin(Vector2.get_zero());
    this.UIObject.set_anchorMax(Vector2.get_zero());
    Vector2 zero = Vector2.get_zero();
    ref Vector2 local3 = ref zero;
    Rect rect1 = transform2.get_rect();
    double num1 = (double) ((Rect) ref rect1).get_width() * (double) ((Rect) ref cameraViewport).get_x();
    local3.x = (__Null) num1;
    ref Vector2 local4 = ref zero;
    Rect rect2 = transform2.get_rect();
    double num2 = (double) ((Rect) ref rect2).get_height() * (double) ((Rect) ref cameraViewport).get_y();
    local4.y = (__Null) num2;
    RectTransform uiObject = this.UIObject;
    // ISSUE: variable of the null type
    __Null x = screenPoint.x;
    Rect rect3 = transform2.get_rect();
    double width = (double) ((Rect) ref rect3).get_width();
    double num3 = x * width - zero.x;
    // ISSUE: variable of the null type
    __Null y = screenPoint.y;
    Rect rect4 = transform2.get_rect();
    double height = (double) ((Rect) ref rect4).get_height();
    double num4 = y * height - zero.y;
    Vector2 vector2 = new Vector2((float) num3, (float) num4);
    uiObject.set_anchoredPosition(vector2);
  }

  public void ReStart()
  {
    this.Start();
  }
}
