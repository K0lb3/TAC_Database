﻿// Decompiled with JetBrains decompiler
// Type: AdjustTownIconPos
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

public class AdjustTownIconPos : MonoBehaviour
{
  [SerializeField]
  private float SetOffsetPos;

  public AdjustTownIconPos()
  {
    base.\u002Ector();
  }

  private void Start()
  {
  }

  public void AdjustIconPos()
  {
    Vector3 localPosition = ((Component) this).get_transform().get_localPosition();
    Rect safeArea = SetCanvasBounds.GetSafeArea(false);
    foreach (GameObject gameObject in GameObjectID.FindGameObjects("BGSCROLL"))
    {
      AdjustTownBGScale component = (AdjustTownBGScale) gameObject.GetComponent<AdjustTownBGScale>();
      if (Object.op_Inequality((Object) component, (Object) null) && (double) ((Rect) ref safeArea).get_width() < (double) Screen.get_width())
      {
        float num = this.SetOffsetPos * ((float) (1.0 + (1.0 - (double) ((Rect) ref safeArea).get_width() / (double) Screen.get_width())) + component.RevisionScale);
        ref Vector3 local = ref localPosition;
        local.x = (__Null) (local.x - (double) num);
        ((Component) this).get_transform().set_localPosition(localPosition);
        break;
      }
    }
  }
}
