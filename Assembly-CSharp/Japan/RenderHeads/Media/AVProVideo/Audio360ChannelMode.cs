﻿// Decompiled with JetBrains decompiler
// Type: RenderHeads.Media.AVProVideo.Audio360ChannelMode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace RenderHeads.Media.AVProVideo
{
  public enum Audio360ChannelMode
  {
    TBE_8_2,
    TBE_8,
    TBE_6_2,
    TBE_6,
    TBE_4_2,
    TBE_4,
    TBE_8_PAIR0,
    TBE_8_PAIR1,
    TBE_8_PAIR2,
    TBE_8_PAIR3,
    TBE_CHANNEL0,
    TBE_CHANNEL1,
    TBE_CHANNEL2,
    TBE_CHANNEL3,
    TBE_CHANNEL4,
    TBE_CHANNEL5,
    TBE_CHANNEL6,
    TBE_CHANNEL7,
    HEADLOCKED_STEREO,
    HEADLOCKED_CHANNEL0,
    HEADLOCKED_CHANNEL1,
    AMBIX_4,
    AMBIX_9,
    AMBIX_9_2,
    AMBIX_16,
    AMBIX_16_2,
    STEREO,
    INVALID,
  }
}
