﻿// Decompiled with JetBrains decompiler
// Type: RenderHeads.Media.AVProVideo.ErrorCode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace RenderHeads.Media.AVProVideo
{
  public enum ErrorCode
  {
    None = 0,
    LoadFailed = 100, // 0x00000064
    DecodeFailed = 200, // 0x000000C8
  }
}
