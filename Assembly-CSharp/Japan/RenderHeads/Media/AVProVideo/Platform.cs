﻿// Decompiled with JetBrains decompiler
// Type: RenderHeads.Media.AVProVideo.Platform
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace RenderHeads.Media.AVProVideo
{
  public enum Platform
  {
    Windows = 0,
    MacOSX = 1,
    iOS = 2,
    tvOS = 3,
    Android = 4,
    WindowsPhone = 5,
    WindowsUWP = 6,
    WebGL = 7,
    PS4 = 8,
    Count = 9,
    Unknown = 100, // 0x00000064
  }
}
