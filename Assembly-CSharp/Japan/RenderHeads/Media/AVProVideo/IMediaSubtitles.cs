﻿// Decompiled with JetBrains decompiler
// Type: RenderHeads.Media.AVProVideo.IMediaSubtitles
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace RenderHeads.Media.AVProVideo
{
  public interface IMediaSubtitles
  {
    bool LoadSubtitlesSRT(string data);

    int GetSubtitleIndex();

    string GetSubtitleText();
  }
}
