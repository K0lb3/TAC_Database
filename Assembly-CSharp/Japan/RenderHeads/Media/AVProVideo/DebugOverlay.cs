﻿// Decompiled with JetBrains decompiler
// Type: RenderHeads.Media.AVProVideo.DebugOverlay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace RenderHeads.Media.AVProVideo
{
  [AddComponentMenu("AVPro Video/Debug Overlay", -99)]
  public class DebugOverlay : MonoBehaviour
  {
    [SerializeField]
    private MediaPlayer _mediaPlayer;
    [SerializeField]
    private int _guiDepth;
    [SerializeField]
    private float _displaySize;
    private int _debugOverlayCount;
    [SerializeField]
    private bool _displayControls;
    private const int s_GuiStartWidth = 10;
    private const int s_GuiWidth = 180;

    public DebugOverlay()
    {
      base.\u002Ector();
    }

    public bool DisplayControls
    {
      get
      {
        return this._displayControls;
      }
      set
      {
        this._displayControls = value;
      }
    }

    public MediaPlayer CurrentMediaPlayer
    {
      get
      {
        return this._mediaPlayer;
      }
      set
      {
        if (!Object.op_Inequality((Object) this._mediaPlayer, (Object) value))
          return;
        this._mediaPlayer = value;
      }
    }

    private void SetGuiPositionFromVideoIndex(int index)
    {
    }

    private void Update()
    {
      this._debugOverlayCount = 0;
      this._guiDepth = -1000;
      this._displaySize = 1f;
      this._debugOverlayCount = 0;
    }
  }
}
