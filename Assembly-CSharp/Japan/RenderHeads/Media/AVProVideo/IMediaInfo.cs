﻿// Decompiled with JetBrains decompiler
// Type: RenderHeads.Media.AVProVideo.IMediaInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace RenderHeads.Media.AVProVideo
{
  public interface IMediaInfo
  {
    float GetDurationMs();

    int GetVideoWidth();

    int GetVideoHeight();

    Rect GetCropRect();

    float GetVideoFrameRate();

    float GetVideoDisplayRate();

    bool HasVideo();

    bool HasAudio();

    int GetAudioTrackCount();

    string GetAudioTrackId(int index);

    string GetCurrentAudioTrackId();

    int GetCurrentAudioTrackBitrate();

    int GetVideoTrackCount();

    string GetVideoTrackId(int index);

    string GetCurrentVideoTrackId();

    int GetCurrentVideoTrackBitrate();

    string GetPlayerDescription();

    bool PlayerSupportsLinearColorSpace();

    bool IsPlaybackStalled();

    float[] GetTextureTransform();

    long GetEstimatedTotalBandwidthUsed();
  }
}
