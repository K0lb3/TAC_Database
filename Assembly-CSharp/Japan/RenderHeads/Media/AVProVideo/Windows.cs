﻿// Decompiled with JetBrains decompiler
// Type: RenderHeads.Media.AVProVideo.Windows
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace RenderHeads.Media.AVProVideo
{
  public static class Windows
  {
    public const string AudioDeviceOutputName_Vive = "HTC VIVE USB Audio";
    public const string AudioDeviceOutputName_Rift = "Headphones (Rift Audio)";

    public enum VideoApi
    {
      MediaFoundation,
      DirectShow,
    }
  }
}
