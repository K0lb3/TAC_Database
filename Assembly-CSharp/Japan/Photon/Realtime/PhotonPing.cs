﻿// Decompiled with JetBrains decompiler
// Type: Photon.Realtime.PhotonPing
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace Photon.Realtime
{
  public abstract class PhotonPing : IDisposable
  {
    public string DebugString = string.Empty;
    protected internal int PingLength = 13;
    protected internal byte[] PingBytes = new byte[13]
    {
      (byte) 125,
      (byte) 125,
      (byte) 125,
      (byte) 125,
      (byte) 125,
      (byte) 125,
      (byte) 125,
      (byte) 125,
      (byte) 125,
      (byte) 125,
      (byte) 125,
      (byte) 125,
      (byte) 0
    };
    public bool Successful;
    protected internal bool GotResult;
    protected internal byte PingId;

    public virtual bool StartPing(string ip)
    {
      throw new NotImplementedException();
    }

    public virtual bool Done()
    {
      throw new NotImplementedException();
    }

    public virtual void Dispose()
    {
      throw new NotImplementedException();
    }

    protected internal void Init()
    {
      this.GotResult = false;
      this.Successful = false;
      this.PingId = (byte) (Environment.TickCount % (int) byte.MaxValue);
    }
  }
}
