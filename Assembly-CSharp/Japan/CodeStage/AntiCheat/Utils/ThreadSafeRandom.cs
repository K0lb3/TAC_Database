﻿// Decompiled with JetBrains decompiler
// Type: CodeStage.AntiCheat.Utils.ThreadSafeRandom
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace CodeStage.AntiCheat.Utils
{
  public class ThreadSafeRandom
  {
    private static readonly Random Global = new Random();
    [ThreadStatic]
    private static Random local;

    public static int Next(int minInclusive, int maxExclusive)
    {
      Random local = ThreadSafeRandom.local;
      if (local != null)
        return local.Next(minInclusive, maxExclusive);
      int Seed;
      lock ((object) ThreadSafeRandom.Global)
        Seed = ThreadSafeRandom.Global.Next();
      return (ThreadSafeRandom.local = new Random(Seed)).Next(minInclusive, maxExclusive);
    }

    public static int Next()
    {
      return ThreadSafeRandom.Next(1, int.MaxValue);
    }

    public static int Next(int maxExclusive)
    {
      return ThreadSafeRandom.Next(1, maxExclusive);
    }
  }
}
