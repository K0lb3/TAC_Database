﻿// Decompiled with JetBrains decompiler
// Type: UnityWeakReference`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using UnityEngine;

internal class UnityWeakReference<T> : WeakReference where T : Object
{
  public UnityWeakReference(T target)
    : base((object) target)
  {
  }

  public override bool IsAlive
  {
    get
    {
      return Object.op_Inequality((Object) (object) this.Target, (Object) null);
    }
  }

  public T Target
  {
    get
    {
      return base.Target as T;
    }
    set
    {
      this.Target = (object) value;
    }
  }

  public bool TryGetTarget(out T target)
  {
    target = base.Target as T;
    return Object.op_Inequality((Object) (object) target, (Object) null);
  }
}
