﻿// Decompiled with JetBrains decompiler
// Type: SoundSettings
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

public class SoundSettings : ScriptableObject
{
  public string Tap;
  public string OK;
  public string Cancel;
  public string Select;
  public string Buzzer;
  public string Swipe;
  public string ScrollList;
  public string WindowPop;
  public string WindowClose;
  public const float BGMCrossFadeTime = 1f;
  private static SoundSettings mInstance;

  public SoundSettings()
  {
    base.\u002Ector();
  }

  public static SoundSettings Current
  {
    get
    {
      if (Object.op_Equality((Object) SoundSettings.mInstance, (Object) null))
      {
        SoundSettings.mInstance = (SoundSettings) Resources.Load<SoundSettings>(nameof (SoundSettings));
        Object.DontDestroyOnLoad((Object) SoundSettings.mInstance);
      }
      return SoundSettings.mInstance;
    }
  }
}
