﻿// Decompiled with JetBrains decompiler
// Type: OUInt
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using CodeStage.AntiCheat.ObscuredTypes;
using MessagePack;

[MessagePackObject(true)]
public struct OUInt
{
  private ObscuredUInt value;

  public OUInt(uint value)
  {
    this.value = (ObscuredUInt) value;
  }

  public static implicit operator OUInt(uint value)
  {
    return new OUInt(value);
  }

  public static implicit operator uint(OUInt value)
  {
    return (uint) value.value;
  }

  public static OUInt operator ++(OUInt value)
  {
    ref OUInt local = ref value;
    local.value = (ObscuredUInt) ((uint) local.value + 1U);
    return value;
  }

  public static OUInt operator --(OUInt value)
  {
    ref OUInt local = ref value;
    local.value = (ObscuredUInt) ((uint) local.value - 1U);
    return value;
  }

  public override string ToString()
  {
    return this.value.ToString();
  }

  public string ToString(string format)
  {
    return this.value.ToString(format);
  }
}
