﻿// Decompiled with JetBrains decompiler
// Type: BatchChunk
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

[DisallowMultipleComponent]
[ExecuteInEditMode]
public abstract class BatchChunk : MonoBehaviour
{
  public Mesh Mesh;
  public Material Material;

  protected BatchChunk()
  {
    base.\u002Ector();
  }

  private void Awake()
  {
    if (!Application.get_isPlaying())
      return;
    ((Behaviour) this).set_enabled(false);
  }

  public abstract void FillTriangles(
    int baseVertex,
    Vector3[] vertices,
    Vector3[] normals,
    Vector2[] uv,
    Color32[] colors,
    Vector3[] centers,
    int baseIndex,
    int[] indices);

  public virtual int VertexCount
  {
    get
    {
      return Object.op_Inequality((Object) this.Mesh, (Object) null) ? this.Mesh.get_vertexCount() : 0;
    }
  }

  public virtual int IndexCount
  {
    get
    {
      return Object.op_Inequality((Object) this.Mesh, (Object) null) ? this.Mesh.get_triangles().Length : 0;
    }
  }
}
