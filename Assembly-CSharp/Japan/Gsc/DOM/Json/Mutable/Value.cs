﻿// Decompiled with JetBrains decompiler
// Type: Gsc.DOM.Json.Mutable.Value
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Network;
using System.Collections;
using System.Runtime.InteropServices;

namespace Gsc.DOM.Json.Mutable
{
  [StructLayout(LayoutKind.Sequential, Size = 1)]
  public struct Value
  {
    public void SetNull()
    {
    }

    public void SetObject()
    {
    }

    public void SetArray()
    {
    }

    public void Set(IRequestObject value)
    {
    }

    public void Set(IEnumerable value)
    {
    }

    public void Set(bool value)
    {
    }

    public void Set(string value)
    {
    }

    public void Set(int value)
    {
    }

    public void Set(uint value)
    {
    }

    public void Set(long value)
    {
    }

    public void Set(ulong value)
    {
    }

    public void Set(float value)
    {
    }

    public void Set(double value)
    {
    }
  }
}
