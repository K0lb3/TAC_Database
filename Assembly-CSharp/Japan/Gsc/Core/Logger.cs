﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Core.Logger
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Network;
using System;
using System.Threading;
using UnityEngine;

namespace Gsc.Core
{
  public class Logger
  {
    private static bool initialized;

    public static event Application.LogCallback Callback
    {
      add
      {
        Application.LogCallback comparand = Logger.Callback;
        Application.LogCallback logCallback;
        do
        {
          logCallback = comparand;
          comparand = Interlocked.CompareExchange<Application.LogCallback>(ref Logger.Callback, (Application.LogCallback) Delegate.Combine((Delegate) logCallback, (Delegate) value), comparand);
        }
        while (comparand != logCallback);
      }
      remove
      {
        Application.LogCallback comparand = Logger.Callback;
        Application.LogCallback logCallback;
        do
        {
          logCallback = comparand;
          comparand = Interlocked.CompareExchange<Application.LogCallback>(ref Logger.Callback, (Application.LogCallback) Delegate.Remove((Delegate) logCallback, (Delegate) value), comparand);
        }
        while (comparand != logCallback);
      }
    }

    public static void Init()
    {
      if (Logger.initialized)
        return;
      // ISSUE: reference to a compiler-generated field
      if (Logger.\u003C\u003Ef__mg\u0024cache0 == null)
      {
        // ISSUE: reference to a compiler-generated field
        // ISSUE: method pointer
        Logger.\u003C\u003Ef__mg\u0024cache0 = new Application.LogCallback((object) null, __methodptr(_HandleLog));
      }
      // ISSUE: reference to a compiler-generated field
      Application.remove_logMessageReceived(Logger.\u003C\u003Ef__mg\u0024cache0);
      // ISSUE: reference to a compiler-generated field
      if (Logger.\u003C\u003Ef__mg\u0024cache1 == null)
      {
        // ISSUE: reference to a compiler-generated field
        // ISSUE: method pointer
        Logger.\u003C\u003Ef__mg\u0024cache1 = new Application.LogCallback((object) null, __methodptr(_HandleLog));
      }
      // ISSUE: reference to a compiler-generated field
      Application.add_logMessageReceived(Logger.\u003C\u003Ef__mg\u0024cache1);
      Logger.initialized = true;
    }

    public static void HandleLog(string logMessage, string stackTrace, LogType logType)
    {
      if (logType != 1 && logType != 4 && logType != null)
        return;
      UnityErrorLogSender.Instance.Send(logMessage, stackTrace, logType);
    }

    private static void _HandleLog(string logMessage, string stackTrace, LogType logType)
    {
      Logger.HandleLog(logMessage, stackTrace, logType);
      // ISSUE: reference to a compiler-generated field
      if (Logger.Callback == null)
        return;
      // ISSUE: reference to a compiler-generated field
      Logger.Callback.Invoke(logMessage, stackTrace, logType);
    }
  }
}
