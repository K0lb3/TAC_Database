﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Tasks.ParallelWorker
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

namespace Gsc.Tasks
{
  public class ParallelWorker : MonoBehaviour
  {
    private List<IEnumerator> tasks;

    public ParallelWorker()
    {
      base.\u002Ector();
    }

    public int TaskCount
    {
      get
      {
        return this.tasks.Count;
      }
    }

    public void AddTask(ITask task)
    {
      this.AddTask(ParallelWorker._AddTask(task));
    }

    public void AddTask(IEnumerator task)
    {
      this.tasks.Add(task);
    }

    [DebuggerHidden]
    private static IEnumerator _AddTask(ITask task)
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new ParallelWorker.\u003C_AddTask\u003Ec__Iterator0()
      {
        task = task
      };
    }

    private void Start()
    {
      this.StartCoroutine(this.Run());
    }

    [DebuggerHidden]
    private IEnumerator Run()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new ParallelWorker.\u003CRun\u003Ec__Iterator1()
      {
        \u0024this = this
      };
    }
  }
}
