﻿// Decompiled with JetBrains decompiler
// Type: Gsc.App.NetworkHelper.WebRequest
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace Gsc.App.NetworkHelper
{
  public class WebRequest : ApiRequest<WebRequest, WebResponse>
  {
    private readonly string method;
    private readonly string path;
    private readonly byte[] payload;

    public WebRequest(string method, string path, byte[] unencryptedPayload, byte[] payload)
    {
      this.method = method;
      this.path = path;
      this.UnencryptedPayload = unencryptedPayload;
      this.payload = payload;
    }

    public override string GetMethod()
    {
      return this.method;
    }

    public override byte[] GetPayload()
    {
      return this.payload;
    }

    public override string GetPath()
    {
      return "/" + this.path;
    }
  }
}
