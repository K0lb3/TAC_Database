﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Network.Encoding.SerializerCompressorHelper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;
using System.IO;
using UnityEngine;

namespace Gsc.Network.Encoding
{
  public static class SerializerCompressorHelper
  {
    public static byte[] Encode<T>(
      T objectToEncode,
      bool serializeWithMessagePack = false,
      CompressMode compressMode = CompressMode.Lz4,
      bool useFromJson = false)
    {
      try
      {
        byte[] numArray = (byte[]) null;
        if (!serializeWithMessagePack && compressMode == CompressMode.None)
          return (byte[]) (object) objectToEncode;
        using (MemoryStream memoryStream = new MemoryStream())
        {
          if (serializeWithMessagePack && compressMode == CompressMode.Lz4)
          {
            if (useFromJson)
              SerializerCompressorHelper.LZ4SerializeFromJson((Stream) memoryStream, (string) (object) objectToEncode);
            else
              SerializerCompressorHelper.LZ4Serialize<T>((Stream) memoryStream, objectToEncode);
          }
          else if (serializeWithMessagePack && compressMode != CompressMode.Lz4)
          {
            if (useFromJson)
            {
              SerializerCompressorHelper.SerializeFromJson((Stream) memoryStream, (string) (object) objectToEncode);
              memoryStream.Position = 0L;
              numArray = memoryStream.ToArray();
            }
            else
            {
              SerializerCompressorHelper.Serialize<T>((Stream) memoryStream, objectToEncode);
              memoryStream.Position = 0L;
              numArray = memoryStream.ToArray();
            }
          }
          else if (!serializeWithMessagePack && typeof (T) == typeof (byte[]))
            numArray = (byte[]) (object) objectToEncode;
          numArray = memoryStream.ToArray();
        }
        return numArray;
      }
      catch (Exception ex)
      {
        Debug.LogError((object) ex);
        return (byte[]) null;
      }
    }

    public static T Decode<T>(
      byte[] dataToDecode,
      bool deserializeWithMessagePack = false,
      CompressMode decompressMode = CompressMode.Lz4,
      bool useToJson = false,
      bool printExceptions = true)
    {
      try
      {
        if (!deserializeWithMessagePack && decompressMode == CompressMode.None)
          return (T) Convert.ChangeType((object) dataToDecode, typeof (T));
        T obj;
        using (MemoryStream memoryStream = decompressMode == CompressMode.Lz4 || decompressMode == CompressMode.None && deserializeWithMessagePack ? new MemoryStream(dataToDecode) : new MemoryStream())
        {
          if (decompressMode == CompressMode.Lz4 && deserializeWithMessagePack)
          {
            obj = !useToJson ? SerializerCompressorHelper.LZ4Deserialize<T>((Stream) memoryStream) : (T) Convert.ChangeType((object) SerializerCompressorHelper.LZ4DeserializeToJson((Stream) memoryStream), typeof (T));
            return obj;
          }
          if (decompressMode == CompressMode.None && deserializeWithMessagePack)
          {
            obj = !useToJson ? SerializerCompressorHelper.Deserialize<T>((Stream) memoryStream) : (T) Convert.ChangeType((object) SerializerCompressorHelper.DeserializeToJson((Stream) memoryStream), typeof (T));
            return obj;
          }
          obj = (T) Convert.ChangeType((object) memoryStream.ToArray(), typeof (T));
        }
        return obj;
      }
      catch (Exception ex)
      {
        if (printExceptions)
          Debug.LogError((object) ex);
        throw ex;
      }
    }

    private static void LZ4Serialize<T>(Stream outputStream, T objectToSerialize)
    {
      LZ4MessagePackSerializer.Serialize<T>(outputStream, objectToSerialize);
    }

    private static void LZ4SerializeFromJson(Stream outputStream, string jsonToSerialize)
    {
      byte[] buffer = LZ4MessagePackSerializer.FromJson(jsonToSerialize);
      outputStream.Write(buffer, 0, buffer.Length);
    }

    private static void Serialize<T>(Stream outputStream, T objectToSerialize)
    {
      MessagePackSerializer.Serialize<T>(outputStream, objectToSerialize);
    }

    private static void SerializeFromJson(Stream outputStream, string jsonToSerialize)
    {
      byte[] buffer = MessagePackSerializer.FromJson(jsonToSerialize);
      outputStream.Write(buffer, 0, buffer.Length);
    }

    private static T LZ4Deserialize<T>(Stream inputStream)
    {
      return LZ4MessagePackSerializer.Deserialize<T>(inputStream);
    }

    private static string LZ4DeserializeToJson(Stream inputStream)
    {
      byte[] bytes;
      using (BinaryReader binaryReader = new BinaryReader(inputStream))
        bytes = binaryReader.ReadBytes((int) inputStream.Length);
      return LZ4MessagePackSerializer.ToJson(bytes);
    }

    private static T Deserialize<T>(Stream inputStream)
    {
      return MessagePackSerializer.Deserialize<T>(inputStream);
    }

    private static string DeserializeToJson(Stream inputStream)
    {
      byte[] bytes;
      using (BinaryReader binaryReader = new BinaryReader(inputStream))
        bytes = binaryReader.ReadBytes((int) inputStream.Length);
      return MessagePackSerializer.ToJson(bytes);
    }
  }
}
