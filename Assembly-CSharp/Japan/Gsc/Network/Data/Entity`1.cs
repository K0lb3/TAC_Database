﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Network.Data.Entity`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace Gsc.Network.Data
{
  public abstract class Entity<T> : IEntity, IObject where T : Entity<T>
  {
    private uint ver;

    public string pk { get; protected set; }

    public abstract void Update();

    public abstract void ResolveRefs();

    public T Clone()
    {
      return (T) this.MemberwiseClone();
    }

    IEntity IEntity.Clone()
    {
      return (IEntity) this.Clone();
    }

    protected bool IsUpdatedOnce()
    {
      bool flag = (int) this.ver != (int) EntityRepository.ver;
      this.ver = EntityRepository.ver;
      return flag;
    }
  }
}
