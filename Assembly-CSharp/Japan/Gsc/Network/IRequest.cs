﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Network.IRequest
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace Gsc.Network
{
  public interface IRequest
  {
    CustomHeaders CustomHeaders { get; }

    bool IsUseEncryption { get; }

    byte[] UnencryptedPayload { get; set; }

    bool isDone { get; }

    WebTaskResult GetResult();

    string GetRequestID();

    string GetHost();

    string GetUrl();

    string GetPath();

    string GetMethod();

    IWebTask Cast();

    IWebTask Send();

    void Retry();

    byte[] GetPayload();

    Type GetErrorResponseType();

    WebTaskResult InquireResult(WebTaskResult result, WebInternalResponse response);
  }
}
