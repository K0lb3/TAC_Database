﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Auth.GAuth.GAuth.API.Request.AccessTokenVerify
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Auth.GAuth.GAuth.API.Generic;

namespace Gsc.Auth.GAuth.GAuth.API.Request
{
  public class AccessTokenVerify : GAuthRequest<AccessTokenVerify, Gsc.Auth.GAuth.GAuth.API.Response.AccessTokenVerify>
  {
    private const string ___path = "/v2/accesstoken/verify";

    public override string GetPath()
    {
      return SDK.Configuration.Env.AuthApiPrefix + "/v2/accesstoken/verify";
    }

    public override string GetMethod()
    {
      return "POST";
    }
  }
}
