﻿// Decompiled with JetBrains decompiler
// Type: SyncStateTime
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

public class SyncStateTime : StateMachineBehaviour
{
  public SyncStateTime()
  {
    base.\u002Ector();
  }

  public virtual void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  {
    if ((double) ((AnimatorStateInfo) ref stateInfo).get_length() <= 0.0 || (double) (((AnimatorStateInfo) ref stateInfo).get_length() * (((AnimatorStateInfo) ref stateInfo).get_normalizedTime() % 1f)) > (animator.get_updateMode() != 2 ? (double) Time.get_deltaTime() : (double) Time.get_unscaledDeltaTime()))
      return;
    float length = ((AnimatorStateInfo) ref stateInfo).get_length();
    float num = (animator.get_updateMode() != 2 ? Time.get_time() : Time.get_unscaledTime()) % length / length;
    if ((double) num <= 0.0)
      return;
    animator.Play(((AnimatorStateInfo) ref stateInfo).get_fullPathHash(), layerIndex, num);
  }
}
