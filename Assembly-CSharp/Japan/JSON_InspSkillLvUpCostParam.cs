﻿// Decompiled with JetBrains decompiler
// Type: JSON_InspSkillLvUpCostParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

[MessagePackObject(true)]
[Serializable]
public class JSON_InspSkillLvUpCostParam
{
  public int id;
  public JSON_InspSkillLvUpCostParam.JSON_CostData[] costs;

  [MessagePackObject(true)]
  [Serializable]
  public class JSON_CostData
  {
    public int gold;
  }
}
