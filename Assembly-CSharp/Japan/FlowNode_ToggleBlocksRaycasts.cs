﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_ToggleBlocksRaycasts
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

[FlowNode.NodeType("Toggle/BlocksRaycasts", 32741)]
[FlowNode.Pin(10, "Disable", FlowNode.PinTypes.Input, 0)]
[FlowNode.Pin(11, "Enable", FlowNode.PinTypes.Input, 0)]
[FlowNode.Pin(1, "Output", FlowNode.PinTypes.Output, 0)]
public class FlowNode_ToggleBlocksRaycasts : FlowNodePersistent
{
  [FlowNode.ShowInInfo]
  [FlowNode.DropTarget(typeof (GameObject), true)]
  public GameObject Target;

  public override void OnActivate(int pinID)
  {
    CanvasGroup canvasGroup = !Object.op_Inequality((Object) this.Target, (Object) null) ? (CanvasGroup) null : (CanvasGroup) this.Target.GetComponent<CanvasGroup>();
    switch (pinID)
    {
      case 10:
        if (Object.op_Inequality((Object) canvasGroup, (Object) null))
        {
          canvasGroup.set_blocksRaycasts(false);
          break;
        }
        break;
      case 11:
        if (Object.op_Inequality((Object) canvasGroup, (Object) null))
        {
          canvasGroup.set_blocksRaycasts(true);
          break;
        }
        break;
    }
    this.ActivateOutputLinks(1);
  }
}
