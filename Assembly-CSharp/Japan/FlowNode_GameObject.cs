﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_GameObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

[AddComponentMenu("")]
[FlowNode.NodeType("Common/GameObject", 32741)]
public class FlowNode_GameObject : FlowNode
{
  [FlowNode.ShowInInfo(true)]
  public Component Target;

  public override FlowNode.Pin[] GetDynamicPins()
  {
    return Object.op_Inequality((Object) this.Target, (Object) null) ? (FlowNode.Pin[]) ((object) this.Target).GetType().GetCustomAttributes(typeof (FlowNode.Pin), true) : base.GetDynamicPins();
  }

  public override bool OnDragUpdate(object[] objectReferences)
  {
    return false;
  }

  public override bool OnDragPerform(object[] objectReferences)
  {
    return false;
  }

  public override void OnActivate(int pinID)
  {
    if (!Object.op_Inequality((Object) this.Target, (Object) null))
      return;
    ((IFlowInterface) this.Target)?.Activated(pinID);
  }

  public static void ActivateOutputLinks(Component caller, int pinID)
  {
    FlowNode_GameObject[] componentsInParent = (FlowNode_GameObject[]) caller.GetComponentsInParent<FlowNode_GameObject>();
    for (int index = 0; index < componentsInParent.Length; ++index)
    {
      if (Object.op_Equality((Object) componentsInParent[index].Target, (Object) caller))
        componentsInParent[index].ActivateOutputLinks(pinID);
    }
  }
}
