﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_OnDestroy
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

[FlowNode.NodeType("Event/OnDestroy", 58751)]
[FlowNode.Pin(1, "Output", FlowNode.PinTypes.Output, 0)]
public class FlowNode_OnDestroy : FlowNodePersistent
{
  protected override void OnDestroy()
  {
    this.ActivateOutputLinks(1);
    base.OnDestroy();
  }
}
