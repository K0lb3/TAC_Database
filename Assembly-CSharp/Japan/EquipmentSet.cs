﻿// Decompiled with JetBrains decompiler
// Type: EquipmentSet
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;
using UnityEngine;

public class EquipmentSet : ScriptableObject
{
  public EquipmentSet.EquipmentType Type;
  public bool PrimaryHidden;
  public bool PrimaryForceOverride;
  public GameObject PrimaryHand;
  public List<GameObject> PrimaryHandChangeLists;
  public bool SecondaryHidden;
  public bool SecondaryForceOverride;
  public GameObject SecondaryHand;
  public List<GameObject> SecondaryHandChangeLists;
  public List<GameObject> OptionEquipmentLists;

  public EquipmentSet()
  {
    base.\u002Ector();
  }

  public enum EquipmentType
  {
    Melee,
    Bow,
    Gun,
  }
}
