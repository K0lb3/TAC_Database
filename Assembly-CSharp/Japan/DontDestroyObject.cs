﻿// Decompiled with JetBrains decompiler
// Type: DontDestroyObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

public class DontDestroyObject : MonoBehaviour
{
  public DontDestroyObject()
  {
    base.\u002Ector();
  }

  private void Awake()
  {
    Object.DontDestroyOnLoad((Object) this);
  }
}
