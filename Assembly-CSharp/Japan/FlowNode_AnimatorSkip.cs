﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_AnimatorSkip
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

[FlowNode.NodeType("Animator/Skip", 32741)]
[FlowNode.Pin(1, "Input", FlowNode.PinTypes.Input, 0)]
[FlowNode.Pin(10, "Output", FlowNode.PinTypes.Output, 1)]
public class FlowNode_AnimatorSkip : FlowNode
{
  [FlowNode.ShowInInfo]
  public string StateName = string.Empty;
  [FlowNode.ShowInInfo]
  [FlowNode.DropTarget(typeof (GameObject), true)]
  public GameObject Target;
  private int m_PrevStateHash;

  public override void OnActivate(int pinID)
  {
    Animator component = (Animator) (!Object.op_Inequality((Object) this.Target, (Object) null) ? ((Component) this).get_gameObject() : this.Target).GetComponent<Animator>();
    if (Object.op_Inequality((Object) component, (Object) null))
    {
      AnimatorStateInfo animatorStateInfo = component.GetCurrentAnimatorStateInfo(0);
      if (!string.IsNullOrEmpty(this.StateName))
      {
        if (((AnimatorStateInfo) ref animatorStateInfo).IsName(this.StateName) && this.m_PrevStateHash != ((AnimatorStateInfo) ref animatorStateInfo).get_fullPathHash())
        {
          component.Play(((AnimatorStateInfo) ref animatorStateInfo).get_fullPathHash(), 0, 1f);
          this.m_PrevStateHash = ((AnimatorStateInfo) ref animatorStateInfo).get_fullPathHash();
        }
      }
      else if (this.m_PrevStateHash != ((AnimatorStateInfo) ref animatorStateInfo).get_fullPathHash())
      {
        component.Play(((AnimatorStateInfo) ref animatorStateInfo).get_fullPathHash(), 0, 1f);
        this.m_PrevStateHash = ((AnimatorStateInfo) ref animatorStateInfo).get_fullPathHash();
      }
    }
    this.ActivateOutputLinks(1);
  }
}
