﻿// Decompiled with JetBrains decompiler
// Type: JSON_LocalNotificationInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

public class JSON_LocalNotificationInfo
{
  public int pk;
  public JSON_LocalNotificationInfo.Fields fields;

  public class Fields
  {
    public int id;
    public string trophy_iname;
    public int push_flg;
    public string push_word;
  }
}
