﻿// Decompiled with JetBrains decompiler
// Type: AssetManager_Extensions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

public static class AssetManager_Extensions
{
  public static string ToPath(this AssetManager.AssetFormats platform)
  {
    switch (platform)
    {
      case AssetManager.AssetFormats.AndroidGeneric:
      case AssetManager.AssetFormats.AndroidATC:
        return "aatc/";
      case AssetManager.AssetFormats.AndroidDXT:
        return "adxt/";
      case AssetManager.AssetFormats.AndroidPVR:
        return "apvr/";
      case AssetManager.AssetFormats.AndroidETC2:
        return "aetc2/";
      case AssetManager.AssetFormats.Windows:
        return "win32/";
      default:
        return "ios/";
    }
  }
}
