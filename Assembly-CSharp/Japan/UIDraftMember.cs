﻿// Decompiled with JetBrains decompiler
// Type: UIDraftMember
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof (RectTransform))]
[ExecuteInEditMode]
[AddComponentMenu("UI/Expose")]
public class UIDraftMember : MonoBehaviour
{
  [SerializeField]
  private bool mSearchGraphicComponent;
  public bool UseGraphic;

  public UIDraftMember()
  {
    base.\u002Ector();
  }

  private void Awake()
  {
    if (!Application.get_isEditor())
      return;
    ((Component) this).set_tag("EditorOnly");
    if (!this.mSearchGraphicComponent)
      return;
    if (Object.op_Inequality((Object) ((Component) this).GetComponent<Text>(), (Object) null))
      this.UseGraphic = true;
    if (Object.op_Inequality((Object) ((Component) this).GetComponent<RawImage>(), (Object) null))
      this.UseGraphic = true;
    if (Object.op_Inequality((Object) ((Component) this).GetComponent<Image>(), (Object) null))
      this.UseGraphic = true;
    this.mSearchGraphicComponent = false;
  }
}
