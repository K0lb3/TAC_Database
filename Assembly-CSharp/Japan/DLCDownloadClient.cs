﻿// Decompiled with JetBrains decompiler
// Type: DLCDownloadClient
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Diagnostics;
using UnityEngine;

public abstract class DLCDownloadClient
{
  protected long _downloadStart;
  protected long _downloadEnd;

  public bool IsDone { get; protected set; }

  public string HasError { get; protected set; }

  public int ContentLength { get; protected set; }

  public bool IsGotRest { get; protected set; }

  public bool CanWriteStream { get; set; }

  public virtual int LoadingSize { get; protected set; }

  public virtual byte[] DataBytes { get; protected set; }

  public virtual int DownloadedSize { get; protected set; }

  public double DownloadTime
  {
    get
    {
      return (double) (this._downloadEnd - this._downloadStart) / (double) Stopwatch.Frequency;
    }
  }

  public abstract void Download(string url, int size, MonoBehaviour coroutineExecuter = null);

  public abstract void Abort();

  public abstract void Dispose();
}
