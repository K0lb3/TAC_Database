﻿// Decompiled with JetBrains decompiler
// Type: OBool
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using CodeStage.AntiCheat.ObscuredTypes;
using MessagePack;

[MessagePackObject(true)]
public struct OBool
{
  private ObscuredBool value;

  public OBool(bool value)
  {
    this.value = (ObscuredBool) value;
  }

  public static implicit operator OBool(bool value)
  {
    return new OBool(value);
  }

  public static implicit operator bool(OBool value)
  {
    return (bool) value.value;
  }

  public override string ToString()
  {
    return this.value.ToString();
  }
}
