﻿// Decompiled with JetBrains decompiler
// Type: MyMsgInput
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Globalization;
using System.Text.RegularExpressions;

public static class MyMsgInput
{
  public static bool isLegal(string name)
  {
    return string.IsNullOrEmpty(name) || new StringInfo(name).LengthInTextElements >= name.Length && !new Regex("^[0-9０-９\\-]+$").IsMatch(name) && !new Regex("^\\s+$").IsMatch(name);
  }
}
