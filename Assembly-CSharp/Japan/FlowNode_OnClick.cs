﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_OnClick
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[AddComponentMenu("")]
[FlowNode.NodeType("Event/OnClickButton", 58751)]
[FlowNode.Pin(1, "Clicked", FlowNode.PinTypes.Output, 0)]
public class FlowNode_OnClick : FlowNodePersistent
{
  [FlowNode.ShowInInfo]
  [FlowNode.DropTarget(typeof (Button), true)]
  public Button Target;
  private Button mBound;

  private void Start()
  {
    this.Bind();
    ((Behaviour) this).set_enabled(false);
  }

  private void Bind()
  {
    if (!Object.op_Inequality((Object) this.Target, (Object) null) || !Object.op_Inequality((Object) this.Target, (Object) this.mBound))
      return;
    // ISSUE: method pointer
    ((UnityEvent) this.Target.get_onClick()).AddListener(new UnityAction((object) this, __methodptr(OnClick)));
    this.mBound = this.Target;
  }

  private void OnClick()
  {
    this.Activate(1);
  }

  public override void OnActivate(int pinID)
  {
    if (pinID != 1)
      return;
    this.ActivateOutputLinks(1);
  }
}
