﻿// Decompiled with JetBrains decompiler
// Type: MailIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

public class MailIcon : MonoBehaviour
{
  public GameObject ItemIconTemplate;
  public GameObject CoinIconTemplate;
  public GameObject GoldIconTemplate;
  public GameObject ArenaCoinIconTemplate;
  public GameObject MultiCoinIconTemplate;
  public GameObject EventCoinIconTemplate;
  public GameObject KakeraCoinIconTemplate;
  public GameObject SetIconTemplate;
  public GameObject ArtifactIconTemplate;
  public GameObject ConceptCardIconTemplate;
  public GameObject CurrentIcon;

  public MailIcon()
  {
    base.\u002Ector();
  }
}
