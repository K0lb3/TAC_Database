﻿// Decompiled with JetBrains decompiler
// Type: PhotonNetworkingMessage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

public enum PhotonNetworkingMessage
{
  OnConnectedToPhoton,
  OnLeftRoom,
  OnMasterClientSwitched,
  OnPhotonCreateRoomFailed,
  OnPhotonJoinRoomFailed,
  OnCreatedRoom,
  OnJoinedLobby,
  OnLeftLobby,
  OnDisconnectedFromPhoton,
  OnConnectionFail,
  OnFailedToConnectToPhoton,
  OnReceivedRoomListUpdate,
  OnJoinedRoom,
  OnPhotonPlayerConnected,
  OnPhotonPlayerDisconnected,
  OnPhotonRandomJoinFailed,
  OnConnectedToMaster,
  OnPhotonSerializeView,
  OnPhotonInstantiate,
  OnPhotonMaxCccuReached,
  OnPhotonCustomRoomPropertiesChanged,
  OnPhotonPlayerPropertiesChanged,
  OnUpdatedFriendList,
  OnCustomAuthenticationFailed,
  OnCustomAuthenticationResponse,
  OnWebRpcResponse,
  OnOwnershipRequest,
  OnLobbyStatisticsUpdate,
  OnPhotonPlayerActivityChanged,
  OnOwnershipTransfered,
}
