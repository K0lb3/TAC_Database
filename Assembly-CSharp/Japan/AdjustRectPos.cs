﻿// Decompiled with JetBrains decompiler
// Type: AdjustRectPos
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

public class AdjustRectPos : MonoBehaviour
{
  [SerializeField]
  private Vector3 SetOffsetPos;
  private Rect lastSafeArea;
  private Vector3 initPos;
  private Vector3 lastSetOffsetPos;

  public AdjustRectPos()
  {
    base.\u002Ector();
  }

  private void Start()
  {
    RectTransform component = (RectTransform) ((Component) this).GetComponent<RectTransform>();
    if (!Object.op_Inequality((Object) component, (Object) null))
      return;
    this.initPos = ((Transform) component).get_localPosition();
    this.ApplySafeAreaPos(SetCanvasBounds.GetSafeArea(false));
  }

  private void ApplySafeAreaPos(Rect area)
  {
    RectTransform component = (RectTransform) ((Component) this).GetComponent<RectTransform>();
    if (Object.op_Inequality((Object) component, (Object) null))
    {
      if ((double) (((Rect) ref area).get_width() / (float) Screen.get_width()) < 1.0)
        ((Transform) component).set_localPosition(Vector3.op_Addition(this.initPos, this.SetOffsetPos));
      else
        ((Transform) component).set_localPosition(this.initPos);
    }
    this.lastSafeArea = area;
    this.lastSetOffsetPos = this.SetOffsetPos;
  }
}
