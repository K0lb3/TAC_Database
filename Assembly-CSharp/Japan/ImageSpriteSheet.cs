﻿// Decompiled with JetBrains decompiler
// Type: ImageSpriteSheet
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using SRPG;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[AddComponentMenu("UI/ImageSpriteSheet (透明)")]
public class ImageSpriteSheet : Image
{
  [HeaderBar("▼スプライトシートのパス")]
  [SerializeField]
  [StringIsResourcePath(typeof (SpriteSheet))]
  private string m_SpriteSheetPath;
  [SerializeField]
  private string m_DefaultKey;
  private SpriteSheet m_SpriteSheet;
  private bool m_IsInitialized;
  private string m_BeforeInitializationSetKey;

  public ImageSpriteSheet()
  {
    base.\u002Ector();
  }

  protected virtual void Start()
  {
    ((UIBehaviour) this).Start();
    if (!Application.get_isPlaying())
      return;
    this.m_IsInitialized = true;
    this.ForceLoad();
    if (string.IsNullOrEmpty(this.m_BeforeInitializationSetKey))
      return;
    this.SetSprite(this.m_BeforeInitializationSetKey);
  }

  public void ForceLoad()
  {
    if (string.IsNullOrEmpty(this.m_SpriteSheetPath) || !Object.op_Equality((Object) this.m_SpriteSheet, (Object) null))
      return;
    this.m_SpriteSheet = AssetManager.Load<SpriteSheet>(this.m_SpriteSheetPath);
    this.SetSprite(this.m_DefaultKey);
  }

  public string DefaultKey
  {
    get
    {
      return this.m_DefaultKey;
    }
  }

  public void SetSprite(string key)
  {
    this.set_sprite(this.GetSprite(key));
    if (this.m_IsInitialized)
      return;
    this.m_BeforeInitializationSetKey = key;
  }

  public Sprite GetSprite(string key)
  {
    return Object.op_Inequality((Object) this.m_SpriteSheet, (Object) null) ? this.m_SpriteSheet.GetSprite(key) : (Sprite) null;
  }

  protected virtual void OnPopulateMesh(VertexHelper toFill)
  {
    if (Object.op_Inequality((Object) this.get_sprite(), (Object) null))
      base.OnPopulateMesh(toFill);
    else
      toFill.Clear();
  }
}
