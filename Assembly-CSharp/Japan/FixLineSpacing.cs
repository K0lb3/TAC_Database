﻿// Decompiled with JetBrains decompiler
// Type: FixLineSpacing
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof (Text))]
public class FixLineSpacing : MonoBehaviour
{
  public bool NoBestFit;

  public FixLineSpacing()
  {
    base.\u002Ector();
  }

  private void OnEnable()
  {
  }

  private void Awake()
  {
    if (!((Behaviour) this).get_enabled())
      return;
    Text component = (Text) ((Component) this).GetComponent<Text>();
    Text text = component;
    text.set_lineSpacing(text.get_lineSpacing() * 2f);
    if (this.NoBestFit)
      component.set_resizeTextForBestFit(false);
    ((Behaviour) this).set_enabled(false);
  }
}
