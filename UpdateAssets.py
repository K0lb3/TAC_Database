from lib.AssetManagement.AssetManagerP import DownloadNProcess, download_queue, process_queue
from lib.AssetManagement.Download import get_assetlist
from lib import respath
import os


def main():
	inp = input('Select Version:\nGL - Global Version\nJP - Japanese Version\nSelection:\t').upper()
	if inp == 'GL':
		pathPost = 'GL'
		region = 'global'
		typ = 'apvr'
	elif inp == 'JP':
		pathPost = 'JP'
		region = 'japan'
		typ = 'win32'
	else:
		input('wrong input')
		main()
		return
	
	assetlist = get_assetlist(region, typ)
	origin = os.path.join(respath, f'RAW_Assets_{pathPost}')
	dest = os.path.join(respath, f'Assets{pathPost}')
	
	assets = checkFiles(origin, assetlist['assets'])
	DownloadAndConvertAssets(origin, dest, assets, assetlist['host'], assetlist['version'], typ = typ)
	PostWork(dest)
	print()


def checkFiles(dest, assets, IGNOR = []):
	NotFound = []
	Outdated = []
	
	for asset in assets if type(assets) == list else [val for key, val in assets.items()]:
		if not (any(string in asset['path'] for string in IGNOR) or 'IsCombined' in asset['flags']):
			try:
				if asset['size'] != os.path.getsize(os.path.join(dest, asset['id'])):
					Outdated.append(asset)
			except FileNotFoundError:
				NotFound.append(asset)
	Download = NotFound + Outdated
	print('To  Download:\t%s/%s assets' % (len(Download), len(assets)))
	print('Outdated:\t%s\nNot found:\t%s' % (len(Outdated), len(NotFound)))
	return Download


def PostWork(path):
	ccpath = os.path.join(path, 'ConceptCard')
	sapath = os.path.join(path, 'StreamingAssets')
	
	if os.path.isdir(ccpath):
		ConceptCardResize(ccpath)
	
	#if os.path.isdir(sapath):
	#	StreamingAssetsConvertion(sapath)

def ConceptCardResize(CardPath):
	from PIL import  Image
	for card in os.listdir(CardPath):
		f = os.path.join(CardPath, card)
		try:
			img = Image.open(f)
		except:
			print(f'Can\'t open{card} as image.')
			continue
		if (1024, 612) != img.size:
			print('Resize:', card)
			img = img.crop((0, 0, 1024, 612))
			img.save(f)

def DownloadAndConvertAssets(rawFolder, destFolder, assets, host_dl, version, typ = 'aatc', IGNOR = [], download_workers = 4, process_workers = 4):
	from random import shuffle
	overflow_queue = []
	if len(assets)>30000:
		overflow_queue = assets[30000:]
		assets = assets[:30000]
	# shuffle and filter
	shuffle(assets)
	qsize = 0
	for asset in assets:
		qsize += 1
		download_queue.put(asset)
	print('Files that will be downloaded:',qsize)
	#print(download_queue.qsize())
	
	# start threads
	threads = [
			DownloadNProcess(download_queue, host_dl, version, rawFolder, destFolder, typ = typ)
			for i in range(download_workers)
	]
	for t in threads:
		t.start()
	
	# wait for threads to finish and kill surviving threads
	download_queue.join()
	for t in threads:
		t.terminate()
	if overflow_queue:
		print('First batch processed, starting next')
		DownloadAndConvertAssets(rawFolder, destFolder, overflow_queue, host_dl, version, typ, IGNOR, download_workers, process_workers)
	print('All Files Processes')


if __name__ == '__main__':
	main()
