import os
import zlib
import multiprocessing
import sys
from unitypack.unityfolder import ExportFile
from lib.shared import respath
from lib.AssetManagement.Download import get_assetlist


def main():
	inp = input('Select Version:\nGL - Global Version\nJP - Japanese Version\nSelection:\t').upper()
	if inp == 'GL':
		pathPost = 'GL'
		region = 'global'
	elif inp == 'JP':
		pathPost = 'JP'
		region = 'japan'
	elif inp == 'TW':
		pathPost = 'TW'
		region = 'taiwan'
	else:
		input('wrong input')
		main()
		return
	
	print('Fetching Assetlist')
	assets = list(get_assetlist(region, 'aatc')['assets'].values())
	origin = os.path.join(respath, 'RAW_Assets_%s' % pathPost)
	dest = os.path.join(respath, 'Assets%s' % pathPost)
	processAssets(origin, dest, assets)


def processAssets(rawFolder, destFolder, assets, workers = 8):
	from random import shuffle
	queue = multiprocessing.JoinableQueue()
	# shuffle and filter
	shuffle(assets)
	for asset in assets:
		queue.put(asset)
	print(queue.qsize())
	
	# start threads
	Processses = [
			LocalProcess(queue, rawFolder, destFolder)
			for i in range(workers)
	]
	for t in Processses:
		t.start()
	
	# wait for threads to finish and kill surviving threads
	queue.join()
	for t in Processses:
		t.terminate()
	print('All Files Processes')

class LocalProcess(multiprocessing.Process):
	def __init__(self, queue,  rawFolder = '', destFolder = ''):
		super(LocalProcess, self).__init__()
		self.queue = queue
		self.daemon = True
		self.rawFolder = rawFolder
		self.destFolder = destFolder
		if destFolder:
			os.makedirs(destFolder, exist_ok = True)	

	def run(self):
		while not self.queue.empty():
			try:
				asset = self.queue.get()
				# Download
				data = open(os.path.join(self.rawFolder, asset['id']), 'rb').read()
				# Process
				if 'Compressed' in asset['flags']:
					data = zlib.decompress(data)
				if 'IsCombined' in asset['flags']:
					pass
				else:
					try:
						ExportFile(data, os.path.join(self.destFolder, *asset['path'].split('/')))
					except Exception as e:
						print("Error Processing: %s\n%s" % (asset['path'], e))
			except multiprocessing.queues.Empty:
				break
			except Exception as e:
				print("Download Error: %s" % e)
			
			self.queue.task_done()
			if self.queue.empty():
				break
		try:
			sys.exit(1)
		except:
			pass

if __name__ == '__main__':
	main()
