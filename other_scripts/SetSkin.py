import json
import uuid
import http.client

def main():
    acc = TAC_API('alchemist.gu3.jp',print_req=True)
    cuid=input('Account ID:\t')
    password=input('Password:\t')
    acc.req_linked_account(cuid,password)
    login=acc.print_login(ret=True)
    units={unit['iname']:[job['iid'] for job in unit['jobs']] for unit in login['units']}

    while True:
        u = input('Unit Iname:\t')
        if not u:
            break

        if u in units:
            skin = input('Skin Iname:\t')
            if skin:
                if acc.unit_set_skin(units[u],skin,raw=True)['stat']==0:
                    print('Skin set')
                else:
                    print('Failed to set skin')

class TAC_API(object):
	def __init__(self,api="app.alcww.gumi.sg",device_id=str(uuid.uuid4()),secret_key=str(uuid.uuid4()),idfa=str(uuid.uuid4()),idfv=str(uuid.uuid4()),cuid='',print_req=False):
		object.__init__(self)
		self.api=api
		self.device_id=device_id
		self.secret_key=secret_key
		self.idfa=idfa
		self.idfv=idfv
		self.name=''
		self.ticket=0
		self.access_token=''
		self.cuid=cuid
		self.fuid=''
		self.print_req=print_req	#print request url
		###Note
		#	either load account data via setting the variables yourself,
		# 	or use self.load_gu3c
		#	or use create_account
		#	JP logins are a bit tricky, they require the idfa and idfv from the login,
		#	they aren't saved in the gu3c.dat and have to be saved somewhere else

	def app_start(self):
		if not self.api=='alchemist.gu3.jp':
			res=self.req_chk_player()
			if res['result'] != 1:
				input('Problem during player check:\t%s'%res)
			self.req_bundle()
		self.req_product()
		self.req_achieve_auth()
		login=self.req_login()
		self.req_login_param()
		self.req_home()

	def req_accesstoken(self,raw=False):
		self.ticket=0
		body = {
			"access_token": "",
			"param": {
				"device_id": self.device_id,
				"secret_key": self.secret_key,
				"idfa": self.idfa,	# Google advertising ID
				"idfv": self.idfv,
				"udid":""
			}
		}
		res = self.api_request("/gauth/accesstoken", body,'POST',True)
		if 'access_token' in res['body']:
			self.access_token=res['body']['access_token']
		else:
			print('Failed receiving access_token',res)
		return res if raw else res['body']
		
	def req_chk_player(self,raw=False):
		body = {
			"access_token": "",
			"param": {
				"device_id": self.device_id,
				"secret_key": self.secret_key,
				"idfa": self.idfa,	# Google advertising ID
			}
		}
		res_body = self.api_request("/player/chkplayer", body,'POST', True)
		try:
			res_body['body']
		except:
			print('error: failed to retrieve chklayer')
			res_body=self.req_chk_player(True)
		return res_body if raw else res_body['body']
		
	def req_achieve_auth(self):
		return(self.api_connect('/achieve/auth',{},'GET'))
	def req_home(self,raw=False):
		return self.api_request('/home',{"param": {"is_multi_push": 1}},raw=raw)
	
	def req_bundle(self,raw=False):
		return self.api_request('/bundle',raw=raw)

	def req_product(self,raw=False):
		return self.api_request('/product',raw=raw)

	def req_login(self,raw=False):
		if not self.cuid:
			login=self.api_request("/login",{"param":{"device":"HUAWEI HUAWEI MLA-L12","dlc":"apvr"}},raw=True)
			if login['body']:
				self.cuid=login['body']['player']['cuid']
				self.fuid=login['body']['player']['fuid']
			return login if raw else login['body']
		return self.api_request("/login",{"param":{"device":"HUAWEI HUAWEI MLA-L12","dlc":"apvr"}},raw=raw)

	def req_login_param(self,relogin=0,raw=False):
		res = self.api_request('/login/param',{"param": {"relogin": int(relogin)}},raw=True)
		for key,val in res['body'].items():

			if type(val)==list and len(val) and type(val[0])==dict:
				subkey='iname' if 'iname' in val[0] else 'i' if 'i' in val[0] else False
				if key:
					val={item[subkey]:item for item in val}
			self.__setattr__(key,val)
		return res if raw else res['body']

	####	Unit	#############################
	def unit_set_skin(self,jobIDs,skin,raw=False):
		body={
			'param':{
				'sets':[
					{
						'iid':	jobid,
						'iname':	skin
					}
					for jobid in jobIDs
				]
			}
		}
		return self.api_request('/unit/skin/set',body=body,raw=raw)

	####	Register Account Linking	##################################################################
	def link_account(self,password='tagatame'):
		if not self.cuid:
			self.req_login()
		body={
				"ticket": "0",
				"access_token": "",
				"email": self.cuid,
				"password": password,
				"disable_validation_email": True,
				"device_id": self.device_id,
				"secret_key": self.secret_key,
				"udid": ""
			}
		ret=self.api_connect('/auth/email/register',body,ignoreStat=True)
		if ret["is_succeeded"]:
			return True
		else:
			return False

	def req_linked_account(self,cuid,password):
		body={
				"access_token": "",
				"email": cuid,
				"password": password,
				"idfv": self.idfv,
				"udid": ""
			}
		ret=self.api_connect('/auth/email/device',body,ignoreStat=True, no_access_token=True)
		if 'device_id' in ret:
			self.device_id=ret['device_id']
			self.secret_key=ret['secret_key']
			print('Login successfull, emulating login')
			self.app_start()
		else:
			print('Login failed')


	####	CONNECTION ###########################################################
	def api_request(self,url,body={},request='POST',raw=False,retry=False):
		if url[0]!= '/':
			url='/%s'%url

		res_body=self.api_connect(url,body,request)
		
		try:
			ret = res_body['body']
			if 'player' in ret and 'stamina' in ret['player']:
				self.ap=ret['player']['stamina']['pt']
		except Exception as e:
			print('error: failed to retrieve %s'%url)
			print(e)
			print(res_body)
			if retry:
				ret=self.api_request(url,body,request,raw,retry=False)
			else:
				raw=True
		return res_body if raw else ret

	def api_connect(self,url, body={},request="POST",api=False, ignoreStat=False, no_access_token=False):
		#print(self.access_token)
		if not api:
			api=self.api
		body['ticket']=self.ticket

		#create headers
		RID=str(uuid.uuid4()).replace('-','')
		headers={
			'X-GUMI-DEVICE-PLATFORM': 'android',
			'X-GUMI-DEVICE-OS': 'android',
			'X-Gumi-Game-Environment': 'sg_production',
			"X-GUMI-TRANSACTION": RID,
			'X-GUMI-REQUEST-ID': RID,
			'X-GUMI-CLIENT': 'gscc ver.0.1',
			'X-Gumi-User-Agent': json.dumps({
				"device_model":"HUAWEI HUAWEI MLA-L12",
				"device_vendor":"<unknown>",
				"os_info":"Android OS 4.4.2 / API-19 (HUAWEIMLA-L12/381180418)",
				"cpu_info":"ARMv7 VFPv3 NEON VMH","memory_size":"1.006GB"
				}),
			"User-Agent": "Dalvik/1.6.0 (Linux; U; Android 4.4.2; HUAWEI MLA-L12 Build/HUAWEIMLA-L12)",
			"X-Unity-Version": "5.3.6p1",
			"Content-Type": "application/json; charset=utf-8",
			"Host": api,
			"Connection": "Keep-Alive",
			"Accept-Encoding": "gzip",
			"Content-Length": len(json.dumps(body))
			}
		if url!="/gauth/accesstoken" and url!='/gauth/register' and not no_access_token:
			if self.access_token == "":
				self.access_token = self.req_accesstoken()['access_token']
			headers["Authorization"] = "gauth " + self.access_token

		if self.print_req:
			print(api+url)
		try:
			con = http.client.HTTPSConnection(api)
			con.connect()
			con.request(request, url, json.dumps(body), headers)
			res_body = con.getresponse().read()
			#print(res_body)
			con.close()
		except http.client.RemoteDisconnected:
			return self.api_connect(url, body)
		try:
			json_res= json.loads(res_body)
			if not ignoreStat and json_res['stat'] == 5002:
				print('Error 5002 ~ have to login again')
				self.access_token=""
				json_res = self.api_connect(url, body)
			self.ticket+=1
			return(json_res)
		except Exception as e:
			print(e)
			print(url)
			print(res_body)
			#return self.api_connect(url, body)
			raise RecursionError('-')

	def print_login(self,json_res=False,ret=False):
		if not json_res:
			json_res=self.req_login()
		print("--------------------------------------------")
		print("Name:", json_res["player"]["name"])
		print("P. Lv:", json_res["player"]["lv"])
		print("User Code:", json_res["player"]["cuid"])
		print("Friend ID:", json_res["player"]["fuid"])
		print("Created at:", json_res["player"]["created_at"])
		print("Exp:", json_res["player"]["exp"])
		print("Stamina:", json_res["player"]["stamina"]["pt"], "/", json_res["player"]["stamina"]["max"])
		print("Zeni:", json_res["player"]["gold"])
		print("Gems:", json_res["player"]["coin"]["paid"], "Paid,", json_res["player"]["coin"]["com"], "Shared,",json_res["player"]["coin"]["free"], "Free")
		print("--------------------")
		if ret:
			return json_res

main()