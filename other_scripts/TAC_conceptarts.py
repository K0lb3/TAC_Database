import urllib.request as urllib2
import json
import os

rootpath = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
DB = os.path.join(rootpath, *['resources','AssetsJP', 'Data', 'MasterParam'])
Units = [unit for unit in json.load(
    open(DB, 'rb'))['Unit'] if unit['iname'][:9] != 'UN_V2_EN_']
Folder = os.path.join(rootpath, *['resources','ConceptArt'])
Images = []


def main():
    for fp in os.listdir(Folder):
        with open(os.path.join(Folder, fp), 'rb') as f:
            Images.append(f.read())

    ca = TwitterScrapping()
    ca = TryFindingUnits(ca)
    DownloadImages(ca)


def DownloadImages(ca):
    for content, image, units in ca:
        print(image, units)
        imagefile = urllib2.urlopen(image).read()
        if imagefile not in Images:
            if len(units) == 2 and 'UN_V2_ZAYIN' in units:
                units.remove('UN_V2_ZAYIN')
            if len(units) == 1:
                name = units[0]
            else:
                name = image[-19:-4]

            while name+'.jpg' in os.listdir(Folder):
                name += '2'

            print(name)
            #download and saveimage
            with open(os.path.join(Folder, name+'.jpg'), 'wb') as f:
                f.write(imagefile)


def TryFindingUnits(ca):
    for unit in Units:
        for a in ca:
            if len(a) == 2:
                a.append([])
            if len(a) != 3:
                print(a[1])
                continue
            if unit['name'] in a[0]:
                a[2].append(unit['iname'])

    with open(os.path.join(Folder, 'Match.json'), 'wb') as f:
        f.write(json.dumps(ca, indent='\t', ensure_ascii=False).encode('utf-8'))

    return ca


def TwitterScrapping():
    # collect all images
    headers = {
        "accept"	:	"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
        "accept-language"	:	"de-DE,de;q=0.9,en-US;q=0.8,en;q=0.7",
        "cache-control"	:	"max-age=0",
        "upgrade-insecure-requests"	:	"1",
        "user-agent"	:	"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36",
    }

    contents = []
    images = []

    def Reduce(html, searchString):
        html = html[html.index(searchString)+len(searchString):]
        return html

    def Month(month):
        if month < 10:
            return '0%d' % month
        else:
            return month

    url = ('https://twitter.com/search?q=%E3%80%90%E3%82%BF%E3%82%AC%E3%82%BF%E3%83%A1%E8%B3%87%E6%96%99%E9%A4%A8%E3%80%91%20%23%E3%82%BF%E3%82%AC%E3%82%BF%E3%83%A1%E8%B3%87%E6%96%99%E9%A4%A8%20' +
           'since%3A{year}-{month}-01%20until%3A{year2}-{month2}-01&src=typd')
    query = '【タガタメ資料館】%20%23タガタメ資料館%20since%3A{year}-{month}-01%20until%3A{year2}-{month2}-01&src=typd'

    spans = [(year, month) for year in range(2018, 2020)
             for month in range(1, 13)]
    print(spans)
    oldspan = spans.pop(0)
    while spans:
        newspan = spans.pop(0)
        cquery = query.format(year=oldspan[0], month=Month(
            oldspan[1]), year2=newspan[0], month2=Month(newspan[1]))
        print(cquery)
        request = urllib2.Request(url.format(year=oldspan[0], month=Month(
            oldspan[1]), year2=newspan[0], month2=Month(newspan[1])), headers=headers)
        content = urllib2.urlopen(request).read()
        scrap = content.decode('utf-8')
        # with open('Twitter%s.html'%cquery, 'wb') as f:
        #	 f.write(content)

        smarker = '<p class="TweetTextSize  js-tweet-text tweet-text" lang="ja" data-aria-label-part="0">【<strong>タガ</strong><strong>タメ</strong><strong>資料</strong><strong>館</strong>】\n'
        while smarker in scrap:
            print(len(contents)+1)
            scrap = Reduce(scrap, smarker)
            contents.append(scrap[:scrap.index(' <a href=')])
            scrap = Reduce(scrap, '<img data-aria-label-part src="')
            images.append(scrap[:scrap.index('" alt="')])

        oldspan = newspan

    print(len(contents))
    print(len(images))

    save = [
        [contents[i], images[i]]
        for i in range(len(contents))
    ]

    with open(os.path.join(Folder, 'Twitter_ConceptArts.json'), 'wb') as f:
        f.write(json.dumps(save, indent=4, ensure_ascii=False).encode('utf-8'))

    return save


if '__main__' == __name__:
    main()
