import json,os

main_path= os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
out_dir = os.path.join(main_path, 'export')
data_gl = os.path.join(main_path, *['resources','AssetsGL','Data'])
data_jp = os.path.join(main_path, *['resources','AssetsJP','Data'])
maps_gl = os.path.join(main_path, *['resources','AssetsGL','LocalMaps'])
maps_jp = os.path.join(main_path, *['resources','AssetsGL','LocalMaps'])
###########functions#####################################

def FH(parent_dir, filename): #file handle
	return open(os.path.join(parent_dir, filename),'rb')

def LoadJSON(fh):
	try:
		return json.loads(fh.read().decode('utf-8-sig'))
	except:
		return json.loads(fh.read().decode('utf-8'))

def get_files():
	for key in ('MasterParam','QuestParam','QuestDropParam'):
		print(key)
		res = Transformed_Difference(LoadJSON(FH(data_jp,key)),LoadJSON(FH(data_gl,key)))
		saveAsJSON(f'{key}_dif.json', res)

	#Map Differences
	# map_dif = {}
	# for fp in os.listdir(maps_jp):
	# 	if os.path.isfile(os.path.join(maps_gl,fp)):
	# 		dif = Difference(json.load(FH(maps_jp,fp)),json.load(FH(maps_gl,fp)))
	# 		if dif:
	# 			map_dif[fp] = dif
	# saveAsJSON(f'LocalMaps_dif.json', map_dif)


def Transformed_Difference(new,old):
	new,old=(Transform(new),Transform(old))
	dif={
		key:Difference(new[key],old[key]) if key in old else {'added':new[key]}
		for key in list(new.keys())
		if key not in old or new[key]!=old[key]
	}
	return dif

def GeneateKey(item, main):
	if main == 'Tobira':
		return f"{item['unit_iname']}_{item['category']}"
	else:
		raise KeyError

def Transform(master):
	for main, tree in master.items():
		if type(tree)==list and type(tree[0])==dict:
			for key in ['iname','id','key','type','round',None]:
				if key in tree[0]:
					break
			if key:
				master[main]={item[key]:item for item in sorted(tree, key=lambda k: k[key])}
			else:
				try:
					master[main]={GeneateKey(item, main):item for item in sorted(tree, key=lambda k: GeneateKey(k, main))}
				except:
					print(main)
			
	return master


def Difference(new,old):
	dif={}
	if new==old:
		return dif
		
	def cleanUp(dif):
		if not dif['added']:
			del dif['added']
		if not dif['removed']:
			del dif['removed']
		if not dif['changed']:
			del dif['changed']
		return dif

	if type(new)==dict:
		dif['added']={}
		dif['removed']={}
		dif['changed']={}
		oldkeys=list(old.keys())
		for key in list(new.keys()):
			if key in old:
				tdif=Difference(new[key],old[key])
				if tdif:
					dif['changed'][key]=tdif
				oldkeys.remove(key)
			else:
				#print('added',key)
				dif['added'][key]=new[key]
		for key in oldkeys:
			#print('removed',key)
			dif['removed']={
				key:old[key]
				for key in oldkeys
			}
		return cleanUp(dif)

	elif type(new)==list:
		if type(new[0])==dict:
			for i in range(len(new)):
				return ([
					Difference(new[i],old[i])
				])
		dif['added']=[]
		dif['removed']=[]
		dif['changed']=[]
		for item in new:
			if item not in old:
				dif['added'].append(item)
		for item in old:
			if item not in new:
				dif['removed'].append(item)
		return cleanUp(dif)

	else:
		if new!=old:
			return {'new':new,'old':old}
		else:
			return {}

def saveAsJSON(name, var):
	with open(os.path.join(out_dir,name), "wb") as f:
		f.write(json.dumps(var, indent='\t', ensure_ascii=False).encode('utf8'))

#code ################################
get_files()
