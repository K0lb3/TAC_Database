import json
import os
PATH_export=os.path.join(os.path.dirname(os.path.realpath(__file__)),'export')
PATH_save=os.path.join(os.path.dirname(os.path.realpath(__file__)),'_converted2')


global DIRS
with open (os.path.join(PATH_export,'Database.json'), 'rb') as f:
    DIRS = json.loads(f.read(),encoding='UTF8')

def main():
    text=''

    for key,floor in DIRS['Towerrewards'].items():
        if '_ITEM_' in key:
            text+='\nFloor %s:'%key[-3:]
            for reward in floor['mTowerRewardItems']:
                if not reward['num']:
                    continue
                if reward['type']=='Coin':
                    text+='\n\tGems: %d'%(reward['num'])
                elif reward['type']=='Gold':
                    text+='\n\tZeni: %d'%(reward['num'])
                else:
                    text+='\n\t%s: %d'%(DIRS[reward['type']][reward['iname']]['name'],reward['num'])
            text+='\n'

    print (text)

main()