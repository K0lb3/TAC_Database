import os
import json 
path=os.path.dirname(os.path.realpath(__file__))
DB=os.path.join(path,*[os.pardir,'export','Database.json'])
with open(DB,'rb') as f:
	DIRS=json.loads(f.read())

def TACScale(ini,max,lv,mlv):
	return int(ini + ((max-ini) / (mlv-1) * (lv-1)))

def main():
    mainc = DIRS
    unitLevel=85
    cunit=DIRS['Unit'][input('Unit Iname:\t')]
    unit={}
    unit['stats']={
        stat:TACScale(cunit['ini_status']['param'][stat],cunit['max_status']['param'][stat],85,100)
        for stat in cunit['ini_status']['param']
        }
    
 # add jobs

    jobs=[]
    if 'jobs' in cunit:
        for i in cunit['jobs']:
            job = DIRS['Job'][i]

            jobs.append({
                'stats': job['stats'],
                'jm': DIRS['Buff'][DIRS['Skill'][job['master']]['target_buff_iname']]['buffs'],
                'modifiers': job['ranks'][-1]['status']
            })

    ### combine JM values:
    jm={'Scale':{},'Add':{}}
    for job in jobs:
        for mod in job['jm']:
            if mod['type'] not in jm[mod['calc']]:
                jm[mod['calc']][mod['type']]=0
            jm[mod['calc']][mod['type']]+=mod['value_max']

    for i in range(0,3):
        j_stats={}
        for key,stat in unit['stats'].items():
            try:
                # print(key)
                # print(jobs[i]['stats'][key])
                # print( int(stat*((jobs[i]['modifiers'][key]+100)/100 if key in jobs[i]['modifiers'] else 1)))
                # print( ((jm['Scale'][key]+100)/100) if key in jm['Scale'] else 1)
                # print( jm['Add'][key] if key in jm['Add'] else 0)
                j_stats[key]=int(jobs[i]['stats'][key]+                                                         #job stat
                    int(stat*((jobs[i]['modifiers'][key]+100)/100 if key in jobs[i]['modifiers'] else 1))*   #unit stat * job modifier                
                    ((jm['Scale'][key]+100)/100 if key in jm['Scale'] else 1)+                          #jm scale
                    (jm['Add'][key] if key in jm['Add'] else 0))                                             #jm add
            except:
                pass
        print(j_stats)


    main()
main()